const { cookieService } = require('component-cookie-service')

const cookiesKeys = {
  manuscriptId: 'msid',
}

const initCookieService = context => {
  const { req, res } = context
  return cookieService({ req, res })
}

const setManuscriptId = (manuscriptId, context) => {
  const cookies = initCookieService(context)
  const maxAge = 1000 * 60 * 5 // 5 minutes
  return cookies.set({
    cookie: cookiesKeys.manuscriptId,
    value: manuscriptId,
    options: {
      maxAge,
      httpOnly: false,
    },
  })
}

const removeManuscriptId = context => {
  const cookies = initCookieService(context)
  return cookies.remove(cookiesKeys.manuscriptId)
}

module.exports = {
  setManuscriptId,
  removeManuscriptId,
}
