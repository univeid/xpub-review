const { includes } = require('lodash')

const initialize = ({
  models,
  logEvent,
  useCases,
  eventsService,
  notificationService,
  articleTypeWithRIPE,
  articleTypeEditorials,
  articleTypeWithPeerReview,
  getUserWithWorkloadUseCase,
}) => ({
  execute: async ({ manuscriptId, userId }) => {
    const { Manuscript, Team, TeamMember, ArticleType, User, File } = models

    const mainFile = await File.findOneBy({
      queryObject: {
        manuscriptId,
        type: File.Types.manuscript,
      },
    })
    if (!mainFile)
      throw new ValidationError('At least one main manuscript is required.')

    const manuscript = await Manuscript.find(
      manuscriptId,
      '[articleType, journal]',
    )
    const { journal, articleType: currentArticleType } = manuscript

    const journalEditorialAssistants = await TeamMember.findAllByJournalAndRole(
      { journalId: manuscript.journalId, role: Team.Role.editorialAssistant },
    )
    const manuscriptEditorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    if (
      journalEditorialAssistants.length !== 0 &&
      !manuscriptEditorialAssistant
    ) {
      const getUserWithWorkload = await getUserWithWorkloadUseCase.initialize(
        models,
      )

      await useCases.assignEditorialAssistantOnManuscriptUseCase
        .initialize({
          eventsService,
          models: { TeamMember, Team, User },
          getUserWithWorkload,
          manuscriptStatuses: [
            ...Manuscript.InProgressStatuses,
            Manuscript.Statuses.inQA,
            Manuscript.Statuses.technicalChecks,
            Manuscript.Statuses.qualityChecksRequested,
            Manuscript.Statuses.qualityChecksSubmitted,
          ],
        })
        .execute({
          manuscriptId,
          submissionId: manuscript.submissionId,
          journalId: journal.id,
          sectionId: manuscript.sectionId,
          specialIssueId: manuscript.specialIssueId,
        })
    }

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId,
      sectionId: manuscript.sectionId,
    })

    const authorTeamMembers = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
      eagerLoadRelations: 'user',
    })
    const submittingAuthor = authorTeamMembers.find(tm => tm.isSubmitting)

    let strategy

    if (includes(ArticleType.EditorialTypes, currentArticleType.name)) {
      strategy = articleTypeEditorials
    } else if (includes(ArticleType.TypesWithRIPE, currentArticleType.name)) {
      strategy = articleTypeWithRIPE
    } else {
      strategy = articleTypeWithPeerReview
    }

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    await strategy.execute({
      models,
      journal,
      manuscript,
      triageEditor,
      eventsService,
      submittingAuthor,
      authorTeamMembers,
      editorialAssistant,
      notificationService,
    })

    notificationService.sendSubmittingAuthorConfirmation({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
