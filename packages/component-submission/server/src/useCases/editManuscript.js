const initialize = ({ eventsService, models: { Manuscript } }) => ({
  execute: async ({ manuscriptId }) => {
    const manuscript = await Manuscript.find(manuscriptId)
    if (
      [
        Manuscript.Statuses.technicalChecks,
        Manuscript.Statuses.inQA,
        Manuscript.Statuses.qualityChecksSubmitted,
      ].includes(manuscript.status)
    ) {
      eventsService.publishSubmissionEvent({
        submissionId: manuscript.submissionId,
        eventName: 'SubmissionEdited',
      })
    }
  },
})

const authsomePolicies = ['admin', 'isEditorialAssistant']

module.exports = { initialize, authsomePolicies }
