module.exports = {
  execute: async ({
    journal,
    manuscript,
    eventsService,
    submittingAuthor,
    editorialAssistant,
    notificationService,
    models: { Manuscript },
  }) => {
    manuscript.updateProperties({
      status: Manuscript.Statuses.makeDecision,
      submittedDate: new Date().toISOString(),
    })

    await manuscript.save()

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    notificationService.notifyEditorsWhenNewManuscriptSubmitted({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
    })
  },
}
