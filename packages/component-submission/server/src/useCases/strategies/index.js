const articleTypeWithPeerReview = require('./articleTypeWithPeerReview')
const articleTypeWithRIPE = require('./articleTypeWithRIPE')
const articleTypeEditorials = require('./articleTypeEditorials')

module.exports = {
  articleTypeWithPeerReview,
  articleTypeWithRIPE,
  articleTypeEditorials,
}
