module.exports = {
  execute: async ({
    journal,
    manuscript,
    triageEditor,
    eventsService,
    submittingAuthor,
    editorialAssistant,
    notificationService,
    models: { Manuscript, Team, TeamMember },
  }) => {
    manuscript.updateProperties({
      status: Manuscript.Statuses.makeDecision,
      submittedDate: new Date().toISOString(),
    })

    await manuscript.save()

    const RIPEGlobalTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.researchIntegrityPublishingEditor,
        manuscriptId: null,
        journalId: null,
        sectionId: null,
        specialIssueId: null,
      },
      eagerLoadRelations: 'members.user.identities',
    })
    const RIPEUser = RIPEGlobalTeam && RIPEGlobalTeam.members[0].user

    const RIPETeam = new Team({
      role: Team.Role.researchIntegrityPublishingEditor,
      manuscriptId: manuscript.id,
    })
    await RIPETeam.save()
    const RIPETeamMember = RIPETeam.addMember({
      user: RIPEUser,
      options: {
        status: TeamMember.Statuses.accepted,
      },
    })
    await RIPETeamMember.save()

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    notificationService.notifyEditorsWhenNewManuscriptSubmitted({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
      editor: RIPETeamMember,
    })
    if (triageEditor) {
      notificationService.notifyEditorsWhenNewManuscriptSubmitted({
        journal,
        manuscript,
        submittingAuthor,
        editorialAssistant,
        editor: triageEditor,
      })
    }
  },
}
