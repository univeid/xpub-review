const { sortBy } = require('lodash')

const initialize = ({ File, Manuscript }) => ({
  execute: async ({ fileId, type }) => {
    const file = await File.find(fileId)

    const manuscript = await Manuscript.find(file.manuscriptId, 'files')

    const filesByCurrentType = manuscript.files.filter(
      mfile => mfile.type === file.type && mfile.id !== file.id,
    )

    sortBy(filesByCurrentType, 'position').forEach((file, index) => {
      file.updateProperties({ position: index })
    })

    const filesByNewTypeLength = manuscript.files.filter(
      file => file.type === type,
    ).length

    const fileToUpdate = manuscript.files.find(file => file.id === fileId)
    fileToUpdate.updateProperties({ type, position: filesByNewTypeLength })

    await manuscript.updateWithFiles(manuscript.files)

    return {
      ...fileToUpdate,
      filename: fileToUpdate.fileName,
    }
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
