process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const chance = new Chance()

const { editAuthorFromManuscriptUseCase } = require('../src/useCases')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = { author_edited: 'author_edited' }

describe('Edit Author from Manuscript Use Case', () => {
  it('should update author information', async () => {
    const { Manuscript, Team } = models
    const input = {
      givenNames: chance.first(),
      surname: chance.first(),
      email: chance.email(),
      country: chance.country(),
      aff: chance.company(),
    }
    const manuscript = fixtures.generateManuscript({ Manuscript })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
        isCorresponding: true,
      },
      role: Team.Role.author,
    })

    const authorTeamMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: false,
        isCorresponding: false,
      },
      role: Team.Role.author,
    })
    const result = await editAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        params: {
          manuscriptId: manuscript.id,
          authorTeamMemberId: authorTeamMember.id,
          authorInput: {
            isCorresponding: true,
            ...input,
          },
        },
      })
    const updatedMember = result.find(
      member => member.id === authorTeamMember.id,
    )
    expect(updatedMember.isCorresponding).toBe(true)
    expect(updatedMember.alias.givenNames).toBe(input.givenNames)
    expect(updatedMember.alias.surname).toBe(input.surname)
    expect(updatedMember.alias.email).toBe(input.email)
    expect(updatedMember.alias.country).toBe(input.country)
    expect(updatedMember.alias.aff).toBe(input.aff)
  })
  it('should make the submitting author corresponding if the tag is removed', async () => {
    const { Team, Manuscript } = models
    const manuscript = fixtures.generateManuscript({ Manuscript })
    const submittingAuthor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })
    const correspondingAuthor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isCorresponding: true,
      },
      role: Team.Role.author,
    })

    const result = await editAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        params: {
          manuscriptId: manuscript.id,
          authorTeamMemberId: correspondingAuthor.id,
          authorInput: {
            isCorresponding: false,
          },
        },
      })

    const updatedSubmittingMember = result.find(
      member => member.id === submittingAuthor.id,
    )
    const updatedCorrespondingMember = result.find(
      member => member.id === correspondingAuthor.id,
    )

    expect(updatedSubmittingMember.isCorresponding).toBe(true)
    expect(updatedCorrespondingMember.isCorresponding).toBe(false)
  })
  it('should make the other authors non-corresponding if the tag is added', async () => {
    const { Manuscript, Team } = models
    const manuscript = fixtures.generateManuscript({ Manuscript })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
        isCorresponding: true,
      },
      role: Team.Role.author,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })
    const editedAuthor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const result = await editAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        params: {
          manuscriptId: manuscript.id,
          authorTeamMemberId: editedAuthor.id,
          authorInput: {
            isCorresponding: true,
          },
        },
      })

    const updatedCorrespondingMember = result.find(
      member => member.id === editedAuthor.id,
    )
    const updatedNonCorrespondingMembers = result.filter(
      member => member.id !== editedAuthor.id,
    )
    expect(updatedCorrespondingMember.isCorresponding).toBe(true)
    updatedNonCorrespondingMembers.forEach(member => {
      expect(member.isCorresponding).toBe(false)
    })
  })
  it('should successfully change isCorresponding field in a revision', async () => {
    const { Manuscript, Team } = models
    const manuscript = fixtures.generateManuscript({ Manuscript })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.triageEditor,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.academicEditor,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.reviewer,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
        isCorresponding: true,
      },
      role: Team.Role.author,
    })
    const authorTeamMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: false,
        isCorresponding: true,
      },
      role: Team.Role.author,
    })
    const result = await editAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        params: {
          manuscriptId: manuscript.id,
          authorTeamMemberId: authorTeamMember.id,
          authorInput: {
            isCorresponding: true,
          },
        },
      })
    const updatedMember = result.find(
      member => member.id === authorTeamMember.id,
    )
    expect(updatedMember.isCorresponding).toBe(true)
  })
})
