const {
  getTeamRoles,
  generateFile,
  generateJournal,
  generateFileTypes,
  generateTeamMember,
  generateManuscript,
  generateArticleType,
  getTeamMemberStatuses,
  getArticleTypesEditorialTypes,
} = require('component-generators')

const useCases = require('../src/useCases')

let models = {}
let Manuscript = {}
let TeamMember = {}
let File = {}

const logEvent = () => jest.fn(async () => {})
logEvent.actions = { manuscript_submitted: 'manuscript_submitted' }
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  sendSubmittingAuthorConfirmation: jest.fn(),
}

const { submitManuscriptUseCase } = useCases

describe('submitManuscript', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Manuscript: {
        find: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        Statuses: getTeamMemberStatuses(),
        findTriageEditor: jest.fn(),
        findAllByJournalAndRole: jest.fn(),
        findAllByManuscriptAndRole: jest.fn(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
      },
      ArticleType: {
        EditorialTypes: getArticleTypesEditorialTypes(),
      },
      File: {
        findOneBy: jest.fn(),
        Types: generateFileTypes(),
      },
    }
    ;({ Manuscript, TeamMember, File } = models)
  })
  it('Executes the main flow', async () => {
    const journal = generateJournal()
    const file = generateFile({ type: 'manuscript' })
    const articleType = generateArticleType()
    const manuscript = generateManuscript({ journal, articleType })
    const author = generateTeamMember()
    const articleTypeWithRIPE = { execute: jest.fn() }
    const articleTypeEditorials = { execute: jest.fn() }
    const articleTypeWithPeerReview = { execute: jest.fn() }

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValue([])
    jest.spyOn(File, 'findOneBy').mockResolvedValue(file)
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([author])

    await submitManuscriptUseCase
      .initialize({
        models,
        logEvent,
        notificationService,
        articleTypeWithRIPE,
        articleTypeEditorials,
        articleTypeWithPeerReview,
      })
      .execute({ manuscriptId: manuscript.id })

    expect(articleTypeWithPeerReview.execute).toHaveBeenCalledTimes(1)
  })
})
