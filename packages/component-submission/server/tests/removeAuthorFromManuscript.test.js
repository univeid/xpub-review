const {
  generateManuscript,
  getTeamRoles,
  generateTeamMember,
  getManuscriptStatuses,
} = require('component-generators')

const { removeAuthorFromManuscriptUseCase } = require('../src/useCases')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = { author_removed: 'author_removed' }

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    findAllByManuscriptAndRole: jest.fn(),
    removeOneAndUpdateMany: jest.fn(),
  },
  Manuscript: {
    find: jest.fn(),
    Statuses: getManuscriptStatuses(),
  },
}
const { Team, TeamMember, Manuscript } = models
describe('Remove Author from Manuscript Use Case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('throws an error if there are not enough authors', async () => {
    const manuscript = generateManuscript()
    const author = generateTeamMember({
      team: { role: Team.Role.author },
      isSubmitting: true,
      isCorresponding: true,
    })
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([author])

    const result = removeAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorTeamMemberId: author.id,
      })

    await expect(result).rejects.toThrow(
      `There are not enough authors on manuscript ${manuscript.id}`,
    )
  })
  it('throws an error if author to be removed is not found in the authors team', async () => {
    const manuscript = generateManuscript()
    const author = generateTeamMember({
      team: { role: Team.Role.author },
      isSubmitting: true,
      isCorresponding: true,
    })
    const coAuthor = generateTeamMember({
      team: { role: Team.Role.author },
      isSubmitting: false,
      isCorresponding: true,
    })
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([author, coAuthor])

    const result = removeAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorTeamMemberId: 'some-id',
      })

    await expect(result).rejects.toThrow(
      `Author some-id has not been found in authors team of manuscript ${manuscript.id}`,
    )
  })
  it('removes the author from the team', async () => {
    const manuscript = generateManuscript()
    const author = generateTeamMember({
      team: { role: Team.Role.author },
      isSubmitting: true,
      isCorresponding: false,
    })
    const coAuthor = generateTeamMember({
      team: { role: Team.Role.author },
      isSubmitting: false,
      isCorresponding: true,
    })
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([author, coAuthor])
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)

    const result = await removeAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorTeamMemberId: coAuthor.id,
      })

    expect(result.length).toBe(1)
    expect(result[0].isCorresponding).toBeTruthy()
  })
})
