const { getActiveJournalsUseCase } = require('../src/useCases')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const Chance = require('chance')

const chance = new Chance()

describe('Get active journals use case', () => {
  const { Journal, Team } = models
  const journals = [
    {
      name: chance.company(),
      activationDate: Date.now() + 3454,
      isActive: false,
    },
    {
      name: chance.company(),
      activationDate: new Date().toISOString(),
      isActive: true,
    },
    {
      name: chance.company(),
      activationDate: new Date().toISOString(),
      isActive: true,
    },
  ]
  journals.map(journal =>
    fixtures.generateJournal({ properties: journal, Journal }),
  )
  const { userId } = dataService.createGlobalUser({
    models,
    fixtures,
    role: Team.Role.admin,
  })

  it('returns an array', async () => {
    const res = await getActiveJournalsUseCase
      .initialize(models)
      .execute({ userId })
    expect(Array.isArray(res)).toBeTruthy()
  })

  it('returns the correct number of active journals', async () => {
    const res = await getActiveJournalsUseCase
      .initialize(models)
      .execute({ userId })
    expect(res.length).toEqual(2)
  })
})
