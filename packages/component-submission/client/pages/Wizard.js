import React, { Fragment, useState } from 'react'
import { get } from 'lodash'
import { Formik } from 'formik'
import { Redirect } from 'react-router'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { Steps, Spinner } from '@pubsweet/ui'
import { compose, withHandlers, withProps } from 'recompose'

import { withGQL as withFilesGQL } from 'component-files/client'

import { MultiAction, withSteps, withFetching } from '@hindawi/ui'

import withSubmissionGQL from '../graphql'
import {
  autosaveForm as autoSaveFormHandler,
  setInitialValues,
  validateWizard,
  parseError,
} from '../utils'
import { SubmissionStatement, WizardButtons, wizardSteps } from '../components'

// handlers start
const onUploadFile = ({ match, uploadFile }) => (
  file,
  { type, push, setFetching, setError, ...rest },
) => {
  const manuscriptId = get(match, 'params.manuscriptId', '')

  const fileInput = {
    type,
    size: file.size,
  }

  setFetching(true)
  uploadFile({ entityId: manuscriptId, fileInput, file })
    .then(uploadedFile => {
      setFetching(false)
      push(uploadedFile)
    })
    .catch(e => {
      setFetching(false)
      setError(parseError(e))
    })
}

const onDeleteFile = ({ deleteFile }) => (
  file,
  { index, remove, setError, setFetching },
) => {
  setFetching(true)
  deleteFile(file.id)
    .then(() => {
      setFetching(false)
      remove(index)
    })
    .catch(e => {
      setFetching(false)
      setError(parseError(e))
    })
}

const onChangeList = ({ updateManuscriptFile }) => ({
  fileId,
  sourceProps,
  toListName: type,
  destinationProps,
}) => {
  updateManuscriptFile({
    variables: {
      fileId,
      type,
    },
  })
    .then(r => {
      const file = r.data.updateManuscriptFile
      sourceProps.remove(sourceProps.index)
      destinationProps.push(file)
    })
    .catch(e => {
      destinationProps.setError(parseError(e))
    })
}

const onDeleteAuthor = ({ match, removeAuthor }) => ({
  values,
  setFieldValue,
}) => (
  { id: authorTeamMemberId },
  { setError, clearError, setEditMode, setFetching, setWizardEditMode },
) => {
  const manuscriptId = get(match, 'params.manuscriptId', '')
  clearError()
  setFetching(true)
  removeAuthor({
    variables: { manuscriptId, authorTeamMemberId },
  })
    .then(r => {
      setFetching(false)
      setEditMode(false)
      setFieldValue('authors', r.data.removeAuthorFromManuscript)
    })
    .catch(e => {
      setFetching(false)
      setError(parseError(e))
    })
}

const onEditAuthor = ({ match, editAuthor }) => ({ values, setFieldValue }) => (
  { id: authorTeamMemberId, ...authorInput },
  { setError, clearError, setEditMode, setFetching, setWizardEditMode },
) => {
  const manuscriptId = get(match, 'params.manuscriptId', '')
  clearError()
  setFetching(true)
  editAuthor({
    variables: { manuscriptId, authorInput, authorTeamMemberId },
  })
    .then(r => {
      setFetching(false)
      setWizardEditMode(false)
      setFieldValue('authors', r.data.editAuthorFromManuscript)
    })
    .catch(e => {
      setFetching(false)
      setError(parseError(e))
    })
}

const onSaveAuthor = ({ addAuthorToManuscript, match }) => ({
  values,
  setFieldValue,
}) => (
  { id, ...authorInput },
  {
    index,
    formFns,
    setError,
    clearError,
    setEditMode,
    setFetching,
    setWizardEditMode,
  },
) => {
  clearError()
  setFetching(true)
  const manuscriptId = get(match, 'params.manuscriptId')
  addAuthorToManuscript({
    variables: { manuscriptId, authorInput },
  })
    .then(r => {
      setFetching(false)
      setWizardEditMode(false)
      setFieldValue('authors', r.data.addAuthorToManuscript)
    })
    .catch(e => {
      setFetching(false)
      setError(parseError(e))
    })
}

const autosaveForm = ({ match, updateDraft, updateAutosave }) => values => {
  autoSaveFormHandler({
    values,
    updateDraft,
    updateAutosave,
  })
}

const onSubmit = ({
  step,
  history,
  editMode,
  nextStep,
  showModal,
  setFetching,
  submitManuscript,
  editManuscript,
  journal,
}) => (values, formikBag) => {
  if (step !== wizardSteps.length - 1) {
    if (step === 2 && get(values, 'authors', []).length === 0) {
      formikBag.setFieldError('authors', 'At least one author is required.')
    } else {
      nextStep()
      formikBag.resetForm(values)
    }
  } else if (editMode) {
    editManuscript({ variables: { values } })
    history.goBack()
  } else if (!editMode) {
    showModal({
      title:
        'By submitting the manuscript, you agree to the following statements:',
      content: SubmissionStatement,
      confirmText: 'AGREE & SUBMIT',
      cancelText: 'BACK TO SUBMISSION',
      onConfirm: ({ hideModal, setFetching, setError }) => {
        setFetching(true)
        submitManuscript({
          variables: { values },
        })
          .then(r => {
            setFetching(false)
            hideModal()
            history.push('/confirmation-page', journal.name)
          })
          .catch(e => {
            setFetching(false)
            setError(parseError(e))
          })
      },
    })
  }
}
// handlers end

const defineProps = ({ data }) => {
  const { manuscript } = data
  const manuscriptStatus = get(manuscript, 'status')
  const activeJournals = get(data, 'getActiveJournals', [])
  const journal = activeJournals.find(
    journal => journal.id === manuscript.journalId,
  )
  return {
    initialValues: setInitialValues(manuscript),
    editMode: get(manuscript, 'status', 'draft') !== 'draft',
    manuscriptStatus,
    permissionError:
      manuscriptStatus === 'accepted' || manuscriptStatus === 'rejected',
    activeJournals,
    journal,
  }
}

const Wizard = ({
  step,
  theme,
  journal,
  history,
  onSubmit,
  prevStep,
  showModal,
  isFetching,
  autosaveForm,
  onEditAuthor,
  onSaveAuthor,
  initialValues,
  onDeleteAuthor,
  permissionError,
  data: { loading },
  editMode,
  ...rest
}) => {
  const [isFileUploading, setIsFileUploading] = useState(false)
  if (permissionError) {
    return <Redirect to="/404" />
  }
  if (loading) {
    return <Spinner />
  }
  return (
    <Fragment>
      <Steps currentStep={step}>
        {wizardSteps.map(({ stepTitle }) => (
          <Steps.Step key={stepTitle} title={stepTitle} />
        ))}
      </Steps>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validate={validateWizard(step)}
      >
        {({ errors, handleSubmit, setFieldValue, values, setValues }) => (
          <Root isFirst={step === 0}>
            {autosaveForm(values)}

            {React.createElement(wizardSteps[step].component, {
              journal,
              setValues,
              setFieldValue,
              formValues: values,
              wizardErrors: errors,
              onEditAuthor: onEditAuthor({ values, setFieldValue }),
              onSaveAuthor: onSaveAuthor({ values, setFieldValue }),
              onDeleteAuthor: onDeleteAuthor({ values, setFieldValue }),
              setIsFileUploading,
              ...rest,
            })}

            <WizardButtons
              editMode={editMode}
              handleSubmit={handleSubmit}
              history={history}
              isFetching={isFetching}
              isFileUploading={isFileUploading}
              isFirst={step === 0}
              isLast={step === wizardSteps.length - 1}
              journal={journal}
              prevStep={prevStep}
            />
          </Root>
        )}
      </Formik>
    </Fragment>
  )
}
// #region compose
export default compose(
  withSteps,
  withFetching,
  withSubmissionGQL,
  withFilesGQL(),
  withModal({
    component: MultiAction,
    modalKey: 'submission-wizard',
  }),
  withProps(defineProps),
  withHandlers({
    // autosave
    autosaveForm,
    onUploadFile,
    onDeleteFile,
    onChangeList,
    onDeleteAuthor,
    onEditAuthor,
    onSaveAuthor,
    onSubmit,
  }),
)(Wizard)
// #endregion

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  margin: 0 auto;
  margin-top: calc(${th('gridUnit')} * 10);
  margin-bottom: calc(${th('gridUnit')} * 10);

  padding: calc(${th('gridUnit')} * 10);

  width: calc(${th('gridUnit')} * ${({ isFirst }) => (isFirst ? 148 : 280)});
`
// #endregion
