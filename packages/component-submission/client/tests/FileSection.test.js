import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { renderWithDragAndFormik as render } from './testUtils'
import FileSection from '../components/FileSection'

const files = [
  {
    id: 'file1',
    originalName: 'myfile.docx',
    size: 51312,
  },
  {
    id: 'file2',
    originalName: 'myfile.pdf',
    size: 984512,
  },
]

// eslint-disable-next-line
describe.skip('FileSection', () => {
  afterEach(cleanup)

  it('should display all the files passed as props', () => {
    const props = {
      compProps: {
        files,
      },
    }
    const { getByText } = render(FileSection, props)

    files.forEach(f => expect(getByText(f.originalName)).toBeInTheDocument())
  })

  it('should be able to add files', () => {
    const onUploadMock = jest.fn()
    const props = {
      compProps: {
        files,
        onUploadFile: onUploadMock,
      },
    }
    render(FileSection, props)

    fireEvent.change(document.querySelector('input[type=file]'), {
      target: {
        files: [
          new File(['heihei'], 'AmazingReport.pdf', {
            type: 'application/pdf',
          }),
        ],
      },
    })

    expect(onUploadMock).toHaveBeenCalledTimes(1)
    expect(onUploadMock.mock.calls[0][0].name).toEqual('AmazingReport.pdf')
  })
})
