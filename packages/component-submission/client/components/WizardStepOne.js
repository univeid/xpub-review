/* eslint-disable react/jsx-no-comment-textnodes */
import React, { Fragment } from 'react'
import {
  Row,
  Item,
  Menu,
  SearchableSelect,
  Label,
  Text,
  ActionLink,
  validators,
  RadioGroup,
  ValidatedFormField,
} from '@hindawi/ui'
import { H2 } from '@pubsweet/ui'
import { get, flatMap } from 'lodash'
import { Modal } from 'component-modal'

import CallForPapers from '../components/CallForPapersModal'

const WizardStepOne = ({ activeJournals, setValues, formValues }) => {
  const resetFields = (options = {}) => () => {
    setValues({
      ...formValues,
      meta: {
        ...formValues.meta,
        articleTypeId: undefined,
        conflictOfInterest: '',
        dataAvailability: '',
        fundingStatement: '',
      },

      ...options,
    })
  }

  const onSelectJournal = v =>
    resetFields({
      journalId: v ? v.value : undefined,
      issueType: 'regularIssue',
      sectionId: undefined,
      specialIssueId: undefined,
    })()

  const onSelectSpecialIssue = v =>
    resetFields({
      specialIssueId: v ? v.value : undefined,
      sectionId: undefined,
      issueType: 'specialIssue',
    })()

  const issueTypeValue = get(formValues, 'issueType')
  const issueTypesOptions = [
    {
      label: 'Regular Issue',
      value: 'regularIssue',
      checked: issueTypeValue === 'regularIssue',
    },
    {
      label: 'Special Issue',
      value: 'specialIssue',
      checked: issueTypeValue === 'specialIssue',
    },
  ]

  const journalsOptions = activeJournals.map(({ id, name }) => ({
    value: id,
    label: name,
  }))

  const selectedJournal = formValues.journalId
    ? activeJournals.find(j => j.id === formValues.journalId)
    : undefined

  const specialIssueOptions =
    selectedJournal &&
    [
      ...selectedJournal.specialIssues,
      ...flatMap(selectedJournal.sections, section => section.specialIssues),
    ].map(({ id, name, callForPapers }) => ({
      value: id,
      label: name,
      callForPapers,
    }))

  const hasSectionsDropdown =
    selectedJournal &&
    !!selectedJournal.sections.length &&
    issueTypeValue === 'regularIssue'

  const hasIssuesDropdown =
    selectedJournal &&
    !!specialIssueOptions.length &&
    issueTypeValue === 'specialIssue'

  const specialIssueId = get(formValues, 'specialIssueId')
  const specialIssueSelected =
    specialIssueId &&
    specialIssueOptions.find(si => si.value === specialIssueId)
  const callForPapers = get(specialIssueSelected, 'callForPapers')

  return (
    <Fragment>
      <H2>1. Select Journal</H2>
      <Row mb={10}>
        <Text align="center" mb={2} mt={2} secondary>
          Please select a Journal
        </Text>
      </Row>
      <Row>
        <Item data-test-id="journal-select" vertical>
          <Label mb={1} required>
            Journal
          </Label>
          <ValidatedFormField
            component={SearchableSelect}
            name="journalId"
            onChange={onSelectJournal}
            options={journalsOptions}
            placeholder="Select journal"
            validate={[validators.required]}
          />
        </Item>
      </Row>

      {selectedJournal && !!specialIssueOptions.length && (
        <Fragment>
          <Row>
            <Item vertical>
              <Label mb={1}>Submit to an Issue</Label>
            </Item>
          </Row>
          <Row>
            <Item data-test-id="issue-selector" vertical>
              <ValidatedFormField
                component={RadioGroup}
                name="issueType"
                onChange={resetFields({
                  specialIssueId: undefined,
                  sectionId: undefined,
                })}
                options={issueTypesOptions}
              />
            </Item>
          </Row>
        </Fragment>
      )}

      {hasSectionsDropdown && (
        <Row>
          <Item data-test-id="section-select" vertical>
            <Label mb={1} required>
              Section
            </Label>
            <ValidatedFormField
              component={Menu}
              name="sectionId"
              options={selectedJournal.sections.map(({ id, name }) => ({
                value: id,
                label: name,
              }))}
              placeholder="Select section"
              validate={[validators.required]}
            />
          </Item>
        </Row>
      )}

      {hasIssuesDropdown && (
        <Fragment>
          <Row>
            <Item data-test-id="special-issue-select" vertical>
              <Label mb={1} required>
                Special Issue
              </Label>
              <ValidatedFormField
                component={SearchableSelect}
                name="specialIssueId"
                onChange={onSelectSpecialIssue}
                options={specialIssueOptions}
                placeholder="Select special issue"
                validate={[validators.required]}
              />
            </Item>
          </Row>
          {specialIssueSelected && (
            <Row mt={2}>
              <Item alignItems="center">
                <Modal
                  callForPapers={callForPapers}
                  component={CallForPapers}
                  modalKey="callForPapers"
                >
                  {showModal => (
                    <ActionLink
                      data-test-id="call-for-papers"
                      flex={1}
                      fontSize="13px"
                      fontWeight="bold"
                      onClick={showModal}
                    >
                      Please read Call for Papers
                    </ActionLink>
                  )}
                </Modal>
              </Item>
            </Row>
          )}
        </Fragment>
      )}
    </Fragment>
  )
}

export default WizardStepOne
