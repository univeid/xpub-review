import gql from 'graphql-tag'
import { fragments as fileFragments } from 'component-files/client'

export const teamMember = gql`
  fragment teamMember on TeamMember {
    id
    isSubmitting
    isCorresponding
    invited
    responded
    reviewerNumber
    user {
      id
    }
    status
    alias {
      aff
      email
      country
      name {
        surname
        givenNames
      }
    }
  }
`
export const specialIssue = gql`
  fragment specialIssue on SpecialIssue {
    id
    name
    callForPapers
    isActive
    endDate
    customId
  }
`

export const draftManuscriptDetails = gql`
  fragment draftManuscriptDetails on Manuscript {
    id
    journalId
    submissionId
    preprintValue
    meta {
      ... on ManuscriptMeta {
        title
        abstract
        agreeTc
        articleTypeId
        dataAvailability
        fundingStatement
        conflictOfInterest
      }
    }
    status
    authors {
      ...teamMember
    }
    files {
      ...fileDetails
    }
    specialIssue {
      ...specialIssue
    }
    section {
      id
      name
    }
  }
  ${specialIssue}
  ${teamMember}
  ${fileFragments.fileDetails}
`

export const activeJournals = gql`
  fragment activeJournals on Journal {
    id
    name
    code
    apc
    preprints {
      type
      format
    }
    preprintDescription
    articleTypes {
      id
      name
    }
    specialIssues {
      ...specialIssue
    }
    sections {
      id
      name
      customId
      specialIssues {
        ...specialIssue
      }
    }
  }
  ${specialIssue}
`
