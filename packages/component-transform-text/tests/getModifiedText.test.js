const { getModifiedText } = require('../src')

const text =
  'Lorem {Ipsum} is simply {dummy} text of the printing and typesetting industry.'

describe('getModifiedText', () => {
  it('it should replace all the string that match with pattern sent', () => {
    const result = getModifiedText(
      text,
      { pattern: '{Ipsum}', replacement: 'Lorem' },
      { pattern: '{dummy}', replacement: 'no brain' },
    )
    expect(result).toEqual(
      'Lorem Lorem is simply no brain text of the printing and typesetting industry.',
    )
  })
  it('it should replace only what pattern finds', () => {
    const result = getModifiedText(
      text,
      { pattern: '{dummy}', replacement: 'no brain' },
      { pattern: '{strawberry}', replacement: 'fruit' },
    )
    expect(result).toEqual(
      'Lorem {Ipsum} is simply no brain text of the printing and typesetting industry.',
    )
  })
})
