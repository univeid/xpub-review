module.exports.sortResultsByKeyOrder = ({ keys, results, getKey }) => {
  const resultsByKey = results.reduce((acc, val) => {
    const key = getKey(val)
    acc.set(key, val)
    return acc
  }, new Map())
  return keys.map(key => resultsByKey.get(key))
}

module.exports.groupResultsByKey = ({ keys, results, getKey }) => {
  const groupedResults = results.reduce((acc, val) => {
    const key = getKey(val)
    acc.set(key, [...(acc.get(key) || []), val])
    return acc
  }, new Map())
  return keys.map(key => groupedResults.get(key))
}
