const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateArticleType(props) {
    return {
      id: chance.guid(),
      name: chance.name(),
      hasPeerReview: true,
      save: jest.fn(),
      updateProperties: jest.fn(),
      ...props,
    }
  },
  getArticleTypesTypes() {
    return types
  },
  getArticleTypesEditorialTypes() {
    return [types.editorial, types.commentary]
  },
}

const types = {
  commentary: 'Commentary',
  editorial: 'Editorial',
  retraction: 'Retraction',
  caseSeries: 'Case Series',
  caseReport: 'Case Report',
  reviewArticle: 'Review Article',
  researchArticle: 'Research Article',
  letterToTheEditor: 'Letter to the Editor',
  expressionOfConcern: 'Expression of Concern',
  erratum: 'Erratum',
  corrigendum: 'Corrigendum',
}
