const Chance = require('chance')

const chance = new Chance()

module.exports = {
  generateFile(props) {
    return {
      id: chance.guid(),
      created: chance.date(),
      fileName: chance.name(),
      originalName: chance.name(),
      save: jest.fn(),
      ...props,
    }
  },
  generateFileTypes() {
    return types
  },
}

const types = {
  figure: 'figure',
  manuscript: 'manuscript',
  coverLetter: 'coverLetter',
  supplementary: 'supplementary',
  reviewComment: 'reviewComment',
  responseToReviewers: 'responseToReviewers',
}
