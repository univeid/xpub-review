const Chance = require('chance')

const chance = new Chance()
const statuses = {
  pending: 'pending',
  accepted: 'accepted',
  declined: 'declined',
  submitted: 'submitted',
  expired: 'expired',
  removed: 'removed',
  active: 'active',
}
const statusExpiredLabels = {
  overdue: 'OVERDUE',
  unresponsive: 'UNRESPONSIVE',
  unsubscribed: 'UNSUBSCRIBED',
}

module.exports = {
  generateTeamMember(props) {
    const teamMember = {
      id: chance.guid(),
      userId: chance.guid(),
      teamId: chance.guid(),
      status: statuses.pending,
      alias: {
        surname: chance.name(),
        givenNames: chance.name(),
        email: chance.email(),
      },
      user: {
        id: chance.guid(),
      },
      delete: jest.fn(),
      ...props,
    }
    Object.assign(teamMember, {
      updateProperties: jest.fn(props => {
        Object.assign(teamMember, props)
        return teamMember
      }),
      save: jest.fn(() => teamMember),
      toDTO: jest.fn(() => teamMember),
    })
    return teamMember
  },
  getTeamMemberStatuses() {
    return statuses
  },
  getStatusExpiredLabels() {
    return statusExpiredLabels
  },
}
