const Chance = require('chance')
const { get } = require('lodash')

const chance = new Chance()

module.exports = {
  generateAuditLog(props) {
    return {
      id: chance.guid(),
      created: chance.date(),
      transformLog: function transformLog({
        userMember,
        target,
        activityLogEvents,
      }) {
        return {
          id: this.id,
          created: this.created,
          version: this.version,
          action: get(activityLogEvents, this.action),
          user: {
            role: get(userMember, 'role'),
            email: get(userMember, 'alias.email'),
            reviewerNumber: get(userMember, 'reviewerNumber'),
          },
          target:
            target && target.team.role
              ? {
                  role: target.team.role,
                  email: target.getEmail(),
                  reviewerNumber: target.reviewerNumber,
                }
              : {
                  email: get(target, 'email'),
                },
        }
      },
      ...props,
    }
  },
}
