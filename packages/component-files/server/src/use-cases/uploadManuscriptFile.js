const uuid = require('uuid')
const { chain } = require('lodash')
const {
  getFileNumber,
  isMimetypeValid,
  decomposeFilename,
} = require('../fileUtils')

const initialize = ({ models: { File, Manuscript }, s3Service, logEvent }) => ({
  async execute({ entityId, fileInput, fileData, userId }) {
    const { stream, filename, mimetype } = await fileData

    if (!isMimetypeValid({ mimetype, type: fileInput.type, File }))
      throw new Error('Please upload one of the supported file types.')

    const key = `${entityId}/${uuid.v4()}`
    let parsedName = filename

    const manuscript = await Manuscript.find(entityId, 'files')
    const manuscriptVersions = await Manuscript.findManuscriptsBySubmissionId({
      submissionId: manuscript.submissionId,
      excludedStatus: Manuscript.Statuses.deleted,
      eagerLoadRelations: '[files]',
    })
    const lastFile = chain(manuscriptVersions)
      .flatMap(m => m.files)
      .filter(file => file.originalName === filename)
      .sortBy('created')
      .last()
      .value()

    if (lastFile) {
      const [name, ext] = decomposeFilename(lastFile.originalName)
      const fileNumber = getFileNumber(lastFile)
      parsedName = `${name} (${fileNumber}).${ext}`
    }

    await s3Service.upload({
      key,
      stream,
      mimetype,
      metadata: {
        filename,
        type: fileInput.type,
      },
    })

    const file = new File({
      manuscriptId: entityId,
      commentId: null,
      size: fileInput.size,
      fileName: parsedName,
      providerKey: key,
      mimeType: mimetype,
      originalName: filename,
      type: File.Types[fileInput.type],
    })
    await file.save()

    manuscript.assignFile(file)

    await manuscript.save()
    if (manuscript.status !== 'draft') {
      logEvent({
        userId,
        manuscriptId: entityId,
        action: logEvent.actions.file_added,
        objectType: logEvent.objectType.file,
        objectId: file.id,
      })
    }

    return {
      ...file,
      filename: file.fileName,
    }
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
