import gql from 'graphql-tag'

export const declineEQS = gql`
  mutation declineEQS($manuscriptId: String!, $input: DeclineEQSInput) {
    declineEQS(manuscriptId: $manuscriptId, input: $input)
  }
`
export const approveEQS = gql`
  mutation approveEQS($manuscriptId: String!, $input: ApproveEQSInput) {
    approveEQS(manuscriptId: $manuscriptId, input: $input)
  }
`

export const returnToApprovalEditor = gql`
  mutation returnToApprovalEditor(
    $manuscriptId: String!
    $input: ReturnToApprovalEditorInput!
  ) {
    returnToApprovalEditor(manuscriptId: $manuscriptId, input: $input)
  }
`
