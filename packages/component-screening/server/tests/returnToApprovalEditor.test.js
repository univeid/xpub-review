process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { returnToApprovalEditorUseCase } = require('../src/useCases')

const notificationService = {
  notifyApprovalEditor: jest.fn(),
}
const chance = new Chance()

describe('Return manuscript to approval editor use case', () => {
  let journal
  let manuscript
  let token
  let reason

  const { PeerReviewModel, Journal, Team, Manuscript, TeamMember } = models

  TeamMember.getApprovalEditorRole = jest.fn(() => 'triageEditor')

  beforeEach(async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    journal = fixtures.generateJournal({
      properties: {
        peerReviewModel,
      },
      Journal,
    })
    token = chance.guid()
    reason = chance.paragraph()
    await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    manuscript = fixtures.generateManuscript({
      properties: {
        journal,
        technicalCheckToken: token,
        status: Manuscript.Statuses.inQA,
        hasPassedEqa: null,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: Team.Role.author,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.submitted },
      role: Team.Role.reviewer,
    })
  })

  it('should return error when token is invalid', async () => {
    const anotherToken = chance.guid()
    const response = returnToApprovalEditorUseCase
      .initialize({ notificationService, models })
      .execute({
        manuscriptId: manuscript.id,
        input: { token: anotherToken, reason },
      })
    return expect(response).rejects.toThrow('Invalid token.')
  })
  it('should throw error if the manuscript already passed EQA', async () => {
    manuscript.hasPassedEqa = true
    const result = returnToApprovalEditorUseCase
      .initialize({ notificationService, models })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
      })
    return expect(result).rejects.toThrow('Manuscript already handled.')
  })
})
