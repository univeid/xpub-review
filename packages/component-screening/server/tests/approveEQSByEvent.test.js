const {
  getTeamRoles,
  generateJournal,
  generateManuscript,
  generateTeamMember,
  getTeamMemberStatuses,
  getManuscriptStatuses,
  generatePeerReviewModel,
} = require('component-generators')

const { approveEQSByEventUseCase } = require('../src/useCases')

let models = {}
let PeerReviewModel = {}
let Journal = {}
let Manuscript = {}
let TeamMember = {}

const logEvent = () => jest.fn(async () => {})
logEvent.actions = { eqs_approved: 'eqs_approved' }
logEvent.objectType = { manuscript: 'manuscript' }

const useCases = {
  assignTriageEditorOnManuscriptUseCase: {
    initialize: jest.fn(() => ({
      execute: jest.fn(),
    })),
  },
}
const eventsService = {}
const notificationService = { notifyApprovalEditor: jest.fn() }
const getUserWithWorkloadUseCase = { initialize: jest.fn() }
const getUserWithWorkloadForEditorialConflictsUseCase = {
  initialize: jest.fn(),
}

describe('approveEQSByEvent', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      PeerReviewModel: {
        findOneByManuscriptParent: jest.fn(),
      },
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        Statuses: getManuscriptStatuses(),
        findOneBy: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        Statuses: getTeamMemberStatuses(),
        findTriageEditor: jest.fn(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
      },
    }
    ;({ PeerReviewModel, Journal, Manuscript, TeamMember } = models)
  })
  it('Throws an error if the manuscript is already handled', async () => {
    const data = {}
    const manuscript = generateManuscript({ hasPassedEqs: true })

    jest.spyOn(Manuscript, 'findOneBy').mockResolvedValue(manuscript)

    const res = approveEQSByEventUseCase
      .initialize({ models })
      .execute({ data })

    return expect(res).rejects.toThrow('Manuscript already handled.')
  })
  it('Executes the main flow', async () => {
    const data = {}
    const peerReviewModel = generatePeerReviewModel({ hasTriageEditor: true })
    const journal = generateJournal()
    const manuscript = generateManuscript({
      hasPassedEqs: null,
      getSubmittingAuthor: jest.fn(),
      getHasSpecialIssueEditorialConflictOfInterest: jest.fn(),
    })
    const teamMember = generateTeamMember()

    jest.spyOn(Manuscript, 'findOneBy').mockResolvedValue(manuscript)
    jest.spyOn(Journal, 'find').mockResolvedValue(journal)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(teamMember)
    jest
      .spyOn(PeerReviewModel, 'findOneByManuscriptParent')
      .mockResolvedValue(peerReviewModel)

    await approveEQSByEventUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        notificationService,
        getUserWithWorkloadUseCase,
        getUserWithWorkloadForEditorialConflictsUseCase,
      })
      .execute({ data })

    expect(
      useCases.assignTriageEditorOnManuscriptUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
  })
})
