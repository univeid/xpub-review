const approveEQSUseCase = require('./approveEQS')
const declineEQSUseCase = require('./declineEQS')
const returnToDraftUseCase = require('./returnToDraft')
const voidManuscriptUseCase = require('./voidManuscript')
const approveEQSByEventUseCase = require('./approveEQSByEvent')
const declineEQSByEventUseCase = require('./declineEQSByEvent')
const returnToApprovalEditorUseCase = require('./returnToApprovalEditor')
const assignTriageEditorOnManuscriptUseCase = require('./assignTriageEditorOnManuscript')

module.exports = {
  approveEQSUseCase,
  declineEQSUseCase,
  returnToDraftUseCase,
  voidManuscriptUseCase,
  approveEQSByEventUseCase,
  declineEQSByEventUseCase,
  returnToApprovalEditorUseCase,
  assignTriageEditorOnManuscriptUseCase,
}
