const initialize = ({
  notificationService,
  models: { Manuscript, TeamMember, Team, ArticleType, PeerReviewModel },
}) => ({
  async execute({ manuscriptId, input }) {
    const manuscript = await Manuscript.find(manuscriptId)
    const approvalEditorRole = await TeamMember.getApprovalEditorRole({
      ArticleType,
      PeerReviewModel,
      TeamRole: Team.Role,
      manuscriptId: manuscript.id,
    })
    const { token } = input
    if (manuscript.status !== Manuscript.Statuses.inQA) {
      throw new ValidationError(
        `Cannot return manuscript in the current status.`,
      )
    }

    if (manuscript.hasPassedEqa !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    const newStatus =
      approvalEditorRole === Team.Role.academicEditor
        ? Manuscript.Statuses.makeDecision
        : Manuscript.Statuses.pendingApproval
    manuscript.updateProperties({
      technicalCheckToken: null,
      hasPassedEqa: true,
      status: newStatus,
    })
    await manuscript.save()

    const approvalEditor = await TeamMember.findApprovalEditor({
      manuscript,
      ArticleType,
      PeerReviewModel,
      TeamRole: Team.Role,
    })
    const { journal } = manuscript
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const submittingAuthor = manuscript.getSubmittingAuthor()

    notificationService.notifyApprovalEditor({
      manuscript,
      approvalEditor,
      submittingAuthor,
      editorialAssistant,
      comments: input.reason,
      journalName: journal.name,
    })
  },
})

const authsomePolicies = ['isEditorialAssistant', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
