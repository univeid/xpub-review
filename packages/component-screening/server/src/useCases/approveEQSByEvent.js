const initialize = ({
  models,
  logEvent,
  useCases,
  eventsService,
  notificationService,
  getUserWithWorkloadUseCase,
  getUserWithWorkloadForEditorialConflictsUseCase,
}) => ({
  async execute({ data }) {
    const { submissionId } = data
    const {
      Team,
      User,
      Journal,
      Manuscript,
      TeamMember,
      SpecialIssue,
      PeerReviewModel,
    } = models

    const manuscript = await Manuscript.findOneBy({
      queryObject: { submissionId },
      eagerLoadRelations: '[teams.members, articleType]',
    })
    if (manuscript.hasPassedEqs !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    const journal = await Journal.find(
      manuscript.journalId,
      '[teams.members, peerReviewModel]',
    )
    manuscript.journal = journal

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    const submittingAuthor = manuscript.getSubmittingAuthor()

    const hasSpecialIssueEditorialConflictOfInterest = await manuscript.getHasSpecialIssueEditorialConflictOfInterest(
      models,
    )

    manuscript.updateProperties({
      hasPassedEqs: true,
      technicalCheckToken: null,
      status: Manuscript.Statuses.submitted,
    })
    await manuscript.save()

    const peerReviewModel = await PeerReviewModel.findOneByManuscriptParent(
      manuscript,
    )

    await logEvent({
      userId: null,
      manuscriptId: manuscript.id,
      action: logEvent.actions.eqs_approved,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })

    if (peerReviewModel.hasTriageEditor) {
      const getEligibleUserUseCase = hasSpecialIssueEditorialConflictOfInterest
        ? await getUserWithWorkloadForEditorialConflictsUseCase.initialize(
            models,
          )
        : await getUserWithWorkloadUseCase.initialize(models)

      await useCases.assignTriageEditorOnManuscriptUseCase
        .initialize({
          eventsService,
          models: { TeamMember, Team, User, SpecialIssue },
          getEligibleUserUseCase,
          manuscriptStatuses: Manuscript.InProgressStatuses,
        })
        .execute({
          submissionId,
          manuscriptId: manuscript.id,
          journalId: journal.id,
          sectionId: manuscript.sectionId,
          specialIssueId: manuscript.specialIssueId,
        })
    }

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    if (!triageEditor) return

    triageEditor.user = await User.find(triageEditor.userId)
    notificationService.notifyApprovalEditor({
      manuscript,
      submittingAuthor,
      editorialAssistant,
      journalName: journal.name,
      approvalEditor: triageEditor,
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: [],
}
