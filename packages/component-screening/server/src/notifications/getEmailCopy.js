const config = require('config')

const staffEmail = config.get('staffEmail')

const initialize = () => ({
  getEmailCopy({
    comments,
    emailType,
    titleText,
    journalName,
    targetUserName,
  }) {
    let paragraph
    let hasIntro = true
    let hasLink = false
    const hasSignature = true
    switch (emailType) {
      case 'approval-editor-manuscript-returned-by-eqa':
        hasIntro = false
        paragraph = `We regret to inform you that ${titleText} has been returned with comments. Please click the link below to access the manuscript.<br/><br/>
          Comments: ${comments}<br/><br/>`
        break
      case 'academic-editor-manuscript-approved-by-eqa':
        hasIntro = false
        paragraph = `${targetUserName} has confirmed your decision to accept ${titleText}.<br/><br/>
        No further action is required at this time. To review this decision, please visit the manuscript details.<br/><br/>
        Thank you for handling this manuscript on behalf of ${journalName}.<br/><br/>`
        break
      case 'author-manuscript-approved-by-eqa':
        paragraph = `I am delighted to inform you that the review of your manuscript titled "${titleText}" has been completed and your article will be published in ${journalName}.<br/><br/>
        Please visit the manuscript details page to review the editorial notes and any comments from external reviewers.<br/><br/>
        Your article will now be passed to our production team for processing, and you will hear directly from them should we require any further information.<br/><br/>
        Thank you for choosing to publish with ${journalName}.<br/><br/>`
        break
      case 'reviewer-manuscript-approved-by-eqa':
        paragraph = `Thank you for your review of ${titleText} for ${journalName}. After taking into account the reviews and the recommendation of the Academic Editor, I can confirm this article will now be published.<br/><br/>
        No further action is required at this time. To see more details about this decision please view the manuscript details page.<br/><br/>
        If you have any questions about this decision, then please email them to ${staffEmail} as soon as possible. Thank you for reviewing for ${journalName}.<br/><br/>`
        break
      case 'approval-editor-manuscript-approved-after-eqs':
        hasLink = true
        paragraph = `A new manuscript titled ${titleText} has been submitted to ${journalName}.<br/><br/>
        To begin the review process, please visit the manuscript details page.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return { paragraph, hasLink, hasIntro, hasSignature }
  },
})

module.exports = {
  initialize,
}
