const { pick } = require('lodash')

module.exports.initialize = ({
  models: { Journal, Manuscript, User, Team, TeamMember },
}) => ({
  async publishSubmissionEvent({ submissionId, eventName }) {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      eagerLoadRelations: [
        'files',
        `journal.[
          teams.members,
          peerReviewModel
        ]`,
        'section',
        'articleType',
        'reviewerSuggestions',
        'reviews.comments.files',
        'teams.members.user.identities',
        'specialIssue.peerReviewModel',
      ],
    })
    if (manuscripts.length === 0) return

    let peerReviewModel = {}
    const manuscript = manuscripts[0]

    if (manuscript.specialIssue) {
      ;({ peerReviewModel } = manuscript.specialIssue)
    } else {
      ;({ peerReviewModel } = manuscript.journal)
    }

    const data = {
      submissionId,
      manuscripts: manuscripts.map(manuscript => {
        const properties = pick(manuscript, [
          'id',
          'title',
          'files',
          'version',
          'customId',
          'abstract',
          'preprintValue',
          'journalId',
          'sectionId',
          'acceptedDate',
          'specialIssueId',
          'dataAvailability',
          'fundingStatement',
          'conflictOfInterest',
          'qualityChecksSubmittedDate',
        ])

        properties.files.forEach(file => {
          delete file.manuscriptId
          delete file.commentId
        })

        const articleTypeName = manuscript.articleType
          ? manuscript.articleType.name
          : undefined

        const manuscriptData = {
          ...properties,
          submissionCreatedDate: manuscript.created,
          editors: [
            ...getEditors({
              Team,
              peerReviewModel,
              teams: manuscript.teams.filter(
                team =>
                  team.role !== Team.Role.author &&
                  team.role !== Team.Role.reviewer &&
                  team.role !== Team.Role.submittingStaffMember,
              ),
            }),
          ],
          submittingStaffMembers: manuscript.getSubmittingStaffMembersForEventData(
            { Team },
          ),
          authors: manuscript.getAuthorsForEventData(),
          reviewers: manuscript.getReviewersForEventData({
            Team,
          }),
          reviews: manuscript.getReviewsForEventData(),
          articleType: { name: articleTypeName },
        }

        return manuscriptData
      }),
    }

    await applicationEventBus.publishMessage({
      data,
      event: eventName,
    })
  },
  async publishUserEvent({ userId, eventName }) {
    const user = await User.find(userId, 'identities')
    delete user.passwordResetTimestamp
    delete user.passwordResetToken
    delete user.confirmationToken
    delete user.invitationToken
    delete user.unsubscribeToken

    user.identities.forEach(identity => {
      delete identity.userId
      delete identity.passwordHash
    })

    await applicationEventBus.publishMessage({
      event: eventName,
      data: { ...user },
    })
  },
  async publishJournalEvent({ journalId, eventName }) {
    const journal = await Journal.find(
      journalId,
      `[
        sections.[
          teams.members.user.identities,
          specialIssues.[
            teams.members.user.identities,
            peerReviewModel
          ]
        ],
        specialIssues.[
          teams.members.user.identities,
          peerReviewModel
        ],
        teams.members.user.identities,
        peerReviewModel,
        journalArticleTypes.articleType
      ]`,
    )

    journal.sections = journal.sections.map(section => {
      section.specialIssues = section.specialIssues.map(specialIssue => ({
        ...specialIssue,
        peerReviewModel: { name: specialIssue.peerReviewModel.name },
        editors: getEditors({
          teams: specialIssue.teams,
          peerReviewModel: specialIssue.peerReviewModel,
          Team,
        }),
      }))

      return {
        ...section,
        editors: getEditors({
          teams: section.teams,
          peerReviewModel: journal.peerReviewModel,
          Team,
        }),
      }
    })

    journal.articleTypes = journal.journalArticleTypes.map(
      journalArticleType => ({
        name: journalArticleType.articleType.name,
      }),
    )

    journal.specialIssues = journal.specialIssues.map(specialIssue => ({
      ...specialIssue,
      peerReviewModel: { name: specialIssue.peerReviewModel.name },
      editors: getEditors({
        teams: specialIssue.teams,
        peerReviewModel: specialIssue.peerReviewModel,
        Team,
      }),
    }))

    const data = {
      ...journal,
      peerReviewModel: { name: journal.peerReviewModel.name },
      editors: getEditors({
        teams: journal.teams,
        peerReviewModel: journal.peerReviewModel,
        Team,
      }),
    }

    delete data.teams
    delete data.journalArticleTypes
    delete data.peerReviewModelId
    data.sections.forEach(section => {
      delete section.teams
      delete section.journalId
    })
    data.specialIssues.forEach(specialIssue => {
      delete specialIssue.teams
      delete specialIssue.peerReviewModelId
      delete specialIssue.journalId
      delete specialIssue.sectionId
    })
    data.sections.forEach(section =>
      section.specialIssues.forEach(specialIssue => {
        delete specialIssue.teams
        delete specialIssue.peerReviewModelId
        delete specialIssue.journalId
        delete specialIssue.sectionId
      }),
    )

    await applicationEventBus.publishMessage({
      data,
      event: eventName,
    })
  },
})

const getEditors = ({ peerReviewModel, teams, Team }) => {
  const teamReducer = (acc, team) => {
    const members = team.members.map(member =>
      member.getEventData({
        peerReviewModel,
        role: team.role,
        Team,
      }),
    )
    return [...acc, ...members]
  }

  return teams.reduce(teamReducer, [])
}
