const { get, merge } = require('lodash')
const {
  setPublicationDate,
  setManuscriptId,
  setTitle,
  setAbstract,
  setArticleType,
  setVersion,
  setFundingGroup,
  setSection,
  setSpecialIssue,
  setHistory,
  setContributors,
  setCounts,
} = require('./setManuscriptData/index')

const setManuscriptData = ({
  authors,
  customizer,
  manuscript,
  figureFiles,
  academicEditor,
}) => {
  if (!manuscript) throw new Error('Manuscript was not provided')

  const publicationDateTemplate = setPublicationDate()
  const manuscriptIdTemplate = setManuscriptId(manuscript)
  const versionTemplate = setVersion({ version: manuscript.version })
  const titleTemplate = setTitle({ title: manuscript.title })
  const abstractTemplate = setAbstract({ abstract: manuscript.abstract })
  const articleTypeTemplate = setArticleType({
    articleTypeName: manuscript.articleType.name,
  })
  const sectionTemplate = setSection({
    sectionName: get(manuscript, 'section.name'),
  })
  const specialIssueTemplate = setSpecialIssue({
    specialIssueId: get(manuscript, 'specialIssue.customId'),
    specialIssueName: get(manuscript, 'specialIssue.name'),
  })
  const historyTemplate = setHistory({
    customizer,
    submittedDate: manuscript.submittedDate,
    finalRevisionDate: manuscript.updated, // wrong
    peerReviewPassedDate: manuscript.peerReviewPassedDate,
  })
  const contributorsTemplate = setContributors({
    authors,
    customizer,
    academicEditor,
  })
  const fundingGroupTemplate = setFundingGroup()
  const countsTemplate = setCounts({
    figureFilesCount: figureFiles && figureFiles.length,
  })

  return merge(
    publicationDateTemplate,
    manuscriptIdTemplate,
    articleTypeTemplate,
    versionTemplate,
    titleTemplate,
    contributorsTemplate,
    historyTemplate,
    abstractTemplate,
    fundingGroupTemplate,
    sectionTemplate,
    specialIssueTemplate,
    countsTemplate,
  )
}

module.exports = {
  setManuscriptData,
}
