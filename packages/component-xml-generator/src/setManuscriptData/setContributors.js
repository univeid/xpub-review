const { mergeWith } = require('lodash')

const {
  setAcademicEditorContributor,
} = require('./setContributors/setAcademicEditorContributor')
const {
  setAuthorContributors,
} = require('./setContributors/setAuthorContributors')

const setContributors = ({ authors, academicEditor, customizer }) => {
  const authorContributorsTemplate = setAuthorContributors({ authors })
  const academicEditorContributorTemplate = setAcademicEditorContributor(
    academicEditor,
  )

  return mergeWith(
    authorContributorsTemplate,
    academicEditorContributorTemplate,
    customizer, // we need this to merge the contributors array within objects
  )
}

module.exports = {
  setContributors,
}
