const { set } = require('lodash')

const setAbstract = ({ abstract }) => {
  const abstractTemplate = {
    _text: abstract,
  }

  return set({}, 'article.front.article-meta.abstract', abstractTemplate)
}

module.exports = {
  setAbstract,
}
