const { mergeWith } = require('lodash')
const { setDate } = require('./setDate')

const setHistory = ({
  customizer,
  submittedDate,
  finalRevisionDate,
  peerReviewPassedDate,
}) => {
  const submittedDateTemplate = setDate({
    date: submittedDate,
    dateType: 'received',
  })

  let finalRevisionDateTemplate = {}
  if (finalRevisionDate) {
    finalRevisionDateTemplate = setDate({
      dateType: 'rev-recd',
      date: finalRevisionDate,
    })
  }

  let peerReviewPassedDateTemplate = {}
  if (peerReviewPassedDate) {
    peerReviewPassedDateTemplate = setDate({
      dateType: 'accepted',
      date: peerReviewPassedDate,
    })
  }

  return mergeWith(
    submittedDateTemplate,
    finalRevisionDateTemplate,
    peerReviewPassedDateTemplate,
    customizer,
  )
}

module.exports = {
  setHistory,
}
