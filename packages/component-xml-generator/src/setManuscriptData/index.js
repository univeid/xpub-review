const { setAbstract } = require('./setAbstract')
const { setArticleType } = require('./setArticleType')
const { setContributors } = require('./setContributors')
const { setCounts } = require('./setCounts')
const { setDate } = require('./setDate')
const { setFundingGroup } = require('./setFundingGroup')
const { setHistory } = require('./setHistory')
const { setManuscriptId } = require('./setManuscriptId')
const { setPublicationDate } = require('./setPublicationDate')
const { setSection } = require('./setSection')
const { setSpecialIssue } = require('./setSpecialIssue')
const { setTitle } = require('./setTitle')
const { setVersion } = require('./setVersion')

module.exports = {
  setAbstract,
  setArticleType,
  setContributors,
  setCounts,
  setDate,
  setFundingGroup,
  setHistory,
  setManuscriptId,
  setPublicationDate,
  setSection,
  setSpecialIssue,
  setTitle,
  setVersion,
}
