const { set, merge } = require('lodash')

const setAcademicEditorContributor = academicEditor => {
  if (!academicEditor) return

  if (!academicEditor.user)
    throw new Error(`Editor ${academicEditor.id} does not have a loaded user`)
  if (!academicEditor.user.identities)
    throw new Error(
      `Editor ${academicEditor.id} does not have loaded identities`,
    )

  const orcidIdentity = academicEditor.user.identities.find(
    identity => identity.type === 'orcid',
  )

  const academicEditorTemplate = {
    _attributes: { 'contrib-type': 'Academic Editor' },
    role: { _attributes: { 'content-type': '1' } },
    name: {
      surname: { _text: academicEditor.alias.surname },
      'given-names': { _text: academicEditor.alias.givenNames },
      prefix: { _text: academicEditor.alias.title },
    },
    email: { _text: academicEditor.alias.email },
    xref: {
      _attributes: {
        'ref-type': 'aff',
        rid: `I1`,
      },
      sup: {
        _text: 1,
      },
    },
  }

  if (orcidIdentity) {
    set(academicEditorTemplate, 'contrib-id', {
      _attributes: { 'contrib-id-type': 'orcid' },
      _text: `https://orcid.org/${orcidIdentity.identifier}`,
    })
  }

  const setAcademicEditor = set(
    {},
    'article.front.article-meta.contrib-group.contrib',
    [academicEditorTemplate],
  )

  const academicEditorAffiliationTemplate = {
    _attributes: {
      id: `I1`,
    },
    sup: {
      _text: 1,
    },
    'addr-line': { _text: academicEditor.alias.aff || '' },
    country: academicEditor.alias.country || 'UK',
  }

  const setAcademicEditorAffiliation = set(
    {},
    'article.front.article-meta.aff',
    [academicEditorAffiliationTemplate],
  )

  return merge(setAcademicEditor, setAcademicEditorAffiliation)
}

module.exports = {
  setAcademicEditorContributor,
}
