const { set, merge } = require('lodash')

const setAuthorContributors = ({ authors }) => {
  const authorsTemplate = authors.map((author, i) => {
    if (!author.user)
      throw new Error(`Author ${author.id} does not have a loaded user`)
    if (!author.user.identities)
      throw new Error(`Author ${author.id} does not have loaded identities`)

    const orcidIdentity = author.user.identities.find(
      identity => identity.type === 'orcid',
    )

    const authorTemplate = {
      _attributes: {
        'contrib-type': 'Author',
        corresp: author.isCorresponding ? 'Yes' : 'No',
        submitting: author.isSubmitting ? 'Yes' : 'No',
      },
      role: { _attributes: { 'content-type': '1' } },
      name: {
        surname: { _text: author.alias.surname },
        'given-names': { _text: author.alias.givenNames },
        prefix: { _text: author.alias.title },
      },
      email: { _text: author.alias.email },
      xref: {
        _attributes: {
          'ref-type': 'aff',
          rid: `I${i + 2}`,
        },
        sup: { _text: i + 2 },
      },
    }

    if (orcidIdentity) {
      set(authorTemplate, 'contrib-id', {
        _attributes: { 'contrib-id-type': 'orcid' },
        _text: `https://orcid.org/${orcidIdentity.identifier}`,
      })
    }

    const authorAffiliationTemplate = {
      _attributes: { id: `I${i + 2}` },
      sup: { _text: i + 2 },
      'addr-line': { _text: author.alias.aff || '' },
      country: author.alias.country || 'UK',
    }

    return { authorTemplate, authorAffiliationTemplate }
  })

  const setAuthors = set(
    {},
    'article.front.article-meta.contrib-group.contrib',
    authorsTemplate.map(template => template.authorTemplate),
  )

  const setAuthorAffiliationsTemplate = set(
    {},
    'article.front.article-meta.aff',
    authorsTemplate.map(template => template.authorAffiliationTemplate),
  )

  return merge(setAuthors, setAuthorAffiliationsTemplate)
}

module.exports = {
  setAuthorContributors,
}
