const { set } = require('lodash')

const setVersion = ({ version }) => {
  const versionTemplate = {
    _text: version,
  }

  return set({}, 'article.front.article-meta.article-version', versionTemplate)
}

module.exports = {
  setVersion,
}
