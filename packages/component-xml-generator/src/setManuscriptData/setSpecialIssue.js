const { set } = require('lodash')

const setSpecialIssue = ({ specialIssueId, specialIssueName }) => {
  if (!specialIssueName || !specialIssueId) return {}

  const specialIssueTemplate = {
    'special-issue-id': {
      _text: specialIssueId,
    },
    'special-issue-title': {
      _text: specialIssueName,
    },
  }

  return set({}, 'article.front.article-meta', specialIssueTemplate)
}

module.exports = {
  setSpecialIssue,
}
