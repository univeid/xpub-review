const { set } = require('lodash')

const setArticleType = ({ articleTypeName }) => {
  const articleTypeTemplate = {
    'subj-group': [
      {
        _attributes: {
          'subj-group-type': 'heading',
        },
        subject: {
          _text: articleTypeName,
        },
      },
    ],
  }

  return set(
    {},
    'article.front.article-meta.article-categories',
    articleTypeTemplate,
  )
}

module.exports = {
  setArticleType,
}
