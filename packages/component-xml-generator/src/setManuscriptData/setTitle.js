const { set } = require('lodash')

const setTitle = ({ title }) => {
  const titleTemplate = {
    'article-title': {
      _text: title,
    },
  }

  return set({}, 'article.front.article-meta.title-group', titleTemplate)
}

module.exports = {
  setTitle,
}
