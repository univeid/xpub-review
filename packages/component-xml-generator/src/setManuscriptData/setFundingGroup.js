const { set } = require('lodash')

const setFundingGroup = () =>
  set({}, 'article.front.article-meta.funding-group', [])

module.exports = {
  setFundingGroup,
}
