const { setReviews } = require('./setReviews')
const { mergeWith } = require('lodash')

const setRevGroup = ({ reviews, TeamRoles, customizer }) => {
  if (!reviews) return

  const {
    adminReviews,
    authorReviews,
    reviewerReviews,
    triageEditorReviews,
    academicEditorReviews,
    editorialAssistantReviews,
  } = reviews

  const xmlRoles = {
    admin: 'admin',
    author: 'author',
    reviewer: 'reviewer',
    academicEditor: 'editor',
    triageEditor: 'triageEditor',
    editorialAssistant: 'editorialAssistant',
  }

  let affStartingPosition = 0

  const reviewerReviewsTemplate = setReviews({
    revType: xmlRoles[TeamRoles.reviewer],
    reviews: reviewerReviews,
    affStartingPosition,
  })
  affStartingPosition += reviewerReviews.length

  const authorReviewsTemplate = setReviews({
    revType: xmlRoles[TeamRoles.author],
    reviews: authorReviews,
    affStartingPosition,
  })
  affStartingPosition += authorReviews.length

  const academicEditorReviewsTemplate = setReviews({
    revType: xmlRoles[TeamRoles.academicEditor],
    reviews: academicEditorReviews,
    affStartingPosition,
  })
  affStartingPosition += academicEditorReviews.length

  const triageEditorReviewsTemplate = setReviews({
    revType: xmlRoles[TeamRoles.triageEditor],
    reviews: triageEditorReviews,
    affStartingPosition,
  })
  affStartingPosition += triageEditorReviews.length

  const adminReviewsTemplate = setReviews({
    revType: xmlRoles[TeamRoles.admin],
    reviews: adminReviews,
    affStartingPosition,
  })
  affStartingPosition += adminReviews.length

  const editorialAssistantReviewsTemplate = setReviews({
    revType: xmlRoles[TeamRoles.editorialAssistant],
    reviews: editorialAssistantReviews,
    affStartingPosition,
  })
  affStartingPosition += editorialAssistantReviews.length

  return mergeWith(
    adminReviewsTemplate,
    authorReviewsTemplate,
    reviewerReviewsTemplate,
    triageEditorReviewsTemplate,
    academicEditorReviewsTemplate,
    editorialAssistantReviewsTemplate,
    customizer,
  )
}

module.exports = {
  setRevGroup,
}
