const { mergeWith } = require('lodash')

const { setScreenerContacts } = require('./setContacts/setScreenerContacts')
const {
  setEditorialAssistantContact,
} = require('./setContacts/setEditorialAssistantContact')
const {
  setCorrespondingEditorialAssistantContact,
} = require('./setContacts/setCorrespondingEditorialAssistantContact')

const setContacts = ({
  screeners,
  customizer,
  editorialAssistant,
  correspondingEditorialAssistant,
}) => {
  const correspondingEditorialAssistantTemplate = setCorrespondingEditorialAssistantContact(
    { correspondingEditorialAssistant },
  )

  const editorialAssistantTemplate = setEditorialAssistantContact({
    editorialAssistant,
  })

  const screenersTemplate = setScreenerContacts({ screeners })

  return mergeWith(
    correspondingEditorialAssistantTemplate,
    editorialAssistantTemplate,
    screenersTemplate,
    customizer, // we need this to merge the contacts array within objects
  )
}

module.exports = {
  setContacts,
}
