const { set } = require('lodash')

const setJournalMeta = journal => {
  if (!journal) throw new Error('Journal is needed to set journal meta.')

  const journalMeta = {
    'journal-id': [
      {
        _attributes: {
          'journal-id-type': 'publisher',
        },
        _text: journal.code,
      },
      {
        _attributes: {
          'journal-id-type': 'email',
        },
        _text: journal.email,
      },
    ],
    'journal-subcode': {
      _text: journal.code,
    },
    'journal-title-group': {
      'journal-title': {
        _text: journal.name,
      },
    },
    issn: [
      {
        _attributes: {
          'pub-type': 'epub',
        },
        _text: journal.issn,
      },
    ],
    publisher: {
      'publisher-name': {
        _text: 'Hindawi',
      },
    },
  }

  return set({}, 'article.front.journal-meta', journalMeta)
}

module.exports = {
  setJournalMeta,
}
