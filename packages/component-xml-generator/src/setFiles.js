const { set } = require('lodash')

const setFiles = files => {
  if (!files.length) return {}
  const templateFiles = files.map(file => ({
    item_type: {
      _text: file.type,
    },
    item_description: {
      _text: file.originalName,
    },
    item_name: {
      _text: file.fileName,
    },
  }))

  return set({}, 'article.front.files.file', templateFiles)
}

module.exports = {
  setFiles,
}
