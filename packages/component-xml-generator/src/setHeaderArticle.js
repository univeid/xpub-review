const { set } = require('lodash')

const setHeaderArticle = articleType => {
  const articleTemplate = {
    _attributes: {
      'dtd-version': '1.1d1',
      'article-type': articleType,
    },
  }

  return set({}, 'article', articleTemplate)
}

module.exports = {
  setHeaderArticle,
}
