const { merge, isArray } = require('lodash')
const convert = require('xml-js')

const { setFiles } = require('./setFiles')
const { setHeaderArticle } = require('./setHeaderArticle')
const { setHeader } = require('./setHeader')
const { setRevGroup } = require('./setRevGroup')
const { setContacts } = require('./setContacts')
const { setQuestions } = require('./setQuestions')
const { setJournalMeta } = require('./setJournalMeta')
const { setManuscriptData } = require('./setManuscriptData')

const customizer = (objValue, srcValue) => {
  if (isArray(objValue)) return objValue.concat(srcValue)
}

const generateXML = async ({
  authors,
  reviews,
  TeamRoles,
  screeners,
  manuscript,
  figureFiles,
  academicEditor,
  editorialAssistant,
  correspondingEditorialAssistant,
}) => {
  const xmlHeader = setHeader()
  const headerArticle = setHeaderArticle(manuscript.articleType.name)
  const manuscriptData = setManuscriptData({
    authors,
    manuscript,
    customizer,
    figureFiles,
    academicEditor,
  })
  const files = setFiles(manuscript.files)
  const questions = setQuestions(manuscript)
  const journalMeta = setJournalMeta(manuscript.journal)
  const revGroup = setRevGroup({
    reviews,
    TeamRoles,
    customizer,
    manuscript,
  })

  const contacts = setContacts({
    screeners,
    customizer,
    editorialAssistant,
    correspondingEditorialAssistant,
  })

  const templateObject = merge(
    xmlHeader,
    headerArticle,
    journalMeta,
    manuscriptData,
    files,
    questions,
    revGroup,
    contacts,
  )

  return convert.json2xml(templateObject, {
    compact: true,
    ignoreComment: true,
    spaces: 2,
    fullTagEmptyElement: true,
  })
}

module.exports = {
  generateXML,
}
