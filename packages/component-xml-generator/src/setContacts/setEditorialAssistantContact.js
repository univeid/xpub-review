const { set } = require('lodash')

const setEditorialAssistantContact = ({ editorialAssistant }) => {
  if (!editorialAssistant) return {}

  const editorialAssistantTemplate = {
    _attributes: { role: 'EA' },
    name: {
      surname: { _text: editorialAssistant.alias.surname },
      'given-names': { _text: editorialAssistant.alias.givenNames },
    },
    email: { _text: editorialAssistant.alias.email },
  }

  return set({}, 'article.front.contacts.contact-person', [
    editorialAssistantTemplate,
  ])
}

module.exports = {
  setEditorialAssistantContact,
}
