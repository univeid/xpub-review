const { set } = require('lodash')

const setCorrespondingEditorialAssistantContact = ({
  correspondingEditorialAssistant,
}) => {
  if (!correspondingEditorialAssistant) return {}

  const editorialAssistantTemplate = {
    _attributes: { role: 'EALeader' },
    name: {
      surname: { _text: correspondingEditorialAssistant.alias.surname },
      'given-names': {
        _text: correspondingEditorialAssistant.alias.givenNames,
      },
    },
    email: { _text: correspondingEditorialAssistant.alias.email },
  }

  return set({}, 'article.front.contacts.contact-person', [
    editorialAssistantTemplate,
  ])
}

module.exports = {
  setCorrespondingEditorialAssistantContact,
}
