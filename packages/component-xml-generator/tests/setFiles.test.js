const { generateFile } = require('component-generators')
const { setFiles } = require('../src/setFiles')

describe('setFiles setter', () => {
  it('returns an empty object if there are no files', () => {
    const files = []

    const res = setFiles(files)

    expect(res).toEqual({})
  })
  it('returns a correct object', () => {
    const file = generateFile({
      type: 'file-type',
      fileName: 'some-file-name',
      originalName: 'some-original-file-name',
    })
    const files = [file]

    const res = setFiles(files)

    expect(res.article.front.files.file).toEqual([
      {
        item_type: {
          _text: file.type,
        },
        item_description: {
          _text: file.originalName,
        },
        item_name: {
          _text: file.fileName,
        },
      },
    ])
  })
})
