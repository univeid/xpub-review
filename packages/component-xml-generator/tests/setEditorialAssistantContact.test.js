const { generateTeamMember } = require('component-generators')
const {
  setEditorialAssistantContact,
} = require('../src/setContacts/setEditorialAssistantContact')

describe('setEditorialAssistantContact setter', () => {
  it('returns an empty object if there is no editorial assistant', () => {
    const editorialAssistant = undefined

    const res = setEditorialAssistantContact({
      editorialAssistant,
    })

    expect(res).toEqual({})
  })
  it('returns a correct object', () => {
    const editorialAssistant = generateTeamMember()

    const res = setEditorialAssistantContact({
      editorialAssistant,
    })

    expect(res.article.front.contacts['contact-person']).toEqual([
      {
        _attributes: { role: 'EA' },
        name: {
          surname: { _text: editorialAssistant.alias.surname },
          'given-names': {
            _text: editorialAssistant.alias.givenNames,
          },
        },
        email: { _text: editorialAssistant.alias.email },
      },
    ])
  })
})
