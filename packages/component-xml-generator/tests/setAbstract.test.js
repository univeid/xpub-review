const { setAbstract } = require('../src/setManuscriptData/setAbstract')

describe('setAbstract setter', () => {
  it('returns a correct object', () => {
    const abstract = 'some-abstract'

    const res = setAbstract({ abstract })

    expect(res.article.front['article-meta'].abstract).toEqual({
      _text: abstract,
    })
  })
})
