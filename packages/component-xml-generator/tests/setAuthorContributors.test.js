const { generateTeamMember, generateIdentity } = require('component-generators')
const {
  setAuthorContributors,
} = require('../src/setManuscriptData/setContributors/setAuthorContributors')

describe('setAuthorContributors setter', () => {
  it('throws an error if the user is not loaded', () => {
    const author = generateTeamMember({ user: undefined })

    try {
      setAuthorContributors({ authors: [author] })
    } catch (err) {
      expect(err.message).toBe(
        `Author ${author.id} does not have a loaded user`,
      )
    }
  })
  it('throws an error if the user identities are not loaded', () => {
    const author = generateTeamMember()

    try {
      setAuthorContributors({ authors: [author] })
    } catch (err) {
      expect(err.message).toBe(
        `Author ${author.id} does not have loaded identities`,
      )
    }
  })
  it('returns a correct object if the user has no orcid identity', () => {
    const author = generateTeamMember({ user: { identities: [] } })

    const res = setAuthorContributors({ authors: [author] })

    expect(res.article.front['article-meta']['contrib-group'].contrib).toEqual([
      {
        _attributes: {
          'contrib-type': 'Author',
          corresp: author.isCorresponding ? 'Yes' : 'No',
          submitting: author.isSubmitting ? 'Yes' : 'No',
        },
        role: { _attributes: { 'content-type': '1' } },
        name: {
          surname: { _text: author.alias.surname },
          'given-names': { _text: author.alias.givenNames },
          prefix: { _text: author.alias.title },
        },
        email: { _text: author.alias.email },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I2`,
          },
          sup: {
            _text: 2,
          },
        },
      },
    ])

    expect(res.article.front['article-meta'].aff).toEqual([
      {
        _attributes: {
          id: `I2`,
        },
        sup: {
          _text: 2,
        },
        'addr-line': { _text: author.alias.aff || '' },
        country: author.alias.country || 'UK',
      },
    ])
  })
  it('returns a correct object', () => {
    const identity = generateIdentity({ type: 'orcid', identifier: 'klaus' })
    const author = generateTeamMember({
      user: { identities: [identity] },
    })

    const res = setAuthorContributors({ authors: [author] })

    expect(res.article.front['article-meta']['contrib-group'].contrib).toEqual([
      {
        _attributes: {
          'contrib-type': 'Author',
          corresp: author.isCorresponding ? 'Yes' : 'No',
          submitting: author.isSubmitting ? 'Yes' : 'No',
        },
        'contrib-id': {
          _attributes: { 'contrib-id-type': 'orcid' },
          _text: `https://orcid.org/${identity.identifier}`,
        },
        role: { _attributes: { 'content-type': '1' } },
        name: {
          surname: { _text: author.alias.surname },
          'given-names': { _text: author.alias.givenNames },
          prefix: { _text: author.alias.title },
        },
        email: { _text: author.alias.email },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I2`,
          },
          sup: {
            _text: 2,
          },
        },
      },
    ])

    expect(res.article.front['article-meta'].aff).toEqual([
      {
        _attributes: {
          id: `I2`,
        },
        sup: {
          _text: 2,
        },
        'addr-line': { _text: author.alias.aff || '' },
        country: author.alias.country || 'UK',
      },
    ])
  })
})
