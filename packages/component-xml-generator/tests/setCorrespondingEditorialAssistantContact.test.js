const { generateTeamMember } = require('component-generators')
const {
  setCorrespondingEditorialAssistantContact,
} = require('../src/setContacts/setCorrespondingEditorialAssistantContact')

describe('setCorrespondingEditorialAssistantContact setter', () => {
  it('returns an empty object if there is no editorial assistant', () => {
    const correspondingEditorialAssistant = undefined

    const res = setCorrespondingEditorialAssistantContact({
      correspondingEditorialAssistant,
    })

    expect(res).toEqual({})
  })
  it('returns a correct object', () => {
    const correspondingEditorialAssistant = generateTeamMember()

    const res = setCorrespondingEditorialAssistantContact({
      correspondingEditorialAssistant,
    })

    expect(res.article.front.contacts['contact-person']).toEqual([
      {
        _attributes: { role: 'EALeader' },
        name: {
          surname: { _text: correspondingEditorialAssistant.alias.surname },
          'given-names': {
            _text: correspondingEditorialAssistant.alias.givenNames,
          },
        },
        email: { _text: correspondingEditorialAssistant.alias.email },
      },
    ])
  })
})
