const { generateTeamMember, generateIdentity } = require('component-generators')
const {
  setAcademicEditorContributor,
} = require('../src/setManuscriptData/setContributors/setAcademicEditorContributor')

describe('setAcademicEditorContributor setter', () => {
  it('throws an error if the user is not loaded', () => {
    const academicEditor = generateTeamMember({ user: undefined })

    try {
      setAcademicEditorContributor(academicEditor)
    } catch (err) {
      expect(err.message).toBe(
        `Editor ${academicEditor.id} does not have a loaded user`,
      )
    }
  })
  it('throws an error if the user identities are not loaded', () => {
    const academicEditor = generateTeamMember()

    try {
      setAcademicEditorContributor(academicEditor)
    } catch (err) {
      expect(err.message).toBe(
        `Editor ${academicEditor.id} does not have loaded identities`,
      )
    }
  })
  it('returns a correct object if the user has no orcid identity', () => {
    const academicEditor = generateTeamMember({ user: { identities: [] } })

    const res = setAcademicEditorContributor(academicEditor)

    expect(res.article.front['article-meta']['contrib-group'].contrib).toEqual([
      {
        _attributes: { 'contrib-type': 'Academic Editor' },
        role: { _attributes: { 'content-type': '1' } },
        name: {
          surname: { _text: academicEditor.alias.surname },
          'given-names': { _text: academicEditor.alias.givenNames },
          prefix: { _text: academicEditor.alias.title },
        },
        email: { _text: academicEditor.alias.email },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I1`,
          },
          sup: {
            _text: 1,
          },
        },
      },
    ])

    expect(res.article.front['article-meta'].aff).toEqual([
      {
        _attributes: {
          id: `I1`,
        },
        sup: {
          _text: 1,
        },
        'addr-line': { _text: academicEditor.alias.aff || '' },
        country: academicEditor.alias.country || 'UK',
      },
    ])
  })
  it('returns a correct object', () => {
    const identity = generateIdentity({ type: 'orcid', identifier: 'klaus' })
    const academicEditor = generateTeamMember({
      user: { identities: [identity] },
    })

    const res = setAcademicEditorContributor(academicEditor)

    expect(res.article.front['article-meta']['contrib-group'].contrib).toEqual([
      {
        _attributes: { 'contrib-type': 'Academic Editor' },
        'contrib-id': {
          _attributes: { 'contrib-id-type': 'orcid' },
          _text: `https://orcid.org/${identity.identifier}`,
        },
        role: { _attributes: { 'content-type': '1' } },
        name: {
          surname: { _text: academicEditor.alias.surname },
          'given-names': { _text: academicEditor.alias.givenNames },
          prefix: { _text: academicEditor.alias.title },
        },
        email: { _text: academicEditor.alias.email },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I1`,
          },
          sup: {
            _text: 1,
          },
        },
      },
    ])

    expect(res.article.front['article-meta'].aff).toEqual([
      {
        _attributes: {
          id: `I1`,
        },
        sup: {
          _text: 1,
        },
        'addr-line': { _text: academicEditor.alias.aff || '' },
        country: academicEditor.alias.country || 'UK',
      },
    ])
  })
})
