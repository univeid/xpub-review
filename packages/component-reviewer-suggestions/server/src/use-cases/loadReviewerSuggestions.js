const { get, chain, last, initial, isEmpty } = require('lodash')
const Promise = require('bluebird')

const initialize = ({
  models: { Manuscript, ReviewerSuggestion },
  reviewerSuggestionsService,
}) => ({
  async execute(manuscriptId) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[journal,teams.members]',
    )

    const wrongStatuses = [
      Manuscript.Statuses.draft,
      Manuscript.Statuses.technicalChecks,
    ]
    if (wrongStatuses.includes(manuscript.status)) return []

    const localReviewerSuggestions = await ReviewerSuggestion.findBy({
      manuscriptId: manuscript.id,
    })
    if (!isEmpty(localReviewerSuggestions))
      return localReviewerSuggestions.map(suggestion => suggestion.toDTO())

    if (!manuscript.journal) throw new Error('Journal is required.')
    if (!manuscript.teams) throw new Error('Teams are required.')

    const reviewersData = await reviewerSuggestionsService.getReviewerSuggestions(
      manuscript,
    )

    const reviewers = parseReviewerSuggestions(reviewersData)

    const reviewerSuggestions = []

    await Promise.each(reviewers, async reviewer => {
      const newReviewer = new ReviewerSuggestion(reviewer)
      await newReviewer.save()
      reviewerSuggestions.push(newReviewer)
    })

    return reviewerSuggestions.map(suggestion => suggestion.toDTO())
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isAcademicEditorOnManuscript',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}

function parseReviewerSuggestions(data) {
  return chain(data)
    .filter(rev => rev.profileUrl && get(rev, 'contact.emails.length', 0) > 0)
    .map(rev => ({
      email: process.env.PUBLONS_MOCK_EMAIL
        ? process.env.PUBLONS_MOCK_EMAIL.replace(
            '__NAME__',
            `${rev.contact.emails[0].email.split('@')[0]}`,
          )
        : rev.contact.emails[0].email,
      givenNames: initial(rev.publishingName.split(' ')).join(' '),
      surname: last(rev.publishingName.split(' ')),
      profileUrl: rev.profileUrl,
      numberOfReviews: rev.numVerifiedReviews,
      aff: get(rev, 'recentOrganizations[0].name', 'Non-affiliated'),
      manuscriptId: rev.manuscriptId,
      type: rev.type,
    }))
    .uniqBy('email')
    .value()
}
