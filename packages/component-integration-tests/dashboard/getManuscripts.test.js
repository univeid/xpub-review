const models = require('@pubsweet/models')
const { db } = require('@pubsweet/db-manager')
const { uniq } = require('lodash')
const { getManuscriptsUseCase } = require('component-dashboard')

describe('get manuscripts use case', () => {
  const { Team, TeamMember, Manuscript, Journal } = models

  afterAll(done => {
    db.destroy()
    done()
  })

  it('returns all manuscripts if the user is admin', async () => {
    const { userId } = await TeamMember.findOneByRole({
      role: Team.Role.admin,
    })
    const { manuscripts } = await getManuscriptsUseCase
      .initialize(models)
      .execute({ input: {}, userId })

    const submissionIds = manuscripts.map(m => m.submissionId)
    expect(submissionIds.length).toEqual(uniq(submissionIds).length)

    const draftManuscript = manuscripts.find(
      m => m.status === Manuscript.Statuses.draft,
    )
    expect(draftManuscript.version).toEqual('1')
  })

  describe('as an admin', () => {
    let userId
    beforeAll(async () => {
      ;({ userId } = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      }))
    })
    it('should return the version 1 draft of a manuscript', async () => {
      const { Manuscript } = models
      const result = await getManuscriptsUseCase
        .initialize(models)
        .execute({ input: {}, userId })

      const draftManuscript = result.manuscripts.find(
        m => m.version === '1' && m.status === Manuscript.Statuses.draft,
      )
      expect(draftManuscript).toBeDefined()
    })
    it('should not return the version 2 draft of a manuscript', async () => {
      const { Manuscript } = models
      const result = await getManuscriptsUseCase
        .initialize(models)
        .execute({ input: {}, userId })

      const draftManuscript = result.manuscripts.find(
        m => m.version !== '1' && m.status === Manuscript.Statuses.draft,
      )
      expect(draftManuscript).toBeUndefined()
    })
  })

  describe('as an editorial assistant', () => {
    let userId
    let journalId
    beforeAll(async () => {
      ;[{ id: journalId }] = await Journal.all()
      ;({ userId } = await TeamMember.findOneByJournalAndRole({
        journalId,
        role: Team.Role.editorialAssistant,
      }))
    })
    it('should not return draft manuscripts', async () => {
      const { Manuscript } = models
      const result = await getManuscriptsUseCase
        .initialize(models)
        .execute({ input: {}, userId })

      expect(
        result.manuscripts.every(m => m.status !== Manuscript.Statuses.draft),
      ).toBeTruthy()
    })
    it('should not see manuscripts from unassigned journals', async () => {
      const result = await getManuscriptsUseCase
        .initialize(models)
        .execute({ input: {}, userId })

      expect(
        result.manuscripts.every(m => m.journalId === journalId),
      ).toBeTruthy()
    })
  })

  describe('as a manuscript user', () => {
    it('should  return draft manuscripts if the user is author', async () => {
      const { Team, TeamMember, Manuscript } = models
      const { userId } = await TeamMember.findOneByRole({
        role: Team.Role.author,
      })
      const result = await getManuscriptsUseCase
        .initialize(models)
        .execute({ input: {}, userId })

      const draftManuscript = result.manuscripts.find(
        m => m.version === '1' && m.status === Manuscript.Statuses.draft,
      )

      expect(draftManuscript).toBeDefined()
    })
    it('should  not return manuscript if reviewer teamMember status is declined ', async () => {
      const { Manuscript, TeamMember, Team } = models
      const manuscripts = await Manuscript.all()
      const underReviewManuscript = manuscripts.find(
        manuscript => manuscript.status === Manuscript.Statuses.underReview,
      )
      const reviewerTeamMembers = await TeamMember.findAllByManuscriptAndRole({
        manuscriptId: underReviewManuscript.id,
        role: Team.Role.reviewer,
      })
      const declinedReviewer = reviewerTeamMembers.find(
        teamMember => teamMember.status === TeamMember.Statuses.declined,
      )
      const result = await getManuscriptsUseCase
        .initialize(models)
        .execute({ input: {}, userId: declinedReviewer.userId })
      const manuscriptIds = result.manuscripts.map(manuscript => manuscript.id)
      expect(!manuscriptIds.includes(underReviewManuscript.id)).toBeTruthy()
    })
  })
})
