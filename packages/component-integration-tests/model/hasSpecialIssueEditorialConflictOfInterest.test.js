const models = require('@pubsweet/models')

const { groupBy } = require('lodash')
const { db } = require('@pubsweet/db-manager')

const {
  useCases: { getManuscriptHasSpecialIssueEditorialConflictOfInterestUseCase },
} = require('component-model')

describe('get manuscript hasSpecialIssueEditorialConflictOfInterest', () => {
  const { Manuscript } = models
  let manuscript = {}
  beforeAll(async () => {
    const manuscripts = await Manuscript.all()
    const groupedSubmissions = groupBy(manuscripts, 'submissionId')
    const submissionWithMostVersions = Object.values(groupedSubmissions).find(
      submission => submission.length > 1,
    )
    ;[manuscript] = submissionWithMostVersions
  })
  afterAll(done => {
    db.destroy()
    done()
  })

  it('returns false if the manuscript authors are not editors on the Special Issue', async () => {
    const hasCOI = await getManuscriptHasSpecialIssueEditorialConflictOfInterestUseCase
      .initialize(models)
      .execute({ manuscript })

    expect(hasCOI).toEqual(false)
  })
  it('returns true if the manuscript authors are editors on the Special Issue', async () => {
    const manuscript = await Manuscript.findOneBy({
      queryObject: { title: 'COI' },
    })

    const hasCOI = await getManuscriptHasSpecialIssueEditorialConflictOfInterestUseCase
      .initialize(models)
      .execute({ manuscript })

    expect(hasCOI).toEqual(true)
  })
})
