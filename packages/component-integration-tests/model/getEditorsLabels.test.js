const models = require('@pubsweet/models')

const { groupBy } = require('lodash')
const { db } = require('@pubsweet/db-manager')

const {
  useCases: { getManuscriptPeerReviewModelUseCase },
} = require('component-model')

describe('get manuscript editors labels', () => {
  const { Manuscript } = models
  let manuscript = {}
  beforeAll(async () => {
    const manuscripts = await Manuscript.all()
    const groupedSubmissions = groupBy(manuscripts, 'submissionId')
    const submissionWithManyVersions = Object.values(groupedSubmissions).find(
      submission => submission.length > 1,
    )
    ;[manuscript] = submissionWithManyVersions
  })
  afterAll(done => {
    db.destroy()
    done()
  })

  it('returns Academic Editor and Chief Editor when the PRM is Chief Minus', async () => {
    const peerReviewModel = await getManuscriptPeerReviewModelUseCase
      .initialize(models)
      .execute({ manuscript })

    expect(peerReviewModel).toHaveProperty('academicEditorLabel')
    expect(peerReviewModel).toHaveProperty('triageEditorLabel')

    expect(peerReviewModel.academicEditorLabel).toEqual('Academic Editor')
    expect(peerReviewModel.triageEditorLabel).toEqual('Chief Editor')
  })
  it('returns Academic Editor and Chief Editor when the manuscript is on a Special Issue but there is a conflict of interest', async () => {
    const manuscript = await Manuscript.findOneBy({
      queryObject: { title: 'COI' },
    })
    const peerReviewModel = await getManuscriptPeerReviewModelUseCase
      .initialize(models)
      .execute({ manuscript })

    expect(peerReviewModel.academicEditorLabel).toEqual('Academic Editor')
    expect(peerReviewModel.triageEditorLabel).toEqual('Chief Editor')
  })
})
