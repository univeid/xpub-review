const models = require('@pubsweet/models')
const db = require('@pubsweet/db-manager')

const {
  useCases: { getAcademicEditorWorkloadUseCase },
} = require('component-model')

describe('get academic editor workload', () => {
  afterAll(done => {
    db.destroy()
    done()
  })

  it('returns correct workload', async () => {
    const { Team, TeamMember } = models
    const academicEditor = await TeamMember.findOneByRole({
      role: Team.Role.academicEditor,
    })

    const workload = await getAcademicEditorWorkloadUseCase
      .initialize(models)
      .execute(academicEditor)

    expect(Number(workload)).toBeGreaterThan(0)
  })
})
