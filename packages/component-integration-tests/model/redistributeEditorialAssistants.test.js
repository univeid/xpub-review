const models = require('@pubsweet/models')
const { db } = require('@pubsweet/db-manager')

const {
  useCases: { redistributeEditorialAssistantsUseCase },
} = require('component-model')

describe('Redistribute editorial assistant', () => {
  const { Journal } = models
  let journalId

  beforeAll(async () => {
    const journals = await Journal.all()
    journalId = journals[0].id
  })

  afterAll(done => {
    db.destroy()
    done()
  })

  it('returns false if the manuscript authors are not editors on the Special Issue', async () => {
    await redistributeEditorialAssistantsUseCase
      .initialize({
        models,
        transaction: jest.fn(),
        logger: {
          info: jest.fn(),
          warn: jest.fn(),
          error: jest.fn(),
        },
      })
      .execute(journalId)

    expect(true).toBe(false)
  })
})
