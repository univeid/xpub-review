const models = require('@pubsweet/models')

const { groupBy } = require('lodash')

const {
  useCases: { getSubmissionUseCase },
} = require('component-model')

const { db } = require('@pubsweet/db-manager')

describe('get submission use case', () => {
  const { Team, TeamMember, Manuscript } = models
  let manuscript = {}

  beforeAll(async () => {
    const manuscripts = await Manuscript.all()
    const groupedSubmissions = groupBy(manuscripts, 'submissionId')
    const submissionWithMostVersions = Object.values(groupedSubmissions).find(
      submission => submission.length > 1,
    )
    ;[manuscript] = submissionWithMostVersions
  })
  afterAll(done => {
    db.destroy()
    done()
  })
  it('some test', () => {
    const vale = true
    expect(vale).toBeTruthy()
  })
  it('returns all versions of a manuscript when the user is an admin', async () => {
    const { submissionId } = manuscript

    const { userId } = await TeamMember.findOneByRole({
      role: Team.Role.admin,
    })

    const submission = await getSubmissionUseCase
      .initialize(models)
      .execute({ submissionId, userId })

    expect(submission.length).toBeGreaterThan(1)
    expect(submission[0].role).toEqual(Team.Role.admin)
  })

  it('returns all versions of a manuscript if the Editorial Assistant is assigned on the manuscript', async () => {
    const { id: manuscriptId, submissionId } = manuscript
    const { userId } = await TeamMember.findOneByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.editorialAssistant,
    })

    const submission = await getSubmissionUseCase
      .initialize(models)
      .execute({ submissionId, userId })

    expect(submission[0].role).toEqual(Team.Role.editorialAssistant)
  })
})
