const { db } = require('@pubsweet/db-manager')
const models = require('@pubsweet/models')

const useCases = require('component-peer-review')

const notificationService = {
  notifyEditorialAssistant: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}
const jobsService = {
  cancelJobs: jest.fn(),
}
const logEvent = () => jest.fn()
logEvent.actions = {
  academic_editor_assigned: '',
}
logEvent.objectType = { user: 'user' }

describe('Peer Review component', () => {
  const { Manuscript, TeamMember, Team } = models
  afterAll(done => {
    db.destroy()
    done()
  })
  describe('assign Academic Editor', () => {
    it('should throw error if the manuscript authors are editors on the Special Issue', async () => {
      const manuscript = await Manuscript.findOneBy({
        queryObject: { title: 'COI' },
      })
      const academicEditorTeamMembers = await TeamMember.findAllBySpecialIssueAndRole(
        {
          specialIssueId: manuscript.specialIssueId,
          role: Team.Role.academicEditor,
        },
      )

      try {
        await useCases.assignAcademicEditorUseCase
          .initialize({
            models,
            useCases,
            logEvent,
            jobsService,
            eventsService,
            notificationService,
          })
          .execute({
            submissionId: manuscript.submissionId,
            userId: academicEditorTeamMembers[0].userId,
          })
      } catch (e) {
        expect(e.message).toEqual('User cannot be assigned.')
      }
      const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndUser(
        {
          manuscriptId: manuscript.id,
          role: Team.Role.academicEditor,
          userId: academicEditorTeamMembers[0].userId,
        },
      )
      expect(academicEditor).toBeFalsy()
    })
    it('should add academicEditor to team if the manuscript authors are not editors on the Special Issue', async () => {
      const manuscript = await Manuscript.findOneBy({
        queryObject: { title: 'on SI' },
      })
      const academicEditorTeamMembers = await TeamMember.findAllBySpecialIssueAndRole(
        {
          specialIssueId: manuscript.specialIssueId,
          role: Team.Role.academicEditor,
        },
      )

      await useCases.assignAcademicEditorUseCase
        .initialize({
          models,
          useCases,
          logEvent,
          jobsService,
          eventsService,
          notificationService,
        })
        .execute({
          submissionId: manuscript.submissionId,
          userId: academicEditorTeamMembers[0].userId,
        })
      const updatedManuscript = await Manuscript.findOneBy({
        queryObject: { title: 'on SI' },
      })
      const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndUser(
        {
          manuscriptId: updatedManuscript.id,
          role: Team.Role.academicEditor,
          userId: academicEditorTeamMembers[0].userId,
        },
      )
      expect(updatedManuscript.status).toEqual(
        Manuscript.Statuses.academicEditorAssigned,
      )
      expect(academicEditor.status).toEqual(TeamMember.Statuses.accepted)
    })
  })
})
