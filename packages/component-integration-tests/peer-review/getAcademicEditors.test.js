const models = require('@pubsweet/models')
const { db } = require('@pubsweet/db-manager')

const { getAcademicEditorsUseCase } = require('component-peer-review')

describe('get academic editors use case', () => {
  const { Manuscript, TeamMember, Team } = models

  afterAll(done => {
    db.destroy()
    done()
  })

  it('returns academic editors from parent journal if has editorial conflicts', async () => {
    const manuscript = await Manuscript.findOneBy({
      queryObject: { title: 'COI' },
    })

    const { id: manuscriptId } = manuscript
    const searchValue = ''

    const academicEditorTeamMembers = await TeamMember.findAllBySpecialIssueAndRole(
      {
        specialIssueId: manuscript.specialIssueId,
        role: Team.Role.academicEditor,
      },
    )
    const result = await getAcademicEditorsUseCase
      .initialize(models)
      .execute({ manuscriptId, searchValue })

    expect(academicEditorTeamMembers.includes(result[0])).toBeFalsy()
    expect(result.length).toBeGreaterThan(0)
  })
})
