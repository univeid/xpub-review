const { db } = require('@pubsweet/db-manager')
const models = require('@pubsweet/models')
const { transaction } = require('objection')

const { authorAndAcademicEditor } = require('component-quality-check')
const useCases = require('component-quality-check')

const notificationService = {
  notifyTriageEditorAboutEditorReplacement: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}
const logEvent = jest.fn(async () => {})
logEvent.actions = {
  quality_checks_coi_author_academic_editor:
    'quality_checks_coi_author_academic_editor',
}
logEvent.objectType = { manuscript: 'manuscript' }

describe('Handle COI use case', () => {
  const { Manuscript, Team, Review, TeamMember } = models
  afterAll(done => {
    db.destroy()
    done()
  })

  it('should set the makeDecision status when the approval editor is academic editor', async () => {
    const manuscript = await Manuscript.findOneBy({
      queryObject: { title: 'Author Academic Editor COI' },
    })
    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.accepted,
        role: Team.Role.academicEditor,
      },
    )
    const review = await Review.findOneByManuscriptAndTeamMember({
      manuscriptId: manuscript.id,
      teamMemberId: academicEditor.id,
    })

    await authorAndAcademicEditor
      .initialize({
        models,
        logEvent,
        useCases,
        transaction,
        eventsService,
        notificationService,
      })
      .execute({
        academicEditorId: academicEditor.id,
        submissionId: manuscript.submissionId,
      })

    const updatedManuscript = await Manuscript.findOneBy({
      queryObject: { title: 'Author Academic Editor COI' },
    })
    const updatedAcademicEditor = await TeamMember.find(academicEditor.id)
    const updatedReview = await Review.find(review.id)

    expect(updatedManuscript.status).toEqual(Manuscript.Statuses.makeDecision)
    expect(updatedAcademicEditor.status).toEqual(TeamMember.Statuses.removed)
    expect(updatedReview.isValid).toEqual(false)
    expect(logEvent).toHaveBeenCalledTimes(1)
  })
})
