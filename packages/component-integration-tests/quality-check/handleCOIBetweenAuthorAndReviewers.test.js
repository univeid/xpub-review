const { db } = require('@pubsweet/db-manager')
const models = require('@pubsweet/models')
const { transaction } = require('objection')

const { authorAndReviewers } = require('component-quality-check')

const notificationService = {
  notifyAcademicEditor: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}
const logEvent = jest.fn(async () => {})
logEvent.actions = {
  quality_checks_coi_author_reviewer: 'quality_checks_coi_author_reviewer',
}
logEvent.objectType = { manuscript: 'manuscript' }

describe('Handle COI use case', () => {
  const { Manuscript, Team, Review, TeamMember } = models
  afterAll(done => {
    db.destroy()
    done()
  })

  it('should set the reviewCompleted status when there are valid reviews', async () => {
    const manuscript = await Manuscript.findOneBy({
      queryObject: { title: 'Author Reviewer COI' },
    })
    const validReviews = await Review.findAllValidAndSubmittedByManuscriptAndRole(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
      },
    )
    await TeamMember.findOneByManuscriptAndRole({
      role: Team.Role.author,
      manuscriptId: manuscript.id,
    })
    const reviewerId = validReviews[0].teamMemberId
    const reviewId = validReviews[0].id

    await authorAndReviewers
      .initialize({
        models,
        logEvent,
        transaction,
        eventsService,
        notificationService,
      })
      .execute({
        reviewerIds: [reviewerId],
        submissionId: manuscript.submissionId,
      })

    const updatedManuscript = await Manuscript.findOneBy({
      queryObject: { title: 'Author Reviewer COI' },
    })
    const updatedReviewer = await TeamMember.find(reviewerId)
    const updatedReview = await Review.find(reviewId)

    expect(updatedManuscript.status).toEqual(
      Manuscript.Statuses.reviewCompleted,
    )
    expect(updatedReviewer.status).toEqual(TeamMember.Statuses.removed)
    expect(updatedReview.isValid).toEqual(false)
    expect(logEvent).toHaveBeenCalledTimes(1)
  })
})
