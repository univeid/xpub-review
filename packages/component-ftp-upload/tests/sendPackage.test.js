const { ProductionPackageManager } = require('../src/PackageManager')

describe('sendPackage', () => {
  let packageManager
  beforeEach(() => {
    packageManager = new ProductionPackageManager({
      journalCode: 'BCA',
      manuscriptCustomId: '123',
    })
  })
  it('Deletes local files if an error is thrown', async () => {
    Object.assign(packageManager, {
      downloadAllS3Files: jest.fn(),
      createMainArchive: jest.fn(),
      uploadFile: jest.fn(),
      deleteLocalArchives: jest.fn(),
    })

    try {
      await packageManager.sendPackage({})
    } catch (err) {
      expect(packageManager.deleteLocalArchives).toHaveBeenCalledTimes(1)
      return expect(err.message).toEqual('Failed to send package.')
    }

    expect('Test').toBe('failed')
  })
  it('Executes the main flow', async () => {
    Object.assign(packageManager, {
      downloadAllS3Files: jest.fn(),
      createMainArchive: jest.fn(),
      uploadFile: jest.fn(),
      deleteLocalArchives: jest.fn(),
    })
    const mainArchive = { name: 'BCA.zip' }

    jest.spyOn(packageManager, 'downloadAllS3Files').mockResolvedValue({})
    jest
      .spyOn(packageManager, 'createMainArchive')
      .mockResolvedValue({ mainArchive })

    await packageManager.sendPackage({})

    expect(packageManager.downloadAllS3Files).toHaveBeenCalledTimes(1)
    expect(packageManager.createMainArchive).toHaveBeenCalledTimes(1)
    expect(packageManager.uploadFile).toHaveBeenCalledTimes(1)
    expect(packageManager.deleteLocalArchives).toHaveBeenCalledTimes(1)
  })
})
