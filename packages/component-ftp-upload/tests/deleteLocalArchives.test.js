const { ProductionPackageManager } = require('../src/PackageManager')

describe('deleteLocalArchives', () => {
  let packageManager
  beforeEach(() => {
    packageManager = new ProductionPackageManager({
      journalCode: 'BCA',
      manuscriptCustomId: '123',
    })
  })
  it("Doesn't delete undefined archives", async () => {
    packageManager.deleteLocalArchive = jest.fn()

    await packageManager.deleteLocalArchives({
      mainArchive: undefined,
      figureFilesArchive: undefined,
      supplementalFilesArchive: undefined,
    })

    expect(packageManager.deleteLocalArchive).toHaveBeenCalledTimes(0)
  })
})
