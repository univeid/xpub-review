const { sendPackageToProduction } = require('./src/sendPackageToProduction')
const { sendSubmittedPackageToMTS } = require('./src/sendSubmittedPackageToMTS')
const { sendAcceptedPackageToMTS } = require('./src/sendAcceptedPackageToMTS')
const { sendRejectedPackageToMTS } = require('./src/sendRejectedPackageToMTS')

module.exports = {
  sendPackageToProduction,
  sendSubmittedPackageToMTS,
  sendAcceptedPackageToMTS,
  sendRejectedPackageToMTS,
}
