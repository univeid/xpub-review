# Component FTP Upload

This component gathers all the manuscript files and the .xml file, then creates a .zip archive and sends it to a specific FTP server with a configurable package name.
