/* eslint-disable no-console */
const AWS = require('aws-sdk')
const FtpDeploy = require('ftp-deploy')
const { promisify } = require('util')
const archiver = require('archiver')
const fs = require('fs')
const config = require('config')

const s3Config = config.get('pubsweet-component-aws-s3')
const productionFTPConfig = config.get('production-ftp.ftp')
const mtsFTPConfig = config.get('mts-service.ftp')

class PackageManager {
  constructor({
    journalCode,
    ftpConfig,
    remoteRoot,
    packageName,
    manuscriptCustomId,
  }) {
    this.journalCode = journalCode
    this.manuscriptCustomId = manuscriptCustomId
    this.ftpConfig = ftpConfig
    this.s3 = PackageManager.initializeS3(s3Config)
    this.remoteRoot = remoteRoot
    this.packageName = packageName
  }

  static initializeS3(config) {
    const s3 = new AWS.S3({
      secretAccessKey: config.secretAccessKey,
      accessKeyId: config.accessKeyId,
      region: config.region,
      params: {
        Bucket: config.bucket,
      },
      useAccelerateEndpoint: true,
    })
    const asyncGetObject = promisify(s3.getObject.bind(s3))
    const asyncUploadS3 = promisify(s3.upload.bind(s3))
    const deleteObjectsS3 = promisify(s3.deleteObjects.bind(s3))

    return { asyncGetObject, asyncUploadS3, deleteObjectsS3 }
  }

  async downloadAllS3Files({ mainFiles, supplementalFiles, figureFiles }) {
    const mainS3Files = await this.downloadS3Files(mainFiles)
    const supplementalS3Files = await this.downloadS3Files(supplementalFiles)
    const figureS3Files = await this.downloadS3Files(figureFiles)

    return {
      mainS3Files,
      figureS3Files,
      supplementalS3Files,
    }
  }

  downloadS3Files(files = []) {
    return Promise.all(
      files.map(async file => {
        const s3File = await this.s3.asyncGetObject({ Key: file.providerKey })
        return {
          content: s3File.Body,
          name: file.fileName,
        }
      }),
    )
  }

  async createMainArchive({
    xmlFile,
    mainS3Files,
    figureS3Files,
    supplementalS3Files,
  }) {
    const parsedXMLFile = {
      content: xmlFile,
      name: `${this.journalCode.toUpperCase()}-${this.manuscriptCustomId}.xml`,
    }

    const figureFilesArchive = await PackageManager.archiveFiles({
      files: figureS3Files,
      archiveName: 'figure.zip',
    })

    const supplementalFilesArchive = await PackageManager.archiveFiles({
      files: supplementalS3Files,
      archiveName: 'Supplementary Materials.zip',
    })

    const allFiles = [
      ...mainS3Files,
      parsedXMLFile,
      figureFilesArchive,
      supplementalFilesArchive,
    ]

    const mainArchive = await PackageManager.archiveFiles({
      files: allFiles.filter(file => file),
      archiveName: `${this.packageName}.zip`,
    })

    return {
      mainArchive,
      figureFilesArchive,
      supplementalFilesArchive,
    }
  }

  static async archiveFiles({ files, archiveName }) {
    if (!files.length) return

    const packageOutput = fs.createWriteStream(archiveName)
    const archive = archiver('zip')

    archive.pipe(packageOutput)

    files.forEach(file => {
      archive.append(file.content, { name: file.name })
    })

    archive.on('end', err => {
      if (err) {
        console.error('Archive failed to create')
        throw new Error(err)
      }
    })
    archive.on('close', () => {
      console.info(
        `Archive created ${archiveName} - ${archive.pointer()} bytes written to zip file`,
      )
    })

    await archive.finalize()

    return {
      content: fs.createReadStream(archiveName),
      name: archiveName,
    }
  }

  async deleteLocalArchives({
    mainArchive,
    figureFilesArchive,
    supplementalFilesArchive,
  }) {
    if (mainArchive)
      await this.deleteLocalArchive({
        archiveName: mainArchive.name,
      })

    if (figureFilesArchive)
      await this.deleteLocalArchive({
        archiveName: figureFilesArchive.name,
      })

    if (supplementalFilesArchive)
      await this.deleteLocalArchive({
        archiveName: supplementalFilesArchive.name,
      })
  }

  // eslint-disable-next-line class-methods-use-this
  async deleteLocalArchive({ archiveName }) {
    const access = promisify(fs.access)
    const unlink = promisify(fs.unlink)

    await access(archiveName, fs.constants.F_OK)
    await unlink(archiveName)

    console.info(`Deleted local file ${archiveName}`)
  }

  async uploadFile({ fileName }) {
    await this.uploadFileToS3({ fileName })
    await this.uploadFileToFTP({ fileName })
  }

  async uploadFileToS3({ fileName }) {
    const readFile = promisify(fs.readFile)

    const data = await readFile(fileName)
    const params = {
      Body: data,
      Key: `mts/${fileName}`,
    }

    await this.s3.asyncUploadS3(params)
    console.info(`Successfully uploaded ${fileName} to S3`)
  }

  async uploadFileToFTP({ fileName }) {
    const ftpDeploy = new FtpDeploy()
    const config = {
      ...this.ftpConfig,
      remoteRoot: this.remoteRoot,
      include: [fileName],
    }

    await ftpDeploy.deploy(config)
    console.info(`Successfully uploaded ${fileName} to FTP`)
  }

  async sendPackage({
    xmlFile,
    reviewFiles = [],
    figureFiles = [],
    manuscriptFiles = [],
    coverLetterFiles = [],
    supplementalFiles = [],
  }) {
    let mainArchive, figureFilesArchive, supplementalFilesArchive
    try {
      const {
        mainS3Files,
        figureS3Files,
        supplementalS3Files,
      } = await this.downloadAllS3Files({
        mainFiles: [...manuscriptFiles, ...coverLetterFiles, ...reviewFiles],
        figureFiles,
        supplementalFiles,
      })
      ;({
        mainArchive,
        figureFilesArchive,
        supplementalFilesArchive,
      } = await this.createMainArchive({
        xmlFile,
        mainS3Files,
        figureS3Files,
        supplementalS3Files,
      }))

      await this.uploadFile({ fileName: mainArchive.name })

      await this.deleteLocalArchives({
        mainArchive,
        figureFilesArchive,
        supplementalFilesArchive,
      })
    } catch (err) {
      console.error('Failed to send package.')

      await this.deleteLocalArchives({
        mainArchive,
        figureFilesArchive,
        supplementalFilesArchive,
      })

      throw new Error('Failed to send package.')
    }
  }
}

class ProductionPackageManager extends PackageManager {
  constructor({ journalCode, manuscriptCustomId }) {
    const packageName = `${journalCode.toUpperCase()}-${manuscriptCustomId}`
    const remoteRoot = `/${journalCode}`
    super({
      journalCode,
      packageName,
      remoteRoot,
      manuscriptCustomId,
      ftpConfig: productionFTPConfig,
    })
  }
}

class AcceptedPackageManager extends PackageManager {
  constructor({ journalCode, manuscriptVersion, manuscriptCustomId }) {
    const packageName = `${'ACCEPTED'}_${journalCode.toUpperCase()}-${manuscriptCustomId}.${manuscriptVersion}`
    const remoteRoot = `${`/${journalCode}`}/_Accepted`

    super({
      ftpConfig: mtsFTPConfig,
      remoteRoot,
      packageName,
      journalCode,
      manuscriptCustomId,
    })
  }
}

class RejectedPackageManager extends PackageManager {
  constructor({ journalCode, manuscriptVersion, manuscriptCustomId }) {
    const packageName = `${'REJECTED'}_${journalCode.toUpperCase()}-${manuscriptCustomId}.${manuscriptVersion}`
    const remoteRoot = `${`/${journalCode}`}/_Rejected`

    super({
      ftpConfig: mtsFTPConfig,
      remoteRoot,
      packageName,
      journalCode,
      manuscriptCustomId,
    })
  }
}

class SubmittedPackageManager extends PackageManager {
  constructor({ journalCode, manuscriptCustomId }) {
    const packageName = `${journalCode.toUpperCase()}-${manuscriptCustomId}`
    const remoteRoot = `/${journalCode}`

    super({
      ftpConfig: mtsFTPConfig,
      remoteRoot,
      packageName,
      journalCode,
      manuscriptCustomId,
    })
  }
}

module.exports = {
  ProductionPackageManager,
  AcceptedPackageManager,
  RejectedPackageManager,
  SubmittedPackageManager,
}
