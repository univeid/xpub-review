const { ProductionPackageManager } = require('./PackageManager')

module.exports = {
  async sendPackageToProduction({
    xmlFile,
    journalCode,
    reviewFiles,
    figureFiles,
    supplementalFiles,
    manuscriptFiles,
    coverLetterFiles,
    manuscriptCustomId,
  }) {
    const packageManager = new ProductionPackageManager({
      journalCode,
      manuscriptCustomId,
    })
    await packageManager.sendPackage({
      xmlFile,
      reviewFiles,
      figureFiles,
      manuscriptFiles,
      coverLetterFiles,
      supplementalFiles,
    })
  },
}
