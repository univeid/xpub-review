const { SubmittedPackageManager } = require('./PackageManager')

module.exports = {
  async sendSubmittedPackageToMTS({
    xmlFile,
    journalCode,
    figureFiles,
    reviewFiles,
    manuscriptFiles,
    coverLetterFiles,
    supplementalFiles,
    manuscriptCustomId,
  }) {
    const packageManager = new SubmittedPackageManager({
      journalCode,
      manuscriptCustomId,
    })
    await packageManager.sendPackage({
      xmlFile,
      reviewFiles,
      figureFiles,
      manuscriptFiles,
      coverLetterFiles,
      supplementalFiles,
    })
  },
}
