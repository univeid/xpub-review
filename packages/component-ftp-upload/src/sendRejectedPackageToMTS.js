const { RejectedPackageManager } = require('./PackageManager')

module.exports = {
  async sendRejectedPackageToMTS({
    xmlFile,
    journalCode,
    reviewFiles,
    figureFiles,
    supplementalFiles,
    manuscriptFiles,
    coverLetterFiles,
    manuscriptCustomId,
    manuscriptVersion,
  }) {
    const packageManager = new RejectedPackageManager({
      journalCode,
      manuscriptVersion,
      manuscriptCustomId,
    })
    await packageManager.sendPackage({
      xmlFile,
      reviewFiles,
      figureFiles,
      manuscriptFiles,
      coverLetterFiles,
      supplementalFiles,
    })
  },
}
