const { AcceptedPackageManager } = require('./PackageManager')

module.exports = {
  async sendAcceptedPackageToMTS({
    xmlFile,
    journalCode,
    reviewFiles,
    figureFiles,
    supplementalFiles,
    manuscriptFiles,
    coverLetterFiles,
    manuscriptCustomId,
    manuscriptVersion,
  }) {
    const packageManager = new AcceptedPackageManager({
      journalCode,
      manuscriptVersion,
      manuscriptCustomId,
    })
    await packageManager.sendPackage({
      xmlFile,
      reviewFiles,
      figureFiles,
      manuscriptFiles,
      coverLetterFiles,
      supplementalFiles,
    })
  },
}
