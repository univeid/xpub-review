const { configure } = require('@testing-library/react')

const originalConsoleError = console.error
console.error = function newLog(msg) {
  if (msg.includes('Error: Could not parse CSS stylesheet')) return
  originalConsoleError(msg)
}

configure({
  testIdAttribute: 'data-test-id',
})

Object.assign(global, require('@pubsweet/errors'))
