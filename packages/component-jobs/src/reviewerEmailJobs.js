module.exports.initialize = ({ models: { Job, User }, Email, logEvent }) => ({
  async handle(job) {
    try {
      const {
        userId,
        action,
        emailProps,
        manuscriptId,
        logActivity = true,
      } = job.data

      const user = await User.find(userId)
      if (!user.isSubscribedToEmails) {
        return job.done()
      }

      const email = new Email(emailProps)

      await email.sendEmail()
      if (logActivity) {
        logEvent({
          userId: null,
          manuscriptId,
          action,
          objectType: logEvent.objectType.user,
          objectId: userId,
        })
      }
      return job.done()
    } catch (e) {
      return job.done(e)
    }
  },
  async subscribe() {
    Job.subscribe({
      queueName: 'reviewer-email',
      jobHandler: this.handle,
    })
  },
})
