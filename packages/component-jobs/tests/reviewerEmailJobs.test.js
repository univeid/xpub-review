const { initialize } = require('../src/reviewerEmailJobs')
const { generateJob, generateUser } = require('component-generators')

const models = {
  Job: {
    subscribe: jest.fn(),
  },
  User: {
    find: jest.fn(),
  },
}

const logEvent = jest.fn()
logEvent.objectType = {
  user: 'user',
}

const job = generateJob()
const { User } = models
describe('reviewer email jobs', () => {
  let wrapper = async () => {}
  let Email
  beforeEach(() => {
    Email = jest.fn().mockImplementation(() => ({
      async sendEmail() {
        return jest.fn()
      },
    }))
    wrapper = initialize({ models, Email, logEvent })
  })

  it('should have the handler exposed', () => {
    const handler = 'handle'
    expect(wrapper).toHaveProperty(handler)
    expect(typeof wrapper[handler]).toBe('function')
  })

  it('should have the subscribe function exposed', () => {
    const subscribe = 'subscribe'
    expect(wrapper).toHaveProperty(subscribe)
    expect(typeof wrapper[subscribe]).toBe('function')
  })

  it('instantiates a new email with passed email props if user is subscribed', async () => {
    const user = generateUser({ isSubscribedToEmails: true })
    jest.spyOn(User, 'find').mockResolvedValue(user)

    await wrapper.handle(job)
    expect(Email).toHaveBeenCalledWith(job.data.emailProps)
  })

  it('should have logged the event with correct details', async () => {
    const user = generateUser({ isSubscribedToEmails: true })
    jest.spyOn(User, 'find').mockResolvedValue(user)

    await wrapper.handle(job)

    expect(job.data.logActivity).toBe(true)

    expect(logEvent).toHaveBeenCalledWith({
      userId: null,
      manuscriptId: job.data.manuscriptId,
      action: job.data.action,
      objectType: logEvent.objectType.user,
      objectId: job.data.userId,
    })
  })

  it('should have called job.done', () => {
    wrapper.handle(job)
    expect(job.done).toHaveBeenCalled()
  })

  it('does not send an email if the user is unsubscribed', async () => {
    const user = generateUser({ isSubscribedToEmails: false })
    jest.spyOn(User, 'find').mockResolvedValue(user)

    await wrapper.handle(job)
    expect(Email).toHaveBeenCalledTimes(0)
  })
})
