const teams = []

const generateTeam = ({ properties, Team }) => {
  const team = new Team(properties || {})
  teams.push(team)

  return team
}

const getTeamByRole = role => teams.find(team => team.role === role)
const getTeamByManuscriptId = id => teams.find(team => team.manuscriptId === id)
const getTeamsByManuscriptId = id =>
  teams.filter(team => team.manuscriptId === id)
const getTeamByRoleAndJournalId = ({ id, role }) =>
  teams.find(team => team.role === role && team.journalId === id)
const getTeamByRoleAndSectionId = ({ id, role }) =>
  teams.find(team => team.role === role && team.sectionId === id)
const getTeamByRoleAndSpecialIssueId = ({ id, role }) =>
  teams.find(team => team.role === role && team.specialIssueId === id)
const getTeamByRoleAndManuscriptId = ({ id, role }) =>
  teams.find(team => team.role === role && team.manuscriptId === id)
const getGlobalTeam = () =>
  teams.find(team => team.journalId === null && team.manuscriptId === null)
const getGlobalTeamByRole = role =>
  teams.find(
    team =>
      team.role === role &&
      team.manuscriptId === null &&
      team.journalId === null,
  )

module.exports = {
  teams,
  generateTeam,
  getTeamByRole,
  getGlobalTeam,
  getGlobalTeamByRole,
  getTeamByManuscriptId,
  getTeamsByManuscriptId,
  getTeamByRoleAndSectionId,
  getTeamByRoleAndSpecialIssueId,
  getTeamByRoleAndJournalId,
  getTeamByRoleAndManuscriptId,
}
