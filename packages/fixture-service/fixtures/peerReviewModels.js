const peerReviewModels = []

const generatePeerReviewModel = ({ properties, PeerReviewModel }) => {
  const peerReviewModel = new PeerReviewModel(properties || {})
  peerReviewModels.push(peerReviewModel)

  return peerReviewModel
}

module.exports = { generatePeerReviewModel, peerReviewModels }
