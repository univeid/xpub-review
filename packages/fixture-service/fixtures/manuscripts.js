const manuscripts = []

const generateManuscript = ({ properties, Manuscript }) => {
  const manuscript = new Manuscript(properties || {})
  manuscripts.push(manuscript)

  return manuscript
}

module.exports = { manuscripts, generateManuscript }
