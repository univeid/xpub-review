import React from 'react'
import { Redirect } from 'react-router'
import { get } from 'lodash'

import { useRemoteOpener, ShadowedBox, roles } from '@hindawi/ui'
import styled from 'styled-components'
import { Spinner, H2 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { ActivityLogGQL } from 'component-activity-log/client'
import {
  useSubmitQualityChecks,
  SubmitQualityChecksRevision,
} from 'component-quality-check/client'
import { useQuery } from 'react-apollo'
import { useCurrentUser } from 'component-authentication/client'
import { queries } from '../graphql'
import {
  AllowedTo,
  AuthorReply,
  SubmitReview,
  AuthorReviews,
  SubmitRevision,
  useSubmitReview,
  ManuscriptHeader,
  EditorialDecision,
  useSubmitRevision,
  ReviewerReportBox,
  EditorialComments,
  useEditorialDecision,
  ReassignTriageEditor,
  AssignAcademicEditor,
  ManuscriptDetailsTop,
  useReviewerInvitation,
  EditorialRecommendation,
  useGetSubmission,
  useEditorialRecommendation,
  RespondToReviewerInvitation,
  ReviewerDetailsTriageEditor,
  RespondToEditorialInvitation,
  ReviewerDetailsAcademicEditor,
  useRespondToReviewerInvitation,
  useRespondToEditorialInvitation,
} from '../components'
import recommendations from '../recommendations'
import editorialCommentsDecisions from '../editorialCommentsDecisions'

import { getExpandedStatus, getHighlightedStatus } from '../visibilityOptions'

const ManuscriptDetails = ({ history, match }) => {
  const currentUser = useCurrentUser()
  const [
    assignAcademicEditorExpanded,
    toggleAssignAcademicEditor,
  ] = useRemoteOpener()

  const [
    reassignTriageEditorExpanded,
    toggleReassignTriageEditor,
  ] = useRemoteOpener()

  const [editorialDecisionExpanded, toggleEditorialDecision] = useRemoteOpener()

  const {
    role,
    error,
    loading,
    reviews,
    versions,
    reviewers,
    manuscript,
    revisionDraft,
    academicEditor,
    peerReviewModel,
    authorResponse,
    isLatestVersion,
    reviewerReports,
    editorDecisions,
    submittingAuthor,
    editorialReviews,
    triageEditorLabel,
    academicEditorLabel,
    pendingAcademicEditor,
    hasPublishOption,
    refetch: refetchSubmission,
  } = useGetSubmission({ match })
  const { data: triageEditorData } = useQuery(queries.getTriageEditors, {
    variables: { manuscriptId: get(match, 'params.manuscriptId') },
  })

  const { respondToReviewerInvitation } = useRespondToReviewerInvitation({
    match,
    history,
    manuscript,
    userId: currentUser.id,
    totalVersions: versions.length,
  })
  const {
    submitReview,
    updateDraftReview,
    autosaveReviewForm,
    currentReviewerReport,
  } = useSubmitReview({ reviewerReports, userId: currentUser.id, match })
  const {
    addAuthor,
    editAuthor,
    removeAuthor,
    submitRevision,
    updateDraftRevision,
    updateManuscriptFile,
    updateAutosaveRevision,
  } = useSubmitRevision({
    match,
    history,
    revisionDraft,
  })
  const { respondToEditorialInvitation } = useRespondToEditorialInvitation({
    academicEditor: pendingAcademicEditor,
    match,
    history,
  })
  const { inviteReviewer, cancelReviewerInvitation } = useReviewerInvitation({
    match,
    manuscript,
  })
  const { handleEditorialRecommendation } = useEditorialRecommendation({
    match,
    manuscriptId: get(match, 'params.manuscriptId'),
  })
  const { handleEditorialDecision } = useEditorialDecision({
    manuscriptId: get(match, 'params.manuscriptId'),
    role,
    match,
  })
  const {
    updateQualityChecksDraft,
    submitQualityChecks,
  } = useSubmitQualityChecks({ revisionDraft, match, history })

  const errorMessage = get(error, 'message')
  const hasAccessToVersion = !!versions.find(
    ({ id }) => id === get(match, 'params.manuscriptId'),
  )

  if (loading) return <Spinner />
  if (
    (errorMessage && errorMessage.includes('Operation not permitted')) ||
    !hasAccessToVersion
  ) {
    return <Redirect to="/404" />
  }

  const triageEditors = get(triageEditorData, 'getTriageEditors', [])
  const academicEditorStatus = get(academicEditor, 'status', '')
  const academicEditorUserId = get(academicEditor, 'user.id', '')
  const pendingAcademicEditorStatus = get(pendingAcademicEditor, 'status', '')
  const pendingAcademicEditorUserId = get(pendingAcademicEditor, 'user.id', '')
  const manuscriptStatus = get(manuscript, 'status', '')
  const hasPeerReview = get(manuscript, 'articleType.hasPeerReview', '')
  const isApprovalEditor = get(manuscript, 'isApprovalEditor')
  const journalCode = get(manuscript, 'journal.code')
  const getExpanded = getExpandedStatus({ manuscriptStatus, authorResponse })
  const getHighlighted = getHighlightedStatus({ manuscriptStatus })
  const hasTriageEditor = get(peerReviewModel, 'hasTriageEditor')
  const {
    statusColor,
    role: roleOnManuscript,
    hasTriageEditorConflictOfInterest,
  } = manuscript

  const isTriageEditorWithoutCOI = !(
    hasTriageEditorConflictOfInterest &&
    roles.TRIAGE_EDITOR === roleOnManuscript
  )

  return (
    <Root>
      <ManuscriptDetailsTop
        history={history}
        isAdminOrEditorialAssistant={
          role === roles.ADMIN || role === roles.EDITORIAL_ASSISTANT
        }
        isLatestVersion={isLatestVersion}
        manuscript={manuscript}
        match={match}
        refetchSubmission={refetchSubmission}
        versions={versions.map(({ version, id: manuscriptId }, id) => ({
          id,
          label: `Version ${version}`,
          value: manuscriptId,
        }))}
      />
      <CustomShadowedBox statusColor={statusColor}>
        <ManuscriptHeader
          academicEditorLabel={academicEditorLabel}
          hasPeerReview={hasPeerReview}
          hasTriageEditor={hasTriageEditor}
          isApprovalEditor={isApprovalEditor}
          isLatestVersion={isLatestVersion}
          isTriageEditorWithoutCOI={isTriageEditorWithoutCOI}
          manuscript={manuscript}
          match={match}
          toggleAssignAcademicEditor={toggleAssignAcademicEditor}
          toggleReassignTriageEditor={toggleReassignTriageEditor}
          triageEditorLabel={triageEditorLabel}
          triageEditors={triageEditors}
        />
      </CustomShadowedBox>
      <ContextualBoxesContainer>
        <AllowedTo
          action="manuscript:viewEditorialComments"
          data={{
            reviewers,
            manuscriptStatus,
            academicEditorStatus,
            academicEditorUserId,
            userId: currentUser.id,
          }}
          role={role}
          yes={
            <EditorialComments
              academicEditorLabel={academicEditorLabel}
              decisions={editorialCommentsDecisions}
              editorialReviews={editorialReviews}
              startExpanded={getExpanded(
                'manuscript:viewEditorialComments',
                role,
              )}
              triageEditorLabel={triageEditorLabel}
            />
          }
        />
        <AllowedTo
          action="manuscript:viewAuthorReply"
          data={{
            reviewers,
            manuscriptStatus,
            academicEditorStatus,
            academicEditorUserId,
            userId: currentUser.id,
          }}
          role={role}
          yes={
            <AuthorReply
              authorResponse={authorResponse}
              role={role}
              startExpanded={getExpanded('manuscript:viewAuthorReply', role)}
              submittingAuthor={submittingAuthor}
            />
          }
        />
        <AllowedTo
          action="manuscript:respondToReviewerInvitation"
          data={{
            reviewers,
            userId: currentUser.id,
          }}
          role={role}
          yes={
            <RespondToReviewerInvitation
              highlight={getHighlighted(
                'manuscript:respondToReviewerInvitation',
                role,
              )}
              onSubmit={respondToReviewerInvitation}
              startExpanded={getExpanded(
                'manuscript:respondToReviewerInvitation',
                role,
              )}
            />
          }
        />
        <AllowedTo
          action="manuscript:submitReview"
          data={{
            reviews,
            reviewers,
            manuscriptStatus,
            userId: currentUser.id,
          }}
          role={role}
          yes={
            <SubmitReview
              autosaveReviewForm={autosaveReviewForm}
              highlight={getHighlighted('manuscript:submitReview', role)}
              journalCode={journalCode}
              onSubmit={submitReview}
              options={recommendations}
              reviewerReport={currentReviewerReport}
              updateDraftReview={updateDraftReview}
            />
          }
        />
        <AllowedTo
          action="manuscript:viewReviewerReports"
          data={{
            reviews,
            isLatestVersion,
            manuscriptStatus,
            userId: currentUser.id,
          }}
          role={role}
          yes={
            role === 'reviewer' ? (
              <ReviewerReportBox
                isLatestVersion={isLatestVersion}
                options={recommendations}
                reviewerReports={reviewerReports}
                startExpanded={getExpanded(
                  'manuscript:viewReviewerReports',
                  role,
                )}
              />
            ) : (
              <AuthorReviews
                recommendations={recommendations}
                reviewerReports={reviewerReports}
              />
            )
          }
        />
        <AllowedTo
          action="manuscript:submitRevision"
          data={{
            manuscriptStatus,
          }}
          role={role}
          yes={
            <SubmitRevision
              addAuthorToManuscript={addAuthor}
              editAuthor={editAuthor}
              highlight={getHighlighted('manuscript:submitRevision', role)}
              journalCode={journalCode}
              removeAuthor={removeAuthor}
              revisionDraft={revisionDraft}
              submitRevision={submitRevision}
              updateAutosave={updateAutosaveRevision}
              updateDraftRevision={updateDraftRevision}
              updateManuscriptFile={updateManuscriptFile}
            />
          }
        />

        <AllowedTo
          action="manuscript:reassignTriageEditor"
          data={{
            manuscriptStatus,
            triageEditors,
            hasPeerReview,
          }}
          role={role}
          yes={
            <ReassignTriageEditor
              expanded={reassignTriageEditorExpanded}
              manuscript={manuscript}
              match={match}
              role={role}
              toggle={toggleReassignTriageEditor}
              triageEditorLabel={triageEditorLabel}
              triageEditors={triageEditors}
            />
          }
        />

        <AllowedTo
          action="manuscript:assignAcademicEditor"
          data={{
            manuscriptStatus,
            hasPeerReview,
            isTriageEditorWithoutCOI,
          }}
          role={role}
          yes={
            <AssignAcademicEditor
              academicEditorLabel={academicEditorLabel}
              currentUser={currentUser}
              expanded={assignAcademicEditorExpanded}
              manuscript={manuscript}
              match={match}
              role={role}
              toggle={toggleAssignAcademicEditor}
            />
          }
        />
        <AllowedTo
          action="manuscript:respondToEditorialInvitation"
          data={{
            pendingAcademicEditorStatus,
            pendingAcademicEditorUserId,
            userId: currentUser.id,
            isLatestVersion,
          }}
          role={role}
          yes={
            <RespondToEditorialInvitation
              academicEditorLabel={academicEditorLabel}
              highlight={getHighlighted(
                'manuscript:respondToEditorialInvitation',
                role,
              )}
              onSubmit={respondToEditorialInvitation}
              startExpanded={getExpanded(
                'manuscript:respondToEditorialInvitation',
                role,
              )}
            />
          }
        />
        <AllowedTo
          action="manuscript:inviteReviewers"
          data={{
            hasPeerReview,
            userId: currentUser.id,
            manuscriptStatus,
            academicEditorStatus,
            academicEditorUserId,
          }}
          role={role}
          yes={
            <ReviewerDetailsAcademicEditor
              cancelReviewerInvitation={cancelReviewerInvitation}
              highlight={getHighlighted('manuscript:inviteReviewers', role)}
              inviteReviewer={inviteReviewer}
              manuscript={manuscript}
              options={recommendations}
              reviewerReports={reviewerReports}
              reviewers={reviewers}
              startExpanded={getExpanded('manuscript:inviteReviewers', role)}
            />
          }
        />
        <AllowedTo
          action="manuscript:viewReviewers"
          data={{
            academicEditorStatus,
            hasPeerReview,
            isTriageEditorWithoutCOI,
          }}
          role={role}
          yes={
            <ReviewerDetailsTriageEditor
              options={recommendations}
              reviewerReports={reviewerReports}
              reviewers={reviewers}
              startExpanded={getExpanded('manuscript:viewReviewers', role)}
            />
          }
        />
        <AllowedTo
          action="manuscript:makeRecommendation"
          data={{
            userId: currentUser.id,
            manuscriptStatus,
            hasPeerReview,
            isApprovalEditor,
            academicEditorStatus,
            academicEditorUserId,
          }}
          role={role}
          yes={
            <EditorialRecommendation
              hasPeerReview={hasPeerReview}
              hasPublishOption={hasPublishOption}
              highlight={getHighlighted('manuscript:makeRecommendation', role)}
              onSubmit={handleEditorialRecommendation}
              options={recommendations}
              triageEditorLabel={triageEditorLabel}
            />
          }
        />
        <AllowedTo
          action="manuscript:makeDecision"
          data={{
            manuscriptStatus,
            reviews,
            isApprovalEditor,
            isLatestVersion,
            academicEditorStatus,
            hasPeerReview,
            hasTriageEditorConflictOfInterest,
            isTriageEditorWithoutCOI,
          }}
          role={role}
          yes={
            <EditorialDecision
              academicEditorLabel={academicEditorLabel}
              editorDecisions={editorDecisions}
              expanded={editorialDecisionExpanded}
              highlight={getHighlighted('manuscript:makeDecision', role)}
              onSubmit={handleEditorialDecision}
              toggle={toggleEditorialDecision}
            />
          }
        />
        <AllowedTo
          action="manuscript:submitQualityChecksRevision"
          data={{
            manuscriptStatus,
          }}
          role={role}
          yes={
            <SubmitQualityChecksRevision
              addAuthorToManuscript={addAuthor}
              editAuthor={editAuthor}
              highlight
              journalCode={journalCode}
              manuscriptStatus={manuscriptStatus}
              removeAuthor={removeAuthor}
              revisionDraft={revisionDraft}
              submitRevision={submitQualityChecks}
              updateAutosave={updateAutosaveRevision}
              updateDraftRevision={updateQualityChecksDraft}
              updateManuscriptFile={updateManuscriptFile}
            />
          }
        />
        <AllowedTo
          action="manuscript:viewActivityLog"
          role={role}
          yes={
            <ActivityLogGQL
              academicEditorLabel={academicEditorLabel}
              match={match}
              peerReviewModel={peerReviewModel}
              triageEditorLabel={triageEditorLabel}
            />
          }
        />
      </ContextualBoxesContainer>
    </Root>
  )
}

export default ManuscriptDetails

const getStatusColor = ({ statusColor, theme }) =>
  statusColor ? theme[statusColor] : ''

// #region styles
const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 14);
  padding-top: calc(${th('gridUnit')} * 4);
  /* TODO: remove the padding-bottom when the dropdown Menu will be able to flip when not fitting into the viewport*/
  padding-bottom: calc(${th('gridUnit')} * 30);
`
const CustomShadowedBox = styled(ShadowedBox)`
  min-width: 100%;
  padding: 0;
  box-shadow: inset 0px calc(${th('gridUnit')} * 1) ${getStatusColor},
    ${th('shadows.boxShadow')};
  ${H2} {
    text-align: start;
  }
`
const ContextualBoxesContainer = styled(ShadowedBox)`
  min-width: 100%;
  padding: 0 calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 4)
    calc(${th('gridUnit')} * 4);
  margin-top: calc(${th('gridUnit')} * 4);
  box-shadow: ${th('shadows.boxShadow')};

  &:empty {
    padding: 0;
  }
`
// #endregion
