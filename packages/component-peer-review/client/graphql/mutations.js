import gql from 'graphql-tag'
import { fragments as submissionFragments } from 'component-submission/client'
import { reviewDetails, fileFragment } from './fragments'

export const requestRevision = gql`
  mutation requestRevision(
    $manuscriptId: String!
    $content: String!
    $type: AllowedRevisionType!
  ) {
    requestRevision(manuscriptId: $manuscriptId, content: $content, type: $type)
  }
`

export const makeRecommendationToReject = gql`
  mutation makeRecommendationToReject(
    $manuscriptId: String!
    $input: RecommendationToRejectInput
  ) {
    makeRecommendationToReject(manuscriptId: $manuscriptId, input: $input)
  }
`
export const makeRecommendationToPublish = gql`
  mutation makeRecommendationToPublish(
    $manuscriptId: String!
    $input: RecommendationToPublishInput
  ) {
    makeRecommendationToPublish(manuscriptId: $manuscriptId, input: $input)
  }
`

export const makeDecisionToReject = gql`
  mutation makeDecisionToReject($manuscriptId: String!, $content: String!) {
    makeDecisionToReject(manuscriptId: $manuscriptId, content: $content)
  }
`
export const makeDecisionToPublish = gql`
  mutation makeDecisionToPublish($manuscriptId: String!) {
    makeDecisionToPublish(manuscriptId: $manuscriptId)
  }
`

export const getFileSignedUrl = gql`
  mutation getFileSignedUrl($fileId: String!) {
    getFileSignedUrl(fileId: $fileId)
  }
`

export const inviteAcademicEditor = gql`
  mutation inviteAcademicEditor($submissionId: String!, $userId: String!) {
    inviteAcademicEditor(submissionId: $submissionId, userId: $userId)
  }
`

export const assignAcademicEditor = gql`
  mutation assignAcademicEditor($submissionId: String!, $userId: String!) {
    assignAcademicEditor(submissionId: $submissionId, userId: $userId)
  }
`

export const removeAcademicEditor = gql`
  mutation removeAcademicEditor($teamMemberId: String!) {
    removeAcademicEditor(teamMemberId: $teamMemberId)
  }
`

export const cancelAcademicEditorInvitation = gql`
  mutation cancelAcademicEditorInvitation($teamMemberId: String!) {
    cancelAcademicEditorInvitation(teamMemberId: $teamMemberId)
  }
`

export const acceptAcademicEditorInvitation = gql`
  mutation acceptAcademicEditorInvitation($teamMemberId: String!) {
    acceptAcademicEditorInvitation(teamMemberId: $teamMemberId)
  }
`

export const declineAcademicEditorInvitation = gql`
  mutation declineAcademicEditorInvitation(
    $teamMemberId: String!
    $reason: String
  ) {
    declineAcademicEditorInvitation(
      teamMemberId: $teamMemberId
      reason: $reason
    )
  }
`

export const inviteReviewer = gql`
  mutation inviteReviewer($manuscriptId: String!, $input: ReviewerInput!) {
    inviteReviewer(manuscriptId: $manuscriptId, input: $input)
  }
`

export const cancelReviewerInvitation = gql`
  mutation cancelReviewerInvitation(
    $manuscriptId: String!
    $teamMemberId: String!
  ) {
    cancelReviewerInvitation(
      manuscriptId: $manuscriptId
      teamMemberId: $teamMemberId
    )
  }
`

export const makeDecisionToReturn = gql`
  mutation makeDecisionToReturn($manuscriptId: String!, $content: String!) {
    makeDecisionToReturn(manuscriptId: $manuscriptId, content: $content)
  }
`

// #region Reviewer mutations
export const acceptReviewerInvitation = gql`
  mutation acceptReviewerInvitation($teamMemberId: String!) {
    acceptReviewerInvitation(teamMemberId: $teamMemberId)
  }
`

export const declineReviewerInvitation = gql`
  mutation declineReviewerInvitation($teamMemberId: String!) {
    declineReviewerInvitation(teamMemberId: $teamMemberId)
  }
`

export const updateAutosave = gql`
  mutation updateAutosave($params: AutosaveInput) {
    updateAutosave(params: $params) @client
  }
`

export const updateDraftRevision = gql`
  mutation updateDraftRevision(
    $manuscriptId: String!
    $autosaveInput: RevisionAutosaveInput
  ) {
    updateDraftRevision(
      manuscriptId: $manuscriptId
      autosaveInput: $autosaveInput
    ) {
      ...draftManuscriptDetails
      comment {
        id
        type
        content
        files {
          ...manuscriptDetailsFile
        }
      }
    }
  }
  ${submissionFragments.draftManuscriptDetails}
  ${fileFragment}
`

export const submitRevision = gql`
  mutation submitRevision($submissionId: String!) {
    submitRevision(submissionId: $submissionId)
  }
`

export const updateDraftReview = gql`
  mutation updateDraftReview(
    $reviewId: String!
    $input: UpdateDraftReviewInput!
  ) {
    updateDraftReview(reviewId: $reviewId, input: $input) {
      ...reviewDetails
    }
  }
  ${reviewDetails}
`
export const updateAutosaveReview = gql`
  mutation updateAutosave($params: AutosaveInput) {
    updateAutosave(params: $params) @client
  }
`
export const submitReview = gql`
  mutation submitReview($reviewId: String!) {
    submitReview(reviewId: $reviewId)
  }
`

export const reassignTriageEditor = gql`
  mutation reassignTriageEditor(
    $teamMemberId: String!
    $manuscriptId: String!
  ) {
    reassignTriageEditor(
      manuscriptId: $manuscriptId
      teamMemberId: $teamMemberId
    )
  }
`

export const updatePreprintValue = gql`
  mutation updatePreprintValue(
    $manuscriptId: String!
    $preprintValue: String!
  ) {
    updatePreprintValue(
      manuscriptId: $manuscriptId
      preprintValue: $preprintValue
    )
  }
`

export const deleteManuscript = gql`
  mutation deleteManuscript($manuscriptId: String!) {
    deleteManuscript(manuscriptId: $manuscriptId)
  }
`
export const archiveManuscript = gql`
  mutation archiveManuscript($submissionId: String!) {
    archiveManuscript(submissionId: $submissionId)
  }
`
export const withdrawManuscript = gql`
  mutation withdrawManuscript($submissionId: String!) {
    withdrawManuscript(submissionId: $submissionId)
  }
`
// #endregion
