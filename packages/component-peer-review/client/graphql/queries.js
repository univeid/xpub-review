import gql from 'graphql-tag'

import {
  manuscriptFragment,
  userFragment,
  fileFragment,
  teamMember,
} from './fragments'

// Temporary query for redirect
export const getSubmissionIds = gql`
  query getSubmissionIds($customId: String!) {
    getSubmissionIds(customId: $customId) {
      id
      submissionId
    }
  }
`

export const getSubmission = gql`
  query getSubmission($submissionId: String!) {
    getSubmission(submissionId: $submissionId) {
      ...manuscriptDetails
    }
    getDraftRevision(submissionId: $submissionId) {
      ...manuscriptDetails
      comment {
        id
        type
        content
        files {
          ...manuscriptDetailsFile
        }
      }
    }
  }
  ${manuscriptFragment}
  ${fileFragment}
`

export const getCurrentUser = gql`
  query {
    getCurrentUser {
      ...manuscriptDetailsUser
    }
  }
  ${userFragment}
`

export const getAcademicEditors = gql`
  query getAcademicEditors($manuscriptId: String!, $searchValue: String) {
    getAcademicEditors(manuscriptId: $manuscriptId, searchValue: $searchValue) {
      ...teamMember
      workload
    }
  }
  ${teamMember}
`
export const getTriageEditors = gql`
  query getTriageEditors($manuscriptId: String!) {
    getTriageEditors(manuscriptId: $manuscriptId) {
      ...teamMember
    }
  }
  ${teamMember}
`
export const loadReviewerSuggestions = gql`
  query loadReviewerSuggestions($manuscriptId: String!) {
    loadReviewerSuggestions(manuscriptId: $manuscriptId) {
      id
      email
      givenNames
      surname
      aff
      profileUrl
      numberOfReviews
      isInvited
    }
  }
`

export const isUserSubscribedToEmails = gql`
  query isUserSubscribedToEmails($email: String!) {
    isUserSubscribedToEmails(email: $email)
  }
`
