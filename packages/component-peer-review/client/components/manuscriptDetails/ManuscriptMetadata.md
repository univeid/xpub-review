Manuscript details and metadata: abstract, conflict of interest, files

```js
const fragment = {
  metadata: {
    abstract:
      'In sinus, invariably you have the headache, but every headache is not sinus! Sinus and nasal passage problems are the cause of headache, mostly! It is not that you have pain in the sinus area and you need to conclude that you have a sinus disorder. When the nasal passage and sinus are inflamed, it can result in a severe headache. In acute chronic sinus, the intensity of headache varies with the severity of the sinus. Sinus patients complain about many other problems. ',
  },
  conflicts: {
    message:
      'Prevention is better than cure.Well said and we…ou have a sinus disorder. When the nasal passage ',
    hasConflicts: 'yes',
  },
  files: {
    coverLetter: [
      {
        id:
          '8dca903a-05b9-45ab-89b9-9cb99a9a29c6/02db6c5e-2938-45ac-a5ee-67ae63919bb2',
        name: 'Rocketbook-A4-DotGrid.pdf',
        size: 59621,
        originalName: 'Rocketbook-A4-DotGrid.pdf',
      },
    ],
    manuscripts: [
      {
        id:
          '8dca903a-05b9-45ab-89b9-9cb99a9a29c6/5e69e3d9-7f9d-4e8d-b649-6e6a45658d75',
        name: 'manuscript.pdf',
        size: 476862,
        originalName: 'manuscript.pdf',
      },
    ],
    supplementary: [],
    figure: [],
  },
}
;<ManuscriptMetadata fragment={fragment} currentUser={{}} />
```
