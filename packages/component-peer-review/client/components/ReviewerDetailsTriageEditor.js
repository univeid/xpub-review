import React from 'react'

import {
  ReviewersTable,
  ReviewerReports,
  ReviewerDetails,
} from './peerReviewComponents'

const ReviewerDetailsTriageEditor = ({
  options,
  reviewers,
  startExpanded,
  reviewerReports,
  cancelReviewerInvitation,
}) => (
  <ReviewerDetails
    mt={4}
    reviewers={reviewers}
    startExpanded={startExpanded}
    tabButtons={['Reviewer Details', 'Reviewer Reports']}
  >
    <ReviewersTable
      canCancelReviewerInvitation={false}
      onCancelReviewerInvitation={cancelReviewerInvitation}
      reviewers={reviewers}
    />

    <ReviewerReports options={options} reviewerReports={reviewerReports} />
  </ReviewerDetails>
)

export default ReviewerDetailsTriageEditor
