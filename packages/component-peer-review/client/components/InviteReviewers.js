import React, { Fragment } from 'react'

import { ReviewersTable, InviteReviewersForm } from './peerReviewComponents'

const InviteReviewers = ({
  reviewers,
  canInviteReviewers,
  cancelReviewerInvitation,
  canCancelReviewerInvitation,
}) => (
  <Fragment>
    {canInviteReviewers && <InviteReviewersForm />}
    <ReviewersTable
      canCancelReviewerInvitation={canCancelReviewerInvitation}
      onCancelReviewerInvitation={cancelReviewerInvitation}
      reviewers={reviewers}
    />
  </Fragment>
)

export default InviteReviewers
