import { useMutation } from 'react-apollo'
import { get } from 'lodash'
import { mutations } from '../graphql'
import { refetchGetSubmission } from '../graphql/refetchQueries'
import { parseError } from '../utils'

const useRespondToEditorialInvitation = ({
  match,
  history,
  academicEditor,
}) => {
  const [acceptAcademicEditorInvitation] = useMutation(
    mutations.acceptAcademicEditorInvitation,
    {
      refetchQueries: match.params.submissionId
        ? [refetchGetSubmission(match)]
        : [],
    },
  )
  const [declineAcademicEditorInvitation] = useMutation(
    mutations.declineAcademicEditorInvitation,
  )

  const respondToEditorialInvitation = (
    { academicEditorDecision, reason },
    modalProps,
  ) => {
    const teamMemberId = get(academicEditor, 'id')

    modalProps.setFetching(true)
    if (academicEditorDecision === 'yes') {
      acceptAcademicEditorInvitation({
        variables: {
          teamMemberId,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(parseError(e))
        })
    } else {
      declineAcademicEditorInvitation({
        variables: {
          teamMemberId,
          reason,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
          history.replace('/')
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    }
  }

  return { respondToEditorialInvitation }
}

export default useRespondToEditorialInvitation
