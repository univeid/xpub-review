/* eslint-disable @typescript-eslint/no-use-before-define */
import React from 'react'
import { Formik } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, H3, TextField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Label,
  NewTheme,
  validators,
  MenuCountry,
  ItemOverrideAlert,
  ValidatedFormField,
  trimFormStringValues,
} from '@hindawi/ui'

import InviteReviewerModal from './InviteReviewerModal'

const { useModal } = NewTheme

const InviteReviewersForm = () => {
  const { isShowing, toggle } = useModal()

  return (
    <Formik onSubmit={toggle}>
      {({ handleSubmit, resetForm, values }) => (
        <Root>
          <Row justify="space-between" mb={2} mt={5}>
            <H3>Invite Reviewer</H3>

            <Item justify="flex-end">
              <Button
                data-type-id="button-invite-reviewer-clear"
                ml={4}
                onClick={() => resetForm({})}
                secondary
                small
                width={32}
              >
                CLEAR
              </Button>

              <Button
                data-type-id="button-invite-reviewer-invite"
                ml={4}
                onClick={handleSubmit}
                primary
                small
                width={32}
              >
                SEND
              </Button>

              <InviteReviewerModal
                isShowing={isShowing}
                resetForm={resetForm}
                toggle={toggle}
                values={trimFormStringValues(values)}
              />
            </Item>
          </Row>
          <Row>
            <Item mr={4} vertical>
              <Label required>Email</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="reviewer-email"
                inline
                name="email"
                validate={[validators.required, validators.emailValidator]}
              />
            </Item>

            <Item mr={4} vertical>
              <Label required>First Name</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="reviewer-givenNames"
                inline
                name="givenNames"
                validate={[validators.required]}
              />
            </Item>

            <Item mr={4} vertical>
              <Label required>Last Name</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="reviewer-surname"
                inline
                name="surname"
                validate={[validators.required]}
              />
            </Item>

            <Item mr={4} vertical>
              <Label>Affiliation</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="reviewer-aff"
                inline
                name="aff"
              />
            </Item>

            <ItemOverrideAlert vertical>
              <Label>Country</Label>
              <ValidatedFormField
                component={MenuCountry}
                data-test-id="reviewer-country"
                name="country"
              />
            </ItemOverrideAlert>
          </Row>
        </Root>
      )}
    </Formik>
  )
}

export default InviteReviewersForm

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue3')};
  padding: 0 calc(${th('gridUnit')} * 4);
  padding-bottom: calc(${th('gridUnit')} * 2);
`
// #endregion
