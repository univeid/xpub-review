/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react'
import { useQuery, useMutation } from 'react-apollo'
import { useHistory } from 'react-router-dom'
import { NewTheme } from '@hindawi/ui'
import { queries, mutations } from '../../graphql'

const { ConfirmationModal, LoadingModal, InformationModal } = NewTheme

const InviteReviewerModal = ({ resetForm, values, toggle, isShowing }) => {
  if (!isShowing) return null

  const history = useHistory()
  const [, , submissionId, manuscriptId] = history
    ? history.location.pathname.split('/')
    : []

  const {
    data: queryData,
    loading: queryLoading,
    error: queryError,
  } = useQuery(queries.isUserSubscribedToEmails, {
    variables: { email: values?.email },
    fetchPolicy: 'network-only',
  })
  const isUnsubscribed = queryData?.isUserSubscribedToEmails === false

  const [
    inviteReviewerMutation,
    { loading: mutationLoading, error: mutationError },
  ] = useMutation(mutations.inviteReviewer, {
    refetchQueries: [
      {
        query: queries.getSubmission,
        variables: { submissionId },
      },
    ],
  })

  const onConfirm = () => {
    inviteReviewerMutation({
      variables: {
        manuscriptId,
        input: values,
      },
    })
      .then(() => {
        toggle()
        resetForm({})
      })
      .catch(() => {})
  }

  if (queryLoading) return <LoadingModal toggle={toggle} />
  if (queryError)
    return (
      <InformationModal
        error={queryError?.message?.replace('GraphQL error: ', '')}
        icon="info"
        title="An error has occured"
        toggle={toggle}
      />
    )

  if (isUnsubscribed) {
    return (
      <ConfirmationModal
        acceptButtonLabel="OK, SEND INVITATION"
        error={mutationError?.message?.replace('GraphQL error: ', '')}
        icon="info"
        loading={mutationLoading}
        onConfirm={onConfirm}
        subtitle="This reviewer will not receive email invitation because he is unsubscribed. He can still respond to the invitation within the system."
        title="Send invitation to unsubscribed reviewer?"
        toggle={toggle}
      />
    )
  }

  return (
    <ConfirmationModal
      acceptButtonLabel="Send"
      error={mutationError?.message?.replace('GraphQL error: ', '')}
      loading={mutationLoading}
      onConfirm={onConfirm}
      subtitle="This reviewer will receive an email invitation."
      title="Send invitation to reviewer?"
      toggle={toggle}
    />
  )
}

export default InviteReviewerModal
