import React from 'react'
import { Formik } from 'formik'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Modal } from 'component-modal'

import {
  Row,
  Item,
  Menu,
  Label,
  Textarea,
  validators,
  MultiAction,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

const getEditorDecisionsOptions = editorDecisions => {
  const decisionLabels = {
    publish: 'Publish',
    revision: 'Request Revision',
    returnToAcademicEditor: 'Return to Academic Editor',
    reject: 'Reject',
  }
  return editorDecisions.map(decision => ({
    value: decision,
    label: decisionLabels[decision],
  }))
}

const getEditorDecisionsModalProps = academicEditorLabel => ({
  publish: {
    message: 'Publish',
    title: 'Publish Manuscript?',
    subtitle: 'A publish decision is final',
    confirmText: 'Publish manuscript',
  },
  returnToAcademicEditor: {
    message: 'Return Manuscript',
    title: 'Return Manuscript?',
    subtitle: `A returning manuscript to ${academicEditorLabel} decision is final`,
    confirmText: 'Return Manuscript',
  },
  revision: {
    message: 'Revision Requested',
    title: 'Request revision?',
    subtitle: null,
    confirmText: 'Request Revision',
  },
  reject: {
    message: 'Reject',
    title: 'Reject manuscript?',
    subtitle: 'A rejection decision is final',
    confirmText: 'Reject manuscript',
  },
})

const MessageForm = ({ message }) => (
  <Row>
    <Item vertical>
      <Label mb={2} required>
        {message}
      </Label>
      <ValidatedFormField
        component={Textarea}
        data-test-id="triage-editor-decision-message"
        name="message"
        validate={[validators.required]}
      />
    </Item>
  </Row>
)

const EditorialDecision = ({
  toggle,
  expanded,
  onSubmit,
  highlight,
  editorDecisions,
  academicEditorLabel,
}) => {
  const onConfirm = (values, resetForm) => modalProps => {
    onSubmit(values, modalProps).then(() => {
      resetForm({})
      toggle()
    })
  }

  const initialValues = { decision: '', message: '' }
  const options = getEditorDecisionsOptions(editorDecisions)
  const editorDecisionsModalProps = getEditorDecisionsModalProps(
    academicEditorLabel,
  )

  return (
    <Formik initialValues={initialValues}>
      {({ values, values: { decision }, resetForm }) => (
        <ContextualBox
          expanded={expanded}
          highlight={highlight}
          label="Your Editorial Decision"
          mt={4}
          toggle={toggle}
        >
          <Root>
            <Item mb={2} vertical>
              <Label mb={2} required>
                Decision
              </Label>
              <ValidatedFormField
                component={Menu}
                data-test-id="triage-editor-decision-menu"
                inline
                name="decision"
                options={options}
                placeholder="Please select"
                validate={[validators.required]}
                width={62}
              />
            </Item>

            {(decision === 'revision' || decision === 'reject') && (
              <MessageForm message="Comments for Author" />
            )}

            {decision === 'returnToAcademicEditor' && (
              <MessageForm message={`Comments for ${academicEditorLabel}`} />
            )}

            {decision && (
              <Modal
                component={MultiAction}
                modalKey="editorialDecision"
                onConfirm={onConfirm(values, resetForm)}
                {...editorDecisionsModalProps[decision]}
              >
                {showModal => (
                  <Row justify="flex-end">
                    <Button
                      data-test-id="submit-triage-editor-decision"
                      medium
                      onClick={showModal}
                      pl={4}
                      pr={4}
                      primary
                    >
                      Submit decision
                    </Button>
                  </Row>
                )}
              </Modal>
            )}
          </Root>
        </ContextualBox>
      )}
    </Formik>
  )
}

export default EditorialDecision

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding: calc(${th('gridUnit')} * 4);
`
