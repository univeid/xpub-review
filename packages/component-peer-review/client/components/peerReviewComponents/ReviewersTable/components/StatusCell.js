import React from 'react'
import { Text } from '@hindawi/ui'

const StatusCell = ({ reviewer }) => {
  const reviewerStatus = reviewer.reviewerStatusLabel

  return (
    <td>
      <Text display="inline-flex" secondary>
        {reviewerStatus}
      </Text>
    </td>
  )
}

export default StatusCell
