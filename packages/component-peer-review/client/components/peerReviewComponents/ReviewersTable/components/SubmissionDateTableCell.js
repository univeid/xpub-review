import React from 'react'
import { get } from 'lodash'
import { Text } from '@hindawi/ui'
import { DateParser } from '@pubsweet/ui'

const SubmissionDateTableCell = ({ reviewer }) => {
  const review = get(reviewer, 'review')
  const reviewerSubmissionDate = get(reviewer, 'review.submitted')
  const showDate = review && reviewerSubmissionDate !== null

  return (
    <td>
      {showDate && (
        <DateParser timestamp={reviewerSubmissionDate}>
          {timestamp => <Text display="inline-flex">{timestamp}</Text>}
        </DateParser>
      )}
    </td>
  )
}

export default SubmissionDateTableCell
