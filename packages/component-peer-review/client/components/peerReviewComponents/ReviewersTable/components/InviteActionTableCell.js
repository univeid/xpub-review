import React from 'react'
import { HiddenCell } from '../styles.js'
import PersonInvitation from '../../../PersonInvitation'

const InviteActionTableCell = ({ show, reviewer, onRevoke }) => {
  if (!show) {
    return null
  }

  const reviewerIsPending = reviewer.status === 'pending'

  return (
    <HiddenCell>
      {reviewerIsPending && (
        <PersonInvitation
          invitation={reviewer}
          onRevoke={onRevoke}
          withName={false}
        />
      )}
    </HiddenCell>
  )
}

export default InviteActionTableCell
