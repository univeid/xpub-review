import { useMutation } from 'react-apollo'
import { mutations } from '../graphql/'
import {
  refetchGetAuditLogs,
  refetchGetSubmission,
} from '../graphql/refetchQueries'

const useAssignAcademicEditor = ({ role, match, manuscript }) => {
  const options = {
    refetchQueries:
      role === 'admin'
        ? [refetchGetSubmission(match), refetchGetAuditLogs(match)]
        : [refetchGetSubmission(match)],
  }
  const [inviteAcademicEditorMutation] = useMutation(
    mutations.inviteAcademicEditor,
    options,
  )
  const [assignAcademicEditorMutation] = useMutation(
    mutations.assignAcademicEditor,
    options,
  )

  const inviteAcademicEditor = (userId, modalProps) => {
    modalProps.setFetching(true)
    inviteAcademicEditorMutation({
      variables: {
        submissionId: manuscript.submissionId,
        userId,
      },
    })
      .then(() => {
        modalProps.setFetching(true)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  const assignAcademicEditor = (userId, modalProps) => {
    modalProps.setFetching(true)
    assignAcademicEditorMutation({
      variables: {
        submissionId: manuscript.submissionId,
        userId,
      },
    })
      .then(() => {
        modalProps.setFetching(true)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  return {
    inviteAcademicEditor,
    assignAcademicEditor,
  }
}

export default useAssignAcademicEditor
