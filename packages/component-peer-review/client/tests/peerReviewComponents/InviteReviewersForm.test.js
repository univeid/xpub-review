import React from 'react'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from '../testUtils'
import { InviteReviewersForm } from '../..'

const reviewer = {
  email: 'pedro.ruiz@universidaddecolombia.co',
  givenNames: 'Pedro Rúiz',
  surname: 'Gonzales',
}

describe('Invite Reviewers Form', () => {
  afterEach(cleanup)

  it('should validate', done => {
    const { getByText, getAllByText } = render(<InviteReviewersForm />)

    fireEvent.click(getByText(/send/i))

    setTimeout(() => {
      const formErrors = getAllByText(/required/i)
      expect(formErrors).toHaveLength(3)

      done()
    })
  })
})
