const { withAuthsomeMiddleware } = require('helper-service')
const { merge } = require('lodash')
const useCases = require('./useCases')

const makeDecisionResolver = require('./useCases/makeDecision/resolver')
const submitReviewResolver = require('./useCases/submitReview/resolver')
const submitRevisionResolver = require('./useCases/submitRevision/resolver')
const requestRevisionResolver = require('./useCases/requestRevision/resolver')
const makeRecommendationResolver = require('./useCases/makeRecommendation/resolver')
const inviteReviewersResolver = require('./useCases/inviteReviewers/resolver')
const inviteAcademicEditorResolver = require('./useCases/inviteAcademicEditor/resolver')
const removeAcademicEditorResolver = require('./useCases/removeAcademicEditor/resolver')
const reassignTriageEditorResolver = require('./useCases/reassignTriageEditor/resolver')
const assignAcademicEditorResolver = require('./useCases/assignAcademicEditor/resolver')
const updateManuscriptDetailsResolver = require('./useCases/updateManuscriptDetails/resolver')

const mergedResolvers = merge(
  makeDecisionResolver,
  submitReviewResolver,
  submitRevisionResolver,
  requestRevisionResolver,
  inviteReviewersResolver,
  makeRecommendationResolver,
  inviteAcademicEditorResolver,
  removeAcademicEditorResolver,
  reassignTriageEditorResolver,
  assignAcademicEditorResolver,
  updateManuscriptDetailsResolver,
)

module.exports = withAuthsomeMiddleware(mergedResolvers, useCases)
