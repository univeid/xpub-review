const config = require('config')
const { get } = require('lodash')

const publisherConfig = config.get('publisherConfig')

const initialize = ({
  Email,
  getPropsService,
  getEmailCopyService,
  getExpectedDate,
}) => ({
  async notifyAcademicEditor({
    user,
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const {
      link: { reviewLink },
      emailData: { shortReviewLink },
    } = publisherConfig
    const { title } = manuscript
    const { name: journalName } = journal
    const titleText = `the manuscript titled "${title}" by ${submittingAuthor.getName()}`
    const editorialAssistantEmail = get(editorialAssistant, 'alias.email')
    const triageEditorName = triageEditor.getName()
    const emailType = 'reviewer-declined'

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      emailType,
      titleText,
      journalName,
      expectedDate: getExpectedDate({
        timestamp: Date.now(),
        daysExpected: 14,
      }),
      targetUserName: user.alias.surname,
    })

    const emailProps = getPropsService.getProps({
      manuscript,
      toUser: academicEditor,
      fromEmail: `${triageEditorName} <${editorialAssistantEmail}>`,
      subject: `${manuscript.customId}: A reviewer has declined`,
      paragraph,
      signatureName: triageEditorName,
      journalName,
      reviewLink,
      shortReviewLink,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
