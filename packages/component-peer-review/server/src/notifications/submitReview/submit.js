const config = require('config')

const publisherConfig = config.get('publisherConfig')

const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyAcademicEditor({
    reviewer,
    fromName,
    manuscript,
    journalName,
    academicEditor,
    submittingAuthorName,
    editorialAssistantEmail,
  }) {
    const {
      links: { reviewLink },
      emailData: { shortReviewLink },
    } = publisherConfig

    const titleText = `the manuscript titled "${manuscript.title}" by ${submittingAuthorName}`

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText,
      journalName,
      reviewLink,
      shortReviewLink,
      emailType: 'academic-editor',
      targetUserName: reviewer.getLastName(),
    })

    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: academicEditor,
      signatureName: fromName,
      fromEmail: `${fromName} <${editorialAssistantEmail}>`,
      subject: `${manuscript.customId}: A reviewer report has been submitted`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyReviewerForSubmittedReport({
    reviewer,
    fromName,
    manuscript,
    articleType,
    journalName,
    submittingAuthorName,
    editorialAssistantEmail,
  }) {
    const titleText = `on ${articleType} ${manuscript.customId} titled "${manuscript.title}" by ${submittingAuthorName}`

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText,
      journalName,
      emailType: 'reviewer',
      targetUserName: reviewer.getLastName(),
    })
    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: reviewer,
      fromEmail: `${fromName} <${editorialAssistantEmail}>`,
      subject: `${manuscript.customId}: Thank you submitting your reviewer report!`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = { initialize }
