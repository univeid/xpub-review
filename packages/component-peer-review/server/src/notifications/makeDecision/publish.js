const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyEQA({
    journalName,
    manuscript,
    publishingMember,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      emailType: 'EQA-manuscript-published',
      titleText: `The manuscript titled "${title}" by ${submittingAuthorName}`,
      approvalEditorName: publishingMember.getName(),
    })
    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      toUser: editorialAssistant,
      signatureName: journalName,
      subject: `${customId}: Manuscript decision finalized`,
      fromEmail: `${journalName} <${editorialAssistant.alias.email}>`,
    })
    emailProps.bodyProps = bodyProps

    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyTriageEditorForSubmittedReport({
    journal,
    manuscript,
    triageEditor,
    articleTypeName,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const editorialAssistantName = editorialAssistant.getName()
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      journalName,
      emailType: 'triage-editor-contribution',
      titleText: `${articleTypeName} ${manuscript.customId} titled "${
        manuscript.title
      }", by ${submittingAuthor.getName()}. `,
      targetUserName: triageEditor.getLastName(),
    })

    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: triageEditor,
      subject: `${manuscript.customId}: Thank you for submitting your editorial decision`,
      fromEmail: `${editorialAssistantName} <${editorialAssistant.alias.email}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyAcademicEditorForSubmittedReport({
    journal,
    manuscript,
    academicEditor,
    articleTypeName,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal

    const editorialAssistantName = editorialAssistant.getName()
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      journalName,
      emailType: 'academic-editor-contribution',
      titleText: `${articleTypeName} ${manuscript.customId} titled "${
        manuscript.title
      }", by ${submittingAuthor.getName()}. `,
      targetUserName: academicEditor.getName(),
    })

    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: academicEditor,
      subject: `${manuscript.customId}: Thank you for submitting your editorial decision`,
      fromEmail: `${editorialAssistantName} <${editorialAssistant.alias.email}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
