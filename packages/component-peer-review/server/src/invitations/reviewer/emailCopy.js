const config = require('config')

const coiLink = config.get('publisherConfig.links.coiLink')

const getEmailCopy = ({
  emailType,
  titleText,
  expectedDate,
  journalName,
  academicEditorLabel,
}) => {
  let upperContent, manuscriptText, subText, lowerContent
  const hasLink = true
  const hasIntro = true
  const hasSignature = true

  let resend = false
  switch (emailType) {
    case 'reviewer-invitation':
      upperContent = `${titleText}, has been submitted to <strong>${journalName}</strong> for consideration.
        <br/><br/>
        As the ${academicEditorLabel} handling the manuscript, I would be delighted if you would agree to review it and let me know whether you feel it is suitable for publication.`
      subText = `The manuscript's abstract and author information is below to help you decide. Once you have agreed to review, you will be able to download the full article PDF.`
      lowerContent = `
        Reviewers are expected to return their report within 14 days of agreeing to review, 
        however if you need more time please do let us know as we may be able to arrange an alternative deadline.
        <br/><br/>
        To ensure we keep delays to a minimum please accept or decline this invitation within the next 7 days.
        <br/><br/>
        If a potential conflict of interest exists between yourself and either the authors or
        the subject of the manuscript, please decline to handle the manuscript. If a conflict
        becomes apparent during the review process, please let me know at the earliest possible
        opportunity. For more information about our conflicts of interest policies, please
        see:
        <a style="color:#007e92; text-decoration: none;" href="${coiLink}">${coiLink}</a>.`
      break
    case 'reviewer-resend-invitation':
      resend = true
      upperContent = `On ${expectedDate} I invited you to review ${titleText}, submitted to ${journalName} for consideration.<br/><br/>
        I would be grateful if you would agree to review the manuscript and let me know whether you feel
        it is suitable for publication. If you are unable to review this manuscript please decline to review. More details are available by clicking the link below.`
      lowerContent = `Thank you in advance for taking the time to consider this invitation, as it would not be possible for us to run the journal without the help of our reviewers.<br/><br/>
        I am looking forward to hearing from you.`
      break
    case 'reviewer-invitation-after-revision':
      upperContent = `A new version of ${titleText}, has been submitted to ${journalName} for consideration.<div>&nbsp;</div>
      As you reviewed the previous version of this manuscript, I would be delighted if you would agree to review the new version and let me know whether you feel it is suitable for publication.`
      subText = `The manuscript's abstract and author information is below to help you decide. Once you have agreed to review, you will be able to download the full article PDF.`
      lowerContent = `If a potential conflict of interest exists between yourself and either the authors or
        the subject of the manuscript, please decline to handle the manuscript. If a conflict
        becomes apparent during the review process, please let me know at the earliest possible
        opportunity. For more information about our conflicts of interest policies, please
        see:
        <a style="color:#007e92; text-decoration: none;" href="${coiLink}">${coiLink}</a>.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    resend,
    hasLink,
    subText,
    upperContent,
    lowerContent,
    manuscriptText,
    hasIntro,
    hasSignature,
  }
}

module.exports = {
  getEmailCopy,
}
