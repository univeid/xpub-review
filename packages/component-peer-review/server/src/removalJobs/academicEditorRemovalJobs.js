const moment = require('moment-business-days')
const config = require('config')

const businessDays = config.get('reminders.businessDays')

const initialize = ({ Job, logEvent }) => ({
  async scheduleRemovalJobForAcademicEditor({
    days,
    timeUnit,
    invitationId,
    manuscriptId,
  }) {
    const executionDate = businessDays
      ? moment()
          .businessAdd(days)
          .toISOString()
      : moment()
          .add(days, timeUnit)
          .toISOString()

    const params = {
      invitationId,
      manuscriptId,
      action: logEvent.actions.reminder_invitation_removed,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'academic-editor-removal',
    })
  },
})

module.exports = { initialize }
