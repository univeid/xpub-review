const moment = require('moment-business-days')
const config = require('config')

const timeUnit = config.get('reminders.reviewer.acceptInvitation.timeUnit')
const businessDays = config.get('reminders.businessDays')
const publisherConfig = config.get('publisherConfig')

const { getInvitationsEmailCopy } = require('./invitationsEmailCopy')
const { getNotificationsEmailCopy } = require('./notificationsEmailCopy')

const initialize = ({ Job, logEvent }) => ({
  async scheduleFirstAcademicEditorReminder({
    userId,
    titleText,
    firstDate,
    emailProps,
    journalName,
    expectedDate,
    manuscriptId,
    invitationId,
  }) {
    const executionDate = businessDays
      ? moment()
          .businessAdd(firstDate)
          .toISOString()
      : moment()
          .add(firstDate, timeUnit)
          .toISOString()

    const { ...bodyProps } = getInvitationsEmailCopy({
      emailType: `academic-editor-resend-invitation-first-reminder`,
      titleText,
      expectedDate,
      journalName,
    })

    emailProps.bodyProps = bodyProps

    const params = {
      action: logEvent.actions.reminder_invitation_first,
      userId,
      timeUnit,
      manuscriptId,
      emailProps,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'academic-editor-email',
    })
  },
  async scheduleFirstAcademicEditorInviteReviewerReminder({
    userId,
    timeUnit,
    titleText,
    firstDate,
    emailProps,
    manuscriptId,
    invitationId,
  }) {
    let executionDate
    if (process.env.NODE_ENV !== 'production') {
      executionDate = moment()
        .add(firstDate, timeUnit)
        .toISOString()
    } else {
      executionDate = moment()
        .businessAdd(firstDate)
        .toISOString()
    }

    const action = logEvent.actions.reminder_invite_reviewer_first

    const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
      emailType: 'academic-editor-reviewer-invitation-first-reminder',
      titleText,
    })
    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph

    const params = {
      action,
      userId,
      timeUnit,
      emailProps,
      manuscriptId,
      executionDate,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'academic-editor-email',
    })
  },
  async scheduleSecondAcademicEditorReminder({
    userId,
    titleText,
    secondDate,
    emailProps,
    journalName,
    expectedDate,
    manuscriptId,
    invitationId,
  }) {
    const executionDate = businessDays
      ? moment()
          .businessAdd(secondDate)
          .toISOString()
      : moment()
          .add(secondDate, timeUnit)
          .toISOString()

    const { ...bodyProps } = getInvitationsEmailCopy({
      emailType: `academic-editor-resend-invitation-second-reminder`,
      titleText,
      expectedDate,
      journalName,
    })

    emailProps.bodyProps = bodyProps

    const params = {
      action: logEvent.actions.reminder_invitation_second,
      userId,
      timeUnit,
      manuscriptId,
      executionDate,
      emailProps,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'academic-editor-email',
    })
  },
  async scheduleSecondAcademicEditorInviteReviewerReminder({
    userId,
    timeUnit,
    titleText,
    emailProps,
    secondDate,
    manuscriptId,
    invitationId,
  }) {
    const {
      links: { reviewLink },
      emailData: { shortReviewLink },
    } = publisherConfig

    const executionDate = businessDays
      ? moment()
          .businessAdd(secondDate)
          .toISOString()
      : moment()
          .add(secondDate, timeUnit)
          .toISOString()

    const action = logEvent.actions.reminder_invite_reviewer_second

    const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
      emailType: 'academic-editor-reviewer-invitation-second-reminder',
      titleText,
      reviewLink,
      shortReviewLink,
    })
    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph
    const params = {
      action,
      userId,
      timeUnit,
      emailProps,
      manuscriptId,
      executionDate,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'academic-editor-email',
    })
  },
  async scheduleEAInviteReviewerReminder({
    userId,
    timeUnit,
    titleText,
    thirdDate,
    emailProps,
    manuscriptId,
    invitationId,
    triageEditorLabel,
    editorialAssistantLabel,
  }) {
    const executionDate = businessDays
      ? moment()
          .businessAdd(thirdDate)
          .toISOString()
      : moment()
          .add(thirdDate, timeUnit)
          .toISOString()

    const action = logEvent.actions.reminder_not_invited_enough_reviewers

    const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
      emailType: 'ea-academic-editor-not-invited-enough-reviewers',
      editorialAssistantLabel,
      triageEditorLabel,
      titleText,
    })
    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph

    const params = {
      action,
      userId,
      timeUnit,
      emailProps,
      manuscriptId,
      executionDate,
    }

    await Job.schedule({
      params,
      manuscriptId,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'academic-editor-email',
    })
  },
  async scheduleAcademicEditorRemovalNotificationForAcademicEditor({
    days,
    titleText,
    emailProps,
    journalName,
    invitationId,
    expectedDate,
    manuscriptId,
    triageEditorLabel,
    editorialAssistantLabel,
  }) {
    const executionDate = businessDays
      ? moment()
          .businessAdd(days)
          .toISOString()
      : moment()
          .add(days, timeUnit)
          .toISOString()

    const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
      journalName,
      titleText,
      expectedDate,
      triageEditorLabel,
      editorialAssistantLabel,
      emailType: `academic-editor-removed`,
    })

    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph

    const params = {
      invitationId,
      manuscriptId,
      emailProps,
      logActivity: false,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'academic-editor-email',
    })
  },
  async scheduleAcademicEditorRemovalNotificationForTriageEditor({
    days,
    invitationId,
    titleText,
    emailProps,
    expectedDate,
    manuscriptId,
    targetUserName,
  }) {
    const executionDate = businessDays
      ? moment()
          .businessAdd(days)
          .toISOString()
      : moment()
          .add(days, timeUnit)
          .toISOString()

    const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
      emailType: `triage-editor-academic-editor-removed`,
      titleText,
      expectedDate,
      targetUserName,
    })

    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph

    const params = {
      invitationId,
      manuscriptId,
      emailProps,
      logActivity: false,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'triage-editor-email',
    })
  },
  async scheduleAcademicEditorRemovalNotificationForEditorialAssistant({
    days,
    titleText,
    emailProps,
    invitationId,
    expectedDate,
    manuscriptId,
    targetUserName,
  }) {
    const executionDate = businessDays
      ? moment()
          .businessAdd(days)
          .toISOString()
      : moment()
          .add(days, timeUnit)
          .toISOString()

    const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
      emailType: `editorial-assistant-academic-editor-removed`,
      titleText,
      expectedDate,
      targetUserName,
    })

    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph

    const params = {
      invitationId,
      manuscriptId,
      emailProps,
      logActivity: false,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'editorial-assistant-email',
    })
  },
})

module.exports = { initialize }
