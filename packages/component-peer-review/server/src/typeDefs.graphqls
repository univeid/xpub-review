input RecommendationToRejectInput {
  messageForAuthor: String!
  messageForTriage: String
}

input RecommendationToPublishInput {
  messageForAuthor: String
  messageForTriage: String
}

input RevisionAutosaveInput {
  content: String
  files: [FileInput]
  meta: MetadataInput
  authors: [AuthorInput]
}

input UpdateDraftComment {
  id: String
  type: String
  content: String
}

input UpdateDraftReviewInput {
  recommendation: String
  open: String
  submitted: String
  comments: [UpdateDraftComment]
}

input ReviewerInput {
  email: String!
  givenNames: String!
  surname: String!
  aff: String
  country: String
}

extend type Query {
  getAcademicEditors(manuscriptId: String!, searchValue: String): [TeamMember]
  getTriageEditors(manuscriptId: String!): [TeamMember]
  getReviewersInvited(manuscriptId: String!): [TeamMember]
  isUserSubscribedToEmails(email: String!): Boolean
}

extend type Mutation {
  makeDecisionToPublish(manuscriptId: String!): Boolean
  makeDecisionToReject(manuscriptId: String!, content: String!): Boolean
  makeDecisionToReturn(manuscriptId: String!, content: String!): Boolean

  removeAcademicEditor(teamMemberId: String!): Boolean
  acceptAcademicEditorInvitation(teamMemberId: String!): Boolean
  cancelAcademicEditorInvitation(teamMemberId: String!): Boolean
  inviteAcademicEditor(submissionId: String!, userId: String!): Boolean
  declineAcademicEditorInvitation(
    teamMemberId: String!
    reason: String
  ): Boolean

  requestRevision(
    manuscriptId: String!
    content: String!
    type: AllowedRevisionType!
  ): Boolean
  submitRevision(submissionId: String!): Boolean
  updateDraftRevision(
    manuscriptId: String!
    autosaveInput: RevisionAutosaveInput
  ): Manuscript

  updateDraftReview(reviewId: String!, input: UpdateDraftReviewInput!): Review
  submitReview(reviewId: String!): Boolean

  makeRecommendationToReject(
    manuscriptId: String!
    input: RecommendationToRejectInput
  ): Boolean
  makeRecommendationToPublish(
    manuscriptId: String!
    input: RecommendationToPublishInput
  ): Boolean

  inviteReviewer(manuscriptId: String!, input: ReviewerInput!): Boolean
  acceptReviewerInvitation(teamMemberId: String!): Boolean
  declineReviewerInvitation(teamMemberId: String!): Boolean
  cancelReviewerInvitation(
    manuscriptId: String!
    teamMemberId: String!
  ): Boolean

  reassignTriageEditor(teamMemberId: String!, manuscriptId: String!): Boolean

  assignAcademicEditor(submissionId: String!, userId: String!): Boolean

  updatePreprintValue(manuscriptId: String!, preprintValue: String!): Boolean
}

enum AllowedRevisionType {
  minor
  major
  revision
}
