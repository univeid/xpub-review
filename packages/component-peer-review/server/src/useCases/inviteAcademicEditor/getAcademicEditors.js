const { pullAllBy } = require('lodash')

const initialize = ({ TeamMember, Team, Manuscript }) => {
  return {
    execute,
  }

  async function execute({ manuscriptId, searchValue }) {
    const manuscript = await Manuscript.find(manuscriptId)

    let members
    const hasSpecialIssueEditorialConflictOfInterest = await manuscript.getHasSpecialIssueEditorialConflictOfInterest(
      { Team, TeamMember },
    )

    if (
      manuscript.specialIssueId &&
      !hasSpecialIssueEditorialConflictOfInterest
    ) {
      members = await TeamMember.findAllBySpecialIssueAndRoleAndSearchValue({
        specialIssueId: manuscript.specialIssueId,
        role: Team.Role.academicEditor,
        searchValue,
      })
    } else {
      members = await TeamMember.findAllByJournalAndRoleAndSearchValue({
        role: Team.Role.academicEditor,
        journalId: manuscript.journalId,
        searchValue,
      })
    }

    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
    })
    members = members.filter(
      member => !authors.find(author => member.userId === author.userId),
    )

    const declinedManuscriptAcademicEditorMembers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.declined,
      },
    )

    const removedSubmissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.removed],
        submissionId: manuscript.submissionId,
      },
    )

    const academicEditors = pullAllBy(
      members,
      [
        ...declinedManuscriptAcademicEditorMembers,
        ...removedSubmissionAcademicEditorMembers,
      ],
      'userId',
    ).map(tm => tm.toDTO())

    return TeamMember.orderAcademicEditorsByScore({
      academicEditors,
      manuscriptId,
    })
  }
}

const authsomePolicies = [
  'isAuthenticated',
  'isTriageEditor',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
