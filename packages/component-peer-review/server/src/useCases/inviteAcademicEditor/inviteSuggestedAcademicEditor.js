const config = require('config')

function initialize({
  models: { Manuscript, Team, TeamMember },
  useCases: { getSuggestedAcademicEditor, inviteAcademicEditor },
}) {
  return {
    execute,
  }

  async function execute(submissionId) {
    const maxAcademicEditorAutoInvited = config.get(
      'maxAcademicEditorAutoInvited',
    )

    const manuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })

    if (!manuscript.allowAcademicEditorAutomaticInvitation) {
      return
    }
    if (manuscript.status === Manuscript.Statuses.technicalChecks) {
      throw new Error(
        `Automatic academic editor shouldn't be invited on manuscript status: ${manuscript.status}`,
      )
    }

    const academicEditors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.academicEditor,
    })

    if (academicEditors.length >= maxAcademicEditorAutoInvited) {
      manuscript.updateProperties({
        allowAcademicEditorAutomaticInvitation: false,
      })
      await manuscript.save()

      return
    }

    const invitedAcademicEditor = academicEditors.find(({ status }) =>
      [TeamMember.Statuses.pending, TeamMember.Statuses.accepted].includes(
        status,
      ),
    )

    if (invitedAcademicEditor) {
      return
    }

    const academicEditor = await getSuggestedAcademicEditor.execute({
      manuscriptId: manuscript.id,
      journalId: manuscript.journalId,
      sectionId: manuscript.sectionId,
      specialIssueId: manuscript.specialIssueId,
    })

    if (!academicEditor) {
      return
    }

    await inviteAcademicEditor.execute({
      hasWorkloadAssignment: true,
      userId: academicEditor.userId,
      submissionId: manuscript.submissionId,
    })
  }
}

module.exports = { initialize }
