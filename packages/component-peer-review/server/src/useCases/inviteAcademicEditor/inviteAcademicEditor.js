const { find, last } = require('lodash')
const config = require('config')
const { Promise } = require('bluebird')

const timeUnit = config.get(
  'reminders.academicEditor.acceptInvitation.timeUnit',
)
const removalDays = config.get(
  'reminders.academicEditor.acceptInvitation.remove',
)
const editorialAssistantLabel = config.get('roleLabels.editorialAssistant')

const initialize = ({
  logger,
  logEvent,
  jobsService,
  transaction,
  eventsService,
  emailJobsService,
  removalJobsService,
  invitationsService,
  models: { Journal, Manuscript, User, Team, TeamMember, PeerReviewModel, Job },
}) => ({
  async execute({ submissionId, userId, reqUserId, hasWorkloadAssignment }) {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      order: 'ASC',
      orderByField: 'version',
    })

    const manuscript = last(manuscripts)
    if (
      [
        Manuscript.Statuses.deleted,
        Manuscript.Statuses.withdrawn,
        Manuscript.Statuses.revisionRequested,
      ].includes(manuscript.status)
    ) {
      throw new Error(
        `Cannot invite an Academic Editor when the manuscript status is ${manuscript.status}`,
      )
    }

    // Throw early
    const manuscriptAcademicEditorMembers = await TeamMember.findAllByManuscriptAndRole(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
      },
    )
    const invitedAcademicEditor = find(manuscriptAcademicEditorMembers, [
      'userId',
      userId,
    ])

    const academicEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: Team.Role.academicEditor,
    })

    if (
      invitedAcademicEditor &&
      invitedAcademicEditor.status !== TeamMember.Statuses.expired
    ) {
      throw Error(`The ${academicEditorLabel} is already invited.`)
    }

    const pendingSubmissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        submissionId: manuscript.submissionId,
        statuses: [TeamMember.Statuses.pending],
      },
    )

    const expiredAcademicEditors = []
    if (pendingSubmissionAcademicEditorMembers.length > 0) {
      const teamMemberIds = pendingSubmissionAcademicEditorMembers.map(
        tm => tm.id,
      )
      const jobs = await Job.findAllByTeamMembers(teamMemberIds)
      await jobsService.cancelJobs(jobs)

      await Promise.each(
        pendingSubmissionAcademicEditorMembers,
        async teamMember => {
          teamMember.status = TeamMember.Statuses.expired
          expiredAcademicEditors.push(teamMember)
        },
      )
    }

    const invalidAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        submissionId: manuscript.submissionId,
        statuses: [TeamMember.Statuses.declined, TeamMember.Statuses.removed],
      },
    )

    if (find(invalidAcademicEditorMembers, ['userId', userId])) {
      throw Error(`The ${academicEditorLabel} can't be invited.`)
    }

    // Academic Editor
    const pendingAcademicEditors = []
    const academicEditorUser = await User.find(userId, 'identities')
    await Promise.each(manuscripts, async manuscript => {
      const queryObject = {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
      }
      const academicEditorTeam = await Team.findOrCreate({
        queryObject,
        options: queryObject,
      })
      const academicEditor = academicEditorTeam.addMember({
        user: academicEditorUser,
        options: {
          status: TeamMember.Statuses.pending,
          invitedDate: new Date().toISOString(),
        },
      })
      pendingAcademicEditors.push(academicEditor)
    })

    // Manuscript
    if (manuscript.status === Manuscript.Statuses.submitted) {
      manuscript.updateProperties({
        status: Manuscript.Statuses.academicEditorInvited,
      })
    }

    // update allowAcademicEditorAutomaticInvitation to false if it's a manual invitation
    if (reqUserId) {
      manuscript.updateProperties({
        allowAcademicEditorAutomaticInvitation: false,
      })
    }

    // Transaction
    const trx = await transaction.start(Manuscript.knex())
    try {
      await manuscript.$query(trx).update(manuscript)

      await Promise.each(expiredAcademicEditors, academicEditor =>
        academicEditor.$query(trx).update(academicEditor),
      )

      await Promise.each(pendingAcademicEditors, academicEditor =>
        academicEditor.$query(trx).insert(academicEditor),
      )

      await trx.commit()
    } catch (err) {
      logger.error(err)
      await trx.rollback()
      throw new Error('Something went wrong. No data was updated.')
    }

    // Notifications
    const journal = await Journal.find(manuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const triageEditor = await TeamMember.findOneByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.triageEditor,
    })
    const triageEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: Team.Role.triageEditor,
    })

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )
    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.pending,
      },
    )
    academicEditor.user = academicEditorUser

    const authorTeamMembers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.author,
      manuscriptId: manuscript.id,
    })

    invitationsService.sendInvitationToAcademicEditor({
      journal,
      manuscript,
      triageEditor,
      academicEditor,
      submittingAuthor,
      authorTeamMembers,
      editorialAssistant,
      academicEditorLabel,
    })

    emailJobsService.scheduleEmailsWhenAcademicEditorIsInvited({
      journal,
      manuscript,
      triageEditor,
      academicEditor,
      submittingAuthor,
      authorTeamMembers,
      triageEditorLabel,
      editorialAssistant,
      editorialAssistantLabel,
    })

    removalJobsService.scheduleRemovalJobForAcademicEditor({
      timeUnit,
      days: removalDays,
      manuscriptId: manuscript.id,
      invitationId: academicEditor.id,
    })

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionAcademicEditorInvited',
    })

    // Activity Log
    logEvent({
      userId: hasWorkloadAssignment ? null : reqUserId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_sent,
      objectType: logEvent.objectType.user,
      objectId: userId,
    })
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isTriageEditor',
  'admin',
  'editorialAssistant',
]

module.exports = {
  initialize,
  authsomePolicies,
}
