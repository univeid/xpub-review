const config = require('config')
const { Promise } = require('bluebird')

const editorialAssistantLabel = config.get('roleLabels.editorialAssistant')

const initialize = ({
  logEvent,
  jobsService,
  eventsService,
  emailJobsService,
  notificationService,
  models: {
    Job,
    Team,
    User,
    Review,
    Journal,
    Manuscript,
    TeamMember,
    ArticleType,
    PeerReviewModel,
  },
}) => ({
  async execute({ teamMemberId, userId }) {
    const academicEditorMember = await TeamMember.find(teamMemberId)
    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)
    const responded = new Date()

    // Academic Editor
    const submissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRoleAndUser(
      {
        userId,
        role: Team.Role.academicEditor,
        submissionId: manuscript.submissionId,
      },
    )

    await Promise.each(submissionAcademicEditorMembers, async tm => {
      tm.updateProperties({ status: TeamMember.Statuses.accepted, responded })
      await tm.save()
    })

    // Manuscript
    const articleType = await ArticleType.findArticleTypeByManuscript(
      manuscript.id,
    )

    const { hasTriageEditorConflictOfInterest } = manuscript
    const hasAcademicEditorConflictOfInterest = await Review.findOneInvalidByManuscriptAndRole(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
      },
    )

    if (hasTriageEditorConflictOfInterest) {
      manuscript.updateStatus(Manuscript.Statuses.makeDecision)
      await manuscript.save()
    }

    if (
      !hasAcademicEditorConflictOfInterest &&
      !hasTriageEditorConflictOfInterest &&
      manuscript.status !== Manuscript.Statuses.revisionRequested
    ) {
      const newStatus = articleType.hasPeerReview
        ? Manuscript.Statuses.academicEditorAssigned
        : Manuscript.Statuses.academicEditorAssignedEditorialType
      manuscript.updateStatus(newStatus)
      await manuscript.save()
    }

    // Jobs
    const academicEditorMemberJobs = await Job.findAllByTeamMember(
      academicEditorMember.id,
    )
    await jobsService.cancelJobs(academicEditorMemberJobs)

    // Notifications
    const journal = await Journal.find(manuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    academicEditorMember.user = await User.find(academicEditorMember.userId)

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    manuscript.articleType = articleType
    manuscript.journal = journal

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    if (triageEditor) {
      triageEditor.user = await User.find(triageEditor.userId)
    }

    const academicEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: Team.Role.academicEditor,
    })

    const peerReviewModel = await PeerReviewModel.findOneByManuscriptParent(
      manuscript,
    )

    if (peerReviewModel.hasTriageEditor) {
      notificationService.notifyTriageEditor({
        journal,
        manuscript,
        triageEditor,
        submittingAuthor,
        editorialAssistant,
        academicEditorLabel,
        academicEditor: academicEditorMember,
      })
    } else {
      notificationService.notifyEditorialAssistant({
        journal,
        manuscript,
        submittingAuthor,
        editorialAssistant,
        academicEditor: academicEditorMember,
      })
    }
    if (
      hasAcademicEditorConflictOfInterest ||
      hasTriageEditorConflictOfInterest
    ) {
      notificationService.notifyPendingAcademicEditorAfterConflictOfInterests({
        journal,
        manuscript,
        editorialAssistant,
        academicEditor: academicEditorMember,
      })
    } else {
      notificationService.notifyPendingAcademicEditor({
        journal,
        manuscript,
        editorialAssistant,
        academicEditor: academicEditorMember,
      })
    }

    const triageEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: Team.Role.triageEditor,
    })
    emailJobsService.sendAcademicEditorRemindersToInviteReviewers({
      manuscript,
      triageEditor,
      editorialAssistant,
      triageEditorLabel,
      editorialAssistantLabel,
      journalName: journal.name,
      user: academicEditorMember,
      submittingAuthorName: submittingAuthor.getName(),
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionAcademicEditorAccepted',
    })

    // Activity Log
    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_agreed,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

module.exports = { initialize }
