const config = require('config')
const { Promise } = require('bluebird')

const editorialAssistantLabel = config.get('roleLabels.editorialAssistant')

const initialize = ({
  logEvent,
  jobsService,
  eventsService,
  emailJobsService,
  notificationService,
  models: {
    Job,
    Team,
    User,
    Review,
    Journal,
    Manuscript,
    TeamMember,
    ArticleType,
    PeerReviewModel,
  },
}) => ({
  async execute({ teamMemberId, userId }) {
    const pendingAcademicEditor = await TeamMember.find(teamMemberId)
    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)
    const responded = new Date()

    // Academic Editors
    const activeAcademicEditors = await TeamMember.findAllBySubmissionAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.accepted],
        submissionId: manuscript.submissionId,
      },
    )
    const activeAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
        manuscriptId: manuscript.id,
      },
    )
    await Promise.each(activeAcademicEditors, async tm => {
      tm.updateProperties({ status: TeamMember.Statuses.removed })
      await tm.save()
    })

    const pendingAcademicEditors = await TeamMember.findAllBySubmissionAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.pending],
        submissionId: manuscript.submissionId,
      },
    )
    await Promise.each(pendingAcademicEditors, async tm => {
      tm.updateProperties({ status: TeamMember.Statuses.accepted, responded })
      await tm.save()
    })

    // Jobs
    const pendingAcademicEditorJobs = await Job.findAllByTeamMember(
      pendingAcademicEditor.id,
    )
    await jobsService.cancelJobs(pendingAcademicEditorJobs)

    const activeAcademicEditorJobs = await Job.findAllByTeamMember(
      activeAcademicEditor.id,
    )
    await jobsService.cancelJobs(activeAcademicEditorJobs)

    // Notifications
    const journal = await Journal.find(manuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    pendingAcademicEditor.user = await User.find(pendingAcademicEditor.userId)

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    manuscript.articleType = await ArticleType.find(manuscript.articleTypeId)
    manuscript.journal = journal

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    if (triageEditor) {
      triageEditor.user = await User.find(triageEditor.userId)
    }

    const academicEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: Team.Role.academicEditor,
    })

    const peerReviewModel = await PeerReviewModel.findOneByManuscriptParent(
      manuscript,
    )

    if (peerReviewModel.hasTriageEditor) {
      notificationService.notifyTriageEditor({
        journal,
        manuscript,
        triageEditor,
        submittingAuthor,
        editorialAssistant,
        academicEditorLabel,
        academicEditor: pendingAcademicEditor,
      })
    } else {
      notificationService.notifyEditorialAssistant({
        journal,
        manuscript,
        submittingAuthor,
        editorialAssistant,
        academicEditor: pendingAcademicEditor,
      })
    }

    if (manuscript.articleType.hasPeerReview) {
      notificationService.notifyActiveAcademicEditor({
        journal,
        manuscript,
        submittingAuthor,
        editorialAssistant,
        academicEditor: activeAcademicEditor,
        triageEditor,
        submittingAuthorName: submittingAuthor.getName(),
        academicEditorLabel,
      })

      notificationService.notifyPendingAcademicEditor({
        journal,
        manuscript,
        editorialAssistant,
        academicEditor: pendingAcademicEditor,
      })

      const triageEditorLabel = await manuscript.getEditorLabel({
        Team,
        TeamMember,
        PeerReviewModel,
        role: Team.Role.triageEditor,
      })
      emailJobsService.sendAcademicEditorRemindersToInviteReviewers({
        manuscript,
        triageEditor,
        editorialAssistant,
        triageEditorLabel,
        editorialAssistantLabel,
        journalName: journal.name,
        user: pendingAcademicEditor,
        submittingAuthorName: submittingAuthor.getName(),
      })
    }

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionAcademicEditorRemoved',
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionAcademicEditorAccepted',
    })

    // Activity Log
    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_agreed,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
    logEvent({
      userId: null,
      manuscriptId: manuscript.id,
      action: logEvent.actions.role_academic_editor_reassigned,
      objectType: logEvent.objectType.user,
      objectId: userId,
    })
  },
})

module.exports = { initialize }
