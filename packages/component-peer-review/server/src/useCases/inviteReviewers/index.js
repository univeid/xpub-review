const getReviewersInvitedUseCase = require('./getReviewersInvited')

const inviteReviewerUseCase = require('./inviteReviewer')
const acceptReviewerInvitationUseCase = require('./acceptInvitation')
const declineReviewerInvitationUseCase = require('./declineInvitation')
const cancelReviewerInvitationUseCase = require('./cancelInvitation')

const findOrCreateReviewerUseCase = require('./findOrCreateReviewer')
const isUserSubscribedToEmailsUseCase = require('./isUserSubscribedToEmails')
const findOrCreateReviewerUserUseCase = require('./findOrCreateReviewerUser')
const handleEmailsWhenReviewerIsInvitedUseCase = require('./handleEmailsWhenReviewerIsInvited')
const updateReviewerSuggestionInvitedStatusUseCase = require('./updateReviewerSuggestionInvitedStatus')
const dontAllowEditorsAsReviewersOnSpecialIssuesUseCase = require('./dontAllowEditorsAsReviewersOnSpecialIssues')
const cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase = require('./cancelJobsWhenMinimumNumberOfReviewersIsMet')
const dontAllowReviewersToHaveMultipleRolesOnManuscriptUseCase = require('./dontAllowReviewersToHaveMultipleRolesOnManuscriptUseCase')

module.exports = {
  inviteReviewerUseCase,
  getReviewersInvitedUseCase,
  findOrCreateReviewerUseCase,
  isUserSubscribedToEmailsUseCase,
  findOrCreateReviewerUserUseCase,
  acceptReviewerInvitationUseCase,
  cancelReviewerInvitationUseCase,
  declineReviewerInvitationUseCase,
  handleEmailsWhenReviewerIsInvitedUseCase,
  updateReviewerSuggestionInvitedStatusUseCase,
  dontAllowEditorsAsReviewersOnSpecialIssuesUseCase,
  cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase,
  dontAllowReviewersToHaveMultipleRolesOnManuscriptUseCase,
}
