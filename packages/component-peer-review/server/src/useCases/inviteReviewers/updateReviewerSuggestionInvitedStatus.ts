interface ReviewerSuggestion {
  isInvited: boolean
  save(): Promise<void>
}
interface ReviewerSuggestionType {
  findOneBy({ queryObject }): Promise<ReviewerSuggestion>
}

const initialize = (ReviewerSuggestion: ReviewerSuggestionType) => ({
  async execute(email: string, manuscriptId: string) {
    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email,
        manuscriptId,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = true
      await reviewerSuggestion.save()
    }
  },
})

export { initialize }
