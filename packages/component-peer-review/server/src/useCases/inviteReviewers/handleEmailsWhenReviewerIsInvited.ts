interface ManuscriptType {
  journalId: string
  id: string
  getEditorLabel({ Team, TeamMember, PeerReviewModel, role }): Promise<string>
}
interface TeamRoleType {
  author: string
  academicEditor: string
}

const initialize = ({
  models: { Journal, TeamMember, Team, PeerReviewModel },
  invitationsService,
  emailJobsService,
}) => ({
  async execute(
    manuscript: ManuscriptType,
    staffMember: object,
    academicEditor: object,
    reviewer: object,
    TeamRole: TeamRoleType,
  ) {
    const journal = await Journal.find(manuscript.journalId)
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )
    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: TeamRole.author,
    })

    const academicEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: TeamRole.academicEditor,
    })
    await invitationsService.sendInvitationToReviewer({
      authors,
      journal,
      reviewer,
      manuscript,
      academicEditor,
      academicEditorLabel,
      editorialAssistant: staffMember,
      submittingAuthorName: submittingAuthor.getName(),
    })

    await emailJobsService.scheduleEmailsWhenReviewerIsInvited({
      journal,
      authors,
      reviewer,
      manuscript,
      academicEditor,
      editorialAssistant: staffMember,
      submittingAuthorName: submittingAuthor.getName(),
    })
  },
})

export { initialize }
