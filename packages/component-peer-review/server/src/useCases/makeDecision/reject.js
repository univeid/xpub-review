const initialize = ({
  models,
  logger,
  logEvent,
  useCases,
  transaction,
  jobsService,
  eventsService,
  notificationService,
  notifyAcademicEditor,
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const { Team, TeamMember, Manuscript } = models
    const manuscript = await Manuscript.find(manuscriptId, 'articleType')

    const nonRejectableStatuses = Manuscript.NonActionableStatuses
    if (nonRejectableStatuses.includes(manuscript.status)) {
      throw new ValidationError(
        `Cannot reject a manuscript in the current status.`,
      )
    }

    let rejectEditor = await TeamMember.findOneByManuscriptAndUser({
      manuscriptId,
      userId,
    })
    if (!rejectEditor) {
      rejectEditor = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    let useCase = 'rejectWithPeerReviewUseCase'
    if (!manuscript.articleType.hasPeerReview)
      useCase = 'rejectWithNoPeerReviewUseCase'

    await useCases[useCase]
      .initialize({
        logger,
        models,
        transaction,
        jobsService,
        notificationService,
        notifyAcademicEditor,
      })
      .execute({ manuscript, rejectEditor, content })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionRejected',
    })

    logEvent({
      userId,
      manuscriptId,
      objectId: manuscriptId,
      objectType: logEvent.objectType.manuscript,
      action: logEvent.actions.manuscript_rejected,
    })
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isApprovalEditor',
  'editorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
