const makeDecisionToPublishUseCase = require('./publish')
const makeDecisionToRejectUseCase = require('./reject')
const rejectWithPeerReviewUseCase = require('./reject/rejectWithPeerReview')
const rejectWithNoPeerReviewUseCase = require('./reject/rejectWithNoPeerReview')
const makeDecisionToReturnUseCase = require('./return')

module.exports = {
  makeDecisionToPublishUseCase,
  makeDecisionToRejectUseCase,
  rejectWithPeerReviewUseCase,
  rejectWithNoPeerReviewUseCase,
  makeDecisionToReturnUseCase,
}
