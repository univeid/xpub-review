const uuid = require('uuid')
const Promise = require('bluebird')

const initialize = ({
  logger,
  logEvent,
  transaction,
  jobsService,
  eventsService,
  notificationService,
  models: { Journal, Manuscript, Team, TeamMember, Review, Comment, Job },
}) => ({
  async execute({ manuscriptId, userId }) {
    const manuscript = await Manuscript.find(manuscriptId, 'articleType')

    const nonPublishableStatuses = Manuscript.NonActionableStatuses
    if (nonPublishableStatuses.includes(manuscript.status)) {
      throw new ValidationError(
        `Cannot publish a manuscript in the current status.`,
      )
    }

    let publishEditor = await TeamMember.findOneByManuscriptAndUser({
      userId,
      manuscriptId,
    })
    if (!publishEditor) {
      publishEditor = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const review = new Review({
      manuscriptId,
      teamMemberId: publishEditor.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations.publish,
    })

    if (manuscript.hasPassedEqa) {
      manuscript.updateProperties({
        status: Manuscript.Statuses.accepted,
        acceptedDate: new Date().toISOString(),
      })
    } else {
      manuscript.updateProperties({
        status: Manuscript.Statuses.inQA,
        technicalCheckToken: uuid.v4(),
        acceptedDate: new Date().toISOString(),
      })
    }

    const pendingReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.pending,
      },
    )
    const acceptedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.accepted,
      },
    )
    const expiredReviewers = [...pendingReviewers, ...acceptedReviewers].map(
      reviewer => {
        reviewer.status = TeamMember.Statuses.expired
        return reviewer
      },
    )

    // Transaction
    const trx = await transaction.start(Manuscript.knex())
    try {
      const trxManuscript = await manuscript
        .$query(trx)
        .updateAndFetch(manuscript)
      await trxManuscript.$relatedQuery('reviews', trx).insert(review)
      await Promise.each(expiredReviewers, reviewer =>
        reviewer.$query(trx).update(reviewer),
      )

      await trx.commit()
    } catch (err) {
      logger.error(err)
      await trx.rollback()
      throw new Error('Something went wrong. No data was updated.')
    }

    // Data queries
    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const academicEditors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.academicEditor,
    })
    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.reviewer,
    })
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    // Jobs
    const editorialAssistantJobs = await Job.findAllByTeamMemberAndManuscript({
      teamMemberId: editorialAssistant.id,
      manuscriptId,
    })
    const academicEditorsJobs = await Job.findAllByTeamMembers(
      academicEditors.map(academicEditor => academicEditor.id),
    )
    const reviewersJobs = await Job.findAllByTeamMembers(
      reviewers.map(reviewer => reviewer.id),
    )
    jobsService.cancelJobs(editorialAssistantJobs)
    jobsService.cancelJobs(academicEditorsJobs)
    jobsService.cancelJobs(reviewersJobs)

    // Notifications
    notificationService.notifyEQA({
      manuscript,
      publishingMember: publishEditor,
      submittingAuthor,
      editorialAssistant,
      journalName: journal.name,
    })

    if (
      journal.peerReviewModel.approvalEditors.includes(Team.Role.triageEditor)
    ) {
      const triageEditor = await TeamMember.findTriageEditor({
        TeamRole: Team.Role,
        journalId: journal.id,
        manuscriptId: manuscript.id,
        sectionId: manuscript.sectionId,
      })
      notificationService.notifyTriageEditorForSubmittedReport({
        journal,
        manuscript,
        triageEditor,
        articleTypeName: manuscript.articleType.name,
        submittingAuthor,
        editorialAssistant,
      })
    }

    if (
      journal.peerReviewModel.approvalEditors.includes(Team.Role.academicEditor)
    ) {
      const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
        {
          manuscriptId: manuscript.id,
          role: Team.Role.academicEditor,
          status: TeamMember.Statuses.accepted,
        },
      )
      notificationService.notifyAcademicEditorForSubmittedReport({
        journal,
        manuscript,
        academicEditor,
        articleTypeName: manuscript.articleType.name,
        submittingAuthor,
        editorialAssistant,
      })
    }

    // Events and Logs
    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionAccepted',
    })
    logEvent({
      userId,
      manuscriptId,
      objectId: manuscriptId,
      objectType: logEvent.objectType.manuscript,
      action: logEvent.actions.manuscript_accepted,
    })
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isApprovalEditor',
  'editorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
