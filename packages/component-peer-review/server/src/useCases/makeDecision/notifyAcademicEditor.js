module.exports = {
  async notifyAcademicEditor({
    Team,
    User,
    Review,
    journal,
    manuscript,
    TeamMember,
    approvalEditor,
    submittingAuthor,
    editorialAssistant,
    notificationService,
  }) {
    const latestRecommendation = await Review.findLatestEditorialReview({
      manuscriptId: manuscript.id,
      TeamRole: Team.Role,
    })
    if (!latestRecommendation) {
      return
    }

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    if (!academicEditor) {
      return
    }

    academicEditor.user = await User.find(academicEditor.userId)

    if (
      !journal.peerReviewModel.approvalEditors.includes(
        Team.Role.academicEditor,
      )
    ) {
      notificationService.notifyAcademicEditor({
        journal,
        manuscript,
        approvalEditor,
        academicEditor,
        submittingAuthor,
        editorialAssistant,
      })
    } else {
      notificationService.notifyAcademicEditorForSubmittedReport({
        journal,
        manuscript,
        academicEditor,
        articleTypeName: manuscript.articleType.name,
        submittingAuthor,
        editorialAssistant,
      })
    }
  },
}
