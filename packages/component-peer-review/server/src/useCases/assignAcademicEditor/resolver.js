const config = require('config')
const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')
const events = require('component-events')

const useCases = require('./index')
const urlService = require('../../urlService/urlService')

const jobs = require('../../jobsService/jobsService')

const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/inviteAcademicEditor/getEmailCopy')
const acceptInvitationNotifications = require('../../notifications/inviteAcademicEditor/acceptInvitation')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const resolver = {
  Mutation: {
    async assignAcademicEditor(_, { submissionId, userId }, ctx) {
      const eventsService = events.initialize({ models })

      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })

      const jobsService = jobs.initialize({ models })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = acceptInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      return useCases.assignAcademicEditorUseCase
        .initialize({
          models,
          useCases,
          logEvent,
          jobsService,
          eventsService,
          notificationService,
        })
        .execute({ submissionId, userId, reqUserId: ctx.user })
    },
  },
}

module.exports = resolver
