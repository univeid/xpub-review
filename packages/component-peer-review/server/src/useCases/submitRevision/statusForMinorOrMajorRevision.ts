const initialize = ({ TeamMember, Team, Manuscript, Review }) => ({
  async execute(reviewRecommendation, revisionManuscript) {
    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.reviewer,
      manuscriptId: revisionManuscript.id,
    })

    if (reviewers.length === 0)
      return Manuscript.Statuses.academicEditorAssigned

    if (reviewRecommendation === Review.Recommendations.minor)
      return Manuscript.Statuses.reviewCompleted

    return Manuscript.Statuses.underReview
  },
})

export { initialize }
