const BlueBirdPromise = require('bluebird')
const config = require('config')

const removalDays = config.get('reminders.reviewer.acceptInvitation.remove')
const timeUnit = config.get('reminders.reviewer.acceptInvitation.timeUnit')

const initialize = ({
  models: { TeamMember, Team },
  logEvent,
  emailJobsService,
  invitationsService,
  removalJobsService,
}) => ({
  async execute(
    draftManuscript,
    journal,
    academicEditor,
    editorialAssistant,
    submittingAuthor,
  ) {
    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: draftManuscript.id,
      role: Team.Role.reviewer,
      eagerLoadRelations: 'user.identities',
    })
    if (!reviewers) return

    const authors = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.author,
      manuscriptId: draftManuscript.id,
    })

    invitationsService.sendInvitationToReviewerAfterMajorRevision({
      journal,
      authors,
      academicEditor,
      submittingAuthor,
      manuscript: draftManuscript,
      editorialAssistant,
      reviewers,
    })

    await BlueBirdPromise.each(reviewers, reviewer =>
      logEvent({
        userId: null,
        manuscriptId: draftManuscript.id,
        action: logEvent.actions.reviewer_invited,
        objectType: logEvent.objectType.user,
        objectId: reviewer.userId,
      }),
    )
    await BlueBirdPromise.each(reviewers, async reviewer => {
      if (reviewer.user && reviewer.user.isSubscribedToEmails) {
        await emailJobsService.scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision(
          {
            manuscript: draftManuscript,
            reviewer,
            authors,
            academicEditor,
            editorialAssistant,
            submittingAuthorName: submittingAuthor.getName(),
          },
        )
      }

      await removalJobsService.scheduleRemovalJob({
        timeUnit,
        days: removalDays,
        invitationId: reviewer.id,
        manuscriptId: draftManuscript.id,
      })
    })
  },
})

export { initialize }
