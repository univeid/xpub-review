const { transaction } = require('objection')

const initialize = ({
  models,
  logger,
  logEvent,
  eventsService,
  getManuscriptStatus,
  notifyApprovalEditor,
  copyReviewersToNewVersion,
  getReviewersChangedToExpired,
  sendNewReviewersInvitationAndScheduleReminders,
}) => ({
  async execute({ submissionId, userId }) {
    const {
      Team,
      Review,
      Journal,
      TeamMember,
      Manuscript,
      ArticleType,
      PeerReviewModel,
    } = models

    const revisionManuscript = await Manuscript.findOneBy({
      queryObject: {
        submissionId,
        status: Manuscript.Statuses.revisionRequested,
      },
    })
    const journal = await Journal.find(revisionManuscript.journalId)

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: revisionManuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    const draftManuscript = await Manuscript.findOneBy({
      queryObject: {
        submissionId,
        status: Manuscript.Statuses.draft,
      },
    })
    draftManuscript.updateIsLatestVersionFlag(true) // set the flag to true. it will be saved under each corresponding sub-use cases
    revisionManuscript.updateStatus(Manuscript.Statuses.olderVersion)
    revisionManuscript.updateIsLatestVersionFlag(false)

    const reviewRecommendation = await Review.findLatestEditorialReview({
      manuscriptId: revisionManuscript.id,
      TeamRole: Team.Role,
    })
    if (!reviewRecommendation) {
      throw new Error('No recommendation found')
    }

    const responseToRevision = await Review.findOneBy({
      queryObject: {
        recommendation: Review.Recommendations.responseToRevision,
        manuscriptId: draftManuscript.id,
      },
    })
    if (responseToRevision) {
      responseToRevision.setSubmitted(new Date().toISOString())
    }

    const approvalEditorRole = await TeamMember.getApprovalEditorRole({
      ArticleType,
      PeerReviewModel,
      TeamRole: Team.Role,
      manuscript: revisionManuscript,
    })

    draftManuscript.status = await getManuscriptStatus.execute(
      revisionManuscript,
      reviewRecommendation,
      approvalEditorRole,
    )

    let pendingReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: revisionManuscript.id,
        status: TeamMember.Statuses.pending,
      },
    )
    if (pendingReviewers) {
      pendingReviewers = await getReviewersChangedToExpired.execute(
        pendingReviewers,
      )
    }

    let newSubmittedReviewers
    if (reviewRecommendation === Review.Recommendations.major) {
      const submittedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
        {
          role: Team.Role.reviewer,
          manuscriptId: revisionManuscript.id,
          status: TeamMember.Statuses.submitted,
        },
      )

      if (submittedReviewers) {
        newSubmittedReviewers = await copyReviewersToNewVersion.execute(
          draftManuscript,
          submittedReviewers,
        )
      }
    }

    try {
      await transaction(Manuscript.knex(), async trx => {
        await Manuscript.query(trx).upsertGraph(draftManuscript)
        await Manuscript.query(trx).upsertGraph(revisionManuscript)
        await Review.query(trx).upsertGraph(responseToRevision)
        !!pendingReviewers.length &&
          (await TeamMember.query(trx).upsertGraph(pendingReviewers, {
            insertMissing: true,
            relate: true,
          }))
        newSubmittedReviewers &&
          TeamMember.query(trx).upsertGraph(newSubmittedReviewers, {
            insertMissing: true,
            relate: true,
          })
      })
    } catch (err) {
      logger.error(err)
      throw new Error('Something went wrong. No data was updated.')
    }

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: revisionManuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
        eagerLoadRelations: 'user',
      },
    )
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      draftManuscript.id,
    )
    notifyApprovalEditor.execute(
      approvalEditorRole,
      draftManuscript,
      journal,
      academicEditor,
      editorialAssistant,
      submittingAuthor,
    )

    sendNewReviewersInvitationAndScheduleReminders.execute(draftManuscript)

    eventsService.publishSubmissionEvent({
      submissionId: draftManuscript.submissionId,
      eventName: 'SubmissionRevisionSubmitted',
    })

    logEvent({
      objectId: revisionManuscript.id,
      userId,
      manuscriptId: revisionManuscript.id,
      objectType: logEvent.objectType.manuscript,
      action: logEvent.actions.revision_submitted,
    })
  },
})

const authsomePolicies = ['hasAccessToSubmission', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
