const initialize = ({ TeamMember, Team }) => ({
  async execute(draftManuscript, reviewers) {
    const queryObject = {
      role: Team.Role.reviewer,
      manuscriptId: draftManuscript.id,
    }
    const newReviewerTeam = await Team.findOrCreate({
      queryObject,
      options: queryObject,
    })
    if (!newReviewerTeam) return undefined

    const newReviewers = []
    reviewers.forEach(reviewer => {
      const newMember = new TeamMember({
        ...reviewer,
        teamId: newReviewerTeam.id,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        status: TeamMember.Statuses.pending,
      })
      delete newMember.id
      newReviewers.push(reviewer)
    })

    return newReviewers
  },
})

export { initialize }
