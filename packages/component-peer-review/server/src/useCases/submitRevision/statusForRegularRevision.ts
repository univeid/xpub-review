const initialize = ({ Team, TeamMember, Manuscript }) => ({
  async execute(revisionManuscript, approvalEditorRole) {
    if (approvalEditorRole !== Team.Role.academicEditor) {
      return Manuscript.Statuses.submitted
    }

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.pending, TeamMember.Statuses.accepted],
        manuscriptId: revisionManuscript.id,
      },
    )

    if (!academicEditor) {
      return Manuscript.Statuses.submitted
    }

    return Manuscript.Statuses.academicEditorAssigned
  },
})

export { initialize }
