const initialize = ({ models: { Team, TeamMember }, notificationService }) => ({
  async execute(
    approvalEditorRole,
    draftManuscript,
    journal,
    academicEditor,
    editorialAssistant,
    submittingAuthor,
  ) {
    if (!academicEditor && approvalEditorRole === Team.Role.academicEditor)
      return

    if (approvalEditorRole === Team.Role.triageEditor) {
      const triageEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
        {
          manuscriptId: draftManuscript.id,
          role: Team.Role.triageEditor,
          status: TeamMember.Statuses.active,
          eagerLoadRelations: 'user',
        },
      )
      notificationService.notifyTriageEditor({
        draftManuscript,
        journal,
        triageEditor,
        submittingAuthor,
        editorialAssistant,
      })
      return
    }
    notificationService.notifyAcademicEditor({
      draftManuscript,
      journal,
      academicEditor,
      submittingAuthor,
      editorialAssistant,
    })
  },
})

export { initialize }
