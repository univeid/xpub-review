enum ReviewRecommendations {
  revision = 'revision',
  minor = 'minor',
  major = 'major',
}

const initialize = ({
  models: { Review },
  statusForRegularRevisionUseCase,
  statusForMinorOrMajorRevisionUseCase,
}) => ({
  async execute(
    revisionManuscript,
    reviewRecommendation: ReviewRecommendations,
    approvalEditorRole,
  ) {
    if (reviewRecommendation === Review.Recommendations.revision) {
      return statusForRegularRevisionUseCase.execute(
        revisionManuscript,
        approvalEditorRole,
      )
    }

    return statusForMinorOrMajorRevisionUseCase.execute(
      reviewRecommendation,
      revisionManuscript,
    )
  },
})

export { initialize }
