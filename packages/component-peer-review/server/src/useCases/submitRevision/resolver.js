const useCases = require('./index')
const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const Email = require('@pubsweet/component-email-templating')
const config = require('config')
const { getModifiedText } = require('component-transform-text')
const events = require('component-events')
const logger = require('@pubsweet/logger')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const urlService = require('../../urlService/urlService')
const emailJobs = require('../../emailJobs/reviewer/emailJobs')
const removalJobs = require('../../removalJobs/pendingReviewers')
const invitationsReviewer = require('../../invitations/invitations')
const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/submitRevision/getEmailCopy')
const submitRevisionNotifications = require('../../notifications/submitRevision/submit')

const resolver = {
  Query: {},
  Mutation: {
    async submitRevision(_, { submissionId }, ctx) {
      const eventsService = events.initialize({ models })
      const invitationsService = invitationsReviewer.initialize({ Email })
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = submitRevisionNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })
      const { TeamMember, ReviewerSuggestion, Job } = models
      const emailJobsService = emailJobs.initialize({
        Job,
        Email,
        logEvent,
        getPropsService,
      })
      const removalJobsService = removalJobs.initialize({
        Job,
        logEvent,
        TeamMember,
        ReviewerSuggestion,
      })

      const statusForMinorOrMajorRevisionUseCase = useCases.statusForMinorOrMajorRevisionUseCase.initialize(
        models,
      )
      const statusForRegularRevisionUseCase = useCases.statusForRegularRevisionUseCase.initialize(
        models,
      )
      const getManuscriptStatus = useCases.getManuscriptStatusUseCase.initialize(
        {
          models,
          statusForRegularRevisionUseCase,
          statusForMinorOrMajorRevisionUseCase,
        },
      )
      const getReviewersChangedToExpired = useCases.getReviewersChangedToExpiredUseCase.initialize(
        models,
      )
      const copyReviewersToNewVersion = useCases.copyReviewersToNewVersionUseCase.initialize(
        models,
      )
      const notifyApprovalEditor = useCases.notifyApprovalEditorUseCase.initialize(
        { models, notificationService },
      )

      const sendNewReviewersInvitationAndScheduleReminders = useCases.sendNewReviewersInvitationAndScheduleRemindersUseCase.initialize(
        {
          models,
          emailJobsService,
          removalJobsService,
          invitationsService,
          logEvent,
        },
      )

      return useCases.submitRevisionUseCase
        .initialize({
          models,
          logger,
          logEvent,
          eventsService,
          getManuscriptStatus,
          notifyApprovalEditor,
          copyReviewersToNewVersion,
          getReviewersChangedToExpired,
          sendNewReviewersInvitationAndScheduleReminders,
        })
        .execute({ submissionId, userId: ctx.user })
    },
    async updateDraftRevision(_, { manuscriptId, autosaveInput }, ctx) {
      return useCases.updateDraftRevisionUseCase
        .initialize(models)
        .execute({ manuscriptId, autosaveInput })
    },
  },
}

module.exports = resolver
