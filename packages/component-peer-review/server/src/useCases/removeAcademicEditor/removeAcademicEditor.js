const Promise = require('bluebird')

const initialize = ({
  models: {
    Job,
    Team,
    Review,
    Journal,
    TeamMember,
    Manuscript,
    SpecialIssue,
    ReviewerSuggestion,
  },
  logEvent,
  jobsService,
  notificationService,
}) => ({
  async execute({ teamMemberId, userId }) {
    const academicEditorMember = await TeamMember.find(teamMemberId)
    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)

    // Throw early
    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    // Reviewers
    const reviewerTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
      },
    })
    const reviewerMembers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.reviewer,
      manuscriptId: manuscript.id,
      eagerLoadRelations: 'user',
    })
    if (reviewerTeam) await reviewerTeam.delete()

    // Reviewer Suggestions
    const reviewerSuggestions = await ReviewerSuggestion.findBy({
      manuscriptId: manuscript.id,
    })
    await Promise.each(reviewerSuggestions, async reviewerSuggestion => {
      reviewerSuggestion.updateProperties({ isInvited: false })
      await reviewerSuggestion.save()
    })

    // Academic Editor Recommendation
    const academicEditorRecommendation = await Review.findOneBy({
      queryObject: {
        manuscriptId: manuscript.id,
        teamMemberId,
      },
    })
    if (academicEditorRecommendation) {
      await academicEditorRecommendation.delete()
    }

    // Academic Editors
    const submissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRole(
      {
        role: Team.Role.academicEditor,
        submissionId: manuscript.submissionId,
      },
    )
    await Promise.each(submissionAcademicEditorMembers, member => {
      member.updateProperties({ status: TeamMember.Statuses.removed })
      return member.save()
    })

    // Draft Manuscript
    const draftManuscript = await Manuscript.findOneBy({
      queryObject: {
        status: Manuscript.Statuses.draft,
        submissionId: manuscript.submissionId,
      },
    })
    if (draftManuscript) await draftManuscript.delete()

    // Manuscript
    manuscript.updateStatus(Manuscript.Statuses.submitted)
    await manuscript.save()

    // Jobs
    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const admin = await TeamMember.findOneByRole({
      role: Team.Role.admin,
    })

    let staffMember = editorialAssistant
    if (!editorialAssistant) staffMember = admin
    const staffMemberJobs = await Job.findAllByTeamMember(staffMember.id)
    await jobsService.cancelStaffMemberJobs({
      staffMemberJobs,
      manuscriptId: manuscript.id,
    })

    const reviewerAndAcademicEditorJobs = await Job.findAllByTeamMembers([
      teamMemberId,
      ...reviewerMembers.map(r => r.id),
    ])
    await jobsService.cancelJobs(reviewerAndAcademicEditorJobs)

    // Notifications
    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    let specialIssue
    if (manuscript.specialIssueId) {
      specialIssue = await SpecialIssue.find(
        manuscript.specialIssueId,
        'peerReviewModel',
      )
    }
    const triageEditorLabel = specialIssue
      ? specialIssue.peerReviewModel.triageEditorLabel
      : journal.peerReviewModel.triageEditorLabel
    const academicEditorLabel = specialIssue
      ? specialIssue.peerReviewModel.academicEditorLabel
      : journal.peerReviewModel.academicEditorLabel

    notificationService.notifyAuthor({
      journal,
      manuscript,
      triageEditor,
      submittingAuthor,
      triageEditorLabel,
      editorialAssistant,
      academicEditorLabel,
    })

    notificationService.notifyAcademicEditor({
      journal,
      manuscript,
      triageEditor,
      triageEditorLabel,
      editorialAssistant,
      academicEditor: academicEditorMember,
    })

    notificationService.notifyReviewers({
      journal,
      manuscript,
      triageEditor,
      editorialAssistant,
      academicEditorLabel,
      reviewers: reviewerMembers,
    })

    // Activity Log
    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.academic_editor_removed,
      objectType: logEvent.objectType.user,
      objectId: academicEditorMember.userId,
    })
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isTriageEditor',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
