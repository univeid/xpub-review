const config = require('config')
const useCases = require('./index')
const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')
const { s3service } = require('component-files/server')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const {
  useCases: {
    createMajorVersion,
    copyFilesBetweenManuscripts,
    copyAuthorsBetweenManuscripts,
    copyEAsBetweenManuscripts,
    copyTEsBetweenManuscripts,
    copyAEsBetweenManuscripts,
  },
} = require('component-model')

const urlService = require('../../urlService/urlService')
const requestRevisionNotifications = require('../../notifications/requestRevision/requestRevision')

const JobsService = require('../../jobsService/jobsService')
const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/requestRevision/getEmailCopy')
const events = require('component-events')

const resolver = {
  Query: {},
  Mutation: {
    async requestRevision(_, { manuscriptId, type, content }, ctx) {
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = requestRevisionNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })
      const jobsService = JobsService.initialize({ models })
      const eventsService = events.initialize({ models })

      return useCases.requestRevisionUseCase
        .initialize({
          models,
          logEvent,
          useCases: {
            ...useCases,
            createMajorVersion,
            copyFilesBetweenManuscripts,
            copyAuthorsBetweenManuscripts,
            copyEAsBetweenManuscripts,
            copyTEsBetweenManuscripts,
            copyAEsBetweenManuscripts,
          },
          services: {
            jobsService,
            eventsService,
            notificationService,
            s3service,
          },
        })
        .execute({ manuscriptId, userId: ctx.user, type, content })
    },
  },
}

module.exports = resolver
