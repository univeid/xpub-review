const Promise = require('bluebird')

const initialize = ({ models, useCases, logEvent, services }) => ({
  async execute({ manuscriptId, content, type, userId }) {
    const { jobsService, eventsService, notificationService } = services
    const { Team, User, Review, Comment, Manuscript, TeamMember } = models
    const manuscript = await Manuscript.find(
      manuscriptId,
      'journal.peerReviewModel',
    )

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )
    submittingAuthor.user = await User.find(submittingAuthor.userId)

    await useCases.createMajorVersion
      .initialize({
        models,
        useCases,
        services,
      })
      .execute({ manuscript })

    manuscript.updateStatus(Manuscript.Statuses.revisionRequested)

    await manuscript.save()

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    let editorialMember = await TeamMember.findOneByManuscriptAndRoleAndUser({
      userId,
      manuscriptId,
      role: Team.Role.academicEditor,
    })

    if (!editorialMember) {
      editorialMember = await TeamMember.findOneByManuscriptAndRoleAndUser({
        userId,
        manuscriptId,
        role: Team.Role.triageEditor,
      })
    }

    if (!editorialMember) editorialMember = editorialAssistant

    const review = new Review({
      manuscriptId,
      teamMemberId: editorialMember.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations[type],
    })
    await review.save()

    const comment = new Comment({
      content,
      reviewId: review.id,
      type: Comment.Types.public,
    })
    await comment.save()

    const { journal } = manuscript

    notificationService.notifySubmittingAuthor({
      content,
      journal,
      manuscript,
      editor: editorialMember,
      submittingAuthor,
      editorialAssistant,
    })

    const isRegularRevision = type === Review.Recommendations.revision
    let eventAction
    if (isRegularRevision) {
      eventAction = 'revision_requested'

      eventsService.publishSubmissionEvent({
        submissionId: manuscript.submissionId,
        eventName: 'SubmissionRevisionRequested',
      })
    } else {
      eventAction =
        type === Review.Recommendations.major
          ? 'revision_requested_major'
          : 'revision_requested_minor'
    }

    logEvent({
      userId,
      manuscriptId,
      objectId: manuscriptId,
      action: logEvent.actions[eventAction],
      objectType: logEvent.objectType.manuscript,
    })

    if (!isRegularRevision) {
      await useCases.requestMinorOrMajorUseCase
        .initialize({
          models,
          useCases,
          jobsService,
          eventsService,
          notificationService,
        })
        .execute({
          journal,
          manuscript,
          submittingAuthor,
          editorialAssistant,
          academicEditor: editorialMember,
        })
    }

    const pendingReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.pending,
      },
    )
    const acceptedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.accepted,
      },
    )

    await Promise.each(
      [...pendingReviewers, ...acceptedReviewers],
      async tm => {
        tm.updateProperties({ status: TeamMember.Statuses.expired })
        await tm.save()
      },
    )

    await useCases.cancelAllJobsUseCase
      .initialize({ models, jobsService })
      .execute({
        manuscript,
        editorialAssistant,
        academicEditor: editorialMember,
      })
  },
})

const authsomePolicies = [
  'isTriageEditor',
  'isAuthenticated',
  'isAcademicEditorOnManuscript',
]

module.exports = {
  initialize,
  authsomePolicies,
}
