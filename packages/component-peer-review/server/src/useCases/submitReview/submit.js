const { minimumNumberOfSubmittedReports } = require('config')
const { isEmpty } = require('lodash')

const initialize = ({
  models: {
    Job,
    Team,
    User,
    Review,
    Journal,
    Comment,
    Manuscript,
    TeamMember,
    SpecialIssue,
  },
  logEvent,
  jobsService,
  eventsService,
  notificationService,
}) => ({
  async execute({ reviewId, userId }) {
    const review = await Review.find(reviewId)

    const publicComment = await Comment.findOneByType({
      reviewId,
      type: Comment.Types.public,
    })
    if (!publicComment)
      throw new ValidationError('Cannot submit an empty review')

    const manuscript = await Manuscript.find(review.manuscriptId, 'articleType')
    const nonActionableStatuses = Manuscript.NonActionableStatuses
    if (nonActionableStatuses.includes(manuscript.status)) {
      throw new AuthorizationError('Unauthorized')
    }

    review.updateProperties({
      submitted: new Date(),
    })
    await review.save()

    let specialIssue
    if (manuscript.specialIssueId) {
      specialIssue = await SpecialIssue.find(
        manuscript.specialIssueId,
        'peerReviewModel',
      )
    }
    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')
    const peerReviewModel = specialIssue
      ? specialIssue.peerReviewModel
      : journal.peerReviewModel

    const newStatus = peerReviewModel.approvalEditors.includes(
      Team.Role.academicEditor,
    )
      ? Manuscript.Statuses.makeDecision
      : Manuscript.Statuses.reviewCompleted

    manuscript.updateProperties({
      status: newStatus,
    })
    await manuscript.save()

    const reviewer = await TeamMember.find(review.teamMemberId, 'user')
    reviewer.updateProperties({
      status: TeamMember.Statuses.submitted,
    })
    await reviewer.save()

    const privateComment = await Comment.findOneByType({
      reviewId,
      type: Comment.Types.private,
    })

    if (isEmpty(privateComment.content)) {
      await privateComment.delete()
    }

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    let staffMember = editorialAssistant
    if (!editorialAssistant) {
      staffMember = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const reviews = await Review.findBy({
      manuscriptId: manuscript.id,
    })
    const submittedReviews = reviews.filter(review => review.submitted)

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    academicEditor.user = await User.find(academicEditor.userId)

    if (submittedReviews.length >= minimumNumberOfSubmittedReports) {
      const academicEditorJobs = await Job.findAllByTeamMember(
        academicEditor.id,
      )
      jobsService.cancelJobs(academicEditorJobs)

      const staffMemberJobs = await Job.findAllByTeamMembers([staffMember.id])
      jobsService.cancelStaffMemberJobs({
        staffMemberJobs,
        manuscriptId: manuscript.id,
      })
    }

    const reviewerJobs = await Job.findAllByTeamMembers([reviewer.id])
    jobsService.cancelJobs(reviewerJobs)

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    // Notifications

    let fromName = editorialAssistant.getName()
    if (triageEditor) {
      fromName = triageEditor.getName()
    }

    notificationService.notifyAcademicEditor({
      reviewer,
      fromName,
      manuscript,
      academicEditor,
      journalName: journal.name,
      submittingAuthorName: submittingAuthor.getName(),
      editorialAssistantEmail: editorialAssistant.alias.email,
    })

    notificationService.notifyReviewerForSubmittedReport({
      reviewer,
      fromName,
      manuscript,
      articleType: manuscript.articleType.name,
      journalName: journal.name,
      submittingAuthorName: submittingAuthor.getName(),
      editorialAssistantEmail: editorialAssistant.alias.email,
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReviewerReportSubmitted',
    })

    logEvent({
      userId,
      objectId: manuscript.id,
      manuscriptId: manuscript.id,
      action: logEvent.actions.review_submitted,
      objectType: logEvent.objectType.manuscript,
    })
  },
})

const authsomePolicies = ['isReviewerOnReview']

module.exports = {
  initialize,
  authsomePolicies,
}
