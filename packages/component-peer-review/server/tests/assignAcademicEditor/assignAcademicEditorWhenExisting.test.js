const models = require('@pubsweet/models')
const {
  assignAcademicEditorWhenExistingUseCase,
} = require('../../src/useCases/assignAcademicEditor')

const { Manuscript, Team, Job } = models

const notificationService = {
  notifyEditorialAssistant: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}
const jobsService = {
  cancelJobs: jest.fn(),
}
const logEvent = jest.fn()
logEvent.actions = {
  academic_editor_assigned: '',
}
logEvent.objectType = { user: 'user' }

describe('Assign Academic Editor when other assigned', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('inserts an academic editor for each version', async () => {
    jest.spyOn(Job, 'findAllByTeamMember').mockResolvedValue()
    jest
      .spyOn(Team, 'findOneByManuscriptAndRole')
      .mockResolvedValue({ addMember: jest.fn() })
    jest.spyOn(Manuscript, 'updateManuscriptAndTeamMembers').mockResolvedValue()

    const manuscripts = [{ id: 'manuscript-1' }, { id: 'manuscript-2' }]
    const latestManuscript = { id: 'manuscript-2' }
    const academicEditors = [
      { id: 'academic-editor-1' },
      { id: 'academic-editor-2' },
    ]
    const user = { id: 'some-user-id' }

    await assignAcademicEditorWhenExistingUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        eventsService,
        notificationService,
      })
      .execute({ manuscripts, latestManuscript, academicEditors, user })

    expect(Job.findAllByTeamMember).toHaveBeenCalledTimes(2)
    expect(Job.findAllByTeamMember).toHaveBeenCalledWith('academic-editor-1')
    expect(Job.findAllByTeamMember).toHaveBeenCalledWith('academic-editor-2')

    expect(Team.findOneByManuscriptAndRole).toHaveBeenCalledTimes(2)
    expect(Team.findOneByManuscriptAndRole).toHaveBeenCalledWith({
      manuscriptId: 'manuscript-1',
      role: Team.Role.academicEditor,
    })
    expect(Team.findOneByManuscriptAndRole).toHaveBeenCalledWith({
      manuscriptId: 'manuscript-2',
      role: Team.Role.academicEditor,
    })

    expect(Manuscript.updateManuscriptAndTeamMembers).toHaveBeenCalledTimes(1)

    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
  })
})
