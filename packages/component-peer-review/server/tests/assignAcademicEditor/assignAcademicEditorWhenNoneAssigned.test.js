const {
  generateTeam,
  generateReview,
  generateManuscript,
} = require('component-generators')

const models = require('@pubsweet/models')
const {
  assignAcademicEditorWhenNoneAssignedUseCase,
} = require('../../src/useCases/assignAcademicEditor')

const { Manuscript, Team, Review } = models

const notificationService = {
  notifyEditorialAssistant: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}
const logEvent = () => jest.fn()
logEvent.actions = {
  academic_editor_assigned: '',
}
logEvent.objectType = { user: 'user' }

describe('Assign Academic Editor when none other assigned', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('inserts an academic editor for each version', async () => {
    jest.spyOn(Manuscript, 'updateManuscriptAndTeams').mockResolvedValue({})
    const manuscript1 = generateManuscript({
      status: Manuscript.Statuses.olderVersion,
    })
    const manuscript2 = generateManuscript({
      status: Manuscript.Statuses.submitted,
    })
    const review = generateReview({ isValid: true })
    const academicEditorTeam = generateTeam({ role: Team.Role.academicEditor })

    const manuscripts = [manuscript1, manuscript2]
    const latestManuscript = manuscript2
    const user = { id: 'some-user-id' }
    jest
      .spyOn(Team, 'findOrCreate')
      .mockResolvedValue({ academicEditorTeam, addMember: () => true })

    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValue(review)

    await assignAcademicEditorWhenNoneAssignedUseCase
      .initialize({
        models,
        logEvent,
        eventsService,
        notificationService,
      })
      .execute({ manuscripts, latestManuscript, user })

    expect(Manuscript.updateManuscriptAndTeams).toHaveBeenCalledTimes(1)
  })
})
