import {
  getTeamRoles,
  getTeamMemberStatuses,
  generateManuscript,
  getManuscriptStatuses,
} from 'component-generators'
import { generateTeamMember } from 'component-generators/teamMember'
import { statusForRegularRevisionUseCase } from '../../src/useCases/submitRevision'

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    findOneByManuscriptAndRoleAndStatuses: jest.fn(),
    Statuses: getTeamMemberStatuses(),
  },
  Manuscript: {
    Statuses: getManuscriptStatuses(),
  },
}

const { Team, TeamMember, Manuscript } = models

describe('Status for regular revision Use Case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('return submitted if approval editor is triage editor', async () => {
    const approvalEditorRole = Team.Role.triageEditor
    const revisionManuscript = generateManuscript()

    const result = await statusForRegularRevisionUseCase
      .initialize({ Team, TeamMember, Manuscript })
      .execute(revisionManuscript, approvalEditorRole)

    expect(result).toEqual(Manuscript.Statuses.submitted)
  })
  it('return submitted if approval editor is academic editor and none is assigned', async () => {
    const approvalEditorRole = Team.Role.academicEditor
    const revisionManuscript = generateManuscript()

    const result = await statusForRegularRevisionUseCase
      .initialize({ Team, TeamMember, Manuscript })
      .execute(revisionManuscript, approvalEditorRole)

    expect(result).toEqual(Manuscript.Statuses.submitted)
  })
  it('return academicEditorAssigned if approval editor is academic editor and one is assigned', async () => {
    const approvalEditorRole = Team.Role.academicEditor
    const revisionManuscript = generateManuscript()
    const academicEditor = generateTeamMember({
      status: TeamMember.Statuses.accepted,
    })

    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue(academicEditor)

    const result = await statusForRegularRevisionUseCase
      .initialize({ Team, TeamMember, Manuscript })
      .execute(revisionManuscript, approvalEditorRole)

    expect(result).toEqual(Manuscript.Statuses.academicEditorAssigned)
  })
})
