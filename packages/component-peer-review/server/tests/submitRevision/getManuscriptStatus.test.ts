import { getRecommendations } from 'component-generators'
import { getManuscriptStatusUseCase } from '../../src/useCases/submitRevision'

const models = {
  Review: {
    Recommendations: getRecommendations(),
  },
}
const useCase = {
  execute: () => ({}),
}
const mockedUseCases = {
  statusForRegularRevisionUseCase: useCase,
  statusForMinorOrMajorRevisionUseCase: useCase,
}

describe('Get manuscript status use cases', () => {
  beforeEach(() => {})
  it('Executes the regular revision use case if the revision is regular', async () => {
    mockedUseCases.statusForRegularRevisionUseCase = {
      execute: jest.fn(),
    }
    await getManuscriptStatusUseCase
      .initialize({
        models,
        ...mockedUseCases,
      })
      .execute({}, models.Review.Recommendations.revision, '')

    expect(
      mockedUseCases.statusForRegularRevisionUseCase.execute,
    ).toHaveBeenCalledTimes(1)
  })
  it('Executes the minor or major revision use case if the revision is minor or major', async () => {
    mockedUseCases.statusForMinorOrMajorRevisionUseCase = {
      execute: jest.fn(),
    }
    const minorOrMajorRecommendations = [
      models.Review.Recommendations.minor,
      models.Review.Recommendations.major,
    ]
    const recommentation =
      minorOrMajorRecommendations[Math.floor(Math.random() * 1)]

    await getManuscriptStatusUseCase
      .initialize({
        models,
        ...mockedUseCases,
      })
      .execute({}, recommentation, '')

    expect(
      mockedUseCases.statusForMinorOrMajorRevisionUseCase.execute,
    ).toHaveBeenCalledTimes(1)
  })
})
