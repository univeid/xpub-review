import {
  getTeamRoles,
  getTeamMemberStatuses,
  generateManuscript,
  getRecommendations,
  getManuscriptStatuses,
} from 'component-generators'
import { generateTeamMember } from 'component-generators/teamMember'
import { statusForMinorOrMajorRevisionUseCase } from '../../src/useCases/submitRevision'

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    findAllByManuscriptAndRole: jest.fn(),
    Statuses: getTeamMemberStatuses(),
  },
  Review: {
    Recommendations: getRecommendations(),
  },
  Manuscript: {
    Statuses: getManuscriptStatuses(),
  },
}
const { Team, TeamMember, Review, Manuscript } = models

describe('Status for minor or major revision Use Case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('return academicEditorAssigned if no reviewers are invited', async () => {
    const reviewRecommendation = Review.Recommendations.revision
    const revisionManuscript = generateManuscript()

    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    const result = await statusForMinorOrMajorRevisionUseCase
      .initialize({ Team, TeamMember, Manuscript, Review })
      .execute(reviewRecommendation, revisionManuscript)

    expect(result).toEqual(Manuscript.Statuses.academicEditorAssigned)
  })
  it('return reviewCompleted if reviewers are invited and minor revision is requested', async () => {
    const reviewRecommendation = Review.Recommendations.minor
    const revisionManuscript = generateManuscript()
    const reviewer = generateTeamMember()
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([reviewer])

    const result = await statusForMinorOrMajorRevisionUseCase
      .initialize({ Team, TeamMember, Manuscript, Review })
      .execute(reviewRecommendation, revisionManuscript)

    expect(result).toEqual(Manuscript.Statuses.reviewCompleted)
  })
  it('return underReview if reviewers are invited and major revision is requested', async () => {
    const reviewRecommendation = Review.Recommendations.major
    const revisionManuscript = generateManuscript()
    const reviewer = generateTeamMember()
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([reviewer])

    const result = await statusForMinorOrMajorRevisionUseCase
      .initialize({ Team, TeamMember, Manuscript, Review })
      .execute(reviewRecommendation, revisionManuscript)

    expect(result).toEqual(Manuscript.Statuses.underReview)
  })
})
