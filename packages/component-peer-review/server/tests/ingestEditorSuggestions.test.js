const { EditorSuggestion, TeamMember, Manuscript } = require('@pubsweet/models')
const { ingestEditorSuggestionsUseCase } = require('../src/useCases')

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

describe('Ingest Editor Submission Scores', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should ingest score for all editors', async () => {
    jest.spyOn(EditorSuggestion, 'saveAll').mockResolvedValueOnce()
    jest.spyOn(EditorSuggestion, 'findOneBy').mockResolvedValue(undefined)
    jest
      .spyOn(TeamMember, 'isSuggestionsListInSyncWithTeamMembers')
      .mockResolvedValueOnce(true)
    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({})
    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue({ status: 'submitted' })

    await ingestEditorSuggestionsUseCase
      .initialize({
        models: { EditorSuggestion, TeamMember, Manuscript },
        eventsService,
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        editorSuggestions: [
          {
            editorId: 'editor-id-1',
            score: 0.2,
          },
          {
            editorId: 'editor-id-2',
            score: 0.9,
          },
        ],
      })

    expect(EditorSuggestion.saveAll).toHaveBeenCalledTimes(1)
    expect(EditorSuggestion.saveAll).toHaveBeenCalledWith([
      {
        manuscriptId: 'some-manuscript-id',
        teamMemberId: 'editor-id-1',
        score: 0.2,
      },
      {
        manuscriptId: 'some-manuscript-id',
        teamMemberId: 'editor-id-2',
        score: 0.9,
      },
    ])
  })
})
