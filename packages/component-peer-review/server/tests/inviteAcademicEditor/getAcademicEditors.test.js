const models = require('@pubsweet/models')
const { getAcademicEditorsUseCase } = require('../../src/useCases')

describe('Get academic editors', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  it('returns an empty array if there is no journal Academic Editor team and does not belong to special Issue', async () => {
    const { Manuscript, TeamMember, Team } = models

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({
      journalId: 'some-journal-id',
      submissionId: 'some-submission-id',
      getHasSpecialIssueEditorialConflictOfInterest: () => false,
    })
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRoleAndSearchValue')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllByJournalAndRoleAndSearchValue')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndStatus')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'orderAcademicEditorsByScore')
      .mockResolvedValueOnce([])

    const academicEditorMembers = await getAcademicEditorsUseCase
      .initialize(models)
      .execute({
        manuscriptId: 'some-manuscript-id',
        searchValue: 'some-search-value',
      })

    expect(academicEditorMembers).toHaveLength(0)

    expect(Manuscript.find).toHaveBeenCalledTimes(1)
    expect(Manuscript.find).toHaveBeenCalledWith('some-manuscript-id')

    expect(
      TeamMember.findAllBySpecialIssueAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(0)

    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      role: Team.Role.academicEditor,
      searchValue: 'some-search-value',
    })

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledWith({
      manuscriptId: 'some-manuscript-id',
      role: Team.Role.author,
    })

    expect(
      TeamMember.findAllByManuscriptAndRoleAndStatus,
    ).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRoleAndStatus).toHaveBeenCalledWith(
      {
        manuscriptId: 'some-manuscript-id',
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.declined,
      },
    )

    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      statuses: [TeamMember.Statuses.removed],
      submissionId: 'some-submission-id',
    })

    expect(TeamMember.orderAcademicEditorsByScore).toHaveBeenCalledTimes(1)
    expect(TeamMember.orderAcademicEditorsByScore).toHaveBeenCalledWith({
      academicEditors: [],
      manuscriptId: 'some-manuscript-id',
    })
  })

  it('returns all academic editors from special issue except those that declined', async () => {
    const { Manuscript, TeamMember, Team } = models

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({
      journalId: 'some-journal-id',
      specialIssueId: 'some-special-issue-id',
      submissionId: 'some-submission-id',
      getHasSpecialIssueEditorialConflictOfInterest: () => false,
    })
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRoleAndSearchValue')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
          toDTO: () => ({ userId: 'user-id-from-dto-1' }),
        },
        {
          userId: 'user-id-2',
        },
      ])
    jest.spyOn(TeamMember, 'findAllByJournalAndRoleAndSearchValue')
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndStatus')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-2',
        },
      ])
    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'orderAcademicEditorsByScore')
      .mockResolvedValueOnce([
        {
          userId: 'ordered-user-id-1',
        },
      ])

    const academicEditorMembers = await getAcademicEditorsUseCase
      .initialize(models)
      .execute({
        manuscriptId: 'some-manuscript-id',
        searchValue: 'some-search-value',
      })

    expect(academicEditorMembers).toHaveLength(1)
    expect(academicEditorMembers).toEqual([
      {
        userId: 'ordered-user-id-1',
      },
    ])

    expect(Manuscript.find).toHaveBeenCalledTimes(1)
    expect(Manuscript.find).toHaveBeenCalledWith('some-manuscript-id')

    expect(
      TeamMember.findAllBySpecialIssueAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllBySpecialIssueAndRoleAndSearchValue,
    ).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
      searchValue: 'some-search-value',
    })

    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(0)

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledWith({
      manuscriptId: 'some-manuscript-id',
      role: Team.Role.author,
    })

    expect(
      TeamMember.findAllByManuscriptAndRoleAndStatus,
    ).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRoleAndStatus).toHaveBeenCalledWith(
      {
        manuscriptId: 'some-manuscript-id',
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.declined,
      },
    )

    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      statuses: [TeamMember.Statuses.removed],
      submissionId: 'some-submission-id',
    })

    expect(TeamMember.orderAcademicEditorsByScore).toHaveBeenCalledTimes(1)
    expect(TeamMember.orderAcademicEditorsByScore).toHaveBeenCalledWith({
      academicEditors: [{ userId: 'user-id-from-dto-1' }],
      manuscriptId: 'some-manuscript-id',
    })
  })

  it('returns all academic editors only from the manuscript`s journal if not Special Issue', async () => {
    const { Manuscript, TeamMember, Team } = models

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({
      journalId: 'some-journal-id',
      submissionId: 'some-submission-id',
      getHasSpecialIssueEditorialConflictOfInterest: () => false,
    })

    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRoleAndSearchValue')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'findAllByJournalAndRoleAndSearchValue')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
          toDTO: () => ({ userId: 'user-id-from-dto-1' }),
        },
        {
          userId: 'user-id-2',
          toDTO: () => ({ userId: 'user-id-from-dto-2' }),
        },
      ])

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndStatus')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'orderAcademicEditorsByScore')
      .mockResolvedValueOnce([
        {
          userId: 'ordered-user-id-1',
        },
        {
          userId: 'ordered-user-id-2',
        },
      ])

    const academicEditorMembers = await getAcademicEditorsUseCase
      .initialize(models)
      .execute({
        manuscriptId: 'some-manuscript-id',
        searchValue: 'some-search-value',
      })

    expect(academicEditorMembers).toHaveLength(2)
    expect(academicEditorMembers).toEqual([
      {
        userId: 'ordered-user-id-1',
      },
      {
        userId: 'ordered-user-id-2',
      },
    ])

    expect(Manuscript.find).toHaveBeenCalledTimes(1)
    expect(Manuscript.find).toHaveBeenCalledWith('some-manuscript-id')

    expect(
      TeamMember.findAllBySpecialIssueAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(0)

    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      journalId: 'some-journal-id',
      searchValue: 'some-search-value',
    })

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledWith({
      manuscriptId: 'some-manuscript-id',
      role: Team.Role.author,
    })

    expect(
      TeamMember.findAllByManuscriptAndRoleAndStatus,
    ).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRoleAndStatus).toHaveBeenCalledWith(
      {
        manuscriptId: 'some-manuscript-id',
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.declined,
      },
    )

    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      statuses: [TeamMember.Statuses.removed],
      submissionId: 'some-submission-id',
    })

    expect(TeamMember.orderAcademicEditorsByScore).toHaveBeenCalledTimes(1)
    expect(TeamMember.orderAcademicEditorsByScore).toHaveBeenCalledWith({
      academicEditors: [
        { userId: 'user-id-from-dto-1' },
        { userId: 'user-id-from-dto-2' },
      ],
      manuscriptId: 'some-manuscript-id',
    })
  })
})
