const path = require('path')
const defaultConfig = require('hindawi-review/config/default')

defaultConfig['pubsweet-server'].db.database = global.__testDbName || 'postgres'

defaultConfig.dbManager.migrationsPath = path.resolve(
  __dirname,
  '..',
  '..',
  'app',
  'db',
  'migrations',
)

module.exports = defaultConfig
