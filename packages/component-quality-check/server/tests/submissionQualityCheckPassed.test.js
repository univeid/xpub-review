const {
  getTeamRoles,
  generateManuscript,
  generateTeamMember,
  getTeamMemberStatuses,
  getManuscriptStatuses,
} = require('component-generators')

const { submissionQualityCheckPassedUseCase } = require('../src/useCases')

let models = {}
let Manuscript = {}
let TeamMember = {}

const notificationService = {
  notifyAuthorWhenMaterialChecksIsCompleted: jest.fn(),
}
describe('Submission Quality Check Passed use-case', () => {
  beforeEach(() => {
    models = {
      TeamMember: {
        Statuses: getTeamMemberStatuses(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
        findSubmittingAuthor: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      User: {
        find: jest.fn(),
      },
      Manuscript: {
        findLastManuscriptBySubmissionId: jest.fn(),
        Statuses: getManuscriptStatuses(),
      },
    }
    ;({ Manuscript, TeamMember } = models)
  })

  it('Should notify author when material checks is completed', async () => {
    const manuscript = generateManuscript()
    const submittingAuthor = generateTeamMember()

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest
      .spyOn(TeamMember, 'findSubmittingAuthor')
      .mockResolvedValue(submittingAuthor)

    await submissionQualityCheckPassedUseCase
      .initialize({
        models,
        notificationService,
      })
      .execute({ data: { submissionId: manuscript.submissionId } })

    expect(manuscript.save).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyAuthorWhenMaterialChecksIsCompleted,
    ).toHaveBeenCalledTimes(1)
  })
})
