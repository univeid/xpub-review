const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const { useCases: mtsProductionUseCases } = require('component-mts-production')
const { generateXML } = require('component-xml-generator')
const events = require('component-events')
const { transaction } = require('objection')
const {
  sendPackageToProduction,
  sendAcceptedPackageToMTS,
} = require('component-ftp-upload')

const { s3service } = require('component-files/server')

const {
  useCases: {
    createMinorVersion,
    createMajorVersion,
    copyFilesBetweenManuscripts,
    copyAuthorsBetweenManuscripts,
    copyEAsBetweenManuscripts,
    copyTEsBetweenManuscripts,
    copyAEsBetweenManuscripts,
  },
} = require('component-model')

const useCases = require('./useCases')
const {
  useCases: { getSuggestedAcademicEditorUseCase },
} = require('component-model')

const {
  useCases: {
    inviteSuggestedAcademicEditorUseCase,
    inviteAcademicEditorUseCase,
    initializeInviteAcademicEditorUseCase,
  },
} = require('component-peer-review')
const notificationService = require('./notifications/notification')

const COI_HANDLERS = {
  reviewers: 'handleCOIBetweenAuthorAndReviewersUseCase',
  triageEditor: 'handleCOIBetweenAuthorAndTriageEditorUseCase',
  academicEditor: 'handleCOIBetweenAuthorAndAcademicEditorUseCase',
}

module.exports = {
  async SubmissionQualityCheckFilesRequested(data) {
    const useCasesToPass = {
      createMajorVersion,
      createMinorVersion,
      copyFilesBetweenManuscripts,
      copyAuthorsBetweenManuscripts,
      copyEAsBetweenManuscripts,
      copyTEsBetweenManuscripts,
      copyAEsBetweenManuscripts,
    }
    const services = { s3service }
    return useCases.requestQualityChecksUseCase
      .initialize({ models, useCases: useCasesToPass, services, logEvent })
      .execute(data)
  },
  async SubmissionPeerReviewCycleCheckPassed(data) {
    return useCases.peerReviewCycleCheckPassedUseCase
      .initialize({ models, notificationService })
      .execute({ data })
  },
  async SubmissionQualityCheckRTCd(data) {
    return useCases.rejectQualityCheckUseCase
      .initialize({ models, logEvent, notificationService })
      .execute({ data })
  },
  async PeerReviewCycleCheckingProcessSentToPeerReview(data) {
    const eventsService = events.initialize({ models })

    const {
      submissionId,
      checks: {
        peerReviewCycle: {
          conflictOfInterest: {
            type,
            triageEditor = {},
            academicEditor = {},
            reviewers = [],
          },
        },
      },
    } = data

    const useCase = useCases[COI_HANDLERS[type]]
    if (!useCase) return

    return useCase
      .initialize({
        models,
        logEvent,
        useCases,
        transaction,
        eventsService,
        notificationService,
        getSuggestedAcademicEditorUseCase,
        inviteSuggestedAcademicEditorUseCase,
        inviteAcademicEditorUseCase,
        initializeInviteAcademicEditorUseCase,
      })
      .execute({
        submissionId,
        triageEditorId: triageEditor.id,
        academicEditorId: academicEditor.id,
        reviewerIds: reviewers.map(r => r.id),
      })
  },
  async SubmissionQualityCheckPassed(data) {
    await useCases.submissionQualityCheckPassedUseCase
      .initialize({ models, notificationService })
      .execute(data)

    await mtsProductionUseCases.handleQualityCheckedSubmissionUseCase
      .initialize({
        models,
        generateXML,
        applicationEventBus,
        sendPackageToProduction,
        sendAcceptedPackageToMTS,
        useCases: mtsProductionUseCases,
      })
      .execute(data)
  },
}
