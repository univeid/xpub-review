const initialize = ({
  models: { Manuscript, TeamMember, Team, User },
  notificationService,
}) => ({
  async execute(data) {
    const { submissionId } = data

    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
      eagerLoadRelations: ['journal'],
    })
    lastManuscript.updateProperties({
      status: Manuscript.Statuses.published,
    })
    await lastManuscript.save()

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: lastManuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      lastManuscript.id,
    )
    submittingAuthor.user = await User.find(submittingAuthor.userId)

    notificationService.notifyAuthorWhenMaterialChecksIsCompleted({
      editorialAssistant,
      submittingAuthor,
      manuscript: lastManuscript,
    })
  },
})

module.exports = {
  initialize,
}
