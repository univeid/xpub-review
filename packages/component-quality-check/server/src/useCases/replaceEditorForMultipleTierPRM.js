const initialize = ({
  models: { Manuscript, TeamMember, Team, User, Journal },
  notificationService,
}) => ({
  async execute({ manuscriptId, academicEditorId }) {
    const manuscript = await Manuscript.find(manuscriptId)
    const academicEditor = await TeamMember.find(academicEditorId)

    const journal = await Journal.find(manuscript.journalId)
    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    if (!triageEditor) return

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    triageEditor.user = await User.find(triageEditor.userId)
    notificationService.notifyTriageEditorAboutEditorReplacement({
      manuscript,
      academicEditor,
      editorialAssistant,
      journalName: journal.name,
      approvalEditor: triageEditor,
    })
  },
})

module.exports = {
  initialize,
}
