const initialize = ({
  models: { Manuscript, File },
  logEvent,
  eventsService,
}) => ({
  async execute({ submissionId, userId }) {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      order: 'desc',
      orderByField: 'version',
      submissionId,
    })
    if (!manuscripts.length)
      throw new ValidationError(`No manuscript was found`)

    const lastManuscript = manuscripts[1]
    if (lastManuscript.status !== Manuscript.Statuses.qualityChecksRequested) {
      throw new ValidationError(
        `Cannot submit Quality Checks in the current status`,
      )
    }

    const mainFile = await File.findOneBy({
      queryObject: {
        manuscriptId: manuscripts[0].id,
        type: File.Types.manuscript,
      },
    })
    if (!mainFile)
      throw new ValidationError('At least one main manuscript is required.')

    manuscripts[0].updateProperties({
      status: Manuscript.Statuses.qualityChecksSubmitted,
      qualityChecksSubmittedDate: new Date().toISOString(),
      isLatestVersion: true,
    })
    await manuscripts[0].save()

    lastManuscript.updateStatus(Manuscript.Statuses.olderVersion)
    lastManuscript.updateIsLatestVersionFlag(false)
    await lastManuscript.save()

    logEvent({
      userId,
      manuscriptId: lastManuscript.id,
      action: logEvent.actions.quality_checks_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: lastManuscript.id,
    })

    eventsService.publishSubmissionEvent({
      submissionId: lastManuscript.submissionId,
      eventName: 'SubmissionQualityChecksSubmitted',
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: ['hasAccessToSubmission', 'admin'],
}
