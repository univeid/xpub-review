const rejectQualityCheckUseCase = require('./rejectQualityCheck')
const conflictOfInterestUseCases = require('./conflictOfInterest')
const submitQualityChecksUseCase = require('./submitQualityChecks')
const requestQualityChecksUseCase = require('./requestQualityChecks')
const peerReviewCycleCheckPassedUseCase = require('./peerReviewCheckPassed')
const updateQualityChecksDraftUseCase = require('./updateQualityChecksDraft')
const submissionQualityCheckPassedUseCase = require('./submissionQualityCheckPassed')
const replaceEditorForSingleTierPRMUseCase = require('./replaceEditorForSingleTierPRM')
const replaceEditorForMultipleTierPRMUseCase = require('./replaceEditorForMultipleTierPRM')

module.exports = {
  rejectQualityCheckUseCase,
  submitQualityChecksUseCase,
  requestQualityChecksUseCase,
  ...conflictOfInterestUseCases,
  updateQualityChecksDraftUseCase,
  peerReviewCycleCheckPassedUseCase,
  submissionQualityCheckPassedUseCase,
  replaceEditorForSingleTierPRMUseCase,
  replaceEditorForMultipleTierPRMUseCase,
}
