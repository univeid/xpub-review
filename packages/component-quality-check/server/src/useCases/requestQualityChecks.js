const initialize = ({
  models,
  useCases,
  services,
  models: { Manuscript },
  logEvent,
}) => ({
  async execute({ submissionId }) {
    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })

    lastManuscript.updateStatus(Manuscript.Statuses.qualityChecksRequested)
    await lastManuscript.save()

    await useCases.createMinorVersion
      .initialize({
        models,
        useCases,
        services,
      })
      .execute({
        manuscript: lastManuscript,
      })

    logEvent({
      userId: null,
      manuscriptId: lastManuscript.id,
      action: logEvent.actions.quality_checks_requested,
      objectType: logEvent.objectType.manuscript,
      objectId: lastManuscript.id,
    })
  },
})

module.exports = {
  initialize,
}
