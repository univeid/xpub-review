const logger = require('@pubsweet/logger')

const initialize = ({
  models,
  logEvent,
  useCases,
  transaction,
  eventsService,
  notificationService,
  getSuggestedAcademicEditorUseCase,
  inviteSuggestedAcademicEditorUseCase,
  inviteAcademicEditorUseCase,
  initializeInviteAcademicEditorUseCase,
}) => ({
  async execute({ submissionId }) {
    const { Journal, Manuscript, TeamMember, Team } = models

    const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: latestManuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )

    const trx = await transaction.start(Manuscript.knex())
    try {
      await latestManuscript.$query(trx).patch({
        status: Manuscript.Statuses.academicEditorInvited,
        hasTriageEditorConflictOfInterest: true,
      })

      await useCases.removeSubmissionUserUseCase
        .initialize({ models })
        .execute({
          submissionId,
          userId: academicEditor.userId,
          role: Team.Role.academicEditor,
          trx,
        })

      await useCases.invalidateReviewUseCase.initialize({ models }).execute({
        manuscriptId: latestManuscript.id,
        teamMemberId: academicEditor.id,
        trx,
      })

      await trx.commit()
    } catch (err) {
      logger.error(err)
      await trx.rollback(err)
      throw new Error('Something went wrong. No data was updated.')
    }

    await useCases.replaceEditorForSingleTierPRMUseCase
      .initialize({
        models,
        notificationService,
        getSuggestedAcademicEditorUseCase,
        inviteSuggestedAcademicEditorUseCase,
        inviteAcademicEditorUseCase,
        initializeInviteAcademicEditorUseCase,
      })
      .execute({ manuscriptId: latestManuscript.id })

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionAcademicEditorRemoved',
    })
    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionReviewInvalidated',
    })

    logEvent({
      userId: null,
      manuscriptId: latestManuscript.id,
      action: logEvent.actions.quality_checks_coi_author_triage_editor,
      objectType: logEvent.objectType.manuscript,
      objectId: latestManuscript.id,
    })

    const journal = await Journal.find(latestManuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: latestManuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const triageEditor = await TeamMember.findOneByManuscriptAndRole({
      manuscriptId: latestManuscript.id,
      role: Team.Role.triageEditor,
      eagerLoadRelations: 'user',
    })

    notificationService.notifyTriageEditorAboutHisConflictOfInterest({
      journal,
      manuscript: latestManuscript,
      triageEditor,
      editorialAssistant,
    })
  },
})

module.exports = {
  initialize,
}
