const { Promise } = require('bluebird')

const initialize = ({ models }) => ({
  async execute({ manuscriptId, teamMemberId, trx }) {
    const { Review } = models

    const reviews = await Review.findAllByManuscriptAndTeamMember({
      manuscriptId,
      teamMemberId,
    })

    if (!reviews.length) return

    reviews.forEach(review => (review.isValid = false))

    return Promise.each(reviews, async review =>
      review.$query(trx).update(review),
    )
  },
})

module.exports = {
  initialize,
}
