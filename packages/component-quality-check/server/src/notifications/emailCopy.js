const getEmailCopy = ({
  title,
  customId,
  emailType,
  journalName,
  reviewerNames,
  academicEditorName,
  articleName,
  invoicingUrl,
  servicesUrl,
}) => {
  let upperContent, subText, lowerContent, paragraph
  let hasLink = true
  const hasIntro = true
  const hasSignature = true
  switch (emailType) {
    case 'author-manuscript-rejected-eqa':
      hasLink = false
      paragraph = `I regret to inform you that your manuscript has been rejected for publication in ${journalName}.
        Thank you for your submission, and please do consider submitting again in the future.`
      break
    case 'author-academic-editor-coi':
      paragraph = `During the final checks of manuscript ${customId}: ${title}, which was recently recommended for publication, we found a potential conflict of interest between at least one of the authors and the Academic Editor, ${academicEditorName}.<br/><br/>
      To ensure a fully objective assessment, we would therefore like to ask a new Academic Editor to take a look at the manuscript, and reviewer reports, and decide how best to proceed. They will then be able either confirm the original decision, request revisions, or reject the manuscript, depending on their interpretation.<br/><br/>
      We’d therefore be grateful if you could visit the link below, and select a new Academic Editor to provide their assessment.<br/><br/>
      Many thanks for your continued contribution.<br/><br/>`
      break
    case 'academic-editor-manuscript-conflict':
      paragraph = `Thank you for handling manuscript ${customId}: ${title}, which you recently recommended for publication. 
        Unfortunately, during final checks, we found a potential conflict of interest between at least one of the authors and the following reviewers: ${reviewerNames}.<br/><br/>
        To ensure a fully objective assessment, we would therefore like to ask you to take a second look at the manuscript and reviewer reports, and decide how best to proceed.
        A number of options are available to you:
        <ul>
          <li>If you have other reviewers who supported publication, and the conflicted review has not adversely affected the review process, you may stick with your original decision to accept the manuscript.</li>
          <li>If you have other supportive reviewers, but the conflicted reviewer has adversely affected the revision of the manuscript, you may explain the situation to the author and ask for minor revisions to undo these.</li>
          <li>If you have no other supportive reviewers, we must ask that you seek further advice from a new reviewer before making a decision.</li>
        </ul>
        Many thanks for your continued contribution. You can take the required action(s) via the following link:
      `
      hasLink = true
      break
    case 'author-materialChecks-completed':
      hasLink = false
      paragraph = `
      Now that all the checks of your files have been completed, we are pleased to inform you that your manuscript is moving into production to ready it for publication online.<br/><br/>
      Our Production team will contact you in the near future with proofs of the final version of your manuscript for your feedback and comments.<br/><br/>
        Thank you again for choosing to publish with ${journalName}.<br/><br/>`
      break
    case 'author-prcc-completed':
      hasLink = true
      paragraph = `I am delighted to inform you that the review of your ${articleName} ${customId} titled ${title} has been completed and your article has been accepted for publication in ${journalName}.<br/><br/>
      Please visit the manuscript details page to review the editorial notes and any comments from external reviewers. If you have deposited your manuscript on a preprint server, now would be a good time to update it with the accepted version. If you have not deposited your manuscript on a preprint server, you are free to do so.<br/><br/>
      We will now check that all of your files are complete before passing them over to our production team for processing. We will let you know soon should we require any further information.<br/><br/>
      As an open access journal, publication of articles in ${journalName} are associated with Article Processing Charges. If applicable, you will receive a separate communication from our Editorial office in relation to this shortly. In regards to payments, we will:<br/><br/>
      <ul>
        <li>
          <strong>Only ever contact you from @hindawi.com email addresses.</strong> If you receive communications that claim to be from us, or one of our journals, but do not come from an @hindawi.com email address, please contact us directly at help@hindawi.com
        </li>
        <li>
          <strong>Only ever request payment through our own invoicing system.</strong>Any email requesting payment will always be from an @hindawi.com email address and will always direct you to our invoicing system with a link beginning ${invoicingUrl}
        </li>
      </ul> <br/>
      If you receive payment requests or information in ways other than this, or have any questions about Article Processing Charges, please contact us at help@hindawi.com.<br/><br/>
      Finally, we have partnered with leading author service providers to offer our authors discounts on a wide range of post-publication services (including videos, posters and more) to help you enhance the visibility and impact of your academic paper. Please <a href="${servicesUrl}">visit our author services page to learn more.</a><br/><br/>
       Thank you for choosing to publish with ${journalName}.<br/><br/>
      `
      break
    case 'triage-editor-conflict-triage-editor':
      hasLink = false
      paragraph = `Thank you for assessing manuscript ${customId}:${title}, which you recently approved for publication in ${journalName}. During our final checks, we found a potential conflict of interest between yourself and at least one of the authors. Typically such conflicts arise through co-publication or a shared institutional affiliation.<br/><br/>
      In light of this, and to ensure a fully objective assessment, we would therefore like to ask a second senior member of the Editorial Board to take a look at the manuscript and reviewer reports, before proceeding.<br/><br/>
      Please note that we are in no way questioning your personal judgement or impartiality. Rather, we wish to avoid any perceived conflict of interest (real or otherwise) to maintain the integrity of the review process. Having a second Editor approve the decision ensures we can achieve this goal without unduly delaying publication for the authors.<br/><br/>
      In case you find you hold a conflict of interest when assessing any future manuscripts, you can inform me or our team and we can have the manuscript reassigned. For more information about conflicts of interest, please see our Publication Ethics page.<br/><br/>
      No further action is required from you at this stage. Many thanks for your understanding and continuing contribution to ${journalName}.<br/><br/>
      `
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }
  return {
    hasLink,
    subText,
    hasIntro,
    paragraph,
    upperContent,
    lowerContent,
    hasSignature,
  }
}

module.exports = {
  getEmailCopy,
}
