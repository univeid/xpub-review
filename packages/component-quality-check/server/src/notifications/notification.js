const config = require('config')
const { services } = require('helper-service')
const { get } = require('lodash')
const Email = require('@pubsweet/component-email-templating')

const { getEmailCopy } = require('./emailCopy')
const { getModifiedText } = require('component-transform-text')
const urlService = require('../urlService/urlService')

const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')
const baseUrl = config.get('pubsweet-client.baseUrl')
const bccAddress = config.get('bccAddress')
const invoicingUrl = config.get('publisherConfig.links.invoicingLink')
const servicesUrl = config.get('publisherConfig.links.authorServiceLink')

module.exports = {
  async notifyAuthor({ journalName, editorialAssistant, author, manuscript }) {
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      emailType: 'author-manuscript-rejected-eqa',
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(author, 'alias.email', ''),
        name: get(author, 'alias.surname', ''),
      },
      content: {
        subject: `${manuscript.customId}: Manuscript rejected`,
        paragraph,
        signatureName: editorialAssistantName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: author.user.id,
          token: author.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: author.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyTriageEditorAboutEditorReplacement({
    manuscript,
    journalName,
    academicEditor,
    approvalEditor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()
    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      customId,
      title,
      academicEditorName: academicEditor.getName(),
      emailType: 'author-academic-editor-coi',
    })
    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(approvalEditor, 'alias.email', ''),
        name: get(approvalEditor, 'alias.surname', ''),
      },
      content: {
        subject: 'Reassessment required due to Conflict of Interest',
        paragraph,
        signatureName: editorialAssistantName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: approvalEditor.user.id,
          token: approvalEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: approvalEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyAcademicEditor({
    journalName,
    editorialAssistant,
    academicEditor,
    manuscript,
    reviewers,
  }) {
    const reviewerNames = reviewers.map(r => r.getName()).join(', ')

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      emailType: 'academic-editor-manuscript-conflict',
      customId: manuscript.customId,
      title: manuscript.title,
      reviewerNames,
    })

    const email = new Email({
      type: 'user',
      bbc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: get(academicEditor, 'alias.email', ''),
        name: get(academicEditor, 'alias.surname', ''),
      },
      content: {
        subject: `Reassessment required due to Conflict of Interest`,
        ctaText: 'MANUSCRIPT DETAILS',
        paragraph,
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        signatureName: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: academicEditor.user.id,
          token: academicEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: academicEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyAuthorWhenMaterialChecksIsCompleted({
    editorialAssistant,
    submittingAuthor,
    manuscript,
  }) {
    const { customId, title, journal } = manuscript

    const editorialAssistantEmail = editorialAssistant.getEmail()
    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName: journal.name,
      emailType: 'author-materialChecks-completed',
      title,
      customId,
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(submittingAuthor, 'alias.email', ''),
        name: get(submittingAuthor, 'alias.surname', ''),
      },
      content: {
        subject: `Your manuscript is moving into production`,
        paragraph,
        signatureName: journal.name,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.userId,
          token: submittingAuthor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: submittingAuthor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyAuthorWhenPRCCIsCompleted({
    manuscript,
    editorialAssistant,
    author,
    triageEditor,
  }) {
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const { customId, title, journal, articleType } = manuscript
    const signatureName = triageEditor ? triageEditor.getName() : journal.name
    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName: journal.name,
      emailType: 'author-prcc-completed',
      title,
      customId,
      articleName: articleType.name,
      invoicingUrl,
      servicesUrl,
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(author, 'alias.email', ''),
        name: get(author, 'alias.surname', ''),
      },
      content: {
        subject: `Your manuscript has been accepted for publication`,
        paragraph,
        signatureName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: author.user.id,
          token: author.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: author.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyTriageEditorAboutHisConflictOfInterest({
    journal,
    manuscript,
    triageEditor,
    editorialAssistant,
  }) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName: journal.name,
      emailType: 'triage-editor-conflict-triage-editor',
      customId: manuscript.customId,
      title: manuscript.title,
    })

    const email = new Email({
      type: 'user',
      bbc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: get(triageEditor, 'alias.email', ''),
        name: get(triageEditor, 'alias.surname', ''),
      },
      content: {
        subject: `Update on your editorial decision`,
        paragraph,
        signatureName: journal.name,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: triageEditor.user.id,
          token: triageEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: triageEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
}
