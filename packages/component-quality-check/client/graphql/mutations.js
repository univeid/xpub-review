import gql from 'graphql-tag'
import { fragments as submissionFragments } from 'component-submission/client'

export const updateQualityChecksDraft = gql`
  mutation updateQualityChecksDraft(
    $manuscriptId: String!
    $autosaveInput: QualityChecksRevisionAutosaveInput
  ) {
    updateQualityChecksDraft(
      manuscriptId: $manuscriptId
      autosaveInput: $autosaveInput
    ) {
      ...draftManuscriptDetails
    }
  }
  ${submissionFragments.draftManuscriptDetails}
`

export const submitQualityChecks = gql`
  mutation submitQualityChecks($submissionId: ID!) {
    submitQualityChecks(submissionId: $submissionId)
  }
`
