import { useMutation } from 'react-apollo'
import { get } from 'lodash'
import { mutations as qualityChecksMutations } from 'component-quality-check/client/graphql'
import { refetchGetSubmission } from '../graphql/refetchQueries'

const useSubmitQualityChecks = ({ revisionDraft, match, history }) => {
  const [updateQualityChecksDraftMutation] = useMutation(
    qualityChecksMutations.updateQualityChecksDraft,
  )
  const [submitQualityChecksMutation] = useMutation(
    qualityChecksMutations.submitQualityChecks,
    {
      refetchQueries: [refetchGetSubmission(match)],
      awaitRefetchQueries: true,
    },
  )

  const submitQualityChecks = (values, modalProps) => {
    modalProps.setFetching(true)

    submitQualityChecksMutation({
      variables: {
        submissionId: get(match, 'params.submissionId', ''),
      },
    })
      .then(() => {
        modalProps.hideModal()
        const path = `/details/${get(match, 'params.submissionId')}/${get(
          revisionDraft,
          'id',
        )}`
        history.push(path)
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  return {
    updateQualityChecksDraft: updateQualityChecksDraftMutation,
    submitQualityChecks,
  }
}

export default useSubmitQualityChecks
