require('dotenv').config()

module.exports = {
  client: 'pg',
  connection: {
    host: process.env.DB_HOST || 'localhost',
    database: process.env.DATABASE || '',
    user: process.env.DB_USER || '',
    password: process.env.DB_PASS || '',
  },
  seeds: {
    directory: `${__dirname}/db/seeds`,
  },
  migrations: {
    directory: `${__dirname}/db/migrations`,
  },
}
