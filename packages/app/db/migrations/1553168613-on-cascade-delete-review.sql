ALTER TABLE review
DROP CONSTRAINT review_manuscript_id_fkey;

ALTER TABLE review
ADD CONSTRAINT review_manuscript_id_fkey
FOREIGN KEY (manuscript_id) REFERENCES manuscript (id) ON DELETE CASCADE;

