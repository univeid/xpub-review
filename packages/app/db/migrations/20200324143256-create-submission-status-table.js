const logger = require('@pubsweet/logger')

exports.up = knex =>
  knex.schema
    .createTable('submission_status', table => {
      table
        .uuid('id')
        .primary()
        .defaultTo(knex.raw('uuid_generate_v4()'))
      table
        .timestamp('created')
        .notNullable()
        .defaultTo(knex.fn.now())
      table
        .timestamp('updated')
        .notNullable()
        .defaultTo(knex.fn.now())
      table
        .string('name')
        .unique()
        .notNullable()
      table.string('category').notNullable()
    })
    .then(_ => logger.info('submission_status table created successfully'))
    .catch(err => logger.error(err))

exports.down = knex =>
  knex.schema
    .dropTable('submission_status')
    .then(_ => logger.info('submission_status table dropped successfully'))
    .catch(err => logger.error(err))
