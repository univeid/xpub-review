const logger = require('@pubsweet/logger')

exports.up = async knex => {
  knex
    .raw(`CREATE INDEX on team_member USING GIN ((alias->'surname'))`)
    .then(() => logger.info('team member surname indexes added successfully'))
}
