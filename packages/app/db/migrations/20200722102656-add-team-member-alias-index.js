const logger = require('@pubsweet/logger')

exports.up = async knex => {
  knex.schema
    .table('team_member', table => {
      table.index('alias', null, 'GIN')
    })
    .then(() => logger.info('team member alias indexes added successfully'))
  knex
    .raw(`CREATE INDEX on team_member USING GIN ((alias->'email'))`)
    .then(() => logger.info('team member email indexes added successfully'))
}
