const path = require('path')

const v2 = require(path.resolve(
  __dirname,
  '../db/queries/views/editorialAssistant/v2.js',
))

const v3 = require(path.resolve(
  __dirname,
  '../db/queries/views/editorialAssistant/v3.js',
))

module.exports.up = async knex => {
  await knex.raw(v2.down)
  await knex.raw(v3.up)
}

module.exports.down = async knex => {
  await knex.raw(v3.down)
}
