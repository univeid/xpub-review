const logger = require('@pubsweet/logger')

exports.up = async knex =>
  knex.schema
    .table('special_issue', table => {
      table.index('journal_id', null, 'hash')
      table.index('section_id', null, 'hash')
      table.index('peer_review_model_id', null, 'hash')
    })
    .then(() => logger.info('special_issue indexes added successfully'))
