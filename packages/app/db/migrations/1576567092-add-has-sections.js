module.exports = {
  up: async knex =>
    knex.schema.table('peer_review_model', t => {
      t.boolean('has_sections').defaultTo(false)
    }),
  down: async () => {},
}
