CREATE TABLE "peer_review_model" (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    created timestamptz NOT NULL DEFAULT now(),
    updated timestamptz NOT NULL DEFAULT now(),
    "name" text NOT NULL,
    approval_editor text ARRAY,

    has_figurehead_editor boolean NOT NULL,
    figurehead_editor_label text,

    has_triage_editor boolean NOT NULL,
    triage_editor_label text,
    triage_editor_assignment_tool text ARRAY,

    handling_editor_label text,
    handling_editor_assignment_tool text ARRAY,

    reviewer_assignement_tool text ARRAY
)