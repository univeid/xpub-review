const logger = require('@pubsweet/logger')

exports.up = async knex =>
  knex.schema
    .table('audit_log', table => {
      table.index('user_id', null, 'hash')
      table.index('manuscript_id', null, 'hash')
    })
    .then(() => logger.info('audit_log indexes added successfully'))
