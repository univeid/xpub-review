const { Promise } = require('bluebird')

const formats = {
  // eslint-disable-next-line prettier/prettier, no-useless-escape, no-control-regex
  arxiv: '(arXiv):[0-9]+[.][0-9]',
}

exports.seed = async knex => {
  await knex('preprint').del()
  await Promise.each(Object.keys(formats), async key => {
    await knex('preprint').insert([
      {
        type: key,
        format: formats[key],
      },
    ])
  })
}
