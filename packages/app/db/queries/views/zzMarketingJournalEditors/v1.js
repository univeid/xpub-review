// View to export review data to Marketing Cloud in SalesForce

module.exports.up = `
CREATE OR REPLACE VIEW "zz_marketing_journal_editors" AS (
  SELECT
  tm."user_id" as user_id,
  t."role" as role,
  i."given_names" as first_name,
  i."surname" as last_name,
  i."country" as country,
  i."aff" as affiliation,
  i."email" as email,
  j."code" as journal_code
FROM "team_member" tm
JOIN "team" t ON t."id" = tm."team_id"
JOIN "identity" i ON i."user_id" = tm."user_id"
JOIN "journal" j ON t."journal_id" = j."id"
WHERE t."role" in ('academicEditor', 'triageEditor')
);
`
module.exports.down = 'DROP VIEW "zz_marketing_journal_editors";'
