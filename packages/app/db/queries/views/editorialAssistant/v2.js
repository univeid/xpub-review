module.exports.up = `
CREATE OR REPLACE VIEW "editorial_assistant" AS (
  SELECT
  tm."user_id" as user_id,
  i."email" as email,
  t."manuscript_id" as manuscript_id,
  t."journal_id" as journal_id,
  tm."status" as status
FROM "team_member" tm
JOIN "team" t ON t."id" = tm."team_id"
JOIN "identity" i ON i."user_id" = tm."user_id"
WHERE t."role" = 'editorialAssistant'
);
`
module.exports.down = 'DROP VIEW "editorial_assistant";'
