const { createTeams, createTeamMembers, createUsers } = require('../seedUtils')

exports.seed = async knex => {
  const journals = await knex.select().table('journal')
  const journal = journals[0]
  const peerReviewModels = await knex.select().table('peer_review_model')

  const peerReviewModel = peerReviewModels.find(
    prm => prm.id === journal.peerReviewModelId,
  )

  if (peerReviewModel.name === 'Chief Minus') {
    await createTeams({ knex, journal })
    await createTeamMembers({ knex, journal, createUsers })
  }
}
