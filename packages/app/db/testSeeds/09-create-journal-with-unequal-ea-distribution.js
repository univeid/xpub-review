const Chance = require('chance')
const {
  createTeam,
  createUsers,
  createTeamMember,
  createSubmission,
  createSubmissions,
} = require('../seedUtils')

const chance = new Chance()

exports.seed = async knex => {
  const peerReviewModels = await knex.select().table('peer_review_model')

  const journal = await knex('journal')
    .insert([
      {
        name: 'Journal of Distribution',
        code: chance.syllable(),
        email: chance.email(),
        apc: chance.integer({ min: 1, max: 999999 }),
        isActive: true,
        activationDate: new Date().toISOString(),
        peerReviewModelId: peerReviewModels[0].id,
      },
    ])
    .returning('*')
    .then(results => results[0])

  const eaTeam = await createTeam({
    knex,
    role: 'editorialAssistant',
    journalId: journal.id,
  })

  const users = await createUsers({ knex, numberOfUsers: 5 })
  const editorialAssistants = []

  users.forEach(async user => {
    const editorialAssistant = await createTeamMember({
      knex,
      teamId: eaTeam.id,
      userId: user.id,
      status: 'pending',
    })
    editorialAssistants.push(editorialAssistant)
  })

  const articleTypes = await knex.select().table('article_type')
  if (articleTypes.length === 0) {
    throw new Error('No article types have been found')
  }
  const articleTypeId = articleTypes[0].id

  const submissions = await createSubmissions({
    knex,
    journalId: journal.id,
    articleTypeId,
    createSubmission,
    numberOfSubmissions: 18,
  })

  const splitSubmissions = splitArrayInChunksOfRandomLength({
    arr: submissions,
    numberOfChunks: editorialAssistants.length,
  })

  splitSubmissions.forEach(submission => {})
}

function splitArrayInChunksOfRandomLength({ arr, numberOfChunks }) {
  const chunks = []

  while (chunks.length < numberOfChunks) {
    const { length } = arr
    /*
     * generate a random length greater than 0
     * and smaller than the array length without the number of chunks
     */
    const randomMultiplier = length - numberOfChunks
    let chunkLength = 1
    if (randomMultiplier > 0) {
      chunkLength = Math.floor(Math.random() * randomMultiplier + 1)
    }

    chunks.push(arr.splice(0, chunkLength))
  }

  // concatenate any remaining items from the original array into a random element of the new one
  if (arr.length > 0) {
    const randomIndex = Math.floor(Math.random() * (chunks.length - 1) + 1)
    chunks[randomIndex] = chunks[randomIndex].concat(arr)
  }

  return chunks
}
