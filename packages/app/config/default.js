require('dotenv').config()
const path = require('path')
const { get } = require('lodash')
const logger = require('./loggerCustom')
const components = require('./components.json')
const publishersConfig = require('../app/config/publisher')
const activityLogEvents = require('./activityLog/events')
const activityLogTypes = require('./activityLog/logTypes')

Object.assign(global, require('@pubsweet/errors'))

const publisherName = process.env.PUBLISHER_NAME || 'hindawi'

const getDbConfig = () => {
  if (process.env.DATABASE) {
    // mode info about these settings right here https://github.com/knex/knex/issues/2820#issuecomment-481710112
    return {
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DATABASE,
      host: process.env.DB_HOST,
      port: 5432,
      ssl: JSON.parse(
        get(process.env, 'DB_SSL', process.env.NODE_ENV === 'production'),
      ),
      idleTimeoutMillis: 30000,
      connectionTimeoutMillis: 20000,
      createTimeoutMillis: 3000,
      acquireTimeoutMillis: 30000,
      reapIntervalMillis: 1000,
      createRetryIntervalMillis: 100,
      propagateCreateError: false,
    }
  }
  return {}
}

module.exports = {
  authsome: {
    mode: path.resolve(__dirname, '../server', 'dist', 'authsome-mode.js'),
    teams: {
      academicEditor: {
        name: 'Academic Editors',
      },
      reviewer: {
        name: 'Reviewer',
      },
    },
  },
  pubsweet: {
    components,
  },
  dbManager: {
    migrationsPath: path.join(process.cwd(), 'db/migrations'),
  },
  'pubsweet-server': {
    db: getDbConfig(),
    pool: { min: 0, max: parseInt(process.env.MAX_CONNECTION_POOL, 10) || 10 },
    ignoreTerminatedConnectionError: true,
    port: 3000,
    logger,
    uploads: 'uploads',
    secret: 'SECRET',
    enableExperimentalGraphql: true,
    graphiql: true,
    morganLogFormat:
      ':remote-addr [:date[clf]] :method :url :graphql[operation] :status :response-time ms',
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    baseUrl: process.env.CLIENT_BASE_URL || 'http://localhost:3000',
    'login-redirect': '/',
  },
  orcid: {
    clientID: process.env.ORCID_CLIENT_ID,
    clientSecret: process.env.ORCID_CLIENT_SECRET,
    callbackPath: '/api/users/orcid/callback',
    successPath: '/profile',
  },
  keycloak: {
    certs: process.env.KEYCLOAK_CERTIFICATES || 'http://localhost:3000/certs',
    admin: {
      username: process.env.KEYCLOAK_USERNAME,
      password: process.env.KEYCLOAK_PASSWORD,
    },
    public: {
      realm: process.env.KEYCLOAK_REALM,
      clientID: process.env.KEYCLOAK_CLIENTID,
      authServerURL: process.env.KEYCLOAK_SERVER_URL,
    },
  },
  'mail-transport': {
    sendmail: true,
  },
  'password-reset': {
    url:
      process.env.PUBSWEET_PASSWORD_RESET_URL ||
      'http://localhost:3000/profile/change-password',
    sender: process.env.PUBSWEET_PASSWORD_RESET_SENDER || 'dev@example.com',
    hoursUntilNextRequest: 24,
  },
  'pubsweet-component-aws-s3': {
    secretAccessKey: process.env.AWS_S3_SECRET_KEY,
    accessKeyId: process.env.AWS_S3_ACCESS_KEY,
    region: process.env.AWS_S3_REGION,
    bucket: process.env.AWS_S3_BUCKET,
    eventStorage: process.env.AWS_S3_EVENT_STORAGE_BUCKET,
  },
  'pubsweet-component-aws-sqs': {
    secretAccessKey: process.env.AWS_SNS_SQS_SECRET_KEY,
    accessKeyId: process.env.AWS_SNS_SQS_ACCESS_KEY,
    endpoint: process.env.AWS_SQS_ENDPOINT,
    region: process.env.AWS_S3_REGION,
    queueName: process.env.AWS_SQS_QUEUE_NAME,
  },
  'mts-service': {
    ftp: {
      user: process.env.FTP_USERNAME,
      password: process.env.FTP_PASSWORD,
      host: process.env.FTP_HOST,
      port: 21,
      localRoot: `./`,
      exclude: ['*.js'],
    },
    doctype: 'article SYSTEM "JATS-archivearticle1-mathml3.dtd"',
    dtdVersion: '1.1d1',
    xml: {
      compact: true,
      ignoreComment: true,
      spaces: 2,
      fullTagEmptyElement: true,
    },
  },
  'production-ftp': {
    ftp: {
      user: process.env.PRODUCTION_FTP_USERNAME,
      password: process.env.PRODUCTION_FTP_PASSWORD,
      host: process.env.PRODUCTION_FTP_HOST,
      port: 21,
      localRoot: `./`,
      exclude: ['*.js'],
    },
  },
  'invite-reset-password': {
    url: process.env.PUBSWEET_INVITE_PASSWORD_RESET_URL || '/invite',
  },
  'forgot-password': {
    url: process.env.PUBSWEET_FORGOT_PASSWORD_URL || '/forgot-password',
  },
  'accept-academic-editor': {
    url:
      process.env.PUBSWEET_ACCEPT_ACADEMIC_EDITOR ||
      '/emails/accept-academic-editor',
  },
  'decline-academic-editor': {
    url:
      process.env.PUBSWEET_DECLINE_ACADEMIC_EDITOR ||
      '/emails/decline-academic-editor',
  },
  'invite-reviewer': {
    url: process.env.PUBSWEET_INVITE_REVIEWER_URL || '/invite-reviewer',
  },
  'accept-review-new-user': {
    url:
      process.env.PUBSWEET_ACCEPT_NEW_REVIEWER ||
      '/emails/accept-review-new-user',
  },
  'accept-review': {
    url: process.env.PUBSWEET_ACCEPT_REVIEWER || '/emails/accept-review',
  },
  'decline-review': {
    url: process.env.PUBSWEET_DECLINE_REVIEWER || '/emails/decline-review',
  },
  'confirm-signup': {
    url: process.env.PUBSWEET_CONFIRM_SIGNUP_URL || '/confirm-signup',
  },
  'eqs-decision': {
    url: process.env.PUBSWEET_EQS_DECISION || '/eqs-decision',
  },
  'eqa-decision': {
    url: process.env.PUBSWEET_EQA_DECISION || '/eqa-decision',
  },
  unsubscribe: {
    url: process.env.PUBSWEET_UNSUBSCRIBE_URL || '/unsubscribe',
  },
  mailer: {
    path: `${__dirname}/${
      JSON.parse(get(process, 'env.EMAIL_SENDING', true))
        ? 'mailer'
        : 'mailer-mock'
    }`,
  },
  SES: {
    accessKey: process.env.AWS_SES_ACCESS_KEY,
    secretKey: process.env.AWS_SES_SECRET_KEY,
    region: process.env.AWS_SES_REGION,
  },
  publicKeys: ['pubsweet-client', 'authsome'],
  publons: {
    key: process.env.PUBLONS_KEY || '',
    reviewersUrl: 'https://api.clarivate.com/reviewer-connect/api/',
  },
  publisherName,
  staffEmail: process.env.EMAIL_SENDER || 'help@hindawi.com',
  accountsEmail: 'accounts@hindawi.com',
  publisherConfig: publishersConfig[publisherName],
  journal: { ...publishersConfig[publisherName].emailData },
  emailFooterText: publishersConfig[publisherName].emailData.footerText,
  features: {
    mts: JSON.parse(get(process, 'env.FEATURE_MTS', true)),
  },
  recommendations: {
    minor: 'minor',
    major: 'major',
    reject: 'reject',
    publish: 'publish',
    type: {
      review: 'review',
      editor: 'editorRecommendation',
    },
  },
  minimumNumberOfInvitedReviewers: 5,
  minimumNumberOfSubmittedReports: 2,
  passwordStrengthRegex: new RegExp(
    '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&,.?;\'*><)([}{}":`~+=_-\\|/])(?=.{6,128})',
  ),
  activityLogEvents,
  activityLogTypes,
  maxAcademicEditorAutoInvited:
    process.env.MAX_ACADEMIC_EDITORS_AUTO_INVITED || 10,
  reminders: {
    businessDays: JSON.parse(get(process, 'env.BUSINESS_DAYS', true)),
    academicEditor: {
      inviteReviewers: {
        days: {
          first: process.env.REMINDER_REVIEWER_INVITATION_FIRST || 5,
          second: process.env.REMINDER_REVIEWER_INVITATION_SECOND || 8,
          third: process.env.REMINDER_REVIEWER_INVITATION_THIRD || 11,
        },
        timeUnit: process.env.REMINDER_REVIEWER_INVITATION_TIME_UNIT || 'days',
      },
      acceptInvitation: {
        days: {
          first: process.env.REMINDER_ACADEMIC_EDITOR_FIRST || 3,
          second: process.env.REMINDER_ACADEMIC_EDITOR_SECOND || 6,
        },
        remove: process.env.REMINDER_REMOVE_ACADEMIC_EDITOR || 8,
        timeUnit: process.env.REMINDER_ACADEMIC_EDITOR_TIME_UNIT || 'days',
      },
    },
    reviewer: {
      acceptInvitation: {
        days: {
          first: process.env.REMINDER_REVIEWER_FIRST || 4,
          second: process.env.REMINDER_REVIEWER_SECOND || 7,
          third: process.env.REMINDER_REVIEWER_THIRD || 14,
        },
        remove: process.env.REMINDER_REMOVE_REVIEWER || 21,
        timeUnit: process.env.REMINDER_REVIEWER_TIME_UNIT || 'days',
      },
      submitReport: {
        days: {
          first: process.env.REMINDER_ACCEPTED_REVIEWER_FIRST || 7,
          second: process.env.REMINDER_ACCEPTED_REVIEWER_SECOND || 13,
          third: process.env.REMINDER_ACCEPTED_REVIEWER_THIRD || 18,
        },
        remove: process.env.REMINDER_REMOVE_ACCEPTED_REVIEWER || 24,
        timeUnit: process.env.REMINDER_ACCEPTED_REVIEWER_TIME_UNIT || 'days',
      },
    },
  },
  roleLabels: {
    triageEditor: 'Chief Editor',
    academicEditor: 'Academic Editor',
    editorialAssistant: 'Editorial Assistant',
    researchIntegrityPublishingEditor: 'Research Integrity Publishing Editor',
  },
  specialIssueExpirationInterval: {
    value: 1,
    timeUnit: 'year',
  },
  newJobCheckIntervalSeconds:
    process.env.NEXT_JOB_CHECK_INTERVAL_SECONDS || 300,
  adminEmail: process.env.ADMIN_EMAIL || 'admin@example.com',
  jobScheduling: process.env.JOB_SCHEDULING || false,
  bccAddress: process.env.BCC_ADDRESS || 'bcc@example.com',
}
