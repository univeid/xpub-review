const { getTeamMemberStatuses, getTeamRoles } = require('component-generators')

const models = {
  Review: { find: jest.fn() },
  Team: { Role: getTeamRoles(), findOneByManuscriptAndUserAndRole: jest.fn() },
  Manuscript: {
    find: jest.fn(),
  },
  TeamMember: {
    find: jest.fn(),
    isApprovalEditor: jest.fn(),
    findOneByManuscriptAndUser: jest.fn(),
    Statuses: getTeamMemberStatuses(),
    findOneByUserAndRoleAndStatusOnManuscript: jest.fn(),
  },
  User: {
    find: jest.fn(),
  },
  PeerReviewModel: {
    findOneByJournal: jest.fn(),
  },
}
const { User, Team, Manuscript, Review, TeamMember, PeerReviewModel } = models
const authsomeMode = require('../src/authsome-mode')

describe('authsome mode', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  describe('admin policy', () => {
    it('returns false when the user is not admin', async () => {
      jest.spyOn(User, 'find').mockResolvedValueOnce({
        userId: 'userId',
        findTeamMemberByRole: () => undefined,
      })

      const result = await authsomeMode(
        'userId',
        { policies: [Team.Role.admin] },
        {},
        { models },
      )
      expect(result).toEqual(false)
    })
    it('returns true when the user is admin', async () => {
      jest.spyOn(User, 'find').mockResolvedValueOnce({
        userId: 'userId',
        findTeamMemberByRole: () => ({}),
      })

      const result = await authsomeMode(
        'userId',
        { policies: [Team.Role.admin] },
        {},
        { models },
      )
      expect(result).toEqual(true)
    })
  })
  describe('is editorial assistant policy', () => {
    it('returns true when the user is editorial assistant on the given manuscript', async () => {
      jest
        .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
        .mockResolvedValueOnce({})

      jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({})

      const result = await authsomeMode(
        'userId',
        { policies: ['isEditorialAssistant'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(
        TeamMember.findOneByUserAndRoleAndStatusOnManuscript,
      ).toHaveBeenCalledTimes(1)
      expect(result).toEqual(true)
    })
    it('returns false when the user is not editorial assistant on the given manuscript', async () => {
      jest
        .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
        .mockReturnValue()

      jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({})

      const result = await authsomeMode(
        'userId',
        { policies: ['isEditorialAssistant'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(
        TeamMember.findOneByUserAndRoleAndStatusOnManuscript,
      ).toHaveBeenCalledTimes(1)
      expect(result).toEqual(false)
    })
  })
  describe('is triage editor policy', () => {
    it('returns false when the user is not Triage Editor on the given manuscript', async () => {
      jest
        .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
        .mockReturnValue()

      jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({})

      const result = await authsomeMode(
        'userId',
        { policies: ['isTriageEditor'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(
        TeamMember.findOneByUserAndRoleAndStatusOnManuscript,
      ).toHaveBeenCalledTimes(1)
      expect(result).toEqual(false)
    })
    it('returns true when the user is Triage Editor on the given manuscript', async () => {
      const { Manuscript, TeamMember } = models

      jest
        .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
        .mockResolvedValueOnce({})

      jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({})

      const result = await authsomeMode(
        'userId',
        { policies: ['isTriageEditor'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(
        TeamMember.findOneByUserAndRoleAndStatusOnManuscript,
      ).toHaveBeenCalledTimes(1)
      expect(result).toEqual(true)
    })
  })
  describe('is academic editor policy', () => {
    it('returns true when the user is academic editor on the given manuscript', async () => {
      jest
        .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
        .mockResolvedValueOnce({})

      jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({})

      const result = await authsomeMode(
        'userId',
        { policies: ['isAcademicEditorOnManuscript'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(
        TeamMember.findOneByUserAndRoleAndStatusOnManuscript,
      ).toHaveBeenCalledTimes(1)
      expect(result).toEqual(true)
    })
    it('returns false when the user is not academic editor on the given manuscript', async () => {
      jest
        .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
        .mockReturnValue()

      jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({})

      const result = await authsomeMode(
        'userId',
        { policies: ['isAcademicEditorOnManuscript'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(
        TeamMember.findOneByUserAndRoleAndStatusOnManuscript,
      ).toHaveBeenCalledTimes(1)
      expect(result).toEqual(false)
    })
  })
  describe('is approval editor policy', () => {
    it('returns true when the user is approval editor', async () => {
      jest.spyOn(TeamMember, 'isApprovalEditor').mockResolvedValueOnce(true)

      const result = await authsomeMode(
        'userId',
        { policies: ['isApprovalEditor'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(result).toEqual(true)
    })
    it('returns false when the user does not have access to the manuscript', async () => {
      jest.spyOn(TeamMember, 'isApprovalEditor').mockResolvedValueOnce(false)

      const result = await authsomeMode(
        'userId',
        { policies: ['isApprovalEditor'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(result).toEqual(false)
    })
  })
  describe('can make recommendation policy', () => {
    it('returns true when the user is the academic editor on the given manuscript', async () => {
      jest
        .spyOn(Manuscript, 'find')
        .mockResolvedValueOnce({ journalId: 'some-journal-id' })
      jest
        .spyOn(PeerReviewModel, 'findOneByJournal')
        .mockResolvedValueOnce({ approvalEditors: ['academicEditor'] })
      jest
        .spyOn(Team, 'findOneByManuscriptAndUserAndRole')
        .mockResolvedValueOnce({})

      const result = await authsomeMode(
        'userId',
        { policies: ['canMakeRecommendation'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(result).toEqual(true)
    })
    it('returns false when the user is not the academic editor on the given manuscript', async () => {
      jest
        .spyOn(Manuscript, 'find')
        .mockResolvedValueOnce({ journalId: 'some-journal-id' })
      jest
        .spyOn(PeerReviewModel, 'findOneByJournal')
        .mockResolvedValueOnce({ approvalEditors: ['academicEditor'] })
      jest.spyOn(Team, 'findOneByManuscriptAndUserAndRole').mockReturnValue()

      const result = await authsomeMode(
        'userId',
        { policies: ['canMakeRecommendation'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(result).toEqual(false)
    })
    it('returns false when no manuscript is given', async () => {
      jest.spyOn(Manuscript, 'find').mockReturnValue()

      const result = await authsomeMode(
        'userId',
        { policies: ['canMakeRecommendation'] },
        { manuscriptId: undefined },
        {
          models,
        },
      )

      expect(result).toEqual(false)
    })
    it('returns false when the approval editor on the peer review model is not the academic editor', async () => {
      jest
        .spyOn(Manuscript, 'find')
        .mockResolvedValueOnce({ journalId: 'some-journal-id' })
      jest
        .spyOn(PeerReviewModel, 'findOneByJournal')
        .mockResolvedValueOnce({ approvalEditors: ['triageEditor'] })

      const result = await authsomeMode(
        'userId',
        { policies: ['canMakeRecommendation'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(result).toEqual(false)
    })
  })
  describe('is reviewer on review policy', () => {
    it('returns true when the user is a reviewer on the given manuscript', async () => {
      jest
        .spyOn(Review, 'find')
        .mockResolvedValueOnce({ manuscriptId: 'some-manuscript-id' })
      jest
        .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
        .mockResolvedValueOnce({})

      const result = await authsomeMode(
        'userId',
        { policies: ['isReviewerOnReview'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(result).toEqual(true)
    })
    it('returns false when the user is not a reviewer on the given manuscript', async () => {
      jest
        .spyOn(Review, 'find')
        .mockResolvedValueOnce({ manuscriptId: 'some-manuscript-id' })
      jest
        .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
        .mockReturnValue()

      const result = await authsomeMode(
        'userId',
        { policies: ['isReviewerOnReview'] },
        { manuscriptId: 'manuscript.id' },
        {
          models,
        },
      )

      expect(result).toEqual(false)
    })
  })

  describe('refererIsSameAsOwner() policy', () => {
    it('should return false when the user that makes the call is different than the editorial assistant id', async () => {
      const result = await authsomeMode(
        '7364e0f1-6e04-4c85-b98f-b362841f4d47',
        { policies: ['refererIsSameAsOwner'] },
        {
          input: {
            editorialAssistantId: '4314e0f1-6e32-4c85-b98f-b362841f4d42',
          },
        },
        {},
      )

      expect(result).toEqual(false)
    })

    it('should return true when the user that makes the call is the same as the editorial assistant id', async () => {
      const result = await authsomeMode(
        '7364e0f1-6e04-4c85-b98f-b362841f4d47',
        { policies: ['refererIsSameAsOwner'] },
        {
          input: {
            editorialAssistantId: '7364e0f1-6e04-4c85-b98f-b362841f4d47',
          },
        },
        {},
      )

      expect(result).toEqual(true)
    })

    it('should return false if the is no userId - aka not logged in', async () => {
      const result = await authsomeMode(
        '',
        { policies: ['refererIsSameAsOwner'] },
        {
          input: {
            editorialAssistantId: '7364e0f1-6e04-4c85-b98f-b362841f4d47',
          },
        },
        {},
      )

      expect(result).toEqual(false)
    })
  })
})
