const models = require('@pubsweet/models')
const { db } = require('@pubsweet/db-manager')
const logger = require('@pubsweet/logger')
const { createQueueService } = require('@hindawi/queue-service')

const { Promise } = require('bluebird')
const { Manuscript, Team, TeamMember, Journal, User } = models
const events = require('component-events')
const eventsService = events.initialize({ models })

const {
  useCases: { getUserWithWorkloadUseCase },
} = require('component-model')

const execute = async () => {
  try {
    await createQueueService()
      .then(queueService => {
        global.applicationEventBus = queueService
      })
      .catch(err => {
        logger.error(err)
        process.exit(1)
      })

    const journals = await Journal.findAllActiveJournals()
    await Promise.each(journals, async ({ id: journalId }) => {
      const journalEditorialAssistants = await TeamMember.findAllByJournalAndRole(
        {
          journalId,
          role: Team.Role.editorialAssistant,
        },
      )
      if (!journalEditorialAssistants.length) return
      const journalEditorialAssistantsUserIds = journalEditorialAssistants.map(
        journalEditorialAssistant => journalEditorialAssistant.userId,
      )
      const submissionsWithWrongEA = await findInProgressSubmissionsWithWrongEA(
        { journalId, journalEditorialAssistantsUserIds },
      )
      if (!submissionsWithWrongEA.length) return

      await Promise.each(
        submissionsWithWrongEA,
        async ({ submissionId, journalId }) => {
          const manuscriptsInSubmission = await Manuscript.findAll({
            queryObject: {
              submissionId,
            },
          })
          await removeWrongEditorialAssistant(manuscriptsInSubmission)
          await assignEditorialAssistant({
            journalId,
            manuscriptsInSubmission,
          })
        },
      )
    })
    process.exit()
  } catch (err) {
    logger.error(err)
    throw new Error('Something went wrong.')
  }
}
execute()
const findInProgressSubmissionsWithWrongEA = ({
  journalId,
  journalEditorialAssistantsUserIds,
}) => {
  const finishedManuscriptStatuses = [
    Manuscript.Statuses.published,
    Manuscript.Statuses.rejected,
    Manuscript.Statuses.void,
    Manuscript.Statuses.withdrawn,
    Manuscript.Statuses.accepted,
    Manuscript.Statuses.refusedToConsider,
    Manuscript.Statuses.deleted,
    Manuscript.Statuses.olderVersion,
  ]
  const result = Manuscript.query()
    .select(
      db.raw('DISTINCT ON (m.submission_id) m.submission_id,m.journal_id'),
    )
    .from('manuscript as m')
    .join('team as t', 'm.id', 't.manuscriptId')
    .join('team_member as tm', 't.id', 'tm.teamId')
    .whereNotIn('tm.userId', journalEditorialAssistantsUserIds)
    .whereNotIn('m.status', finishedManuscriptStatuses)
    .andWhere('m.journalId', journalId)
    .andWhere('tm.status', TeamMember.Statuses.active)
    .andWhere('t.role', Team.Role.editorialAssistant)
  return result
}
async function removeWrongEditorialAssistant(manuscriptsInSubmission) {
  await Promise.each(manuscriptsInSubmission, async ({ id: manuscriptId }) => {
    const currentEditorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    currentEditorialAssistant.updateProperties({
      status: TeamMember.Statuses.removed,
    })
    await currentEditorialAssistant.save()
    logger.info(
      `Removed the EA with user id ${currentEditorialAssistant.userId} from manuscript with id ${manuscriptId}`,
    )
  })
  await eventsService.publishSubmissionEvent({
    submissionId: manuscriptsInSubmission[0].submissionId,
    eventName: 'SubmissionEditorialAssistantRemoved',
  })
}
const getNewEAByWorkload = async journalId => {
  const getUserWithWorkload = await getUserWithWorkloadUseCase.initialize(
    models,
  )
  const userForAssignment = await getUserWithWorkload.execute({
    role: Team.Role.editorialAssistant,
    journalId,
    manuscriptStatuses: [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.inQA,
      Manuscript.Statuses.technicalChecks,
      Manuscript.Statuses.qualityChecksRequested,
      Manuscript.Statuses.qualityChecksSubmitted,
    ],
    teamMemberStatuses: [TeamMember.Statuses.active],
  })

  return User.find(userForAssignment.userId, 'identities')
}
async function assignEditorialAssistant({
  journalId,
  manuscriptsInSubmission,
}) {
  const user = await getNewEAByWorkload(journalId)

  await Promise.each(manuscriptsInSubmission, async ({ id: manuscriptId }) => {
    const editorialAssistantTeam = await Team.findOneByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.editorialAssistant,
    })
    editorialAssistantTeam.addMember({
      user,
      options: { status: TeamMember.Statuses.active },
    })
    await editorialAssistantTeam.saveGraph({
      relate: true,
      noUpdate: true,
    })
    logger.info(
      `Assigned EA with id ${user.id} on manuscript with id ${manuscriptId}`,
    )
  })
  await eventsService.publishSubmissionEvent({
    submissionId: manuscriptsInSubmission[0].submissionId,
    eventName: 'SubmissionEditorialAssistantAssigned',
  })
}
