const { db } = require('@pubsweet/db-manager')
const logger = require('@pubsweet/logger')

const getLatestVersions = async _ => {
  const res = await db
    .select(db.raw(`DISTINCT ON (m.submission_id) m.id`))
    .from('manuscript as m')
    .where(query => query.where('m.version', 1).orWhereNot('m.status', 'draft'))
    .orderByRaw(
      `m.submission_id,
      m.created DESC`,
    )
    .pluck('id')

  return res
}

const updateColumnValue = async batch => {
  await db('manuscript')
    .whereIn('id', batch)
    .update({ isLatestVersion: true })
}

const execute = async _ => {
  const ids = await getLatestVersions()
  logger.info(`Found ${ids.length} manuscripts to update`)
  const limit = 100
  let offset = 0
  while (offset * limit < ids.length) {
    const batch = ids.slice(offset * limit, offset * limit + limit)
    await updateColumnValue(batch)
    logger.info(`Updated isLatestVersion for BATCH ${offset + 1}`)
    ++offset
  }

  process.exit()
}
try {
  execute()
} catch (e) {
  throw e
}
