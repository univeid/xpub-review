const models = require('@pubsweet/models')
const { Promise } = require('bluebird')
const events = require('../../component-events')
const logger = require('@pubsweet/logger')

const eventsService = events.initialize({ models })
const { SpecialIssue, Section } = models
const { createQueueService } = require('@hindawi/queue-service')

const execute = async () => {
  createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })
  const endedSpecialIssues = await SpecialIssue.findAllEndedSpecialIssues()

  if (!endedSpecialIssues.length)
    return logger.info(`There are no ended Special Issues left to update`)

  await Promise.each(
    endedSpecialIssues,
    async endedSpecialIssue => {
      endedSpecialIssue.updateProperties({ isActive: false })
      await endedSpecialIssue.save()

      const { sectionId } = endedSpecialIssue
      let { journalId } = endedSpecialIssue
      let eventName = 'JournalSpecialIssueUpdated'

      if (sectionId) {
        ;({ journalId } = await Section.find(sectionId))
        eventName = 'JournalSectionSpecialIssueUpdated'
      }

      eventsService.publishJournalEvent({
        journalId,
        eventName,
      })
    },
    { concurrency: 10 },
  )
}
execute()
