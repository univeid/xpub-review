const models = require('@pubsweet/models')
const logger = require('@pubsweet/logger')
const { createQueueService } = require('@hindawi/queue-service')
const events = require('component-events')

const eventsService = events.initialize({ models })

const isValidUUID = id =>
  /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/.test(
    id,
  )

const execute = async () => {
  const submissionId = process.argv[2]
  const eventName = process.argv[3]

  if (!submissionId || !isValidUUID(submissionId)) {
    logger.error('Please provide a valid submissionId as argument')
    process.exit(9)
  }

  if (!eventName) {
    logger.error('Please provide a valid event type as argument')
    process.exit(9)
  }

  await createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  await eventsService.publishSubmissionEvent({
    submissionId,
    eventName,
  })

  process.exit()
}

execute()
