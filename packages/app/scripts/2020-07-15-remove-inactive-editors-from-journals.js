const models = require('@pubsweet/models')
const fs = require('fs')
const path = require('path')
const logger = require('@pubsweet/logger')
const { Promise } = require('bluebird')

const { TeamMember } = models

const execute = async () => {
  try {
    const rawData = fs.readFileSync(
      path.join(__dirname, './files/editors-to-remove.json'),
    )
    const editors = JSON.parse(rawData)

    await Promise.each(editors, async editor => {
      await removeInactiveEditor(editor)
    })

    process.exit()
  } catch (err) {
    logger.error(err)
    throw new Error('Something went wrong.')
  }
}
execute()

async function removeInactiveEditor(editor) {
  const editorialMember = await TeamMember.findOneByEmailOnManuscriptParent(
    editor.Email,
  )
  if (!editorialMember) {
    logger.warn(`No team member found with ${editor.Email}`)

    return
  }
  await editorialMember.delete()

  logger.info(`Removed the Editor with email ${editorialMember.alias.email}`)
}
