module.exports = {
  logo: '/assets/logo.png',
  name: 'Hindawi',
  supportEmail: 'help@hindawi.com',
  pageTitle: 'Hindawi Review',
  links: {
    websiteLink: 'https://new.hindawi.com/',
    privacyLink: 'https://new.hindawi.com/privacy/',
    termsLink: 'https://new.hindawi.com/terms/',
    ethicsLink: 'https://new.hindawi.com/ethics/',
    coiLink: 'https://new.hindawi.com/ethics/#conflicts-of-interest',
    dataAvailabilityLink:
      'https://new.hindawi.com/publish-research/authors/research-data/',
    apcLink: 'https://new.hindawi.com/journals/{journalCode}/apc',
    guidelinesLink: 'https://www.hindawi.com/publish-research/reviewers/',
    reviewLink: 'https://review.hindawi.com',
    invoicingLink: 'https://invoicing.hindawi.com',
    authorServiceLink:
      'https://www.hindawi.com/publish-research/authors/author-services/?utm_source=email&utm_medium=marketing%20referral&utm_campaign=HDW_MRKT_GBL_AWA_EMIL_OWN_AUTH_HIND_X2_10170',
  },
  emailData: {
    logo:
      'https://s3-eu-west-1.amazonaws.com/review.hindawi.com/logo-hindawi.png',
    ctaColor: '#63a945',
    logoLink: 'https://hindawi.com',
    shortReviewLink: 'review.hindawi.com',
    privacy:
      'Hindawi respects your right to privacy. Please see our <a style="color: #007e92; text-decoration: none;" href="https://www.hindawi.com/privacy/">privacy policy</a> for information on how we store, process, and safeguard your data.',
    address:
      'Hindawi Limited, 3rd Floor, Adam House, 1 Fitzroy Square, London, W1T 5HF, United Kingdom',
    publisher: 'Hindawi Limited',
    footerText: {
      unregisteredUsers: `This email was sent to {recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a submitted paper, published by Hindawi Limited.`,
      registeredUsers: `This email was sent to {recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a submitted paper, published by Hindawi Limited.`,
    },
  },
}
