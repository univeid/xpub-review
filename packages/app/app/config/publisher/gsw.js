module.exports = {
  logo: '/assets/logo.png',
  name: 'GeoScienceWorld',
  supportEmail: 'editorial@geoscienceworld.org',
  pageTitle: 'Lithosphere Review',
  links: {
    websiteLink: 'https://pubs.geoscienceworld.org/',
    privacyLink: 'https://pubs.geoscienceworld.org/pages/privacy-policy',
    termsLink: 'https://pubs.geoscienceworld.org/pages/terms-and-conditions',
    ethicsLink:
      'https://pubs.geoscienceworld.org/lithosphere/pages/code-of-publishing-ethics',
    coiLink:
      'https://pubs.geoscienceworld.org/lithosphere/pages/instructions-for-authors',
    dataAvailabilityLink:
      'https://pubs.geoscienceworld.org/lithosphere/pages/instructions-for-authors',
    apcLink:
      'https://pubs.geoscienceworld.org/lithosphere/pages/about-lithosphere',
    guidelinesLink:
      'https://pubs.geoscienceworld.org/lithosphere/pages/instructions-for-authors',
    reviewLink: 'https://review.lithosphere.geoscienceworld.org',
    invoicingLink: 'https://invoicing.lithosphere.geoscienceworld.org/',
    authorServiceLink:
      'https://pubs.geoscienceworld.org/lithosphere/pages/instructions-for-authors',
  },
  emailData: {
    logo:
      'https://s3-eu-west-1.amazonaws.com/review.hindawi.com/logo-lithosphere.png',
    ctaColor: '#63a945',
    logoLink: 'https://pubs.geoscienceworld.org/',
    shortReviewLink: 'review.lithosphere.geoscienceworld.org',
    privacy: `GeoScienceWorld respects your right to privacy. Please see our <a style="color: #007e92; text-decoration: none;" href="https://pubs.geoscienceworld.org/pages/privacy-policy">privacy policy</a> for information on how we store, process, and safeguard your data.
      Hindawi respects your right to privacy. Please see our <a style="color: #007e92; text-decoration: none;" href="https://www.hindawi.com/privacy/">privacy policy</a> for information on how we store, process, and safeguard your data.`,
    address:
      'GeoScienceWorld, a nonprofit corporation conducting business in the Commonwealth of Virginia at 1750 Tysons Boulevard, Suite 1500, McLean, Virginia 22102.',
    publisher: 'GeoScienceWorld',
    footerText: {
      unregisteredUsers:
        'This email was sent to {recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a submitted paper, published by GeoScienceWorld and supported by our publishing partner, Hindawi Limited.',
      registeredUsers: `This email was sent to {recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a submitted paper, published by GeoScienceWorld and supported by our publishing partner, Hindawi Limited.`,
    },
  },
}
