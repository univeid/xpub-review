process.env.NODE_ENV = 'development'
process.env.BABEL_ENV = 'development'

const path = require('path')
const config = require('config')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const rules = require('./rules.development')
const resolve = require('./common-resolve')

module.exports = [
  {
    name: 'app',
    watch: true,
    target: 'web',
    mode: 'development',
    context: path.join(__dirname, '..', 'app'),
    entry: {
      app: ['webpack-hot-middleware/client?reload=true', './app'],
    },
    output: {
      path: path.join(__dirname, '..', '_build', 'assets'),
      filename: '[name].js',
      publicPath: '/assets/',
    },
    devtool: 'eval-source-map', // 'cheap-module-source-map',
    module: {
      rules,
    },
    resolve,
    plugins: [
      new HtmlWebpackPlugin({
        title: config.get('publisherConfig.pageTitle'),
        template: path.join(__dirname, '..', 'app/index.html'),
      }),
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.KEYCLOAK': JSON.stringify(config.get('keycloak.public')),
        'process.env.PUBLISHER_NAME':
          JSON.stringify(process.env.PUBLISHER_NAME) || 'hindawi',
      }),
      new webpack.ContextReplacementPlugin(/./, __dirname, {
        [config.authsome.mode]: config.authsome.mode,
      }),
      new CopyWebpackPlugin([
        { from: `../static/${process.env.PUBLISHER_NAME}` },
        {
          from: '../app/silent-check-sso.html',
          to: './silent-check-sso.html',
        },
      ]),
    ],
    node: {
      fs: 'empty',
      __dirname: true,
    },
  },
]
