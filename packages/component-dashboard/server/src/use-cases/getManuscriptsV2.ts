export const initialize = models => ({
  async execute({ input, useCases, userId }) {
    const { Manuscript } = models
    const { getManuscriptsV2UseCase } = useCases
    const queryResults = await getManuscriptsV2UseCase
      .initialize(models)
      .execute({ input, userId })

    const { results, total: totalCount } = queryResults
    const statuses = Object.values(Manuscript.Statuses)
    const manuscripts = results.map(m => m.toDTO())
    return { manuscripts, totalCount, statuses }
  },
})

export const authsomePolicies = ['refererIsSameAsOwner']
