export const initialize = models => ({
  async execute({ input, useCases, userId }) {
    const { getManuscriptsV2UseCase } = useCases
    return getManuscriptsV2UseCase.initialize(models).execute({ input, userId })
  },
})

export const authsomePolicies = ['isAuthenticated']
