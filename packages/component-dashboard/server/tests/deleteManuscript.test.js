process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Journal, Team, Manuscript } = models
const { deleteManuscriptUseCase } = require('../src/use-cases')

describe('Delete manuscript use case', () => {
  it('deletes a draft manuscript as admin', async () => {
    const journal = fixtures.generateJournal({ Journal })

    const admin = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.admin,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.draft,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    const initialManuscriptLength = fixtures.manuscripts.length
    await deleteManuscriptUseCase
      .initialize(models)
      .execute({ manuscriptId: manuscript.id, userId: admin.userId })

    expect(fixtures.manuscripts.length).toBeLessThan(initialManuscriptLength)
  })

  it('deletes a draft manuscript (without an author) as admin', async () => {
    const journal = fixtures.generateJournal({ Journal })

    const admin = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.admin,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.draft,
      },
      Manuscript,
    })

    const initialManuscriptLength = fixtures.manuscripts.length
    await deleteManuscriptUseCase
      .initialize(models)
      .execute({ manuscriptId: manuscript.id, userId: admin.userId })

    expect(fixtures.manuscripts.length).toBeLessThan(initialManuscriptLength)
  })

  it('deletes a draft manuscript as an author', async () => {
    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.draft,
      },
      Manuscript,
    })
    const author = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    const initialManuscriptLength = fixtures.manuscripts.length
    await deleteManuscriptUseCase
      .initialize(models)
      .execute({ manuscriptId: manuscript.id, userId: author.userId })

    expect(fixtures.manuscripts.length).toBeLessThan(initialManuscriptLength)
  })

  it('does not delete the draft manuscript if user is reviewer', async () => {
    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.draft,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    const reviewer = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
    })

    try {
      await deleteManuscriptUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id, userId: reviewer.userId })
    } catch (err) {
      expect(err.message).toEqual('Not enough rights to delete this manuscript')
    }
  })

  it('returns an error when trying to delete a non-draft manuscript', async () => {
    const journal = fixtures.generateJournal({ Journal })
    const admin = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.admin,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.submitted,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const result = deleteManuscriptUseCase
      .initialize(models)
      .execute({ manuscriptId: manuscript.id, userId: admin.userId })
    return expect(result).rejects.toThrow(
      'Only draft manuscripts can be deleted',
    )
  })
})
