import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Col, Card } from '@hindawi/phenom-ui'

import {
  EaDashboardFilters,
  SearchManuscriptsWidget,
  ManuscriptsList,
  ManuscriptsListHeader,
  Pagination,
  StatusFilters,
} from '../components'

import { EAManuscriptsDashboardProvider } from '../store/EADashboard'
import {
  useManuscripts,
  searchManuscriptInputResolverI,
} from '../store/EADashboard/manuscripts'
import { ManagerApiI, connect } from '../store/'

// #region styles
const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 8);
`

const Content = styled(Card)`
  width: 100%;
`

const RightCol = styled(Col)`
  max-width: calc(100% - 275px);
`
// #endregion

export interface FiltersPropsI {
  manuscripts: ManagerApiI<any, searchManuscriptInputResolverI>
}

export const EaDashboard: React.FC = () => {
  const manuscriptsManager = useManuscripts()

  return (
    <Root>
      <Row gutter={[12, 12]}>
        <Col span={24}>
          <SearchManuscriptsWidget />
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Content bordered={false}>
            <Row gutter={[12, 12]}>
              <Col flex="275px">
                <StatusFilters manuscripts={manuscriptsManager} />
              </Col>
              <RightCol flex="auto">
                <EaDashboardFilters manuscripts={manuscriptsManager} />
                <ManuscriptsListHeader manuscripts={manuscriptsManager} />
                <ManuscriptsList
                  loading={manuscriptsManager.loading}
                  manuscripts={manuscriptsManager.value}
                />
                <Row justify="end">
                  <Pagination manuscripts={manuscriptsManager} />
                </Row>
              </RightCol>
            </Row>
          </Content>
        </Col>
      </Row>
    </Root>
  )
}

export default connect(EAManuscriptsDashboardProvider, EaDashboard)
