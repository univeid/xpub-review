import gql from 'graphql-tag'

export const manuscriptMetaDashboard = gql`
  fragment manuscriptMetaDashboard on ManuscriptMeta {
    articleType
    title
  }
`

export const teamMemberDashboard = gql`
  fragment teamMemberDashboard on TeamMember {
    id
    status
    alias {
      name {
        surname
        givenNames
      }
    }
  }
`

export const articleTypeFragment = gql`
  fragment articleTypeDetails on ArticleType {
    id
    name
    hasPeerReview
  }
`

export const authorDashboard = gql`
  fragment authorDashboard on TeamMember {
    id
    isSubmitting
    isCorresponding
    alias {
      aff
      email
      country
      name {
        surname
        givenNames
      }
    }
  }
`

export const dashboardManuscriptDetails = gql`
  fragment dashboardManuscriptDetails on Manuscript {
    id
    submissionId
    role
    status
    customId
    created
    visibleStatus
    inProgress
    peerReviewModel {
      academicEditorLabel
      triageEditorLabel
    }
    journal {
      id
      name
    }
    articleType {
      ...articleTypeDetails
    }
    meta {
      ...manuscriptMetaDashboard
    }
    academicEditor {
      ...teamMemberDashboard
    }
    authors {
      ...authorDashboard
    }
    researchIntegrityPublishingEditor {
      ...teamMemberDashboard
    }
    reviewers {
      id
      status
    }
    specialIssue {
      id
    }
    version
    statusColor
  }
  ${manuscriptMetaDashboard}
  ${teamMemberDashboard}
  ${authorDashboard}
  ${articleTypeFragment}
`

export const journalDetails = gql`
  fragment journalDetails on Journal {
    id
    name
    code
    issn
    apc
    email
    activationDate
    articleTypes {
      id
      name
    }
    isActive
    peerReviewModel {
      id
      name
      hasSections
      hasTriageEditor
      triageEditorLabel
      academicEditorLabel
      hasFigureheadEditor
      figureheadEditorLabel
    }
  }
`
