import React, { useState } from 'react'
import { useCurrentUser } from 'component-authentication/client'

import {
  Row,
  Col,
  Button,
  Radio,
  Space,
  Typography,
  IconRemoveSolid,
} from '@hindawi/phenom-ui'

import { connect } from '../../../store'

import {
  SearchWithAutocomplete,
  Input,
  SearchCategoryWrapper,
  Wrapper,
  Card,
  ManuscriptListingWrapper,
} from './styles'

import { ManuscriptSuggestion, Suggestion } from '../SuggestionsManager'
import { ManuscriptsList } from '../ManuscriptsList/ManuscriptsList'
import { useWidgetManuscripts } from '../../../store/EADashboard/widgetManuscripts'
import { useWidgetFiltersManager } from '../../../store/EADashboard/widgetFilters'
import { useManuscriptSuggestionsManager } from '../../../store/EADashboard/widgetSuggestions'
import { EaDashboardProvider } from '../../../store/EADashboard/index'

const { Title } = Typography

const SearchManuscriptsWidget: React.FC = () => {
  const currentUser = useCurrentUser()
  const [searchCategory, setSearchCategory] = useState('id')
  const [widgetExpanded, setWidgetExpanded] = useState(false)

  const {
    manuscriptCustomId,
    manuscriptByTtitle,
    searchManuscripts,
  } = useWidgetFiltersManager({
    editorialAssistantId: (currentUser as { id: string }).id,
  })

  const manuscriptsSuggestions = useManuscriptSuggestionsManager({
    editorialAssistantId: (currentUser as { id: string }).id,
  })

  const {
    value: manuscripts,
    loading,
    reset: resetManuscripts,
    fetch: fetchManuscripts,
  } = useWidgetManuscripts()

  const getManuscripts = () => {
    fetchManuscripts({
      editorialAssistantId: (currentUser as { id: string }).id,
    })
  }

  const categoryChangeHandler = e => {
    setSearchCategory(e.target.value)
    resetManuscripts()
  }

  const closeWidget = () => {
    setWidgetExpanded(false)
    resetManuscripts()
    manuscriptsSuggestions.reset()
    manuscriptCustomId.set('')
  }

  const renderLabel = ({
    title,
    journal,
  }: ManuscriptSuggestion): JSX.Element => (
    <Space direction="vertical">
      <span>{journal.name}</span>
      <span>{title}</span>
    </Space>
  )

  const renderExpandedContent = () => {
    const hasManuscript = manuscripts.length

    let content = (
      <Space align="center" className="greeter-row" direction="vertical">
        <Title level={2}>
          <div>Nothing to show here. Please type</div>
          <div>or</div>
        </Title>
        <Button onClick={closeWidget} type="secondary">
          Close
        </Button>
      </Space>
    )

    if (hasManuscript || loading) {
      content = (
        <ManuscriptListingWrapper direction="vertical">
          <ManuscriptsList
            limit={1}
            loading={loading}
            manuscripts={manuscripts}
          />
          <div className="close-button-wrapper">
            <Button
              icon={<IconRemoveSolid />}
              onClick={closeWidget}
              type="text"
            >
              Close
            </Button>
          </div>
        </ManuscriptListingWrapper>
      )
    }

    return content
  }

  return (
    <Wrapper className={widgetExpanded && 'widgetExpanded'}>
      <Card bordered={false} className="widget-content">
        <Row gutter={[12, 12]}>
          <Col flex="275px">
            <SearchCategoryWrapper>
              <span>Find manuscript by</span>
              <Radio.Group
                buttonStyle="solid"
                defaultValue="id"
                onChange={categoryChangeHandler}
                size="small"
                value={searchCategory}
              >
                <Radio.Button value="id">ID</Radio.Button>
                <Radio.Button value="title">Title</Radio.Button>
              </Radio.Group>
            </SearchCategoryWrapper>
          </Col>
          <Col flex="auto">
            {searchCategory === 'title' ? (
              <SearchWithAutocomplete
                getSuggestions={manuscriptsSuggestions.fetch}
                loading={manuscriptsSuggestions.loading}
                onFocus={() => setWidgetExpanded(true)}
                placeholder="Search by title"
                renderDisplayValue={({ title }: ManuscriptSuggestion) => title}
                renderLabel={renderLabel}
                renderValue={({ id }: Suggestion) => id}
                selectSuggestion={manuscriptByTtitle.set}
                suggestions={manuscriptsSuggestions.value}
                value={manuscriptByTtitle.value.displayValue}
              />
            ) : (
              <Input.Search
                enterButton
                onChange={e => manuscriptCustomId.set(e.target.value)}
                onFocus={() => setWidgetExpanded(true)}
                onSearch={getManuscripts}
                placeholder="Search by ID"
                value={manuscriptCustomId.value}
              />
            )}
          </Col>
        </Row>
        {widgetExpanded && renderExpandedContent()}
      </Card>
    </Wrapper>
  )
}

export default connect(EaDashboardProvider, SearchManuscriptsWidget)
