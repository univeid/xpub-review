import React from 'react'
import { Button, IconRemove, Checkbox, Space } from '@hindawi/phenom-ui'
import { useCurrentUser } from 'component-authentication/client'
import { Header, Collapse, Title } from './styles'
import { StatusCategoryT } from '../../../store/EADashboard/filters/statusCategories'
import { EaDashboardProvider } from '../../../store/EADashboard/index'
import { connect } from '../../../store'
import {
  useFiltersManager,
  FiltersPropsI,
} from '../../../store/EADashboard/filters'

const { Panel } = Collapse

export const StatusFilters: React.FC<FiltersPropsI> = ({ manuscripts }) => {
  const currentUser = useCurrentUser()

  const { statuses } = useFiltersManager({
    editorialAssistantId: (currentUser as { id: string }).id,
    manuscripts,
  })
  const {
    set: updateSelectedStatuses,
    reset,
    value: statusCategories,
  } = statuses

  const handleCheckboxChange = categoryLabel => newValues => {
    updateSelectedStatuses(categoryLabel, newValues)
  }

  return (
    <div>
      <Header>
        <Title>Manuscripts status</Title>
        <Button icon={<IconRemove />} onClick={reset} type="link">
          Reset all
        </Button>
      </Header>
      <Collapse
        accordion
        defaultActiveKey={['In Review']}
        expandIconPosition="right"
      >
        {statusCategories.map((category: StatusCategoryT) => (
          <Panel header={category.label} key={category.label}>
            <Space direction="vertical" size="small">
              <Checkbox.Group
                onChange={handleCheckboxChange(category.label)}
                options={category.statuses}
                value={category.selectedValues}
              />
            </Space>
          </Panel>
        ))}
      </Collapse>
    </div>
  )
}

export default connect(EaDashboardProvider, StatusFilters)
