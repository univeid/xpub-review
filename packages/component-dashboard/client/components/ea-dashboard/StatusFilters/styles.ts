import { Collapse as _Collapse } from '@hindawi/phenom-ui'
import styled from 'styled-components'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 26px;
`

export const Collapse = styled(_Collapse)`
  .anticon.ant-collapse-arrow {
    transform: translateY(-50%) rotate(90deg) !important;
  }

  .anticon.ant-collapse-arrow.is-active {
    transform: translateY(-50%) rotate(-90deg) !important;
  }

  label {
    vertical-align: middle;
    display: block;
  }
`

export const Title = styled.span`
  font-weight: 700;
`
