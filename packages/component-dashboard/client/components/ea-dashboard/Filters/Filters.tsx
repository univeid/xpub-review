import React from 'react'
import styled from 'styled-components'
import { Form, Row, Col, Input, Button } from '@hindawi/phenom-ui'
import { useCurrentUser } from 'component-authentication/client'
import { Suggestion } from '../SuggestionsManager'
import { SearchWithAutocomplete } from '../SearchWithAutocomplete'
import {
  useFiltersManager,
  FiltersPropsI,
} from '../../../store/EADashboard/filters'
import {
  useJournalSuggestionsManager,
  useSectionSuggestionsManager,
  useSpecialIssueSuggestionsManager,
} from '../../../store/EADashboard/suggestions'
import { EaDashboardProvider } from '../../../store/EADashboard/index'
import { connect } from '../../../store'

const { Item } = Form

const FormItem = styled(Item)`
  margin-bottom: 0;
`

const ColButton = styled(Col)`
  align-items: flex-end;
  display: flex;
  justify-content: flex-end;
`

const Filters: React.FC<FiltersPropsI> = ({ manuscripts }) => {
  const currentUser = useCurrentUser()
  const {
    journal,
    section,
    specialIssue,
    author,
    editor,
    searchManuscripts,
  } = useFiltersManager({
    editorialAssistantId: (currentUser as { id: string }).id,
    manuscripts,
  })
  // journals
  const journalSuggestions = useJournalSuggestionsManager({
    editorialAssistantId: (currentUser as { id: string }).id,
  })

  // sections
  const sectionSuggestions = useSectionSuggestionsManager({
    journalId: journal.value.value,
  })

  // special issues
  const specialIssueSuggestions = useSpecialIssueSuggestionsManager({
    ...(!section?.value?.value &&
      journal?.value?.value && {
        journalId: journal.value.value,
      }),
    sectionId: section?.value?.value || undefined,
  })

  const resetFilters = () => {
    journal.reset()
    section.reset()
    specialIssue.reset()
    journalSuggestions.reset()
    sectionSuggestions.reset()
    specialIssueSuggestions.reset()
    author.reset()
    editor.reset()
    searchManuscripts()
  }

  return (
    <Form layout="vertical">
      <Row gutter={[12, 12]}>
        <Col span={8}>
          <FormItem label="Journal">
            <SearchWithAutocomplete
              getSuggestions={journalSuggestions.fetch}
              loading={journalSuggestions.loading}
              renderDisplayValue={({ name }: Suggestion) => name}
              renderLabel={({ name }: Suggestion) => name}
              renderValue={({ id }: Suggestion) => id}
              selectSuggestion={journal.set}
              suggestions={journalSuggestions.value}
              value={journal.value.displayValue}
            />
          </FormItem>
        </Col>
        <Col span={8}>
          <FormItem label="Section">
            <SearchWithAutocomplete
              disabled={!journal.value}
              getSuggestions={sectionSuggestions.fetch}
              loading={sectionSuggestions.loading}
              renderDisplayValue={({ name }: Suggestion) => name}
              renderLabel={({ name }: Suggestion) => name}
              renderValue={({ id }: Suggestion) => id}
              selectSuggestion={section.set}
              suggestions={sectionSuggestions.value}
              value={section.value.displayValue}
            />
          </FormItem>
        </Col>
        <Col span={8}>
          <FormItem label="Special Issue">
            <SearchWithAutocomplete
              disabled={!journal.value}
              getSuggestions={specialIssueSuggestions.fetch}
              loading={specialIssueSuggestions.loading}
              renderDisplayValue={({ name }: Suggestion) => name}
              renderLabel={({ name }: Suggestion) => name}
              renderValue={({ id }: Suggestion) => id}
              selectSuggestion={specialIssue.set}
              suggestions={specialIssueSuggestions.value}
              value={specialIssue.value.displayValue}
            />
          </FormItem>
        </Col>
      </Row>
      <Row gutter={[12, 12]}>
        <Col span={8}>
          <Form.Item label="Author Email">
            <Input.Search
              enterButton
              onChange={e => author.set(e.target.value)}
              onSearch={searchManuscripts}
              placeholder="Filter by Author Email"
              value={author.value}
            />
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="Editor email">
            <Input.Search
              enterButton
              onChange={e => editor.set(e.target.value)}
              onSearch={searchManuscripts}
              placeholder="Filter by Editor Email"
              value={editor.value}
            />
          </Form.Item>
        </Col>
        <ColButton offset={4} span={4}>
          <Form.Item>
            <Button onClick={resetFilters} type="secondary">
              Clear filters
            </Button>
          </Form.Item>
        </ColButton>
      </Row>
    </Form>
  )
}

export default connect(EaDashboardProvider, Filters)
