import React from 'react'
import { Pagination as PaginationComponent } from '@hindawi/phenom-ui'
import { useCurrentUser } from 'component-authentication/client'
import { useManuscriptsTotal } from '../../../store/EADashboard/manuscripts'
import {
  useFiltersManager,
  FiltersPropsI,
} from '../../../store/EADashboard/filters'
import { EaDashboardProvider } from '../../../store/EADashboard/index'
import { connect } from '../../../store'

const Pagination: React.FC<FiltersPropsI> = ({ manuscripts }) => {
  const manuscriptsTotalCount = useManuscriptsTotal()
  const currentUser = useCurrentUser()

  const { page } = useFiltersManager({
    editorialAssistantId: (currentUser as { id: string }).id,
    manuscripts,
  })

  const handlePageChange = (pageNo: number): void => {
    page.set(pageNo)
  }

  return (
    <PaginationComponent
      current={page.value}
      defaultCurrent={1}
      onChange={handlePageChange}
      pageSize={10}
      showSizeChanger={false}
      total={manuscriptsTotalCount.value}
    />
  )
}

export default connect(EaDashboardProvider, Pagination)
