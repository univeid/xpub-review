import React from 'react'
import styled from 'styled-components'
import { Select } from '@hindawi/phenom-ui'
import { Form } from 'antd'
import { useCurrentUser } from 'component-authentication/client'
import { EaDashboardProvider } from '../../../store/EADashboard/index'
import { connect } from '../../../store'

import {
  useFiltersManager,
  orderOptions,
  orderDirections,
  FiltersPropsI,
} from '../../../store/EADashboard/filters'

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;

  .ant-form .ant-form-item:last-child {
    margin-right: 0;
  }

  .ant-select .ant-select-selector {
    padding: 0 !important;

    .ant-select-selection-item {
      padding-right: 15px !important;
    }
  }

  .ant-select-arrow {
    transform: translateX(10px);
  }
`

const ManuscriptsListHeader: React.FC<FiltersPropsI> = ({ manuscripts }) => {
  const currentUser = useCurrentUser()
  const { orderDirection, orderCriteria } = useFiltersManager({
    editorialAssistantId: (currentUser as { id: string }).id,
    manuscripts,
  })
  const handleCriteriaChange = value => orderCriteria.set(orderOptions[value])
  const handleDirectionChange = value =>
    orderDirection.set(orderDirections[value])

  return (
    <Wrapper>
      <div />
      <Form layout="inline" size="small">
        <Form.Item label="Sort by:">
          <Select
            bordered={false}
            dropdownMatchSelectWidth={100}
            onChange={handleCriteriaChange}
            options={Object.values(orderOptions)}
            value={orderCriteria.value.value}
          />
        </Form.Item>
        <Form.Item label="Order:">
          <Select
            bordered={false}
            dropdownMatchSelectWidth={75}
            onChange={handleDirectionChange}
            options={Object.values(orderDirections)}
            value={orderDirection.value.value}
          />
        </Form.Item>
      </Form>
    </Wrapper>
  )
}

export default connect(EaDashboardProvider, ManuscriptsListHeader)
