import React from 'react'
import styled from 'styled-components'
import { ManuscriptCard } from '../..'
import { PlaceholderList } from './PlaceholderList'

const Center = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

const renderCard = (m, index, arr) => (
  <ManuscriptCard
    academicEditor={m.academicEditor}
    data-test-id="row"
    deleteManuscript={() => {
      // console.log('deleteManuscript')
    }}
    isLast={index === arr.length - 1}
    key={m.id}
    manuscript={m}
    role={m.role}
    withdrawManuscript={() => {
      // console.log('withdrawManuscript')
    }}
  />
)

interface ManuscriptsListProps {
  limit?: number
  loading: boolean
  manuscripts: Array<any>
}

export const ManuscriptsList: React.FC<ManuscriptsListProps> = ({
  limit = 10,
  loading,
  manuscripts,
}) => {
  if (loading) {
    return <PlaceholderList limit={limit} />
  }

  if (manuscripts.length === 0) {
    return <Center>No manuscripts found.</Center>
  }
  const cards = manuscripts.map(renderCard).slice(0, limit)

  return <div>{cards}</div>
}
