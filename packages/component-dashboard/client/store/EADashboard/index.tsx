import React, { createContext } from 'react'
import {
  useFiltersState,
  ValueT as FilterValueT,
  Option,
  SelectOptionT,
} from './filters'
import { useFiltersState as useWidgetFiltersState } from './widgetFilters'
import { StateI } from '../'
import { useManuscriptsState, ValueT as ManuscriptsValueT } from './manuscripts'
import {
  useManuscriptsState as useWidgetManuscriptsState,
  ValueT as WidgetManuscriptsValueT,
} from './widgetManuscripts'

import {
  ValueT as SuggestionValueT,
  useJournalSuggestionsState,
  useSectionSuggestionsState,
  useSpecialIssueSuggestionsState,
  useManuscriptSuggestionsState,
} from './suggestions'

import { StatusCategoriesT } from './filters/statusCategories'

export interface FiltersContextI {
  journal?: StateI<Option>
  section?: StateI<Option>
  specialIssue?: StateI<Option>
  statuses?: StateI<StatusCategoriesT>
  author?: StateI<FilterValueT>
  academicEditor?: StateI<FilterValueT>
  page?: StateI<FilterValueT>
  orderCriteria?: StateI<SelectOptionT>
  orderDirection?: StateI<SelectOptionT>
  widgetManuscriptByTitle?: StateI<FilterValueT>
  widgetManuscriptId?: StateI<FilterValueT>
  widgetManuscripts?: StateI<ManuscriptsValueT>
  journalSuggestions?: StateI<SuggestionValueT>
  sectionSuggestions?: StateI<SuggestionValueT>
  specialIssueSuggestions?: StateI<SuggestionValueT>
  manuscriptSuggestions?: StateI<SuggestionValueT>
}

export interface ManuscriptContextI {
  manuscripts?: StateI<ManuscriptsValueT>
  totalManuscripts?: StateI<number>
}

export const DashboardContext: React.Context<FiltersContextI> = createContext(
  {},
)
export const DashboardManuscriptContext: React.Context<ManuscriptContextI> = createContext(
  {},
)

export const EaDashboardProvider = ({ children }) => {
  const { entries: widgetManuscripts } = useWidgetManuscriptsState()

  const state = {
    ...useFiltersState(), // selected journal, selected section, selected si, author, ae, statuses
    ...useWidgetFiltersState(),
    widgetManuscripts,
    journalSuggestions: useJournalSuggestionsState(),
    sectionSuggestions: useSectionSuggestionsState(),
    specialIssueSuggestions: useSpecialIssueSuggestionsState(),
    manuscriptSuggestions: useManuscriptSuggestionsState(),
  }
  return (
    <DashboardContext.Provider value={state}>
      {children}
    </DashboardContext.Provider>
  )
}

export const EAManuscriptsDashboardProvider = ({ children }) => {
  const { entries: manuscripts, total } = useManuscriptsState()

  const state = {
    manuscripts,
    totalManuscripts: total,
  }

  return (
    <DashboardManuscriptContext.Provider value={state}>
      {children}
    </DashboardManuscriptContext.Provider>
  )
}
