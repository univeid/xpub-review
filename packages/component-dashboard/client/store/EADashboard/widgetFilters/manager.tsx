import { useContext, useMemo } from 'react'
import { DashboardContext } from '../'
import { initialOption, Option } from './state'
import { ManagerApiI } from '../../'
import {
  useWidgetManuscripts,
  searchManuscriptInputResolverI,
} from '../widgetManuscripts'

const useWidgetFilterManager = ({ context }) => {
  const { value, set } = context

  const reset = () => {
    set(initialOption) // or empty string, pass from manager
  }

  return {
    value,
    set,
    reset,
  }
}

export const useManuscriptByTitleManager: () => ManagerApiI<
  Option,
  null
> = () => {
  const context = useContext(DashboardContext).widgetManuscriptByTitle
  const filterManager = useWidgetFilterManager({ context })
  return filterManager
}

export const useManuscriptCustomIdManager: () => ManagerApiI<
  string,
  null
> = () => {
  const context = useContext(DashboardContext).widgetManuscriptId
  return useWidgetFilterManager({ context })
}

export const useWidgetFiltersManager = (
  params: Partial<searchManuscriptInputResolverI>,
) => {
  const manuscripts = useWidgetManuscripts()

  const manuscriptByTtitle = useManuscriptByTitleManager()
  const manuscriptCustomId = useManuscriptCustomIdManager()

  const searchManuscripts = () => {
    const payload: Partial<searchManuscriptInputResolverI> = {
      id: manuscriptByTtitle.value.value,
      ...params,
    }

    manuscripts.fetch(payload)
  }

  useMemo(() => searchManuscripts(), [manuscriptByTtitle.value.value])

  return {
    manuscriptByTtitle,
    manuscriptCustomId,
    searchManuscripts,
  }
}
