import { useContext, useEffect, useMemo } from 'react'
import { useTracking } from 'react-tracking'
import { useLazyQuery } from 'react-apollo'
import { pickBy } from 'lodash'
import { queries } from '../../../graphql'
import { getVisibleStatusForManuscript } from '../filters/statusCategories'
import { ManagerApiI, ManagerT } from '../../'
import { DashboardManuscriptContext } from '../'
import { ValueT } from './state'

const timer = {
  startTime: 0,
  initStartTime: () => (timer.startTime = performance.now()),
  getElapsedTime: () => performance.now() - timer.startTime,
}

let usedFilters = []

export interface searchManuscriptInputResolverI {
  editorialAssistantId?: string
  rangeStart?: number
  rangeEnd?: number
  customId?: string
  id?: string
  authorEmail?: string
  academicEditorEmail?: string
  academicEditorStatus?: string
  journalId?: string
  sectionId?: string
  specialIssueId?: string
  status?: string
  orderBy?: string
  orderByDesc?: string
}

type useManuscriptManagerT = () => ManagerApiI<
  ValueT,
  searchManuscriptInputResolverI
>

type useManuscriptTotalManagerT = () => ManagerApiI<number, null>

export const useManuscriptsTotal: useManuscriptTotalManagerT = () => {
  const { value, set } = useContext(DashboardManuscriptContext).totalManuscripts

  return {
    value,
    set,
  }
}

export const useManuscripts: useManuscriptManagerT = () => {
  const { value, set } = useContext(DashboardManuscriptContext).manuscripts

  const { set: setTotal } = useManuscriptsTotal()
  const tracking = useTracking()
  const query = 'getManuscriptsV2'

  const setTotalCount = data => {
    const totalManuscriptsCount =
      data && data[query] && data[query].totalCount ? data[query].totalCount : 0
    setTotal(totalManuscriptsCount)
  }

  const setManuscripts = data => {
    const fetchedManuscripts =
      data && data[query] && data[query].manuscripts
        ? data[query].manuscripts
        : []

    set(
      fetchedManuscripts.map(manuscript => ({
        ...manuscript,
        visibleStatus: getVisibleStatusForManuscript(manuscript.status),
      })),
    )
  }

  const [queryManuscripts, { data, loading }] = useLazyQuery(queries[query], {
    fetchPolicy: 'network-only',
    onCompleted: () => {
      tracking.trackEvent({
        userRole: 'Editorial Assistant',
        pageTemplate: 'Editorial Assitant Page',
        queryTime: timer.getElapsedTime(),
        filters: usedFilters,
      })

      setTotalCount(data)
      setManuscripts(data)

      usedFilters = []
    },
  })

  const getManuscripts = ({
    rangeStart = 0,
    rangeEnd = 10,
    editorialAssistantId,
    customId,
    id,
    authorEmail,
    academicEditorEmail,
    academicEditorStatus,
    journalId,
    sectionId,
    specialIssueId,
    status,
    orderBy,
    orderByDesc,
  }: Partial<searchManuscriptInputResolverI>) => {
    timer.initStartTime()

    usedFilters = Object.keys(
      pickBy(
        {
          customId,
          id,
          journalId,
          sectionId,
          specialIssueId,
          authorEmail,
          academicEditorEmail,
          status,
          orderBy,
          orderByDesc,
        },
        Boolean,
      ),
    )

    return queryManuscripts({
      variables: {
        input: {
          editorialAssistantId,
          customId,
          id,
          rangeStart,
          rangeEnd,
          status: status || undefined,
          authorEmail: authorEmail || undefined,
          academicEditorEmail: academicEditorEmail || undefined,
          academicEditorStatus: academicEditorStatus || undefined,
          isLatestVersion: true,
          journalId,
          sectionId,
          specialIssueId,
          orderBy,
          orderByDesc,
        },
      },
    })
  }

  return {
    value,
    fetch: getManuscripts,
    set,
    loading,
  }
}
