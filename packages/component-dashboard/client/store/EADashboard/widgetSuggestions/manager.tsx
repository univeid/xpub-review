import { useContext } from 'react'
import { DashboardContext } from '../'
import { useSuggestionsManager } from '../suggestions/useSuggestionsManager'
import { ManagerApiI } from '../../'
import { ValueT } from './state'

interface useSuggestionManagerProps {
  editorialAssistantId?: string
  journalId?: string
  sectionId?: string
}

type useSuggestionManagerT = (
  props?: useSuggestionManagerProps,
) => ManagerApiI<ValueT, string>

export const useManuscriptSuggestionsManager: useSuggestionManagerT = params => {
  const context = useContext(DashboardContext).manuscriptSuggestions
  const query = 'getManuscriptsSuggestions'

  const inputResolver = (searchTerm: string) => ({
    title: searchTerm,
    isLatestVersion: true,
    ...params, // editorialAssistantId: (currentUser as { id: string }).id, // pass this from component
  })

  return useSuggestionsManager({
    context,
    query,
    inputResolver,
  })
}
