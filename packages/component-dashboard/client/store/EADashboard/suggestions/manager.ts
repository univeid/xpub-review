import { useContext } from 'react'
import { DashboardContext } from '../'
import { useSuggestionsManager } from './useSuggestionsManager'
import { ManagerApiI } from '../../'
import { ValueT } from './state'

interface useSuggestionManagerProps {
  editorialAssistantId?: string
  journalId?: string
  sectionId?: string
}

type useSuggestionManagerT = (
  props?: useSuggestionManagerProps,
) => ManagerApiI<ValueT, string>

export const useJournalSuggestionsManager: useSuggestionManagerT = params => {
  const context = useContext(DashboardContext).journalSuggestions
  const query = 'getJournalsSuggestions'

  const inputResolver = (searchTerm: string) => ({
    name: searchTerm,
    ...params, // editorialAssistantId: (currentUser as { id: string }).id, // pass this from component
  })

  return useSuggestionsManager({
    context,
    query,
    inputResolver,
  })
}

export const useSectionSuggestionsManager: useSuggestionManagerT = params => {
  const context = useContext(DashboardContext).sectionSuggestions
  const query = 'getSectionsSuggestions'

  const inputResolver = (searchTerm: string) => ({
    name: searchTerm,
    ...params,
  })

  return useSuggestionsManager({
    context,
    query,
    inputResolver,
  })
}

export const useSpecialIssueSuggestionsManager: useSuggestionManagerT = params => {
  const context = useContext(DashboardContext).specialIssueSuggestions
  const query = 'getSpecialIssuesSuggestions'

  const inputResolver = (searchTerm: string) => ({
    name: searchTerm,
    ...params,
  })

  return useSuggestionsManager({
    context,
    query,
    inputResolver,
  })
}

export const useManuscriptSuggestionsManager: useSuggestionManagerT = params => {
  const context = useContext(DashboardContext).manuscriptSuggestions
  const query = 'getManuscriptsV2'

  const inputResolver = (searchTerm: string) => ({
    name: searchTerm,
    ...params, // editorialAssistantId: (currentUser as { id: string }).id, // pass this from component
  })

  return useSuggestionsManager({
    context,
    query,
    inputResolver,
  })
}
