import { useContext, useMemo } from 'react'
import { DashboardContext } from '../'
import { initialOption, Option } from './state'
import { ManagerApiI } from '../../'
import { searchManuscriptInputResolverI } from '../manuscripts'

import { orderDirections, orderOptions } from '../filters'
import { statusCategories } from './statusCategories'

const useFilterManager = ({ context }) => {
  const { value, set } = context

  const reset = () => {
    set(initialOption) // or empty string, pass from manager
  }

  return {
    value,
    set,
    reset,
  }
}

export const useJournalManager: () => ManagerApiI<Option, null> = () => {
  const context = useContext(DashboardContext).journal
  const filterManager = useFilterManager({ context })
  return filterManager
}

export const useSectionManager: () => ManagerApiI<Option, null> = () => {
  const context = useContext(DashboardContext).section
  return useFilterManager({ context })
}

export const useSpecialIssueManager: () => ManagerApiI<Option, null> = () => {
  const context = useContext(DashboardContext).specialIssue
  return useFilterManager({ context })
}

export const useStatusesManager = () => {
  const { value, set } = useContext(DashboardContext).statuses

  const updateSelectedStatuses = (
    categoryLabel: string,
    newValues: string[],
  ) => {
    const updatedStatusCategories = value.map(category => ({
      ...category,
      selectedValues:
        category.label === categoryLabel ? newValues : category.selectedValues,
    }))

    set(updatedStatusCategories)
  }

  const reset = () => {
    set(statusCategories)
  }

  return {
    set: updateSelectedStatuses,
    reset,
    value,
  }
}

export const useAuthorManager = () => {
  const context = useContext(DashboardContext).author
  const reset = () => context.set('')
  return {
    ...useFilterManager({ context }),
    reset,
  }
}

export const useAcademicEditorManager = () => {
  const context = useContext(DashboardContext).academicEditor
  const reset = () => context.set('')
  return {
    ...useFilterManager({ context }),
    reset,
  }
}

export const usePageManager = () => {
  const context = useContext(DashboardContext).page
  const reset = () => context.set(1)
  return {
    ...useFilterManager({ context }),
    reset,
  }
}

export const useOrderCriteriaManager = () => {
  const context = useContext(DashboardContext).orderCriteria
  const reset = () => context.set(orderOptions.updated)
  return {
    ...useFilterManager({ context }),
    reset,
  }
}

export const useOrderDirectionManager = () => {
  const context = useContext(DashboardContext).orderDirection
  const reset = () => context.set(orderDirections.desc)
  return {
    ...useFilterManager({ context }),
    reset,
  }
}

const getOrderParam = ({ orderCriteria, orderDirection }) => {
  if (orderCriteria.value) {
    if (orderDirection.value.value === orderDirections.desc.value) {
      return { orderByDesc: orderCriteria.value.value }
    }
    return { orderBy: orderCriteria.value.value }
  }
  return {}
}

export interface FiltersPropsI {
  manuscripts: ManagerApiI<any, searchManuscriptInputResolverI>
}

type FiltersManagerParamsType = Partial<searchManuscriptInputResolverI> & {
  manuscripts: ManagerApiI<any, searchManuscriptInputResolverI>
}

export const useFiltersManager = (params: FiltersManagerParamsType) => {
  const { manuscripts } = params

  const journal = useJournalManager()
  const section = useSectionManager()
  const specialIssue = useSpecialIssueManager()

  const author = useAuthorManager()
  const editor = useAcademicEditorManager()
  const page = usePageManager()

  const orderDirection = useOrderDirectionManager()
  const orderCriteria = useOrderCriteriaManager()
  const statuses = useStatusesManager()

  const rangeStart = page.value ? (page.value - 1) * 10 : 0
  const rangeEnd = page.value ? page.value * 10 - 1 : 9

  const selectedStatuses = statuses.value
    .reduce((acc, curr) => [...acc, ...curr.selectedValues], [])
    .join(',')

  const searchManuscripts = () => {
    const payload: Partial<searchManuscriptInputResolverI> = {
      journalId: journal.value.value || undefined,
      sectionId: section.value.value || undefined,
      specialIssueId: specialIssue.value.value || undefined,
      academicEditorEmail: editor.value || undefined,
      ...(editor.value
        ? {
            academicEditorStatus: 'accepted',
          }
        : {}),
      authorEmail: author.value || undefined,
      rangeStart,
      rangeEnd,
      status: selectedStatuses,
      ...getOrderParam({ orderDirection, orderCriteria }),
      ...params,
    }

    manuscripts.fetch(payload)
  }

  useMemo(() => searchManuscripts(), [
    journal.value.value,
    section.value.value,
    specialIssue.value.value,
    page.value,
    orderDirection.value.value,
    orderCriteria.value.value,
    statuses.value,
  ])

  return {
    journal,
    section,
    specialIssue,
    author,
    editor,
    page,
    searchManuscripts,
    orderDirection,
    orderCriteria,
    statuses,
  }
}
