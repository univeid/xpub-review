export type StatusT = {
  checked?: boolean
  label: string
  value: string
}

export type StatusCategoryT = {
  label: string
  statuses: StatusT[]
  selectedValues: string[]
}

export type StatusCategoriesT = StatusCategoryT[]

type getVisibleStatusForManuscriptT = (manuscriptStatus: string) => string

export const statusCategories: StatusCategoriesT = [
  {
    label: 'Submission & Screening',
    statuses: [
      {
        label: 'Complete Submission',
        value: 'draft',
      },
      {
        label: 'In Screening',
        value: 'technicalChecks',
      },
    ],
    selectedValues: [],
  },
  {
    label: 'In Review',
    statuses: [
      {
        label: 'Assign AE',
        value: 'submitted',
      },
      {
        label: 'Pending Editor Response',
        value: 'academicEditorInvited',
      },
      {
        label: 'Invite Reviewers',
        value: 'academicEditorAssigned',
      },
      {
        label: 'Pending Reviewer response',
        value: 'reviewersInvited',
      },
      {
        label: 'Under Review',
        value: 'underReview',
      },
      {
        label: 'Awaiting Recommendation',
        value: 'reviewCompleted',
      },
      {
        label: 'Revision Requested',
        value: 'revisionRequested',
      },
      {
        label: 'Awaiting Decision',
        value: 'pendingApproval,makeDecision',
      },
    ],
    selectedValues: [],
  },
  {
    label: 'Post Review',
    statuses: [
      {
        label: 'In Quality Checks',
        value: 'inQA',
      },
      {
        label: 'Accepted',
        value: 'accepted,qualityChecksRequested,qualityChecksSubmitted',
      },
    ],
    selectedValues: [],
  },
  {
    label: 'Final',
    statuses: [
      {
        label: 'Published',
        value: 'published',
      },
      {
        label: 'Refused To Consider',
        value: 'refusedToConsider',
      },
      {
        label: 'Rejected',
        value: 'rejected',
      },
      {
        label: 'Withdrawn',
        value: 'withdrawn',
      },
      {
        label: 'Deleted',
        value: 'deleted',
      },
    ],
    selectedValues: [],
  },
]

export const allEaStatuses: StatusT[] = statusCategories.flatMap(
  ({ statuses }) => statuses,
)

const inReviewOptions = [
  'submitted',
  'academicEditorInvited',
  'academicEditorAssigned',
  'reviewersInvited',
  'underReview',
  'reviewCompleted',
  'revisionRequested',
  'pendingApproval,makeDecision',
]

export const initialStatusCategories = statusCategories.map(category => ({
  ...category,
  selectedValues:
    category.label === 'In Review' ? inReviewOptions : category.selectedValues,
}))

export const getVisibleStatusForManuscript: getVisibleStatusForManuscriptT = manuscriptStatus =>
  allEaStatuses.find(status => status.value.includes(manuscriptStatus))?.label
