import { useState } from 'react'
import { initialStatusCategories, StatusCategoriesT } from './statusCategories'

export type Option = {
  value: string
  displayValue: string
}

export type ValueT = Option | string | number

export const initialOption: Option = {
  value: null,
  displayValue: '',
}

export type SelectOptionT = {
  value: string
  label: string
}

export const orderOptions: { [key: string]: SelectOptionT } = {
  updated: {
    value: 'updated',
    label: 'Status update date',
  },
  submittedDate: {
    value: 'submittedDate',
    label: 'Submission date',
  },
}

export const orderDirections: { [key: string]: SelectOptionT } = {
  asc: {
    value: 'asc',
    label: 'Ascending',
  },
  desc: {
    value: 'desc',
    label: 'Descending',
  },
}

export const useFiltersState = () => {
  const [selectedJournal, setSelectedJournal] = useState(initialOption)
  const journal = {
    value: selectedJournal,
    set: setSelectedJournal,
  }

  const [selectedSection, setSelectedSection] = useState(initialOption)
  const section = {
    value: selectedSection,
    set: setSelectedSection,
  }

  const [
    selectedSectionSpecialIssue,
    setSelectedSectionSpecialIssue,
  ] = useState(initialOption)
  const specialIssue = {
    value: selectedSectionSpecialIssue,
    set: setSelectedSectionSpecialIssue,
  }

  const [statusCategories, setStatusCategories] = useState(
    initialStatusCategories,
  )
  const statuses = {
    value: statusCategories,
    set: setStatusCategories,
  }

  const [authorEmail, setAuthorEmail] = useState('')
  const author = {
    value: authorEmail,
    set: setAuthorEmail,
  }

  const [AEemail, setAEemail] = useState('')
  const academicEditor = {
    value: AEemail,
    set: setAEemail,
  }

  const [currentPage, setCurrentPage] = useState(1)
  const page = {
    value: currentPage,
    set: setCurrentPage,
  }

  const [orderOption, setOrderOption] = useState(orderOptions.updated)
  const orderCriteria = {
    value: orderOption,
    set: setOrderOption,
  }

  const [orderDir, setOrderDir] = useState(orderDirections.desc)
  const orderDirection = {
    value: orderDir,
    set: setOrderDir,
  }

  return {
    journal,
    section,
    specialIssue,
    statuses,
    author,
    academicEditor,
    page,
    orderCriteria,
    orderDirection,
    statusCategories,
  }
}
