process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { models, fixtures, services } = require('fixture-service')

jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const changePasswordUseCase = require('../src/useCases/changePassword')

describe('Change password use case', () => {
  it('should return success when the old password is correct', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({ User, Identity })

    const oldPasswordHash = user.identities[0].passwordHash
    const input = {
      oldPassword: 'password',
      password: 'N3wPassword!',
    }

    const result = await changePasswordUseCase
      .initialize(services.tokenService, models)
      .execute({ input, userId: user.id })

    expect(result.token).toBeDefined()
    expect(user.identities[0].passwordHash).not.toEqual(oldPasswordHash)
  })

  it('should return an error when the password does not meet minimum requirements', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({ User, Identity })

    const input = {
      oldPassword: 'password',
      password: 'simplepass',
    }

    const result = changePasswordUseCase
      .initialize(services.tokenService, models)
      .execute({ input, userId: user.id })

    return expect(result).rejects.toThrow(
      'Password is too weak. Please check password requirements.',
    )
  })
  it('should return an error when the current password is incorrect', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({ User, Identity })

    const input = {
      oldPassword: 'invalid-password',
      password: 'N3wPassword!',
    }

    const result = changePasswordUseCase
      .initialize(services.tokenService, models)
      .execute({ input, userId: user.id })

    return expect(result).rejects.toThrow('Wrong username or password.')
  })
})
