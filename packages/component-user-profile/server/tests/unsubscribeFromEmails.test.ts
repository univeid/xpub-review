import { generateUser } from 'component-generators'
import { unsubscribeFromEmailsUseCase } from '../src/useCases'

let models
let User

const cancelReviewerJobsUseCase = {
  execute: jest.fn(),
}

describe('unsubscribe from emails use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      User: {
        find: jest.fn(),
      },
    }
    ;({ User } = models)
  })
  it('throw an error if the user is not found', async () => {
    const user = generateUser()

    jest.spyOn(User, 'find').mockResolvedValue(undefined)

    try {
      await unsubscribeFromEmailsUseCase
        .initialize(User, cancelReviewerJobsUseCase)
        .execute({ userId: user.id, token: '' })
    } catch (err) {
      return expect(err.message).toEqual(`Invalid user ${user.id}.`)
    }

    expect('this line').toBe('unreachable')
  })
  it('does nothing if the user is already unsubscribed', async () => {
    const user = generateUser({ isSubscribedToEmails: false })

    jest.spyOn(User, 'find').mockResolvedValue(user)

    await unsubscribeFromEmailsUseCase
      .initialize(User, cancelReviewerJobsUseCase)
      .execute({ userId: user.id, token: '' })

    expect(user.save).toHaveBeenCalledTimes(0)
  })
  it('throws an error if the tokens do not match', async () => {
    const user = generateUser({
      isSubscribedToEmails: true,
      unsubscribeToken: 'TKN',
    })
    const invalidToken = 'TRN'

    jest.spyOn(User, 'find').mockResolvedValue(user)

    try {
      await unsubscribeFromEmailsUseCase
        .initialize(User, cancelReviewerJobsUseCase)
        .execute({ userId: user.id, token: invalidToken })
    } catch (err) {
      return expect(err.message).toEqual(`Invalid token ${invalidToken}.`)
    }

    expect('this line').toBe('unreachable')
  })
  it('executes a correct flow for valid users', async () => {
    const user = generateUser({
      isSubscribedToEmails: true,
      unsubscribeToken: 'TKN',
    })

    jest.spyOn(User, 'find').mockResolvedValue(user)

    await unsubscribeFromEmailsUseCase
      .initialize(User, cancelReviewerJobsUseCase)
      .execute({ userId: user.id, token: user.unsubscribeToken })

    expect(user.updateProperties).toHaveBeenCalledTimes(1)
    expect(user.save).toHaveBeenCalledTimes(1)
    expect(cancelReviewerJobsUseCase.execute).toHaveBeenCalledTimes(1)
  })
})
