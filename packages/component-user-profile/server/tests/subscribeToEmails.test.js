const { generateUser } = require('component-generators')

let models
let User

const { subscribeToEmailsUseCase } = require('../src/useCases')

describe('subscribeToEmails', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      User: {
        find: jest.fn(),
      },
    }
    ;({ User } = models)
  })
  it('throw an error if the user is not found', async () => {
    const user = generateUser()

    jest.spyOn(User, 'find').mockResolvedValue(undefined)

    try {
      await subscribeToEmailsUseCase
        .initialize(models)
        .execute({ input: { userId: user.id } })
    } catch (err) {
      return expect(err.message).toEqual(`Invalid user ${user.id}.`)
    }

    expect('this line').toBe('unreachable')
  })
  it('does nothing if the user is already subscribed', async () => {
    const user = generateUser({ isSubscribedToEmails: true })

    jest.spyOn(User, 'find').mockResolvedValue(user)

    await subscribeToEmailsUseCase
      .initialize(models)
      .execute({ input: { userId: user.id } })

    expect(user.save).toHaveBeenCalledTimes(0)
  })
  it('executes a correct flow for valid users', async () => {
    const user = generateUser({ isSubscribedToEmails: false })

    jest.spyOn(User, 'find').mockResolvedValue(user)

    await subscribeToEmailsUseCase
      .initialize(models)
      .execute({ input: { userId: user.id } })

    expect(user.updateProperties).toHaveBeenCalledTimes(1)
    expect(user.save).toHaveBeenCalledTimes(1)
  })
})
