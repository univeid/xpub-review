import React from 'react'
import { Row, Item, Label, ActionLink, ShadowedBox } from '@hindawi/ui'

const EmailNotifications = ({ isSubscribed, toggleEmailSubscription }) => (
  <ShadowedBox mt={4}>
    <Row alignItems="center">
      <Item>
        <Label>Email Notifications </Label>
      </Item>
      <Item justify="flex-end">
        <ActionLink fontWeight={700} onClick={toggleEmailSubscription}>
          {isSubscribed ? 'Unsubscribe' : 'Re-subscribe'}
        </ActionLink>
      </Item>
    </Row>
  </ShadowedBox>
)

export default EmailNotifications
