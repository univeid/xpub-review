import gql from 'graphql-tag'
import { userDetailsWithIdentities } from './fragments'

export const currentUser = gql`
  query {
    currentUser {
      ...userDetailsWithIdentities
      isSubscribedToEmails
      unsubscribeToken
    }
  }
  ${userDetailsWithIdentities}
`
