// @ts-nocheck

const models = require('@pubsweet/models')

const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')

const resolvers = {
  Query: {
    async getAuditLogs(_, { submissionId }, ctx) {
      return useCases.getAuditLogsUseCase
        .initialize(models)
        .execute(submissionId)
    },
  },
}

export = withAuthsomeMiddleware(resolvers, useCases)
