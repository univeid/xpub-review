const config = require('config')
const logger = require('@pubsweet/logger')
const { User, Identity } = require('@pubsweet/models')
const { roleLabels } = require('./utils')
const KeycloakAdminClient = require('keycloak-admin').default

const {
  public: { authServerURL, realm, clientID },
  admin: { username, password },
} = config.keycloak

const initKeycloakAdminClient = async () => {
  const kcAdminClient = new KeycloakAdminClient({
    baseUrl: authServerURL,
    realmName: 'master',
  })
  await kcAdminClient.auth({
    username,
    password,
    grantType: 'password',
    clientId: 'admin-cli',
  })
  kcAdminClient.setConfig({
    realmName: realm,
  })

  const findOneByEmail = async email => {
    const users = await kcAdminClient.users.find({ email })
    if (!users.length) return
    return users[0]
  }

  const sendVerifyEmail = async ({ id }) => {
    logger.info('Sending keycloak verify email to keycloak user with id: ', id)

    return kcAdminClient.users.sendVerifyEmail({
      id,
      clientId: clientID,
      redirectUri: `${config['pubsweet-client'].baseUrl}`,
    })
  }

  const createFromProfile = ({
    aff,
    email,
    title,
    country,
    surname,
    givenNames,
  }) => {
    logger.info('Creating keycloak user for: ', email)
    return kcAdminClient.users
      .create({
        username: email,
        email,
        enabled: true,
        emailVerified: false,
        firstName: givenNames,
        lastName: surname,
        requiredActions: ['UPDATE_PASSWORD'],
        attributes: {
          country,
          affiliation: aff,
          title,
        },
      })
      .then(sendVerifyEmail)
  }

  const updateUserRole = async (userEmail, roleLabel) => {
    const clientId = process.env.KEYCLOAK_CLIENTID
    const [currentClient] = await kcAdminClient.clients.find({ clientId })

    const [user, role] = await Promise.all([
      findOneByEmail(userEmail),
      kcAdminClient.clients.findRole({
        id: currentClient.id,
        roleName: roleLabel,
      }),
    ])

    if (!user || !role) return

    return kcAdminClient.users.addClientRoleMappings({
      id: user.id,
      clientUniqueId: currentClient.id,
      roles: [role],
    })
  }

  const updateUserRoleToEA = async userEmail =>
    updateUserRole(userEmail, roleLabels.ea)

  return {
    findOneByEmail,
    sendVerifyEmail,
    createFromProfile,
    updateUserRole,
    updateUserRoleToEA,
  }
}

const createSSOUser = async profile => {
  const kcAdminClient = await initKeycloakAdminClient()

  const userFromSSO = await kcAdminClient.findOneByEmail(profile.identity.email)

  if (userFromSSO) {
    logger.info('Skipping keycloak user creation for: ', profile.identity.email)

    if (!userFromSSO.emailVerified) {
      return kcAdminClient.sendVerifyEmail(userFromSSO)
    }

    return userFromSSO
  }

  return kcAdminClient.createFromProfile(profile.identity)
}

const createDBUser = async profile => {
  logger.info('Creating user from keycloak for', profile.email)

  const user = new User({
    agreeTc: true,
    isActive: true,
    id: profile.sub,
    defaultIdentityType: 'local',
  })

  const identity = new Identity({
    type: 'local',
    userId: profile.sub,
    isConfirmed: profile.email_verified,
    email: profile.email,
    aff: profile.affiliation,
    country: profile.country || '',
    surname: profile.family_name,
    givenNames: profile.given_name,
    title: profile.title,
  })

  await user.assignIdentity(identity)

  return user.saveRecursively()
}

const confirmIdentity = async identity => {
  logger.info('Confirming local identity for', identity.email)

  identity.isConfirmed = true
  await identity.save()

  return identity
}

const createRegistrationURL = (redirect_uri = process.env.CLIENT_SERVER_URL) =>
  `${authServerURL}/realms/${realm}/protocol/openid-connect/registrations?client_id=${clientID}&redirect_uri=${encodeURIComponent(
    redirect_uri,
  )}&response_mode=fragment&response_type=code&scope=openid`

const checkUserIdentity = async profile => {
  const identity = await Identity.findOneByEmail(profile.email)
  if (identity) {
    if (profile.email_verified && !identity.isConfirmed) {
      await confirmIdentity(identity)
    }
    return identity
  }

  await createDBUser(profile)
  return profile
}

const updateUserRole = async (userEmail, roleLabel) => {
  const kcAdminClient = await initKeycloakAdminClient()

  return kcAdminClient.updateUserRole(userEmail, roleLabel)
}

const updateUserRoleToEA = async userEmail => {
  if (!userEmail) return

  const kcAdminClient = await initKeycloakAdminClient()

  return kcAdminClient.updateUserRoleToEA(userEmail)
}

module.exports = {
  createSSOUser,
  createDBUser,
  createRegistrationURL,
  checkUserIdentity,
  updateUserRole,
  updateUserRoleToEA,
}
