const roleLabels = {
  ea: 'editorial_assistant',
}

module.exports = {
  roleLabels,
}
