const HindawiBaseModel = require('./server/dist/hindawiBaseModel')
const typeDefs = require('./server/typeDefs')
const resolvers = require('./server/dist/resolvers')
const useCases = require('./server/dist/useCases')

module.exports = {
  HindawiBaseModel,
  typeDefs,
  models: [
    { model: require('./server/dist/model/file'), modelName: 'File' },
    {
      model: require('./server/dist/model/articleType'),
      modelName: 'ArticleType',
    },
    { model: require('./server/dist/model/comment'), modelName: 'Comment' },
    { model: require('./server/dist/model/job'), modelName: 'Job' },
    {
      model: require('./server/dist/model/journalArticleType'),
      modelName: 'JournalArticleType',
      modelLoaders: require('./server/dist/loaders/journal'),
    },
    { model: require('./server/dist/model/review'), modelName: 'Review' },
    { model: require('./server/dist/model/team'), modelName: 'Team' },
    { model: require('./server/dist/model/identity'), modelName: 'Identity' },
    {
      model: require('./server/dist/model/journal'),
      modelName: 'Journal',
      modelLoaders: require('./server/dist/loaders/journal'),
    },
    {
      modelName: 'Manuscript',
      model: require('./server/dist/model/manuscript'),
      modelLoaders: require('./server/dist/loaders/manuscript'),
    },
    {
      modelName: 'TeamMember',
      model: require('./server/dist/model/teamMember'),
      modelLoaders: require('./server/dist/loaders/teamMember'),
    },
    {
      modelName: 'User',
      model: require('./server/dist/model/user'),
    },
    {
      model: require('./server/dist/model/reviewerSuggestion'),
      modelName: 'ReviewerSuggestion',
    },
    { model: require('./server/dist/model/auditLog'), modelName: 'AuditLog' },
    {
      model: require('./server/dist/model/peerReviewModel'),
      modelName: 'PeerReviewModel',
    },
    { model: require('./server/dist/model/section'), modelName: 'Section' },
    {
      model: require('./server/dist/model/specialIssue'),
      modelName: 'SpecialIssue',
      modelLoaders: require('./server/dist/loaders/specialIssue'),
    },
    {
      model: require('./server/dist/model/editorSuggestion'),
      modelName: 'EditorSuggestion',
    },
  ],
  resolvers,
  useCases,
}
