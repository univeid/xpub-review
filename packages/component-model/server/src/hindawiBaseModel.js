const BaseModel = require('@pubsweet/base-model')
const logger = require('@pubsweet/logger')
const findQuery = require('objection-find')

const parseEagerRelations = relations =>
  Array.isArray(relations) ? `[${relations.join(', ')}]` : relations

class HindawiBaseModel extends BaseModel {
  static async find(id, eagerLoadRelations) {
    if (!id) throw new NotFoundError(`Cannot run query with undefined id.`)

    const object = await this.query()
      .findById(id)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations))
    if (!object)
      throw new NotFoundError(`Object not found: ${this.name} with 'id' ${id}`)

    return object
  }

  async delete({ trx } = {}) {
    const deleted = await this.$query(trx).delete()
    logger.info(`Deleted ${deleted} ${this.constructor.name} records.`)
    return this
  }

  async saveRecursively() {
    return this.constructor
      .query()
      .upsertGraph(this, { insertMissing: true, relate: true })
  }

  saveGraph(opts = {}, trx = null) {
    return this.constructor
      .query(trx)
      .upsertGraphAndFetch(
        this,
        Object.assign(opts, { insertMissing: true, noDelete: true }),
      )
  }

  static findAllV2(params) {
    return findQuery(this).build(params)
  }

  static async findOneBy({ queryObject, eagerLoadRelations }) {
    const object = await this.query()
      .where(queryObject)
      .limit(1)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations))

    if (!object.length) {
      return
    }

    return object[0]
  }

  static findBy(queryObject, eagerLoadRelations) {
    try {
      return this.query()
        .where(queryObject)
        .withGraphFetched(parseEagerRelations(eagerLoadRelations))
    } catch (e) {
      throw new Error(e)
    }
  }

  static findIn(field, options, eagerLoadRelations) {
    return this.query()
      .whereIn(field, options)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations))
  }

  static async findOrCreate({ queryObject, eagerLoadRelations, options }) {
    let instance = await this.findOneBy({
      queryObject,
      eagerLoadRelations,
    })

    if (!instance) {
      instance = new this(options)
      await instance.save()
    }

    return instance
  }

  static async findOneByEmail(email, eagerLoadRelations) {
    const object = await this.query()
      .where('email', email.toLowerCase())
      .limit(1)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations))

    if (!object.length) {
      return
    }

    return object[0]
  }

  static async findUniqueJournal({ name, code, email }) {
    const objects = await this.query()
      .whereRaw(
        'LOWER(name) LIKE ? OR LOWER(code) LIKE ? OR LOWER(email) LIKE ?',
        [name.toLowerCase(), code.toLowerCase(), email.toLowerCase()],
      )
      .limit(1)

    if (!objects.length) {
      return
    }

    return objects[0]
  }

  static async findUniqueSection({ name }) {
    const objects = await this.query()
      .whereRaw('LOWER(name) LIKE ?', [name.toLowerCase()])
      .limit(1)

    if (!objects.length) {
      return
    }

    return objects[0]
  }

  static async findAll({
    order,
    queryObject,
    orderByField,
    eagerLoadRelations,
  } = {}) {
    return this.query()
      .skipUndefined()
      .where(queryObject)
      .orderBy(orderByField, order)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations))
  }
}

HindawiBaseModel._parseEagerRelations = parseEagerRelations

module.exports = HindawiBaseModel
