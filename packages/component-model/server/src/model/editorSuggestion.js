const { transaction } = require('objection')
const logger = require('@pubsweet/logger')
const HindawiBaseModel = require('../hindawiBaseModel')

class EditorSuggestion extends HindawiBaseModel {
  static get tableName() {
    return 'editor_suggestion'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: {
          type: 'string',
          format: 'uuid',
        },
        teamMemberId: {
          type: 'string',
          format: 'uuid',
        },
        score: {
          type: 'real',
        },
      },
    }
  }

  static async saveAll(entities) {
    let savedEntities

    try {
      savedEntities = await transaction(EditorSuggestion.knex(), async trx =>
        EditorSuggestion.query(trx).upsertGraphAndFetch(entities),
      )
      savedEntities.forEach(savedEntity => {
        logger.info(`Saved EditorSuggestion with UUID ${savedEntity.id}`)
      })
    } catch (err) {
      logger.info(`Rolled back EditorSuggestion.saveAll()`)
      logger.error(err)
      throw err
    }

    return savedEntities
  }
}

module.exports = EditorSuggestion
