import HindawiBaseModel from "../hindawiBaseModel";

export class AcademicEditor extends HindawiBaseModel {
  static get tableName() {
    return "academic_editor";
  }

  static get schema() {
    return {
      type: "object",
      properties: {
        manuscriptId: { type: "string", format: "uuid" },
        journalId: { type: "string", format: "uuid" },
        userId: { type: "string", format: "uuid" }, // this should be the id from the sso
        email: { type: "string" },
      },
    };
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require("./manuscript"),
        join: {
          from: "academic_editor.manuscriptId",
          to: "manuscript.id",
        },
      },
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require("./user"),
        join: {
          from: "academic_editor.userId",
          to: "user.id",
        },
      },
    };
  }
}
