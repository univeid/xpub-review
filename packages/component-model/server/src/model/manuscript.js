const uuid = require('uuid')
const { raw, transaction } = require('objection')
const findQuery = require('objection-find')
const logger = require('@pubsweet/logger')
const { chain, get, pick, sortBy, groupBy, last, omit } = require('lodash')
const { ref } = require('objection')

const HindawiBaseModel = require('../hindawiBaseModel')
const Review = require('./review')

const {
  EditorialAssistant: EditorialAssistantModel,
} = require('./editorialAssistant')
const { Author: AuthorModel } = require('./author')
const { AcademicEditor: AcademicEditorModel } = require('./academicEditor')
const { TriageEditor: TriageEditorModel } = require('./triageEditor')

class Manuscript extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        submissionId: { type: 'string', format: 'uuid' },
        status: {
          enum: Object.values(Manuscript.Statuses),
          default: Manuscript.Statuses.draft,
        },
        customId: { type: ['string', 'null'] },
        version: { type: 'string', default: '1' },
        title: { type: 'string', default: '' },
        abstract: { type: 'string', default: '' },
        technicalCheckToken: { type: ['string', 'null'], format: 'uuid' },
        hasPassedEqa: { type: ['boolean', 'null'] },
        hasPassedEqs: { type: ['boolean', 'null'] },
        journalId: { type: ['string', null], format: 'uuid' },
        sectionId: { type: ['string', null], format: 'uuid' },
        agreeTc: { type: 'boolean', default: false },
        conflictOfInterest: { type: ['string', 'null'] },
        dataAvailability: { type: ['string', 'null'] },
        fundingStatement: { type: ['string', 'null'] },
        articleTypeId: { type: ['string', 'null'], format: 'uuid' },
        specialIssueId: { type: ['string', 'null'], format: 'uuid' },
        isPostAcceptance: { type: 'boolean', default: false },
        preprintValue: { type: ['string', 'null'] },
        submittedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        acceptedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        qualityChecksSubmittedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        peerReviewPassedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        allowAcademicEditorAutomaticInvitation: {
          type: ['boolean', 'null'],
          default: null,
        },
        hasTriageEditorConflictOfInterest: { type: 'boolean', default: false },
        isLatestVersion: { type: 'boolean', default: true },
      },
    }
  }

  static get relationMappings() {
    return {
      files: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./file'),
        join: {
          from: 'manuscript.id',
          to: 'file.manuscriptId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./team'),
        join: {
          from: 'manuscript.id',
          to: 'team.manuscriptId',
          extra: ['role'],
        },
      },
      reviews: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./review'),
        join: {
          from: 'manuscript.id',
          to: 'review.manuscriptId',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'manuscript.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./section'),
        join: {
          from: 'manuscript.sectionId',
          to: 'section.id',
        },
      },
      logs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./auditLog'),
        join: {
          from: 'manuscript.id',
          to: 'audit_log.manuscriptId',
        },
      },
      reviewerSuggestions: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./reviewerSuggestion'),
        join: {
          from: 'manuscript.id',
          to: 'reviewer_suggestion.manuscriptId',
        },
      },
      articleType: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./articleType'),
        join: {
          from: 'manuscript.articleTypeId',
          to: 'article_type.id',
        },
      },
      specialIssue: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./specialIssue'),
        join: {
          from: 'manuscript.specialIssueId',
          to: 'special_issue.id',
        },
      },
      editorialAssistant: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: EditorialAssistantModel,
        join: {
          from: 'manuscript.id',
          to: 'editorial_assistant.manuscriptId',
        },
      },
      authors: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: AuthorModel,
        join: {
          from: 'manuscript.id',
          to: 'author.manuscriptId',
        },
      },
      academicEditors: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: AcademicEditorModel,
        join: {
          from: 'manuscript.id',
          to: 'academic_editor.manuscriptId',
        },
      },
      triageEditor: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: TriageEditorModel,
        join: {
          from: 'manuscript.id',
          to: 'triage_editor.manuscriptId',
        },
      },
    }
  }

  static get Statuses() {
    return {
      draft: 'draft',
      technicalChecks: 'technicalChecks',
      submitted: 'submitted',
      academicEditorInvited: 'academicEditorInvited',
      academicEditorAssigned: 'academicEditorAssigned',
      reviewersInvited: 'reviewersInvited',
      underReview: 'underReview',
      reviewCompleted: 'reviewCompleted',
      revisionRequested: 'revisionRequested',
      pendingApproval: 'pendingApproval',
      rejected: 'rejected',
      inQA: 'inQA',
      accepted: 'accepted',
      withdrawn: 'withdrawn',
      void: 'void',
      deleted: 'deleted',
      published: 'published',
      olderVersion: 'olderVersion',
      academicEditorAssignedEditorialType:
        'academicEditorAssignedEditorialType',
      makeDecision: 'makeDecision',
      qualityChecksRequested: 'qualityChecksRequested',
      qualityChecksSubmitted: 'qualityChecksSubmitted',
      refusedToConsider: 'refusedToConsider',
    }
  }

  static get NonActionableStatuses() {
    const statuses = this.Statuses
    return [
      statuses.void,
      statuses.draft,
      statuses.deleted,
      statuses.rejected,
      statuses.inQA,
      statuses.accepted,
      statuses.qualityChecksRequested,
      statuses.qualityChecksSubmitted,
      statuses.published,
      statuses.withdrawn,
      statuses.olderVersion,
      statuses.refusedToConsider,
    ]
  }
  static get InProgressStatuses() {
    const statuses = this.Statuses
    return [
      statuses.submitted,
      statuses.underReview,
      statuses.makeDecision,
      statuses.reviewCompleted,
      statuses.pendingApproval,
      statuses.reviewersInvited,
      statuses.revisionRequested,
      statuses.academicEditorInvited,
      statuses.academicEditorAssigned,
    ]
  }

  static async updateMany(manuscripts) {
    try {
      return await transaction(Manuscript.knex(), async trx => {
        const options = { noInsert: true, noDelete: true }
        return Manuscript.query(trx).upsertGraphAndFetch(manuscripts, options)
      })
    } catch (err) {
      logger.error(err)
      throw new Error('Something went wrong. No data was updated.')
    }
  }

  static compareVersion(m1, m2) {
    let v1 = m1.version
    let v2 = m2.version
    if (typeof v1 !== 'string' || typeof v2 !== 'string') return false
    v1 = v1.split('.')
    v2 = v2.split('.')
    const k = Math.min(v1.length, v2.length)
    for (let i = 0; i < k; i += 1) {
      v1[i] = parseInt(v1[i], 10)
      v2[i] = parseInt(v2[i], 10)
      if (v1[i] > v2[i]) return 1
      if (v1[i] < v2[i]) return -1
    }
    if (v1.length === v2.length) return 0
    return v1.length < v2.length ? -1 : 1
  }

  static filterOlderVersions(manuscripts) {
    const submissions = groupBy(manuscripts, 'submissionId')
    const latestVersions = Object.values(submissions).map(versions => {
      if (versions.length === 1) {
        return versions[0]
      }

      const sortedVersions = versions.sort(this.compareVersion)
      const latestManuscript = last(sortedVersions)

      if (latestManuscript.status === this.Statuses.draft) {
        return sortedVersions[sortedVersions.length - 2]
      }

      return latestManuscript
    })

    return latestVersions
  }

  static filterByRole(query, user, Team, role) {
    const filterOutDeleted = query => query.whereNot('m.status', 'deleted')

    const filterOutRevisionDrafts = query =>
      query.where(query =>
        query.where('m.version', 1).orWhereNot('m.status', 'draft'),
      )

    const filterTeamsWhereUserActive = query =>
      query
        .whereNotIn('tm.status', ['declined', 'expired', 'removed'])
        .whereNotNull('t.manuscript_id')

    const filterOnlyWhereUserAssigned = (query, userId) =>
      query.where('u.id', userId)

    const filterWithdrawnIfUserNotAuthor = query =>
      // composed where clause
      query.where(query =>
        query
          .where('t.role', Team.Role.author)
          .orWhereNot('m.status', 'withdrawn'),
      )

    const filterVoidIfUserNotAuthorOrAdmin = query =>
      query.where(query =>
        query
          .where('t.role', Team.Role.author)
          .orWhere('t.role', Team.Role.admin)
          .orWhereNot('m.status', 'void'),
      )

    switch (role) {
      case Team.Role.editorialAssistant:
        return query
          .modify(filterTeamsWhereUserActive)
          .modify(filterVoidIfUserNotAuthorOrAdmin)
          .modify(filterOutRevisionDrafts)
          .modify(filterOnlyWhereUserAssigned, user.id)
      case Team.Role.admin:
        return query.modify(filterOutRevisionDrafts)
      default:
        return query
          .modify(filterOutDeleted)
          .modify(filterTeamsWhereUserActive)
          .modify(filterWithdrawnIfUserNotAuthor)
          .modify(filterVoidIfUserNotAuthorOrAdmin)
          .modify(filterOutRevisionDrafts)
          .modify(filterOnlyWhereUserAssigned, user.id)
    }
  }

  static getLatestVersionOfAllSubmissionsForAdmin() {
    const statuses = this.query()
      .select(
        'r.name as role_name',
        'ss.id as status_id',
        'ss.name as status_name',
        'ss.category as status_category',
        'sl.id as label_id',
        'sl.priority as label_priority',
        'sl.submission_filter_id as filter_id',
        'sl.name as label_name',
        'sf.name as filter_name',
      )
      .from('role as r')
      .where('r.name', 'admin')
      .join('submission_label as sl', 'r.id', 'sl.roleId')
      .join('submission_status as ss', 'sl.submission_status_id', 'ss.id')
      .leftJoin('submission_filter as sf', 'sl.submission_filter_id', 'sf.id')
      .as('statuses')

    const version = this.query()
      .alias('m')
      .select(
        raw(
          `DISTINCT ON (m.submission_id) m.*,
          statuses.role_name as role,
          statuses.filter_name as filter,
          statuses.label_name as visible_status,
          statuses.status_category as status_category,
          statuses.label_priority as label_priority,
          j.name as journal_name`,
        ),
      )
      .orderByRaw(
        `m.submission_id,
        string_to_array(m.version, '.')::int[] DESC,
        statuses.label_priority DESC,
        m.id`,
      )
      .join(statuses, 'm.status', 'statuses.statusName') // TODO: change the role.name to role.id
      .join('journal as j', 'j.id', 'm.journal_id')
    return version
  }

  static getLatestVersionOfAllSubmissions() {
    return this.query()
      .alias('m')
      .select(
        raw(
          `DISTINCT ON (m.submission_id) m.*,
          t.role as role,
          sf.name as filter,
          sl.name as visible_status,
          ss.category as status_category,
          sl.priority as label_priority,
          j.name as journal_name`,
        ),
      )
      .orderByRaw(
        `m.submission_id,
        string_to_array(m.version, '.')::int[] DESC,
        sl.priority DESC,
        m.id`,
      )
      .join('team as t', 'm.id', 't.manuscriptId')
      .join('team_member as tm', 't.id', 'tm.teamId')
      .join('user as u', 'tm.userId', 'u.id')
      .join('role as r', 'r.name', 't.role') // TODO: change the role.name to role.id
      .join('submission_status as ss', 'm.status', 'ss.name') // TODO: change ss.name to ss.id
      .join('submission_label as sl', function() {
        this.on('r.id', 'sl.roleId')
        this.andOn('ss.id', 'sl.submissionStatusId')
      })
      .leftJoin('submission_filter as sf', 'sl.submission_filter_id', 'sf.id')
      .join('journal as j', 'j.id', 'm.journal_id')
  }

  static filterBySearchValue(query, searchValue, manuscriptPropertyFilter) {
    if (!manuscriptPropertyFilter || !manuscriptPropertyFilter.length)
      return query
    if (!searchValue || !searchValue.length) return query

    const filterByCustomId = (query, searchValue) =>
      query.where('custom_id', searchValue)

    const filterByTitle = (query, searchValue) =>
      query.whereRaw('lower(title) LIKE ?', [`%${searchValue.toLowerCase()}%`])

    const filterByJournalName = (query, searchValue) =>
      query.whereRaw('lower(journal_name) LIKE ?', [
        `%${searchValue.toLowerCase()}%`,
      ])

    switch (manuscriptPropertyFilter) {
      case 'customId':
        return query.modify(filterByCustomId, searchValue)
      case 'title':
        return query.modify(filterByTitle, searchValue)
      case 'journalName':
        return query.modify(filterByJournalName, searchValue)
      default:
        throw new Error(`The filter is not defined.`)
    }
  }

  static async getManuscripts({
    user,
    searchValue = '',
    statusFilter = 'all',
    manuscriptPropertyFilter = '',
    dateOrder = 'desc',
    page = '0',
    pageSize = '10',
    Team,
    role = '',
  }) {
    const filterByStatus = (query, statusFilter) => {
      if (!statusFilter || statusFilter === 'all') return query
      return query.where('status', statusFilter)
    }

    const versionQuery =
      role === Team.Role.admin
        ? this.getLatestVersionOfAllSubmissionsForAdmin.bind(this)
        : this.getLatestVersionOfAllSubmissions.bind(this)

    const versions = versionQuery()
      .clone()
      .modify(this.filterByRole, user, Team, role)
      .as('versions')

    return this.query()
      .select('*')
      .from(versions)
      .modify(filterByStatus, statusFilter)
      .modify(this.filterBySearchValue, searchValue, manuscriptPropertyFilter)
      .orderBy('updated', dateOrder)
      .page(page, pageSize)
      .withGraphFetched(
        '[articleType, journal.peerReviewModel, specialIssue.peerReviewModel]',
      )
  }

  static async _findAllOrderedByVersion({
    order = 'asc',
    queryObject,
    eagerLoadRelations,
  }) {
    return this.query()
      .skipUndefined()
      .where(queryObject)
      .orderByRaw(`string_to_array(version, '.')::int[] ${order}`)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findByCustomId(customId) {
    return this.query()
      .where({ customId })
      .first()
  }

  static async findAll({
    order,
    queryObject,
    orderByField,
    eagerLoadRelations,
  }) {
    if (!orderByField || orderByField === 'version')
      return this._findAllOrderedByVersion({
        order,
        queryObject,
        eagerLoadRelations,
      })
    return this.query()
      .skipUndefined()
      .where(queryObject)
      .orderBy(orderByField, order)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findAllByJournalAndUserAndRole({
    role,
    limit,
    userId,
    journalId,
    eagerLoadRelations,
  }) {
    return this.query()
      .skipUndefined()
      .select('m.*')
      .from('manuscript AS m')
      .join('journal as j', 'j.id', 'm.journalId')
      .join('team as t', 't.manuscript_id', 'm.id')
      .join('team_member as tm', 'tm.team_id', 't.id')
      .where('t.role', role)
      .andWhere('tm.user_id', userId)
      .andWhere('j.id', journalId)
      .limit(limit)
      .withGraphJoined(this._parseEagerRelations(eagerLoadRelations))
  }

  static findSubmissionsByJournalAndUserAndRoleAndTeamMemberStatuses({
    role,
    limit,
    userId,
    journalId,
    manuscriptStatuses,
    teamMemberStatuses,
  }) {
    return this.query()
      .skipUndefined()
      .select(raw('DISTINCT ON (m.submission_id) m.submission_id'))
      .from('manuscript AS m')
      .join('journal as j', 'j.id', 'm.journalId')
      .join('team as t', 't.manuscript_id', 'm.id')
      .join('team_member as tm', 'tm.team_id', 't.id')
      .whereIn('tm.status', teamMemberStatuses)
      .whereIn('m.status', manuscriptStatuses)
      .andWhere('t.role', role)
      .andWhere('tm.user_id', userId)
      .andWhere('j.id', journalId)
      .limit(limit)
  }

  static findAllByJournalAndUserAndRoleAndTeamMemberStatuses({
    role,
    limit,
    userId,
    journalId,
    teamMemberStatuses,
    eagerLoadRelations,
  }) {
    return this.findAllByJournalAndUserAndRole({
      role,
      limit,
      userId,
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .whereIn('tm.status', teamMemberStatuses)
  }

  static async findManuscriptByTeamMember(teamMemberId) {
    try {
      return this.query()
        .select('m.*')
        .from('manuscript AS m')
        .join('team AS t', 'm.id', 't.manuscript_id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.id', teamMemberId)
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async _findManuscriptsBySubmissionIdOrderedByVersion({
    order = 'asc',
    submissionId,
    excludedStatus,
    eagerLoadRelations,
  }) {
    return this.query()
      .skipUndefined()
      .whereNot({ status: excludedStatus })
      .andWhere({ submissionId })
      .orderByRaw(`string_to_array(version, '.')::int[] ${order}`)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findManuscriptsBySubmissionId({
    order,
    orderByField,
    submissionId,
    excludedStatus,
    eagerLoadRelations,
  }) {
    if (!orderByField || orderByField === 'version')
      return this._findManuscriptsBySubmissionIdOrderedByVersion({
        order,
        submissionId,
        excludedStatus,
        eagerLoadRelations,
      })
    return this.query()
      .skipUndefined()
      .whereNot({ status: excludedStatus })
      .andWhere({ submissionId })
      .orderBy(orderByField, order)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findOneBySubmissionIdAndStatuses({
    statuses,
    submissionId,
    eagerLoadRelations,
  }) {
    try {
      const results = this.query()
        .select('m.*')
        .from('manuscript as m')
        .whereIn('m.status', statuses)
        .andWhere('m.submission_id', submissionId)
        .withGraphJoined(this._parseEagerRelations(eagerLoadRelations))

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static findDisplayDetailsOfSubmissionByRole({ submissionId, userRole }) {
    return this.query()
      .select(
        'ss.category as category',
        'sl.name as visible_status',
        'm.version',
      )
      .from('submission_status as ss')
      .join('submission_label as sl', 'ss.id', 'sl.submissionStatusId')
      .join('manuscript as m', 'ss.name', 'm.status')
      .join('role as r', 'sl.roleId', 'r.id')
      .whereNot({ status: this.Statuses.draft })
      .andWhere('r.name', userRole)
      .andWhere('m.submission_id', submissionId)
      .orderByRaw(`string_to_array(m.version, '.')::int[] DESC`)
      .limit(1)
      .as('displayDetails')
  }

  static async findAllBySubmissionAndUserAndRole({
    userId,
    userRole,
    submissionId,
    excludedStatuses,
    eagerLoadRelations,
  }) {
    const displayDetails = this.findDisplayDetailsOfSubmissionByRole({
      submissionId,
      userRole,
    })

    const statusCategory = this.query()
      .select('category')
      .from(displayDetails)
      .as('statusCategory')

    const visible_status = this.query()
      .select('visible_status')
      .from(displayDetails)
      .as('visible_status')

    const manuscripts = this.query()
      .select(
        raw(`DISTINCT ON (m.id) m.*, t.role`),
        statusCategory,
        visible_status,
      )
      .from('manuscript AS m')
      .join('team AS t', 'm.id', 't.manuscript_id')
      .join('team_member AS tm', 't.id', 'tm.team_id')
      .join('user as u', 'tm.userId', 'u.id')
      .join('submission_status as ss', 'm.status', 'ss.name')
      .leftJoin('submission_label as sl', 'ss.id', 'sl.submissionStatusId')
      .whereNotIn('m.status', excludedStatuses)
      .andWhere('tm.user_id', userId)
      .andWhere('m.submission_id', submissionId)
      .andWhere('t.role', userRole)
      .andWhere(query =>
        query
          .whereNotIn('tm.status', ['declined', 'expired', 'removed'])
          .whereNotNull('t.manuscript_id'),
      )
      .orderByRaw(`m.id, string_to_array(version, '.')::int[] DESC`)
      .as('manuscripts')

    const results = await this.query()
      .select('*')
      .from(manuscripts)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))

    return results
  }

  static findAllByJournalAndStatuses({ journalId, statuses }) {
    return this.query()
      .select('m.*')
      .from('manuscript as m')
      .whereIn('m.status', statuses)
      .andWhere('m.journal_id', journalId)
  }

  static countSubmissionsByJournalAndStatuses({ journalId, statuses }) {
    const distinctSubmissions = this.query()
      .select(raw(`DISTINCT ON (m.submission_id) m.*`))
      .from('manuscript as m')
      .whereIn('m.status', statuses)
      .andWhere('m.journal_id', journalId)
      .as('distinctSubmissions')

    return this.query()
      .select(raw('count(*)::integer'))
      .from(distinctSubmissions)
      .first()
  }

  static async findAllForAdminBySubmission({
    adminRole,
    submissionId,
    excludedStatuses,
    eagerLoadRelations,
  }) {
    const displayDetails = this.findDisplayDetailsOfSubmissionByRole({
      submissionId,
      userRole: adminRole,
    })

    const statusCategory = this.query()
      .select('category')
      .from(displayDetails)
      .as('statusCategory')

    const visible_status = this.query()
      .select('visible_status')
      .from(displayDetails)
      .as('visible_status')

    const results = await this.query()
      .select('m.*', raw("'admin' as role"), statusCategory, visible_status)
      .from('manuscript AS m')
      .whereNotIn('m.status', excludedStatuses)
      .andWhere('m.submission_id', submissionId)
      .orderByRaw(`string_to_array(version, '.')::int[] DESC`)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))

    return results
  }

  // Temporary query for redirect
  static async findLastManuscriptByCustomId({ customId }) {
    return this.query()
      .whereNot({ status: this.Statuses.draft })
      .andWhere({ customId })
      .orderByRaw(`string_to_array(version, '.')::int[] desc`)
      .first()
  }

  static async findLastManuscriptBySubmissionId({
    submissionId,
    eagerLoadRelations,
  }) {
    return this.query()
      .whereNot({ status: this.Statuses.draft })
      .andWhere({ submissionId })
      .orderByRaw(`string_to_array(version, '.')::int[] desc`)
      .first()
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findAllByExcludedStatuses({ excludedStatuses }) {
    return this.query()
      .select('m.*')
      .from('manuscript AS m')
      .whereNotIn('m.status', excludedStatuses)
      .whereNot(builder =>
        builder.where('version', 1).andWhere('status', 'draft'),
      )
  }
  static async findAllByStatusFromSpecialIssue({ status }) {
    return this.query()
      .select('m.*')
      .from('manuscript AS m')
      .where('m.status', status)
      .whereNotNull('m.special_issue_id')
  }

  // to delete after running replaceManuscriptEAs script
  static async findManuscriptsWithWrongEA({
    Team,
    TeamMember,
    journalId,
    journalEditorialAssistantsUserIds,
  }) {
    return this.query()
      .select('m.*')
      .from('manuscript AS m')
      .join('team as t', 'm.id', 't.manuscriptId')
      .join('team_member as tm', 't.id', 'tm.teamId')
      .whereNotIn('tm.user_id', journalEditorialAssistantsUserIds)
      .andWhere('m.journal_id', journalId)
      .andWhere('tm.status', TeamMember.Statuses.active)
      .andWhere('t.role', Team.Role.editorialAssistant)
  }

  static async findAllWithWrongTriageEditor({ Team, TeamMember }) {
    return this.query()
      .select('m.*')
      .from('manuscript AS m')
      .join('team as t', 'm.id', 't.manuscriptId')
      .join('team_member as tm', 't.id', 'tm.teamId')
      .whereNotExists(query => {
        query
          .select('tmj.id')
          .from('team_member as tmj')
          .join('team as t', 'tmj.team_id', 't.id')
          .where('t.role', Team.Role.triageEditor)
          .andWhere(ref('m.section_id'), null)
          .andWhere(ref('m.special_issue_id'), null)
          .andWhere(`t.journal_id`, ref(`m.journal_id`))
          .andWhere('tmj.user_id', ref('tm.user_id'))
      })
      .andWhere(query => {
        query.whereNotExists(query => {
          query
            .select('tms.id')
            .from('team_member as tms')
            .join('team as t', 'tms.team_id', 't.id')
            .where('t.role', Team.Role.triageEditor)
            .andWhere(ref('m.special_issue_id'), null)
            .andWhere(`t.section_id`, ref(`m.section_id`))
            .andWhere('tms.user_id', ref('tm.user_id'))
        })
      })
      .andWhere(query => {
        query.whereNotExists(query => {
          query
            .select('tmsi.id')
            .from('team_member as tmsi')
            .join('team as t', 'tmsi.team_id', 't.id')
            .where('t.role', Team.Role.triageEditor)
            .andWhere(`t.special_issue_id`, ref(`m.special_issue_id`))
            .andWhere('tmsi.user_id', ref('tm.user_id'))
        })
      })
      .andWhere('tm.status', TeamMember.Statuses.active)
      .andWhere('t.role', Team.Role.triageEditor)
      .andWhere('m.status', 'in', this.InProgressStatuses)
  }

  static async generateUniqueCustomId(maxTries) {
    const id = `66${Math.round(10000 + Math.random() * 89999).toString()}`
    const found = await this.findByCustomId(id)

    if (!found) {
      return id
    }

    if (maxTries <= 0) {
      return undefined
    }

    return this.generateUniqueCustomId(Manuscript, maxTries - 1)
  }

  async getHasSpecialIssueEditorialConflictOfInterest({ TeamMember, Team }) {
    if (!this.specialIssueId) return false

    let authors = []
    if (this.authors && this.authors.length > 0) {
      ;({ authors } = this)
    } else {
      authors = await TeamMember.findAllByManuscriptAndRole({
        manuscriptId: this.id,
        role: Team.Role.author,
      })
    }

    const userIds = authors.map(a => a.userId)
    const editor = await TeamMember.findOneBySpecialIssueAndUsers({
      specialIssueId: this.specialIssueId,
      userIds,
    })

    return !!editor
  }

  async getEditorLabel({ PeerReviewModel, TeamMember, Team, role }) {
    let peerReviewModel

    const hasSpecialIssueEditorialConflictOfInterest = await this.getHasSpecialIssueEditorialConflictOfInterest(
      { TeamMember, Team },
    )
    if (this.specialIssueId && !hasSpecialIssueEditorialConflictOfInterest) {
      peerReviewModel = await PeerReviewModel.findOneBySpecialIssue(
        this.specialIssueId,
      )
    } else {
      peerReviewModel = await PeerReviewModel.findOneByJournal(this.journalId)
    }
    return peerReviewModel[`${role}Label`]
  }

  getSubmittingAuthor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam) {
      throw new Error('Could not find author team')
    }

    if (authorTeam.members.length === 0) {
      throw new Error('Members are required.')
    }

    return authorTeam.members.find(tm => tm.isSubmitting)
  }

  getAuthors() {
    if (!this.teams) {
      logger.warn('Cannot get authors when teams are not loaded.')
      return
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam || authorTeam.members.length === 0) {
      return []
    }

    return sortBy(authorTeam.members, 'position')
  }

  getReviewers() {
    if (!this.teams) {
      logger.warn('Cannot get reviewers when teams are not loaded.')
      return
    }

    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam || reviewerTeam.members.length === 0) {
      return []
    }

    return reviewerTeam.members
  }

  getReviewersForEventData({ Team }) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }
    if (!this.reviewerSuggestions) {
      throw new Error('ReviewerSuggestions are required.')
    }

    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam || reviewerTeam.members.length === 0) {
      return []
    }

    return reviewerTeam.members.map(reviewer => {
      const reviewerData = pick(reviewer, [
        'id',
        'created',
        'updated',
        'userId',
        'status',
        'responded',
      ])

      const orcidIdentity = reviewer.user.identities.find(
        identity => identity.type === 'orcid',
      )

      const externalReviewer = this.reviewerSuggestions.find(
        rs => rs.email === reviewer.alias.email,
      )

      return {
        ...reviewerData,
        ...reviewer.alias,
        orcidId: orcidIdentity ? orcidIdentity.identifier : '',
        fromService: externalReviewer ? externalReviewer.type : '',
        ...reviewer.getDatesForEvents({
          Team,
          role: Team.Role.reviewer,
        }),
      }
    })
  }

  getReviewsForEventData() {
    return this.reviews
      .filter(review => review.submitted !== null)
      .map(review => ({
        ...omit(review, ['manuscriptId']),
        comments: review.comments.map(comment => omit(comment, 'reviewId')),
      }))
  }

  getAuthorsForEventData() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')
    if (!authorTeam || authorTeam.members.length === 0) {
      return []
    }

    return authorTeam.members.map(author => {
      const authorData = pick(author, [
        'id',
        'created',
        'updated',
        'userId',
        'status',
        'position',
        'isSubmitting',
        'isCorresponding',
      ])
      const orcidIdentity = author.user.identities.find(
        identity => identity.type === 'orcid',
      )

      return {
        ...authorData,
        ...author.alias,
        orcidId: orcidIdentity ? orcidIdentity.identifier : '',
        assignedDate: author.created,
      }
    })
  }
  getSubmittingStaffMembersForEventData({ Team }) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const submittingStaffMemberTeam = this.teams.find(
      t => t.role === Team.Role.submittingStaffMember,
    )
    if (
      !submittingStaffMemberTeam ||
      submittingStaffMemberTeam.members.length === 0
    ) {
      return []
    }
    return submittingStaffMemberTeam.members.map(submittingStaffMember => {
      const submittingStaffMemberData = pick(submittingStaffMember, [
        'id',
        'created',
        'updated',
        'userId',
        'status',
      ])
      const orcidIdentity = submittingStaffMember.user.identities.find(
        identity => identity.type === 'orcid',
      )

      return {
        ...submittingStaffMemberData,
        ...submittingStaffMember.alias,
        orcidId: orcidIdentity ? orcidIdentity.identifier : '',
        assignedDate: submittingStaffMember.created,
      }
    })
  }

  getAcademicEditor() {
    if (!this.teams) {
      logger.warn('Cannot get academic editor when teams are not loaded.')
      return
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()

    return academicEditor
  }

  getPendingAcademicEditor() {
    if (!this.teams) {
      logger.warn(
        'Cannot get pending academic editor when teams are not loaded.',
      )
      return
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'pending')
      .value()

    return academicEditor
  }

  getRIPE() {
    if (!this.teams) {
      logger.warn('Cannot get RIPE when teams are not loaded.')
      return
    }

    const RIPE = chain(this.teams)
      .find(t => t.role === 'researchIntegrityPublishingEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()

    return RIPE
  }

  _getTriageEditor() {
    if (!this.teams) {
      logger.warn('Cannot get triage editor when teams are not loaded.')
      return
    }

    const triageEditorTeam = this.teams.find(
      team => team.role === 'triageEditor',
    )

    if (!triageEditorTeam) {
      return
    }

    if (!triageEditorTeam.members) {
      logger.warn('Cannot get triage editor when team members are not loaded.')
      return
    }

    const triageEditor = triageEditorTeam.members.find(
      member => member.status === 'active',
    )

    return triageEditor
  }

  getAcademicEditorByStatus(status) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === status)
      .value()

    return academicEditor
  }

  assignTeam(team) {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  assignFile(file) {
    this.files = this.files || []
    this.files.push(file)
  }

  assignReview(review) {
    this.reviews = this.reviews || []
    this.reviews.push(review)
  }

  submitManuscript() {
    this.status = Manuscript.Statuses.technicalChecks
    this.technicalCheckToken = uuid.v4()
    this.submittedDate = new Date().toISOString()
  }

  updateStatus(status) {
    this.status = status
    return this
  }

  updateIsLatestVersionFlag(newStatus) {
    this.isLatestVersion = newStatus
  }

  setComment() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const responseToRevisionRequest = this.reviews.find(
      review =>
        review.recommendation === Review.Recommendations.responseToRevision,
    )
    if (!responseToRevisionRequest) {
      throw new ValidationError('There has been no request to revision')
    }

    this.comment = responseToRevisionRequest.comments[0].toDTO()
  }

  getLatestEditorReview() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const editorReviews = this.reviews.filter(review =>
      ['admin', 'triageEditor', 'academicEditor'].includes(
        review.member.team.role,
      ),
    )

    return last(sortBy(editorReviews, 'updated'))
  }

  getLatestAcademicEditorRecommendation() {
    const editorReviews = this.reviews.filter(
      review => get(review, 'member.team.role') === 'academicEditor',
    )
    return last(sortBy(editorReviews, 'updated'))
  }

  getReviewersByStatus(status) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }
    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam.members.length === 0) {
      throw new Error('Members are required.')
    }
    return reviewerTeam.members.filter(m => m.status === status)
  }

  getCustomId() {
    return this.role === 'author' &&
      [Manuscript.Statuses.draft, Manuscript.Statuses.technicalChecks].includes(
        this.status,
      )
      ? undefined
      : this.customId
  }

  getSortedFiles() {
    if (!this.files) {
      logger.warn('Cannot sort files when files are not loaded.')
      return []
    }

    return chain(this.files)
      .map(f => ({ ...f, filename: f.fileName }))
      .sortBy('type', 'position')
      .value()
  }

  async updateWithFiles(files) {
    try {
      await transaction(Manuscript.knex(), async trx =>
        this.$relatedQuery('files', trx).upsertGraph(files, {
          noDelete: true,
          noInsert: true,
          relate: true,
        }),
      )
    } catch (err) {
      logger.error(err)
      throw new Error('Something went wrong. No data was updated')
    }
  }

  static async updateManuscriptAndTeams({ manuscript, teams, Team }) {
    return transaction(Manuscript.knex(), async trx => {
      await manuscript.$query(trx).update(manuscript)
      await Team.updateAndSaveMany(teams, trx)
    })
  }
  static async updateManuscriptAndTeamMembers({
    manuscript,
    teamMembers,
    TeamMember,
  }) {
    return transaction(Manuscript.knex(), async trx => {
      await manuscript.$query(trx).update(manuscript)
      await TeamMember.updateAndSaveMany(teamMembers, trx)
    })
  }

  toDTO() {
    const meta = pick(this, [
      'title',
      'agreeTc',
      'abstract',
      'articleTypeId',
      'dataAvailability',
      'fundingStatement',
      'conflictOfInterest',
    ])
    return Object.assign(this, {
      meta,
      files: this.getSortedFiles(),
      customId: this.getCustomId(),
      section: this.section ? this.section.toDTO() : undefined,
      articleType: this.articleType ? this.articleType.toDTO() : undefined,
      teams:
        this.teams && this.teams.length > 0
          ? this.teams.map(team => team.toDTO())
          : [],
    })
  }
}

module.exports = Manuscript
