const HindawiBaseModel = require('../hindawiBaseModel')

class Preprint extends HindawiBaseModel {
  static get tableName() {
    return 'preprint'
  }

  static get schema() {
    return {
      properties: {
        type: { type: 'string' },
        format: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      journalPreprints: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./journalPreprint'),
        join: {
          from: 'preprint.id',
          to: 'journalPreprint.preprintId',
        },
      },
    }
  }
}

module.exports = Preprint
