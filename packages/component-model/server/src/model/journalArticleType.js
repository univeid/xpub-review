const HindawiBaseModel = require('../hindawiBaseModel')

class JournalArticleType extends HindawiBaseModel {
  static get tableName() {
    return 'journal_article_type'
  }

  static get schema() {
    return {
      properties: {
        journalId: { type: 'string', format: 'uuid' },
        articleTypeId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'journal_article_type.journalId',
          to: 'journal.id',
        },
      },
      articleType: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./articleType'),
        join: {
          from: 'journal_article_type.articleTypeId',
          to: 'article_type.id',
        },
      },
    }
  }

  static findAllByJournals(journalIds) {
    return this.query()
      .select('journalId', 'articleType.*')
      .join('articleType', 'articleType.id', 'articleTypeId')
      .whereIn('journalId', journalIds)
  }
}

module.exports = JournalArticleType
