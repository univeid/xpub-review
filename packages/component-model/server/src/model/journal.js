const { chain } = require('lodash')
const Promise = require('bluebird')
const logger = require('@pubsweet/logger')
const HindawiBaseModel = require('../hindawiBaseModel')
const Team = require('./team')
const {
  EditorialAssistant: EditorialAssistantModel,
} = require('./editorialAssistant')

class Journal extends HindawiBaseModel {
  static get tableName() {
    return 'journal'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        publisherName: { type: ['string', null] },
        issn: { type: ['string', null] },
        code: { type: 'string' },
        email: { type: 'string' },
        apc: { type: ['integer'] },
        isActive: { type: ['boolean', false] },
        activationDate: {
          type: ['string', 'null', 'object'],
          format: 'date-time',
        },
        peerReviewModelId: { type: ['string', null], format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'journal.id',
          to: 'manuscript.journalId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Team,
        join: {
          from: 'journal.id',
          to: 'team.journalId',
        },
      },
      journalArticleTypes: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./journalArticleType'),
        join: {
          from: 'journal.id',
          to: 'journal_article_type.journalId',
        },
      },
      articleTypes: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('./articleType'),
        join: {
          from: 'journal.id',
          through: {
            from: 'journal_article_type.journalId',
            to: 'journal_article_type.articleTypeId',
          },
          to: 'article_type.id',
        },
      },
      jobs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./job'),
        join: {
          from: 'journal.id',
          to: 'job.journalId',
        },
      },
      sections: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./section'),
        join: {
          from: 'journal.id',
          to: 'section.journalId',
        },
      },
      peerReviewModel: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./peerReviewModel'),
        join: {
          from: 'journal.peerReviewModelId',
          to: 'peer_review_model.id',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./specialIssue'),
        join: {
          from: 'journal.id',
          to: 'special_issue.journalId',
        },
      },
      journalPreprints: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./journalPreprint'),
        join: {
          from: 'journal.id',
          to: 'journal_preprint.journalId',
        },
      },
      preprints: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('./preprint'),
        join: {
          from: 'journal.id',
          through: {
            from: 'journal_preprint.journal_id',
            to: 'journal_preprint.preprint_id',
          },
          to: 'preprint.id',
        },
      },
      editorialAssistant: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: EditorialAssistantModel,
        join: {
          from: 'journal.id',
          to: 'editorial_assistant.journalId',
        },
      },
    }
  }

  static findAllByManuscriptIds(manuscriptIds) {
    return this.query()
      .select('j.*', 'm.id as manuscriptId')
      .from('journal as j')
      .join('manuscript as m', 'j.id', 'm.journal_id')
      .whereIn('m.id', manuscriptIds)
  }

  static async findAllActiveJournals() {
    try {
      const results = await this.query()
        .select('j.*')
        .from('journal AS j')
        .where('j.isActive', true)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllWithMultipleEAs() {
    return this.query()
      .select('id')
      .from(query =>
        query
          .select('j.id')
          .count('tm.id as numberOfTeamMembers')
          .from({ j: 'journal' })
          .join({ t: 'team' }, 't.journal_id', 'j.id')
          .join({ tm: 'team_member' }, 't.id', 'tm.team_id')
          .where('t.role', 'editorialAssistant')
          .groupBy('j.id')
          .as('nrOfEAs'),
      )
      .where('numberOfTeamMembers', '>', '1')
  }

  static async findJournalByTeamMember(teamMemberId) {
    try {
      const results = await this.query()
        .select('j.*')
        .from('journal AS j')
        .join('team AS t', 'j.id', 't.journal_id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.id', teamMemberId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static getAllActiveJournals() {
    return this.query()
      .alias('journal')
      .withGraphFetched(
        '[specialIssues(onlyActive), sections.specialIssues(onlyActive), preprints, journalPreprints]',
      )
      .where('journal.isActive', true)
      .orderBy('name', 'asc')
  }

  $formatDatabaseJson(json) {
    json = super.$formatDatabaseJson(json)
    const { email } = json

    if (email) {
      return { ...json, email: email.toLowerCase() }
    }

    return json
  }

  getCorrespondingEditorialAssistant() {
    if (!this.teams) {
      throw new Error('Teams are required')
    }
    return chain(this.teams)
      .find(t => t.role === Team.Role.editorialAssistant)
      .get('members', [])
      .find(m => m.isCorresponding === true)
      .value()
  }

  assignTeam(team) {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  async saveJournalAndJournalArticleTypes(articleTypeIds) {
    try {
      const journalArticleTypes = articleTypeIds.map(articleTypeId => ({
        articleTypeId,
      }))
      this.updateProperties({ journalArticleTypes })
      return await this.saveGraph({ relate: true })
    } catch (err) {
      logger.error(err)
      if (err.constraint) {
        throw new ConflictError(`Journal already exists.`)
      }
      throw new Error(err)
    }
  }

  scheduleJob(teamMemberId, jobsService) {
    return jobsService.scheduleJournalActivation({
      journal: this,
      teamMemberId,
    })
  }

  cancelJobs(JobModel) {
    return Promise.each(this.jobs, async job => {
      await JobModel.cancel(job.id)
    })
  }
}

module.exports = Journal
