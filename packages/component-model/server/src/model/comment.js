const HindawiBaseModel = require('../hindawiBaseModel')

class Comment extends HindawiBaseModel {
  static get tableName() {
    return 'comment'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        type: { enum: Object.values(Comment.Types) },
        content: { type: ['string', 'null'] },
        reviewId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      review: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./review'),
        join: {
          from: 'comment.reviewId',
          to: 'review.id',
        },
      },
      files: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./file'),
        join: {
          from: 'comment.id',
          to: 'file.commentId',
        },
      },
    }
  }

  static get Types() {
    return {
      public: 'public',
      private: 'private',
    }
  }

  static async findOneByType({ reviewId, type }) {
    try {
      const results = await this.query()
        .select('c.*')
        .from('comment AS c')
        .join('review AS r', 'r.id', 'c.review_id')
        .where('c.review_id', reviewId)
        .andWhere('c.type', type)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  assignFile(file) {
    this.files = this.files || []
    this.files.push(file)
  }

  toDTO() {
    return { ...this, files: this.files ? this.files.map(f => f.toDTO()) : [] }
  }
}

module.exports = Comment
