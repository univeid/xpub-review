const HindawiBaseModel = require('../hindawiBaseModel')
const Comment = require('./comment')

class Review extends HindawiBaseModel {
  static get tableName() {
    return 'review'
  }

  static get schema() {
    return {
      properties: {
        recommendation: { enum: Object.values(Review.Recommendations) },
        manuscriptId: { type: 'string', format: 'uuid' },
        teamMemberId: { type: 'string', format: 'uuid' },
        submitted: { type: ['string', 'object', 'null'], format: 'date-time' },
        isValid: { type: 'boolean', default: true },
      },
    }
  }

  static get relationMappings() {
    return {
      comments: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./comment'),
        join: {
          from: 'review.id',
          to: 'comment.reviewId',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'review.manuscriptId',
          to: 'manuscript.id',
        },
      },
      member: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./teamMember'),
        join: {
          from: 'review.teamMemberId',
          to: 'team_member.id',
        },
      },
    }
  }

  static get Recommendations() {
    return {
      responseToRevision: 'responseToRevision',
      minor: 'minor',
      major: 'major',
      reject: 'reject',
      publish: 'publish',
      revision: 'revision',
      returnToAcademicEditor: 'returnToAcademicEditor',
    }
  }

  static findAllByManuscriptAndTeamMember({ manuscriptId, teamMemberId }) {
    return this.query()
      .alias('r')
      .join('team_member as tm', 'tm.id', 'r.team_member_id')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('r.team_member_id', teamMemberId)
  }

  static findOneByManuscriptAndTeamMember({ manuscriptId, teamMemberId }) {
    return this.findAllByManuscriptAndTeamMember({ manuscriptId, teamMemberId })
      .clone()
      .limit(1)
      .first()
  }

  static async findOneInvalidByManuscriptAndRole({ manuscriptId, role }) {
    return this.query()
      .alias('r')
      .join('team AS t', 't.manuscript_id', 'r.manuscript_id')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('r.isValid', false)
      .andWhere('t.role', role)
      .first()
  }

  static async findAllValidAndSubmittedByManuscriptAndRole({
    role,
    manuscriptId,
  }) {
    return this.query()
      .alias('r')
      .join('team_member AS tm', 'tm.id', 'r.team_member_id')
      .join('team AS t', 't.id', 'tm.team_id')
      .whereNotNull('r.submitted')
      .andWhere('r.manuscript_id', manuscriptId)
      .andWhere('t.role', role)
      .andWhere('r.is_valid', true)
  }

  static async findAllValidAndSubmitedBySubmissionAndRole({
    role,
    submissionId,
  }) {
    return this.query()
      .alias('r')
      .join('team_member AS tm', 'tm.id', 'r.team_member_id')
      .join('team AS t', 't.id', 'tm.team_id')
      .join('manuscript as m', 'm.id', 't.manuscript_id')
      .where('m.submission_id', submissionId)
      .andWhere('t.role', role)
      .andWhere('r.is_valid', true)
      .whereNotNull('r.submitted')
  }

  static findAllByManuscriptAndRole({
    manuscriptId,
    role,
    eagerLoadRelations,
  }) {
    return this.query()
      .alias('r')
      .join('team_member AS tm', 'tm.id', 'r.team_member_id')
      .join('team AS t', 't.id', 'tm.team_id')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('t.role', role)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }
  static findAllSubmittedByManuscriptAndRole({
    manuscriptId,
    role,
    eagerLoadRelations,
  }) {
    return this.findAllByManuscriptAndRole({
      manuscriptId,
      role,
      eagerLoadRelations,
    })
      .clone()
      .whereNotNull('r.submitted')
  }

  static async findAllValidByManuscriptAndRole({ manuscriptId, role }) {
    return this.query()
      .alias('r')
      .join('team_member AS tm', 'tm.id', 'r.team_member_id')
      .join('team AS t', 't.id', 'tm.team_id')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('t.role', role)
      .andWhere('r.is_valid', true)
  }

  static async findLatestEditorialReview({ manuscriptId, TeamRole }) {
    try {
      const results = await this.query()
        .select('r.*')
        .from('review AS r')
        .join('team_member AS tm', 'tm.id', '=', 'r.team_member_id')
        .join('team AS t', 't.id', '=', 'tm.team_id')
        .where('r.manuscript_id', '=', manuscriptId)
        .andWhere(function andWhereIn() {
          this.whereIn('t.role', [
            TeamRole.admin,
            TeamRole.editorialAssistant,
            TeamRole.triageEditor,
            TeamRole.academicEditor,
          ])
        })
        .orderBy('updated', 'desc')

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllValidByManuscriptId({
    manuscriptId,
    eagerLoadRelations,
  }) {
    return this.query()
      .alias('r')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('r.isValid', true)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  addComment({ content, type }) {
    this.comments = this.comments || []

    const commentTypeAlreadyExists = this.comments.some(c => c.type === type)
    if (commentTypeAlreadyExists)
      throw new ValidationError('Cannot add multiple comments of the same type')

    const comment = new Comment({
      content,
      type,
    })

    this.comments.push(comment)

    return comment
  }

  setSubmitted(date) {
    this.submitted = date
  }

  assignMember(teamMember) {
    this.member = teamMember
  }

  toDTO() {
    return {
      ...this,
      comments: this.comments
        ? this.comments.map(comment => comment.toDTO())
        : [],
      member: this.member ? this.member.toDTO() : undefined,
    }
  }
}

module.exports = Review
