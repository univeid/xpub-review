import HindawiBaseModel from '../hindawiBaseModel'

export class EditorialAssistant extends HindawiBaseModel {
  static get tableName() {
    return 'editorial_assistant'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        userId: { type: 'string', format: 'uuid' }, // this should be the id from the sso
        journalId: { type: 'string', format: 'uuid' },
        status: { type: 'string', format: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'editorial_assistant.manuscriptId',
          to: 'manuscript.id',
        },
      },
      users: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./user'),
        join: {
          from: 'editorial_assistant.userId',
          to: 'user.id',
        },
      },
    }
  }
}
