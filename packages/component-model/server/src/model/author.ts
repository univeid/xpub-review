import HindawiBaseModel from '../hindawiBaseModel'

export class Author extends HindawiBaseModel {
  static get tableName() {
    return 'author'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        userId: { type: 'string', format: 'uuid' }, // this should be the id from the sso
        email: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'author.manuscriptId',
          to: 'manuscript.id',
        },
      },
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./user'),
        join: {
          from: 'author.userId',
          to: 'user.id',
        },
      },
    }
  }
}
