const HindawiBaseModel = require('../hindawiBaseModel')

class JournalPreprint extends HindawiBaseModel {
  static get tableName() {
    return 'journal_preprint'
  }

  static get schema() {
    return {
      properties: {
        journalId: { type: 'string', format: 'uuid' },
        preprintId: { type: 'string', format: 'uuid' },
        description: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'journal_preprint.journalId',
          to: 'journal.id',
        },
      },
      preprint: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./preprint'),
        join: {
          from: 'journal_preprint.preprintId',
          to: 'preprint.id',
        },
      },
    }
  }
}

module.exports = JournalPreprint
