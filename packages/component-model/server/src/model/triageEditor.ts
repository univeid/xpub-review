import HindawiBaseModel from "../hindawiBaseModel";

export class TriageEditor extends HindawiBaseModel {
  static get tableName() {
    return "triage_editor";
  }

  static get schema() {
    return {
      type: "object",
      properties: {
        manuscriptId: { type: "string", format: "uuid" },
        journalId: { type: "string", format: "uuid" },
        userId: { type: "string", format: "uuid" }, // this should be the id from the sso
        email: { type: "string" },
      },
    };
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require("./manuscript"),
        join: {
          from: "triage_editor.manuscriptId",
          to: "manuscript.id",
        },
      },
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require("./user"),
        join: {
          from: "triage_editor.userId",
          to: "user.id",
        },
      },
    };
  }
}
