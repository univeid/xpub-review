const HindawiBaseModel = require('../hindawiBaseModel')
const Comment = require('./comment')

class File extends HindawiBaseModel {
  static get tableName() {
    return 'file'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        type: { enum: Object.values(File.Types) },
        label: { type: ['string', 'null'] },
        providerKey: { type: ['string', 'null'] },
        fileName: { type: 'string' },
        url: { type: ['string', 'null'] },
        mimeType: { type: 'string' },
        size: { type: 'number' },
        originalName: { type: 'string' },
        manuscriptId: { type: ['string', 'null'], format: 'uuid' },
        commentId: { type: ['string', 'null'], format: 'uuid' },
        position: { type: ['integer', null] },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'file.manuscriptId',
          to: 'manuscript.id',
        },
      },
      comment: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./comment'),
        join: {
          from: 'file.commentId',
          to: 'comment.id',
        },
      },
    }
  }

  static get Types() {
    return {
      figure: 'figure',
      manuscript: 'manuscript',
      supplementary: 'supplementary',
      coverLetter: 'coverLetter',
      reviewComment: 'reviewComment',
      responseToReviewers: 'responseToReviewers',
    }
  }

  static findAllByManuscript({ manuscriptId }) {
    return this.query()
      .alias('f')
      .where('f.manuscript_id', manuscriptId)
  }

  static findAllByManuscriptAndType({ manuscriptId, type }) {
    return this.findAllByManuscript({ manuscriptId })
      .clone()
      .andWhere('f.type', type)
  }

  static findAllPublicReviewFilesByManuscript({ manuscriptId }) {
    return this.findAllByManuscriptAndType({
      manuscriptId,
      type: this.Types.reviewComment,
    })
      .clone()
      .join('comment AS c', 'c.id', 'f.comment_id')
      .andWhere('c.type', Comment.Types.public)
  }

  toDTO() {
    const { fileName, ...file } = this

    return {
      filename: fileName,
      ...file,
    }
  }
}

module.exports = File
