const { chain } = require('lodash')
const { transaction } = require('objection')
const logger = require('@pubsweet/logger')
const moment = require('moment-business-days')
const config = require('config')

const HindawiBaseModel = require('../hindawiBaseModel')
const Team = require('./team')

class SpecialIssue extends HindawiBaseModel {
  static get tableName() {
    return 'special_issue'
  }

  static modifiers = {
    onlyActive(builder) {
      builder.where('isActive', true).andWhere('isCancelled', false)
    },
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        journalId: { type: ['string', 'null'], format: 'uuid' },
        sectionId: { type: ['string', 'null'], format: 'uuid' },
        peerReviewModelId: { type: ['string', 'null'], format: 'uuid' },
        customId: { type: ['string'] },
        isActive: { type: 'boolean' },
        isCancelled: { type: 'boolean' },
        cancelReason: { type: ['string', 'null'] },
        callForPapers: { type: 'string' },
        startDate: {
          type: ['string', 'object'],
          format: 'date-time',
        },
        endDate: {
          type: ['string', 'object'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'special_issue.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./section'),
        join: {
          from: 'special_issue.sectionId',
          to: 'section.id',
        },
      },
      peerReviewModel: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./peerReviewModel'),
        join: {
          from: 'special_issue.peerReviewModelId',
          to: 'peer_review_model.id',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./team'),
        join: {
          from: 'special_issue.id',
          to: 'team.specialIssueId',
        },
      },
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'special_issue.id',
          to: 'manuscript.specialIssueId',
        },
      },
    }
  }

  static async findUnique(name) {
    const objects = await this.query()
      .whereRaw('LOWER(name) LIKE ?', [name.toLowerCase()])
      .limit(1)

    if (!objects.length) {
      return
    }

    return objects[0]
  }

  static findAllByManuscripts(manuscriptIds) {
    return this.query()
      .select('si.*', 'm.id as manuscriptId')
      .from('special_issue as si')
      .join('manuscript as m', 'si.id', 'm.specialIssueId')
      .whereIn('m.id', manuscriptIds)
  }

  static async findAllActiveSpecialIssues() {
    try {
      return this.query()
        .from('special_issue')
        .where('start_date', '<', new Date())
        .andWhere('end_date', '>', new Date())
        .andWhere({ is_active: true })
        .andWhere({ is_cancelled: false })
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllPendingActivationSpecialIssues() {
    try {
      return this.query()
        .from('special_issue')
        .where('start_date', '>', new Date())
        .andWhere('end_date', '>', new Date())
        .andWhere({ is_active: false })
        .andWhere({ is_cancelled: false })
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllCancelledSpecialIssues() {
    try {
      return this.query()
        .from('special_issue')
        .where({ is_cancelled: true })
        .andWhere({ is_active: true })
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllEndedSpecialIssues() {
    try {
      return this.query()
        .from('special_issue')
        .where('end_date', '<', new Date())
        .andWhere({ is_cancelled: false })
        .andWhere({ is_active: true })
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllBySection({ sectionId }) {
    try {
      return await this.query()
        .from('special_issue AS si')
        .join('section AS s', 'si.section_id', 's.id')
        .where('s.id', sectionId)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByJournal({ journalId }) {
    try {
      return await this.query()
        .from('special_issue AS si')
        .join('journal AS j', 'si.journal_id', 'j.id')
        .where('j.id', journalId)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllThatShouldBeActive(date) {
    try {
      return this.query()
        .from('special_issue')
        .where('start_date', '<', date)
        .andWhere('end_date', '>', date)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllScheduledForActivation() {
    try {
      return this.query()
        .from('special_issue')
        .where('start_date', '>', new Date().toISOString())
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllScheduledForDeactivation() {
    try {
      return this.query()
        .from('special_issue')
        .where('endDate', '>', new Date().toISOString())
    } catch (e) {
      throw new Error(e)
    }
  }

  static async updateMany(specialIssues) {
    try {
      return await transaction(SpecialIssue.knex(), async trx => {
        const options = { noInsert: true, noDelete: true, relate: true }
        await SpecialIssue.query(trx).upsertGraph(specialIssues, options)
      })
    } catch (err) {
      logger.error(err)
      throw new Error('Something went wrong. No data was updated.')
    }
  }

  async cancelJobByType({ Job, jobType }) {
    const specialIssueJob = await Job.findOneBy({
      queryObject: {
        jobType,
        specialIssueId: this.id,
      },
    })
    if (!specialIssueJob) {
      logger.error(
        `No special issue with specialIssueId ${this.id} and type ${jobType} has been found.`,
      )
      return
    }
    Job.cancel(specialIssueJob.id)
    specialIssueJob.delete()
  }

  async handleActivation({
    adminId,
    oldEndDate,
    jobsService,
    oldStartDate,
    models: { Job },
    dateService: { isDateToday, areDatesEqual, isDateInTheFuture },
  }) {
    if (
      areDatesEqual(oldStartDate, this.startDate) &&
      areDatesEqual(oldEndDate, this.endDate)
    ) {
      return
    }

    await this.cancelJobByType({ Job, jobType: Job.Type.activation })

    if (
      isDateToday(this.startDate) ||
      (isDateInTheFuture(this.endDate) && !isDateInTheFuture(this.startDate))
    ) {
      this.updateProperties({ isActive: true, isCancelled: false })
      return
    }

    if (!isDateInTheFuture(this.startDate)) return

    jobsService.scheduleSpecialIssueActivation({
      specialIssue: this,
      teamMemberId: adminId,
    })
  }

  toDTO() {
    let leadGuestEditorMember
    if (this.teams) {
      leadGuestEditorMember = chain(this.teams)
        .find(
          t => t.role === Team.Role.triageEditor && t.specialIssueId !== null,
        )
        .get('members.0')
        .value()
    }

    const expirationInterval = config.get('specialIssueExpirationInterval')
    this.expirationDate = moment(this.endDate)
      .add(expirationInterval.value, expirationInterval.timeUnit)
      .toISOString()

    return {
      ...this,
      leadGuestEditor: leadGuestEditorMember
        ? leadGuestEditorMember.toDTO()
        : undefined,
    }
  }
}

module.exports = SpecialIssue
