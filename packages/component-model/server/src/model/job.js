const HindawiBaseModel = require('../hindawiBaseModel')

const {
  jobs: { connectToJobQueue },
} = require('pubsweet-server/src')
const logger = require('@pubsweet/logger')
const config = require('config')

const newJobCheckIntervalSeconds = config.get('newJobCheckIntervalSeconds')
const jobScheduling = config.get('jobScheduling') === 'true'

class Job extends HindawiBaseModel {
  static get tableName() {
    return 'job'
  }

  static get schema() {
    return {
      properties: {
        teamMemberId: { type: 'string', format: 'uuid' },
        manuscriptId: { type: ['string', 'null'], format: 'uuid' },
        journalId: { type: ['string', 'null'], format: 'uuid' },
        specialIssueId: { type: ['string', 'null'], format: 'uuid' },
        jobType: {
          type: ['string', 'null'],
          default: null,
          enum: [...Object.values(Job.Type), null],
        },
      },
    }
  }

  static get Type() {
    return {
      activation: 'activation',
      deactivation: 'deactivation',
    }
  }

  static get relationMappings() {
    return {
      member: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./teamMember'),
        join: {
          from: 'job.teamMemberId',
          to: 'team_member.id',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'job.manuscriptId',
          to: 'manuscript.id',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'job.journalId',
          to: 'journal.id',
        },
      },
    }
  }

  static async schedule({
    params,
    jobType,
    journalId,
    teamMemberId,
    manuscriptId,
    executionDate,
    specialIssueId,
    queueName = 'review',
  }) {
    if (!jobScheduling) {
      logger.warn('Jobs are deactivated. No jobs have been scheduled. ')
      return
    }

    const jobQueue = await connectToJobQueue()

    params = JSON.parse(JSON.stringify(params))

    try {
      const jobId = await jobQueue.publishAfter(
        queueName,
        params,
        {},
        executionDate,
      )

      const job = new Job({
        jobType,
        id: jobId,
        journalId,
        teamMemberId,
        manuscriptId,
        specialIssueId,
      })
      await job.save()

      logger.info(
        `Successfully scheduled job ${jobId} on queue ${queueName} at: ${executionDate}`,
      )

      return job
    } catch (e) {
      throw new Error(e)
    }
  }

  static async cancel(id) {
    const jobQueue = await connectToJobQueue()

    try {
      await jobQueue.cancel(id)
      logger.info(`Successfully cancelled job ${id}`)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async subscribe({ queueName, jobHandler }) {
    const jobQueue = await connectToJobQueue()

    return jobQueue.subscribe(
      queueName,
      { newJobCheckIntervalSeconds, teamSize: 10, teamConcurrency: 10 },
      jobHandler,
    )
  }

  static async findAllByTeamMember(teamMemberId) {
    try {
      const results = await this.query()
        .select('j.*')
        .from('job AS j')
        .where('j.team_member_id', teamMemberId)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByTeamMembers(teamMemberIds) {
    return this.query()
      .select('j.*')
      .from('job AS j')
      .whereIn('j.team_member_id', teamMemberIds)
  }

  static async findAllByTeamMemberAndManuscript({
    teamMemberId,
    manuscriptId,
  }) {
    try {
      const results = await this.query()
        .select('job.*')
        .from('job')
        .where('job.team_member_id', teamMemberId)
        .andWhere('job.manuscriptId', manuscriptId)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllWithSpecialIssue() {
    return this.query().whereNotNull('special_issue_id')
  }
}

module.exports = Job
