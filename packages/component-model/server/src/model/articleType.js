const HindawiBaseModel = require('../hindawiBaseModel')

class ArticleType extends HindawiBaseModel {
  static get tableName() {
    return 'article_type'
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
        hasPeerReview: { type: 'boolean' },
      },
    }
  }

  static get relationMappings() {
    return {
      journalArticleTypes: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./journalArticleType'),
        join: {
          from: 'article_type.id',
          to: 'journal_article_type.articleTypeId',
        },
      },
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'article_type.id',
          to: 'manuscript.articleTypeId',
        },
      },
    }
  }

  static async findArticleTypeByManuscript(manuscriptId) {
    try {
      const results = await this.query()
        .select('at.*')
        .from('article_type AS at')
        .join('manuscript AS m', 'at.id', 'm.article_type_id')
        .where('m.id', manuscriptId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static findAllByManuscriptIds(manuscriptIds) {
    return this.query()
      .select('at.*', 'm.id as manuscriptId')
      .from('article_type as at')
      .join('manuscript as m', 'at.id', 'm.article_type_id')
      .whereIn('m.id', manuscriptIds)
  }

  static get Types() {
    return {
      commentary: 'Commentary',
      editorial: 'Editorial',
      retraction: 'Retraction',
      caseSeries: 'Case Series',
      caseReport: 'Case Report',
      reviewArticle: 'Review Article',
      researchArticle: 'Research Article',
      letterToTheEditor: 'Letter to the Editor',
      expressionOfConcern: 'Expression of Concern',
      erratum: 'Erratum',
      corrigendum: 'Corrigendum',
    }
  }

  static get TypesWithPeerReview() {
    return [
      this.Types.caseSeries,
      this.Types.caseReport,
      this.Types.reviewArticle,
      this.Types.researchArticle,
    ]
  }

  static get TypesWithRIPE() {
    return [
      this.Types.retraction,
      this.Types.expressionOfConcern,
      this.Types.erratum,
      this.Types.corrigendum,
    ]
  }

  static get EditorialTypes() {
    return [this.Types.editorial, this.Types.commentary]
  }

  toDTO() {
    return {
      ...this,
    }
  }
}

module.exports = ArticleType
