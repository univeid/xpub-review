const HindawiBaseModel = require('../hindawiBaseModel')

class PeerReviewModel extends HindawiBaseModel {
  static get tableName() {
    return 'peer_review_model'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        approvalEditors: { type: 'array', items: { type: 'string' } },
        hasFigureheadEditor: { type: 'boolean' },
        figureheadEditorLabel: { type: ['string', null] },
        hasSections: { type: 'boolean' },
        hasTriageEditor: { type: 'boolean' },
        triageEditorLabel: { type: 'string' },
        triageEditorAssignmentTool: {
          type: 'array',
          items: { type: 'string' },
        },
        academicEditorLabel: { type: 'string' },
        academicEditorAutomaticInvitation: { type: 'boolean' },
        reviewerAssignmentTool: { type: 'array', items: { type: 'string' } },
      },
    }
  }

  static get jsonAttributes() {
    return []
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./journal'),
        join: {
          from: 'peerReviewModel.id',
          to: 'journal.peerReviewModelId',
        },
      },
    }
  }

  static async findOneByManuscript(manuscriptId) {
    try {
      const result = await this.query()
        .select('prm.*')
        .from('peer_review_model AS prm')
        .join('journal AS j', 'j.peer_review_model_id', 'prm.id')
        .join('manuscript AS m', 'm.journal_id', 'j.id')
        .where('m.id', manuscriptId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static findAllByJournals(journalIds) {
    return this.query()
      .select('prm.*', 'j.id as journalId')
      .from('peer_review_model as prm')
      .join('journal as j', 'prm.id', 'j.peer_review_model_id')
      .whereIn('j.id', journalIds)
  }

  static findAllBySIs(siIds) {
    return this.query()
      .select('prm.*', 'si.id as specialIssueId')
      .from('peer_review_model as prm')
      .join('special_issue as si', 'prm.id', 'si.peer_review_model_id')
      .whereIn('si.id', siIds)
  }

  static async findOneByJournal(journalId) {
    try {
      const result = await this.query()
        .select('prm.*')
        .from('peer_review_model AS prm')
        .join('journal AS j', 'j.peer_review_model_id', 'prm.id')
        .where('j.id', journalId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneBySpecialIssue(specialIssueId) {
    try {
      const result = await this.query()
        .select('prm.*')
        .from('peer_review_model AS prm')
        .join('special_issue AS si', 'si.peer_review_model_id', 'prm.id')
        .where('si.id', specialIssueId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByManuscriptParent({ journalId, specialIssueId }) {
    if (specialIssueId) {
      return PeerReviewModel.findOneBySpecialIssue(specialIssueId)
    }
    return PeerReviewModel.findOneByJournal(journalId)
  }

  toDTO() {
    return { ...this }
  }

  static findAllJournalOnesByManuscriptIds(manuscriptIds) {
    return this.query()
      .select('prm.*', 'm.id as manuscriptId')
      .from('peer_review_model AS prm')
      .join('journal AS j', 'j.peer_review_model_id', 'prm.id')
      .join('manuscript AS m', 'm.journal_id', 'j.id')
      .whereIn('m.id', manuscriptIds)
  }

  static findAllSpecialIssueOnesByManuscriptIds(manuscriptIds) {
    return this.query()
      .select('prm.*', 'm.id as manuscriptId')
      .from('peer_review_model AS prm')
      .join('special_issue AS si', 'si.peer_review_model_id', 'prm.id')
      .join('manuscript AS m', 'm.special_issue_id', 'si.id')
      .whereIn('m.id', manuscriptIds)
  }
}

module.exports = PeerReviewModel
