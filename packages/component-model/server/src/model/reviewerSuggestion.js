const HindawiBaseModel = require('../hindawiBaseModel')

class ReviewerSuggestion extends HindawiBaseModel {
  static get tableName() {
    return 'reviewer_suggestion'
  }

  static get schema() {
    return {
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        givenNames: { type: 'string' },
        surname: { type: 'string' },
        numberOfReviews: { type: 'integer' },
        aff: { type: 'string' },
        email: { type: 'string' },
        profileUrl: { type: 'string' },
        isInvited: { type: 'boolean', default: false },
        type: { type: 'string', enum: ['publons'] },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'reviewer_suggestion.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  $formatDatabaseJson(json) {
    json = super.$formatDatabaseJson(json)
    const { email } = json

    if (email) {
      return { ...json, email: email.toLowerCase() }
    }

    return json
  }

  toDTO() {
    return { ...this }
  }
}

module.exports = ReviewerSuggestion
