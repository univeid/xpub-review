const { transaction } = require('objection')
const logger = require('@pubsweet/logger')

const HindawiBaseModel = require('../hindawiBaseModel')

const Team = require('./team')
const Identity = require('./identity')
const TeamMember = require('./teamMember')

class User extends HindawiBaseModel {
  static get tableName() {
    return 'user'
  }

  static get schema() {
    return {
      properties: {
        defaultIdentityType: { type: 'string' },
        isActive: { type: 'boolean' },
        isSubscribedToEmails: { type: 'boolean', default: true },
        confirmationToken: { type: ['string', 'null'], format: 'uuid' },
        invitationToken: { type: ['string', 'null'], format: 'uuid' },
        passwordResetToken: { type: ['string', 'null'], format: 'uuid' },
        unsubscribeToken: { type: ['string', 'null'], format: 'uuid' },
        agreeTc: { type: 'boolean' },
        passwordResetTimestamp: {
          type: ['string', 'null', 'object'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      identities: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Identity,
        join: {
          from: 'user.id',
          to: 'identity.userId',
        },
      },
      teamMemberships: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./teamMember'),
        join: {
          from: 'user.id',
          to: 'team_member.userId',
        },
      },
      teams: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('./team'),
        join: {
          from: 'user.id',
          through: {
            from: 'team_member.userId',
            to: 'team_member.teamId',
            extra: ['status'],
          },
          to: 'team.id',
        },
      },
    }
  }

  static async findAllWithDefaultIdentity({
    page = 0,
    pageSize = 20,
    orderBy = 'given_names',
    order = 'asc',
    searchValue = '',
  }) {
    const { results, total } = await this.query()
      .alias('u')
      .select(
        'u.id',
        'isActive',
        'surname',
        'given_names',
        'aff',
        'is_confirmed',
        'email',
      )
      .leftJoin('identity as i', 'u.id', 'i.userId')
      .where('i.type', 'local')
      .where(builder => addSearchFilter(builder, searchValue))
      .orderBy(orderBy, order)
      .page(page, pageSize)

    return {
      results: results.map(({ givenNames, surname, ...user }) => ({
        ...user,
        name: {
          givenNames,
          surname,
        },
      })),
      total,
    }
  }

  static async findOneWithDefaultIdentity(userId) {
    const { givenNames, surname, ...user } = await this.query()
      .alias('u')
      .select(
        'u.id',
        'isActive',
        'surname',
        'given_names',
        'aff',
        'is_confirmed',
        'email',
      )
      .leftJoin('identity as i', 'u.id', 'i.userId')
      .where('i.type', 'local')
      .where('u.id', userId)
      .first()

    return {
      ...user,
      name: {
        givenNames,
        surname,
      },
    }
  }

  async hasStaffRole() {
    return this.$relatedQuery('teams')
      .first()
      .then(result => result && Team.StaffRoles.includes(result.role))
  }

  // to do. refactor this. its doing three expensive queries. 1xemail, 2xnames
  static async findByEmailOrName({ input, eagerLoadRelations }) {
    return this.query()
      .from('user as u')
      .join('identity as i', 'i.user_id', 'u.id')
      .where('u.isActive', true)
      .andWhere('i.isConfirmed', true)
      .andWhere('i.email', 'ILIKE', `%${input}%`)
      .orWhereRaw(`concat(lower(surname), ' ', lower(given_names)) ILIKE ?`, [
        `%${input}%`,
      ])
      .orWhereRaw(`concat(lower(given_names), ' ', lower(surname)) ILIKE ?`, [
        `%${input}%`,
      ])
      .limit(10)
      .withGraphFetched(
        HindawiBaseModel._parseEagerRelations(eagerLoadRelations),
      )
  }

  static async findAllWithoutIdentity() {
    return this.query()
      .alias('u')
      .select('u.id')
      .leftJoin('identity as i', 'u.id', 'i.userId')
      .where('i.userId', null)
  }

  static async findOneByEmail(email, eagerLoadRelations) {
    return this.query()
      .alias('u')
      .leftJoin('identity as i', 'u.id', 'i.userId')
      .where('email', email.toLowerCase())
      .withGraphFetched(
        HindawiBaseModel._parseEagerRelations(eagerLoadRelations),
      )
      .limit(1)
      .first()
  }

  get defaultIdentity() {
    if (!this.identities) {
      logger.warn('Cannot get default identity when identities are not loaded.')
      return
    }

    return this.identities.find(
      identity => identity.type === this.defaultIdentityType,
    )
  }

  assignIdentity(identity) {
    this.identities = this.identities || []
    this.identities.push(identity)
  }

  async isEditorialAssistant() {
    const editorialAssistantRole = await this.$query()
      .alias('u')
      .join('team_member as tm', 'tm.userId', 'u.id')
      .join('team as t', 't.id', 'tm.teamId')
      .whereNot('tm.status', TeamMember.Statuses.removed)
      .andWhere('t.role', Team.Role.editorialAssistant)
      .limit(1)
      .first()

    return !!editorialAssistantRole
  }

  async isAdmin() {
    const adminRole = await this.$relatedQuery('teams')
      .alias('t')
      .select('role')
      .where('t.role', Team.Role.admin)
      .first()

    return !!adminRole
  }

  toDTO() {
    return {
      ...this,
      identities:
        this.identities && this.identities.map(identity => identity.toDTO()),
    }
  }

  findTeamMemberByRole(role) {
    return this.$relatedQuery('teamMemberships')
      .alias('tm')
      .rightJoin('team as t', 'tm.teamId', 't.id')
      .where('t.role', role)
      .first()
  }

  findTeamMembersByRoles(roles) {
    return this.$relatedQuery('teamMemberships')
      .alias('tm')
      .rightJoin('team as t', 'tm.teamId', 't.id')
      .whereIn('t.role', roles)
  }

  async getTeamMemberRoleForManuscript(manuscript) {
    const userTeams = await this.$relatedQuery('teams').where(builder =>
      builder
        .where('manuscriptId', manuscript.id)
        .orWhere('journalId', manuscript.journalId)
        .orWhere('role', Team.Role.admin),
    )

    if (userTeams.find(({ role }) => role === Team.Role.author))
      // a user might be both author and triage editor (until the assignment is fixed)
      return Team.Role.author

    const manuscriptTeam = userTeams.find(
      ({ manuscriptId }) => manuscriptId === manuscript.id,
    )
    if (manuscriptTeam) return manuscriptTeam.role

    const journalTeam = userTeams.find(
      ({ journalId }) => journalId === manuscript.journalId,
    )
    if (journalTeam) return journalTeam.role

    if (userTeams.find(({ role }) => role === Team.Role.admin))
      return Team.Role.admin
  }

  async isUserConfirmed() {
    const confirmedIdentity = await this.$relatedQuery(
      'identities',
    ).where(builder =>
      builder.where('type', 'local').andWhere('isConfirmed', true),
    )
    return !!confirmedIdentity[0]
  }

  async updateWithIdentity(identity) {
    try {
      return await transaction(User.knex(), async trx => {
        await this.save()
        return this.$relatedQuery('identities', trx).update(identity)
      })
    } catch (err) {
      logger.error(err)
      throw new Error('Something went wrong. No data was updated.')
    }
  }

  async insertWithIdentity(identity) {
    try {
      return await transaction(User.knex(), async trx => {
        const user = await User.query(trx).insertAndFetch(this)
        await user.$relatedQuery('identities', trx).insert(identity) // objection2.0 fix
        return user
      })
    } catch (err) {
      if (err.constraint === 'identity_email_unique') {
        throw new ConflictError('User already exists.')
      }
      throw new Error('Something went wrong. No data was inserted.')
    }
  }
}

function addSearchFilter(builder, searchValue) {
  if (searchValue) {
    builder
      .whereRaw('i.aff ILIKE ?', [`%${searchValue}%`])
      .orWhereRaw('i.email ILIKE ?', [`%${searchValue}%`])
      .orWhereRaw('i.surname ILIKE ?', [`%${searchValue}%`])
      .orWhereRaw('i.given_names ILIKE ?', [`%${searchValue}%`])
      .orWhereRaw(`concat(i.given_names, ' ', i.surname) ILIKE ?`, [
        `%${searchValue}%`,
      ])
      .orWhereRaw(`concat(i.surname, ' ', i.given_names) ILIKE ?`, [
        `%${searchValue}%`,
      ])
  }
  return builder
}

module.exports = User
