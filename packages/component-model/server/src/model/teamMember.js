const { pick, get, orderBy } = require('lodash')
const logger = require('@pubsweet/logger')
const HindawiBaseModel = require('../hindawiBaseModel')
const EditorSuggestion = require('./editorSuggestion')
const { raw, transaction } = require('objection')

class TeamMember extends HindawiBaseModel {
  static get tableName() {
    return 'team_member'
  }

  static get schema() {
    return {
      properties: {
        userId: { type: 'string', format: 'uuid' },
        teamId: { type: 'string', format: 'uuid' },
        position: { type: ['integer', null] },
        isSubmitting: { type: ['boolean', null] },
        isCorresponding: { type: ['boolean', null] },
        status: {
          enum: Object.values(TeamMember.Statuses),
          default: TeamMember.Statuses.pending,
        },
        reviewerNumber: { type: ['integer', null] },
        responded: { type: ['string', 'object', 'null'], format: 'date-time' },
        alias: {
          type: 'object',
          properties: {
            surname: { type: ['string', 'null'] },
            givenNames: { type: ['string', 'null'] },
            email: { type: 'string' },
            aff: { type: ['string', 'null'] },
            country: { type: ['string', 'null'] },
            title: { type: ['string', 'null'] },
          },
        },
        invitedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./user'),
        join: {
          from: 'team_member.userId',
          to: 'user.id',
        },
      },
      team: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./team'),
        join: {
          from: 'team_member.teamId',
          to: 'team.id',
          extra: ['role'],
        },
      },
      jobs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./job'),
        join: {
          from: 'team_member.id',
          to: 'job.teamMemberId',
        },
      },
    }
  }

  $formatDatabaseJson(json) {
    json = super.$formatDatabaseJson(json)

    if (json.alias) {
      const alias = JSON.parse(json.alias)
      const email = get(alias, 'email', '')
      return { ...json, alias: { ...alias, email: email.toLowerCase() } }
    }

    return json
  }

  static get Statuses() {
    return {
      pending: 'pending',
      accepted: 'accepted',
      declined: 'declined',
      submitted: 'submitted',
      expired: 'expired',
      removed: 'removed',
      active: 'active',
    }
  }

  static get StatusExpiredLabels() {
    return {
      overdue: 'OVERDUE',
      unresponsive: 'UNRESPONSIVE',
      unsubscribed: 'UNSUBSCRIBED',
    }
  }

  static async findAllByStatuses({
    role,
    statuses,
    manuscriptId,
    submissionId,
  }) {
    try {
      const results = await this.query()
        .skipUndefined()
        .select('tm.*')
        .from('team_member AS tm')
        .leftJoin('team AS t', 'tm.team_id', 't.id')
        .leftJoin('manuscript AS m', 't.manuscript_id', '=', 'm.id')
        .whereIn('tm.status', statuses)
        .andWhere('t.role', role)
        .andWhere('m.id', manuscriptId)
        .andWhere('m.submission_id', submissionId)
        .limit(50)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByRoleAndStatusWhereRespondedNull({ role, status }) {
    try {
      return await this.query()
        .alias('tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.status', status)
        .andWhere('t.role', role)
        .andWhere('tm.responded', null)
    } catch (e) {
      throw new Error(e)
    }
  }

  static findOneByUserAndRole({ userId, role, eagerLoadRelations }) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .where('tm.userId', userId)
      .andWhere('t.role', role)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
      .limit(1)
      .first()
  }

  static findOneByUserAndRoles({ userId, roles }) {
    return this.query()
      .alias('tm')
      .join('team as t', 'tm.team_id', 't.id')
      .whereIn('t.role', roles)
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static async findOneByRole({ role }) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('t.role', role)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByEmailOnManuscriptParent(email) {
    try {
      return this.query()
        .alias('tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .whereIn('t.role', ['triageEditor', 'academicEditor'])
        .andWhereRaw(`tm.alias->>'email' = ?`, [email])
        .andWhere('t.manuscript_id', null)
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByUserAndRoleAndStatusOnManuscript({
    role,
    userId,
    status,
    manuscriptId,
  }) {
    const result = await this.query()
      .select('tm.*')
      .from('team_member as tm')
      .join('team as t', 'tm.team_id', 't.id')
      .join('manuscript as m', 't.manuscript_id', 'm.id')
      .where('t.role', role)
      .andWhere('tm.userId', userId)
      .andWhere('tm.status', status)
      .andWhere('m.id', manuscriptId)
      .limit(1)
      .first()
    return result
  }

  static async findOneByUserAndRoleAndStatus({ userId, role, status }) {
    try {
      return this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('tm.userId', userId)
        .andWhere('tm.status', status)
        .andWhere('t.role', role)
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneBySubmissionAndUser({ submissionId, userId, TeamRole }) {
    try {
      const submissionTeamMember = await this.query()
        .select('tm.*', 't.role')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .leftJoin('user as u', 'tm.userId', 'u.id')
        .leftJoin('role as r', 'r.name', 't.role') // TODO: change the role.name to role.id
        .leftJoin('submission_status as ss', 'm.status', 'ss.name') // TODO: change ss.name to ss.id
        .leftJoin('submission_label as sl', function() {
          this.on('r.id', 'sl.roleId')
          this.andOn('ss.id', 'sl.submissionStatusId')
        })
        .where('tm.userId', userId)
        .andWhere('m.submissionId', submissionId)
        .andWhere(query =>
          query
            .whereNotIn('tm.status', ['declined', 'expired', 'removed'])
            .whereNotNull('t.manuscript_id'),
        )
        .orderByRaw('sl.priority DESC NULLS LAST')
        .limit(1)
        .first()

      if (submissionTeamMember) return submissionTeamMember

      return await this.query()
        .select('tm.*', 't.role')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('tm.userId', userId)
        .andWhere('t.role', TeamRole.admin)
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findSubmittingAuthor(manuscriptId) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.isSubmitting', true)
        .andWhere('m.id', '=', manuscriptId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findCorrespondingAuthor(manuscriptId) {
    try {
      return await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('t.role', 'author')
        .andWhere('tm.isCorresponding', true)
        .andWhere('m.id', '=', manuscriptId)
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findCorrespondingEditorialAssistant(journalId) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('journal AS j', 't.journal_id', 'j.id')
        .where('t.role', 'editorialAssistant')
        .andWhere('tm.isCorresponding', true)
        .andWhere('j.id', '=', journalId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async deleteStaffMemberByUserIdAndRole({ userId, role }) {
    try {
      return this.query()
        .delete()
        .from(raw('team_member tm USING team t'))
        .where(raw('t.id = tm.team_id'))
        .andWhere('tm.user_id', userId)
        .andWhere('t.role', role)
        .andWhere('t.journal_id', null)
        .andWhere('t.section_id', null)
        .andWhere('t.special_issue_id', null)
        .andWhere('t.manuscript_id', null)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async getApprovalEditorRole({
    TeamRole,
    manuscript,
    ArticleType,
    PeerReviewModel,
  }) {
    const articleType = await ArticleType.find(manuscript.articleTypeId)
    const academicEditor = await this.findOneByManuscriptAndRoleAndStatus({
      manuscriptId: manuscript.id,
      role: TeamRole.academicEditor,
      status: TeamMember.Statuses.accepted,
    })

    if (ArticleType.TypesWithRIPE.includes(articleType.name)) {
      return TeamRole.researchIntegrityPublishingEditor
    }

    if (manuscript.specialIssueId) {
      if (
        academicEditor ||
        ArticleType.EditorialTypes.includes(articleType.name)
      )
        return TeamRole.academicEditor
      return TeamRole.triageEditor
    }

    const peerReviewModel = await PeerReviewModel.findOneByJournal(
      manuscript.journalId,
    )
    if (articleType.hasPeerReview) {
      const isAcademicEditorApprovalEditor =
        peerReviewModel.approvalEditors.includes(TeamRole.academicEditor) ||
        manuscript.hasTriageEditorConflictOfInterest

      if (isAcademicEditorApprovalEditor) {
        return TeamRole.academicEditor
      }

      return TeamRole.triageEditor
    } else if (academicEditor) {
      return TeamRole.academicEditor
    }

    return TeamRole.triageEditor
  }

  static async isApprovalEditor({ userId, manuscriptId, models }) {
    const { Manuscript, Team, ArticleType, PeerReviewModel } = models
    const manuscript = await Manuscript.find(manuscriptId)

    const approvalEditorRole = await this.getApprovalEditorRole({
      manuscript,
      ArticleType,
      PeerReviewModel,
      TeamRole: Team.Role,
    })
    let status = TeamMember.Statuses.accepted
    if (approvalEditorRole === Team.Role.triageEditor) {
      status = TeamMember.Statuses.active
      if (manuscript.hasTriageEditorConflictOfInterest) {
        return false
      }
    }

    const approvalEditorTeamMembers = await this.findAllByManuscriptAndRoleAndStatus(
      {
        role: approvalEditorRole,
        manuscriptId,
        status,
      },
    )
    if (!approvalEditorTeamMembers.length === 0) {
      return false
    }

    return approvalEditorTeamMembers.some(member => member.userId === userId)
  }

  static async findApprovalEditor({
    manuscript,
    TeamRole,
    ArticleType,
    PeerReviewModel,
  }) {
    const approvalEditorRole = await this.getApprovalEditorRole({
      TeamRole,
      manuscript,
      ArticleType,
      PeerReviewModel,
    })

    const status =
      approvalEditorRole === TeamRole.triageEditor
        ? this.Statuses.active
        : this.Statuses.accepted

    return this.findOneByManuscriptAndRoleAndStatus({
      status,
      role: approvalEditorRole,
      manuscriptId: manuscript.id,
    })
  }

  static async findTriageEditor({
    TeamRole,
    sectionId,
    journalId,
    manuscriptId,
    eagerLoadRelations,
  }) {
    let triageEditor = await this.findOneByManuscriptAndRoleAndStatus({
      manuscriptId,
      role: TeamRole.triageEditor,
      status: this.Statuses.active,
      eagerLoadRelations,
    })

    if (triageEditor) return triageEditor

    if (!journalId && !sectionId) {
      throw new Error('Journal ID or Section ID is required.')
    }

    triageEditor = await this.findOneBySectionAndRole({
      sectionId,
      role: TeamRole.triageEditor,
      eagerLoadRelations,
    })

    if (triageEditor) return triageEditor

    triageEditor = await this.findOneByJournalAndRole({
      journalId,
      role: TeamRole.triageEditor,
      eagerLoadRelations,
    })

    return triageEditor
  }

  static async orderAcademicEditorsByScore({ academicEditors, manuscriptId }) {
    const editorSuggestions = await EditorSuggestion.findBy({
      manuscriptId,
    })
    return orderBy(
      academicEditors,
      academicEditor => {
        const editorSubmissionScore = editorSuggestions.find(
          editorSubmissionScore =>
            editorSubmissionScore.teamMemberId === academicEditor.id,
        )

        return editorSubmissionScore ? editorSubmissionScore.score : 0
      },
      ['desc'],
    )
  }

  static async isSuggestionsListInSyncWithTeamMembers(editorSuggestions) {
    const editorSuggestionIds = editorSuggestions.map(
      editorSuggestion => editorSuggestion.editorId,
    )
    const teamMembersLength = await this.query()
      .whereIn('id', editorSuggestionIds)
      .resultSize()

    return teamMembersLength === editorSuggestionIds.length
  }

  // Transactions

  static async updateAndSaveMany(teamMembers, trx) {
    return this.query(trx).upsertGraph(teamMembers, {
      insertMissing: true,
      relate: true,
    })
  }

  static async removeEditorialAssistantTransaction({
    journalEA,
    manuscriptEAs,
  }) {
    try {
      await transaction(this.knex(), async trx => {
        await this.query(trx).deleteById(journalEA.id)
        await this.query(trx).upsertGraph(manuscriptEAs, {
          insertMissing: true,
          relate: true,
        })
      })
    } catch (err) {
      logger.error(err)
      throw new Error('Something went wrong. No data was updated.')
    }
  }

  static async removeOneAndUpdateMany({
    memberToBeRemovedId,
    membersToBeUpdated,
  }) {
    return transaction(this.knex(), async trx => {
      await this.query(trx).deleteById(memberToBeRemovedId)
      return this.query(trx).upsertGraph(membersToBeUpdated, {
        insertMissing: true,
        relate: true,
      })
    })
  }

  static async updateCorrespondingMember({
    newCorrespondingMember,
    oldCorrespondingMember,
  }) {
    try {
      return await transaction(TeamMember.knex(), async trx => {
        await oldCorrespondingMember.$query(trx).update({
          isCorresponding: false,
          alias: oldCorrespondingMember.alias,
        })
        await newCorrespondingMember.$query(trx).update({
          isCorresponding: true,
          alias: newCorrespondingMember.alias,
        })
      })
    } catch (err) {
      logger.error(err)
      throw new Error(
        'Something went wrong while trying to change the corresponding team member',
      )
    }
  }

  static async handleCorrespondingAuthor({
    manuscriptId,
    editedAuthor,
    inputCorresponding,
  }) {
    const correspondingAuthor = await this.findCorrespondingAuthor(manuscriptId)
    if (inputCorresponding === true) {
      await this.updateCorrespondingMember({
        newCorrespondingMember: editedAuthor,
        oldCorrespondingMember: correspondingAuthor,
      })
    } else {
      const submittingAuthor = await this.findSubmittingAuthor(manuscriptId)
      if (editedAuthor.id === correspondingAuthor.id) {
        await this.updateCorrespondingMember({
          newCorrespondingMember: submittingAuthor,
          oldCorrespondingMember: editedAuthor,
        })
      }
      if (!correspondingAuthor) {
        submittingAuthor.updateProperties({
          isCorresponding: true,
        })
        await submittingAuthor.save()
      }
    }
  }

  // Workload
  static async findAllByJournalWithWorkload({
    role,
    journalId,
    teamMemberStatuses,
    manuscriptStatuses,
  }) {
    return this.query()
      .select('tmj.user_id', 'tmj.alias', 'TMs.workload')
      .from({ tmj: 'team_member' })
      .join({ t: 'team' }, 't.id', 'tmj.team_id')
      .join({ j: 'journal' }, 'j.id', 't.journal_id')
      .leftJoin(
        query =>
          query
            .select('tmm.user_id', raw('count(tmm.id)::integer as workload'))
            .from({ tmm: 'team_member' })
            .join({ tm: 'team' }, 'tm.id', 'tmm.team_id')
            .join({ m: 'manuscript' }, 'm.id', 'tm.manuscript_id')
            .whereIn('m.status', manuscriptStatuses)
            .whereIn('tmm.status', teamMemberStatuses)
            .whereNotNull('tm.manuscript_id')
            .andWhere('tm.role', role)
            .andWhere('m.journal_id', journalId)
            .groupBy('tmm.user_id')
            .as('TMs'),
        'TMs.user_id',
        'tmj.user_id',
      )
      .where('t.role', role)
      .andWhere('j.id', journalId)
      .groupBy('tmj.id', 'j.name', 'TMs.workload')
  }

  static findTeamMembersWorkloadByJournal(argsObject) {
    try {
      return this.findTeamMemberWorkloadByManuscriptParent({
        manuscriptParentId: argsObject.journalId,
        manuscriptParentType: 'journal_id',
        ...argsObject,
      })
        .clone()
        .andWhere(`m.specialIssueId`, null)
        .andWhere(`m.sectionId`, null)
    } catch (e) {
      logger.error(e)
      throw new Error('Something went wrong.')
    }
  }

  static findTeamMembersWorkloadByJournalAndSpecialIssue(argsObject) {
    try {
      return this.findTeamMemberWorkloadByManuscriptParent({
        manuscriptParentId: argsObject.journalId,
        manuscriptParentType: 'journal_id',
        ...argsObject,
      })
        .clone()
        .andWhere(`m.sectionId`, null)
    } catch (e) {
      logger.error(e)
      throw new Error('Something went wrong.')
    }
  }

  static findTeamMembersWorkloadBySection(argsObject) {
    try {
      return this.findTeamMemberWorkloadByManuscriptParent({
        manuscriptParentId: argsObject.sectionId,
        manuscriptParentType: 'section_id',
        ...argsObject,
      })
    } catch (e) {
      logger.error(e)
      throw new Error('Something went wrong.')
    }
  }

  static findTeamMembersWorkloadBySpecialIssue(argsObject) {
    try {
      return this.findTeamMemberWorkloadByManuscriptParent({
        manuscriptParentId: argsObject.specialIssueId,
        manuscriptParentType: 'special_issue_id',
        ...argsObject,
      })
    } catch (e) {
      logger.error(e)
      throw new Error('Something went wrong.')
    }
  }

  static findTeamMemberWorkloadByManuscriptParent({
    role,
    teamMemberStatuses,
    manuscriptStatuses,
    manuscriptParentId,
    manuscriptParentType,
  }) {
    return this.query()
      .select('tm.user_id')
      .count('tm.id as workload')
      .from('team_member as tm')
      .join('team as t', 'tm.team_id', 't.id')
      .join('manuscript as m', 't.manuscript_id', 'm.id')
      .whereIn('tm.status', teamMemberStatuses)
      .whereIn('m.status', manuscriptStatuses)
      .andWhere('t.role', role)
      .andWhere(`m.${manuscriptParentType}`, manuscriptParentId)
      .groupBy('tm.user_id')
  }

  static findTeamMemberWorkloadByUser({
    userId,
    role,
    teamMemberStatuses,
    manuscriptStatuses,
  }) {
    return this.query()
      .count('tm.id')
      .from('team_member as tm')
      .join('team as t', 'tm.team_id', 't.id')
      .join('manuscript as m', 't.manuscript_id', 'm.id')
      .whereIn('tm.status', teamMemberStatuses)
      .whereIn('m.status', manuscriptStatuses)
      .andWhere('tm.user_id', userId)
      .andWhere('t.role', role)
      .limit(1)
      .first()
  }

  static async findAllWithWorkloadByManuscriptParent({
    role,
    journalId,
    sectionId,
    specialIssueId,
    teamMemberStatuses,
    manuscriptStatuses,
  }) {
    try {
      if (specialIssueId) {
        return await this.findTeamMembersWorkloadBySpecialIssue({
          role,
          specialIssueId,
          teamMemberStatuses,
          manuscriptStatuses,
        })
      }

      if (sectionId) {
        return await this.findTeamMembersWorkloadBySection({
          role,
          sectionId,
          teamMemberStatuses,
          manuscriptStatuses,
        })
      }

      if (journalId) {
        return await this.findTeamMembersWorkloadByJournal({
          role,
          journalId,
          teamMemberStatuses,
          manuscriptStatuses,
        })
      }

      throw new Error('No manuscript parent ID has been provided.')
    } catch (e) {
      logger.error(e)
      throw new Error('Something went wrong.')
    }
  }

  static async findAllByManuscriptParentAndRole({
    role,
    journalId,
    sectionId,
    specialIssueId,
  }) {
    try {
      let members = []
      if (specialIssueId) {
        members = await this.findAllBySpecialIssueAndRole({
          role,
          specialIssueId,
        })

        if (members.length) return members
      }

      if (sectionId) {
        members = await this.findAllBySectionAndRole({
          role,
          sectionId,
        })

        if (members.length) return members
      }

      if (journalId) {
        return await TeamMember.findAllByJournalAndRole({
          role,
          journalId,
        })
      }

      throw new Error('No manuscript parent ID has been provided.')
    } catch (e) {
      logger.error(e)
      throw new Error('Something went wrong.')
    }
  }

  // By Journal

  static findAllByJournal({ journalId, eagerLoadRelations }) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('journal AS j', 't.journal_id', 'j.id')
      .where('j.id', journalId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findOneByJournalAndUser({ userId, journalId, eagerLoadRelations }) {
    return this.findAllByJournal({
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static findOneByJournalAndRole({ role, journalId, eagerLoadRelations }) {
    return this.findAllByJournal({
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
      .limit(1)
      .first()
  }

  static findAllByJournalAndRole({ role, journalId, eagerLoadRelations }) {
    return this.findAllByJournal({
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
  }

  static findAllByJournalAndRoleAndSearchValue({
    role,
    journalId,
    searchValue = '',
    eagerLoadRelations,
  }) {
    return this.findAllByJournalAndRole({
      role,
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere(builder =>
        builder
          .whereRaw(`tm.alias->>'email' like ?`, [`${searchValue}%`])
          .orWhereRaw(
            `concat(lower(tm.alias->>'surname'), ' ',lower(tm.alias->>'givenNames')) like ?`,
            [`${searchValue.toLowerCase()}%`],
          )
          .orWhereRaw(
            `concat(lower(tm.alias->>'givenNames'), ' ',lower(tm.alias->>'surname')) like ?`,
            [`${searchValue.toLowerCase()}%`],
          ),
      )
      .limit(50)
  }

  // By Section

  static findAllBySection({ sectionId, eagerLoadRelations }) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('section AS s', 't.section_id', 's.id')
      .andWhere('s.id', sectionId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findOneBySectionAndRole({ role, sectionId, eagerLoadRelations }) {
    return this.findAllBySection({ sectionId, eagerLoadRelations })
      .clone()
      .andWhere('t.role', role)
      .limit(1)
      .first()
  }

  static findOneBySectionAndUser({ userId, sectionId, eagerLoadRelations }) {
    return this.findAllBySection({ sectionId, eagerLoadRelations })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static findAllBySectionAndRole({ role, sectionId, eagerLoadRelations }) {
    return this.findAllBySection({ sectionId, eagerLoadRelations })
      .clone()
      .andWhere('t.role', role)
  }

  // By Special Issue

  static findAllBySpecialIssue({ specialIssueId, eagerLoadRelations }) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('special_issue AS si', 't.special_issue_id', 'si.id')
      .andWhere('si.id', specialIssueId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findAllBySpecialIssues({ specialIssueIds }) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('special_issue AS si', 't.special_issue_id', 'si.id')
      .whereIn('si.id', specialIssueIds)
  }

  static findAllBySpecialIssuesAndUsers({ specialIssueIds, userIds }) {
    return this.findAllBySpecialIssues({ specialIssueIds })
      .clone()
      .andWhereIn('tm.user_id', userIds)
  }

  static findOneBySpecialIssuesAndUsers({ specialIssueIds, userIds }) {
    return this.findAllBySpecialIssues({ specialIssueIds })
      .clone()
      .andWhereIn('tm.user_id', userIds)
      .limit(1)
  }

  static findOneBySpecialIssueAndUser({
    userId,
    specialIssueId,
    eagerLoadRelations,
  }) {
    return this.findAllBySpecialIssue({
      specialIssueId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static findOneBySpecialIssueAndUsers({
    userIds,
    specialIssueId,
    eagerLoadRelations,
  }) {
    return this.findAllBySpecialIssue({
      specialIssueId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere(builder => builder.whereIn('tm.userId', userIds))
      .limit(1)
      .first()
  }

  static findAllBySpecialIssueAndUser({
    userId,
    specialIssueId,
    eagerLoadRelations,
  }) {
    return this.findAllBySpecialIssue({ specialIssueId, eagerLoadRelations })
      .clone()
      .andWhere('tm.userId', userId)
  }

  static findAllBySpecialIssueAndUserAndRoles({
    roles,
    userId,
    specialIssueId,
    eagerLoadRelations,
  }) {
    return this.findAllBySpecialIssueAndUser({
      userId,
      specialIssueId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere(builder => builder.whereIn('t.role', roles))
  }

  static findAllBySpecialIssueAndRole({
    role,
    specialIssueId,
    eagerLoadRelations,
  }) {
    return this.findAllBySpecialIssue({ specialIssueId, eagerLoadRelations })
      .clone()
      .andWhere('t.role', role)
  }

  static findAllBySpecialIssueAndRoleAndSearchValue({
    role,
    searchValue = '',
    specialIssueId,
    eagerLoadRelations,
  }) {
    return this.findAllBySpecialIssueAndRole({
      role,
      specialIssueId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere(builder =>
        builder
          .whereRaw(`tm.alias->>'email' like ?`, [`${searchValue}%`])
          .orWhereRaw(
            `concat(lower(tm.alias->>'surname'), ' ',lower(tm.alias->>'givenNames')) like ?`,
            [`${searchValue.toLowerCase()}%`],
          )
          .orWhereRaw(
            `concat(lower(tm.alias->>'givenNames'), ' ',lower(tm.alias->>'surname')) like ?`,
            [`${searchValue.toLowerCase()}%`],
          ),
      )
  }

  // By Submission

  static findAllBySubmission({ submissionId, eagerLoadRelations }) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('manuscript AS m', 't.manuscript_id', 'm.id')
      .andWhere('m.submission_id', submissionId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findAllBySubmissionAndRole({
    role,
    submissionId,
    eagerLoadRelations,
  }) {
    return this.findAllBySubmission({
      submissionId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
  }

  static findAllBySubmissionAndRoleAndUser({
    role,
    userId,
    submissionId,
    eagerLoadRelations,
  }) {
    return this.findAllBySubmissionAndRole({
      role,
      submissionId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.userId', userId)
  }

  static findAllBySubmissionAndRoleAndStatuses({
    role,
    statuses,
    submissionId,
    eagerLoadRelations,
  }) {
    return this.findAllBySubmissionAndRole({
      role,
      submissionId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere(builder => builder.whereIn('tm.status', statuses))
  }

  // By Manuscript

  static findAllByManuscriptsAndRole({ manuscriptIds, role }) {
    return this.query()
      .select('tm.*', 'm.id as manuscriptId')
      .from('team_member as tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('manuscript AS m', 't.manuscript_id', 'm.id')
      .whereIn('m.id', manuscriptIds)
      .andWhere('t.role', role)
  }

  static findOneByManuscriptsAndRole({ manuscriptIds, role }) {
    return this.findAllByManuscriptsAndRole({ manuscriptIds, role })
      .clone()
      .limit(1)
  }

  static findAllByManuscriptsAndRoleAndStatus({ manuscriptIds, role, status }) {
    return this.findAllByManuscriptsAndRole({ manuscriptIds, role })
      .clone()
      .andWhere('tm.status', status)
  }

  static findOneByManuscriptsAndRoleAndStatus({ manuscriptIds, role, status }) {
    return this.findAllByManuscriptsAndRoleAndStatus({
      manuscriptIds,
      role,
      status,
    })
      .clone()
      .limit(1)
  }

  static findAllByManuscript({ manuscriptId, eagerLoadRelations }) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('manuscript AS m', 't.manuscript_id', 'm.id')
      .where('m.id', manuscriptId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findOneByManuscriptAndRole({
    role,
    manuscriptId,
    eagerLoadRelations,
  }) {
    return this.findAllByManuscript({
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
      .limit(1)
      .first()
  }

  static findOneByManuscriptAndUser({
    userId,
    manuscriptId,
    eagerLoadRelations,
  }) {
    return this.findAllByManuscript({ manuscriptId, eagerLoadRelations })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static findAllByManuscriptAndRole({
    role,
    manuscriptId,
    eagerLoadRelations,
  }) {
    return this.findAllByManuscript({
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
  }

  static findOneByManuscriptAndRoleAndStatus({
    role,
    status,
    manuscriptId,
    eagerLoadRelations,
  }) {
    return this.findAllByManuscriptAndRole({
      role,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.status', status)
      .limit(1)
      .first()
  }

  static findOneByManuscriptAndRoleAndStatuses({
    role,
    statuses,
    manuscriptId,
    eagerLoadRelations,
  }) {
    return this.findAllByManuscriptAndRole({
      role,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere(builder => builder.whereIn('tm.status', statuses))
      .limit(1)
      .first()
  }

  static findOneByManuscriptAndRoleAndUser({
    role,
    userId,
    manuscriptId,
    eagerLoadRelations,
  }) {
    return this.findAllByManuscriptAndRole({
      role,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static findAllByManuscriptAndRoleAndStatus({
    role,
    status,
    manuscriptId,
    eagerLoadRelations,
  }) {
    return this.findAllByManuscriptAndRole({
      role,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.status', status)
  }

  static findAllByManuscriptAndRoleAndExcludedStatuses({
    role,
    manuscriptId,
    excludedStatuses,
  }) {
    return this.findAllByManuscriptAndRole({
      role,
      manuscriptId,
      excludedStatuses,
    })
      .clone()
      .andWhere(builder => builder.whereNotIn('tm.status', excludedStatuses))
  }

  // By Role
  static findAllByRole(role) {
    return this.query()
      .select('tm.*')
      .from('team_member AS tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .andWhere('t.role', role)
  }

  static findAllByUserAndRole({ userId, role }) {
    return this.findAllByRole(role)
      .clone()
      .andWhere('tm.userId', userId)
  }

  setAlias(identity) {
    this.alias = pick(identity, [
      'aff',
      'title',
      'email',
      'surname',
      'country',
      'givenNames',
    ])
  }

  getName() {
    return `${get(this, 'alias.givenNames', '')} ${get(
      this,
      'alias.surname',
      '',
    )}`
  }

  getEmail() {
    return `${get(this, 'alias.email', '')}`
  }

  getLastName() {
    return `${get(this, 'alias.surname', '')}`
  }

  getInvitedDate({ Team, role }) {
    if (role === Team.Role.academicEditor)
      return this.invitedDate ? this.invitedDate : this.created

    return role === Team.Role.reviewer ? this.created : ''
  }

  getDeclinedDate({ Team, role }) {
    return [Team.Role.academicEditor, Team.Role.reviewer].includes(role) &&
      this.status === TeamMember.Statuses.declined
      ? this.responded
      : ''
  }
  getAcceptedDate({ Team, role }) {
    return [Team.Role.academicEditor, Team.Role.reviewer].includes(role) &&
      [
        TeamMember.Statuses.accepted,
        TeamMember.Statuses.submitted,
        TeamMember.Statuses.removed,
      ].includes(this.status)
      ? this.responded
      : ''
  }
  getExpiredDate({ Team, role }) {
    return [Team.Role.academicEditor, Team.Role.reviewer].includes(role) &&
      this.status === TeamMember.Statuses.expired
      ? this.updated
      : ''
  }
  getAssignedDate({ Team, role }) {
    return [Team.Role.triageEditor, Team.Role.editorialAssistant].includes(role)
      ? this.created
      : ''
  }
  getRemovedDate({ Team, role }) {
    return [
      Team.Role.triageEditor,
      Team.Role.academicEditor,
      Team.Role.editorialAssistant,
    ].includes(role) && this.status === TeamMember.Statuses.removed
      ? this.updated
      : ''
  }

  getDatesForEvents({ role, Team }) {
    const invitedDate = this.getInvitedDate({ Team, role })
    const declinedDate = this.getDeclinedDate({ Team, role })
    const acceptedDate = this.getAcceptedDate({ Team, role })
    const expiredDate = this.getExpiredDate({ Team, role })
    const assignedDate = this.getAssignedDate({ Team, role })
    const removedDate = this.getRemovedDate({ Team, role })

    if (role === Team.Role.reviewer)
      return { invitedDate, declinedDate, acceptedDate, expiredDate }
    if (
      [
        Team.Role.triageEditor,
        Team.Role.academicEditor,
        Team.Role.editorialAssistant,
      ].includes(role)
    )
      return {
        invitedDate,
        declinedDate,
        acceptedDate,
        expiredDate,
        assignedDate,
        removedDate,
      }
  }

  getEventData({ peerReviewModel, Team, role }) {
    const getLabel = ({ prm, role }) => {
      if (role === Team.Role.editorialAssistant) return 'Editorial Assistant'
      if (role === Team.Role.triageEditor) return prm.triageEditorLabel
      if (role === Team.Role.academicEditor) return prm.academicEditorLabel
      if (role === Team.Role.researchIntegrityPublishingEditor)
        return 'Research Integrity Publishing Editor'
    }
    const orcidIdentity = this.user.identities.find(
      identity => identity.type === 'orcid',
    )
    return {
      id: this.id,
      userId: this.userId,
      status: this.status,
      isCorresponding: this.isCorresponding,
      ...this.alias,
      role: {
        type: role,
        label: getLabel({ prm: peerReviewModel, role, Team }),
      },
      orcidId: orcidIdentity ? orcidIdentity.identifier : '',
      ...this.getDatesForEvents({
        role,
        Team,
      }),
    }
  }

  toDTO() {
    return {
      ...this,
      invited: this.created,
      user: this.user ? this.user.toDTO() : undefined,
      alias: this.alias
        ? {
            ...this.alias,
            name: {
              surname: this.alias.surname,
              givenNames: this.alias.givenNames,
              title: this.alias.title,
            },
          }
        : undefined,
      role: this.team ? this.team.role : undefined,
    }
  }
}

module.exports = TeamMember
