const HindawiBaseModel = require('../hindawiBaseModel')
const { raw } = require('objection')
const TeamMember = require('./teamMember')
const { transaction } = require('objection')
const logger = require('@pubsweet/logger')

class Team extends HindawiBaseModel {
  static get tableName() {
    return 'team'
  }

  static get schema() {
    return {
      properties: {
        role: {
          type: 'string',
          enum: Object.values(Team.Role),
          default: null,
        },
        manuscriptId: { type: ['string', 'null'] },
        journalId: { type: ['string', 'null'] },
        sectionId: { type: ['string', 'null'] },
        specialIssueId: { type: ['string', 'null'] },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'team.manuscriptId',
          to: 'manuscript.id',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'team.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./section'),
        join: {
          from: 'team.sectionId',
          to: 'section.id',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./specialIssue'),
        join: {
          from: 'team.specialIssueId',
          to: 'special_issue.id',
        },
      },
      members: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./teamMember'),
        join: {
          from: 'team.id',
          to: 'team_member.teamId',
        },
      },
    }
  }

  static async findAllBy({ role, manuscriptId, submissionId }) {
    try {
      const results = await this.query()
        .skipUndefined()
        .select('t.*')
        .from('team AS t')
        .leftJoin('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('t.role', role)
        .andWhere('m.id', manuscriptId)
        .andWhere('m.submissionId', submissionId)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByManuscriptAndRole({
    role,
    manuscriptId,
    eagerLoadRelations,
  }) {
    try {
      const result = await this.query()
        .select('t.*')
        .from('team as t')
        .join('manuscript as m', 'm.id', 't.manuscript_id')
        .where('m.id', manuscriptId)
        .andWhere('t.role', role)
        .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
        .first()

      return result
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByJournalAndRole({
    role,
    journalId,
    eagerLoadRelations,
  }) {
    try {
      const results = await this.query()
        .select('t.*')
        .from('team as t')
        .join('journal as j', 'j.id', 't.journal_id')
        .where('j.id', journalId)
        .andWhere('t.role', role)
        .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByJournal(journalId) {
    try {
      const results = await this.query()
        .select(
          't.*',
          raw('null as special_issue_name'),
          raw('null as section_name'),
        )
        .withGraphFetched('members')
        .from('team AS t')
        .where('t.journal_id', journalId)
        .union(function unionSections() {
          this.select(
            't.*',
            raw('null as special_issue_name'),
            's.name AS section_name',
          )
            .withGraphFetched('members')
            .from('team AS t')
            .leftJoin('section AS s', 's.id', 't.section_id')
            .where('s.journal_id', journalId)
        })
        .union(function unionSpecialIssues() {
          this.select(
            't.*',
            'si.name AS special_issue_name',
            raw('null as section_name'),
          )
            .withGraphFetched('members')
            .from('team AS t')
            .leftJoin('special_issue AS si', 'si.id', 't.special_issue_id')
            .where('si.journal_id', journalId)
        })
        .union(function unionSpecialIssuesSections() {
          this.select(
            't.*',
            'si.name AS special_issue_name',
            's.name AS section_name',
          )
            .withGraphFetched('members')
            .from('team AS t')
            .leftJoin('special_issue AS si', 'si.id', 't.special_issue_id')
            .leftJoin('section AS s', 's.id', 'si.section_id')
            .where('s.journal_id', journalId)
        })

      return results
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByUserAndJournalId({ userId, journalId }) {
    try {
      const results = await this.query()
        .select('t.*')
        .from('team AS t')
        .join('journal AS j', 't.journal_id', 'j.id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.user_id', userId)
        .andWhere('j.id', journalId)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByUserAndManuscriptId({ userId, manuscriptId }) {
    return this.query()
      .alias('t')
      .join('team_member AS tm', 't.id', 'tm.team_id')
      .join('manuscript AS m', 't.manuscript_id', 'm.id')
      .where('tm.user_id', userId)
      .andWhere('m.id', manuscriptId)
      .first()
  }

  static async findOneByManuscriptAndUserAndRole({
    role,
    userId,
    manuscriptId,
  }) {
    try {
      return await this.query()
        .select('t.*')
        .from('team AS t')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.user_id', userId)
        .andWhere('t.manuscript_id', manuscriptId)
        .andWhere('t.role', role)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllBySection({ sectionId }) {
    try {
      return await this.query()
        .select('t.*', 's.name AS sectionName')
        .from('team AS t')
        .join('section AS s', 't.section_id', 's.id')
        .where('s.id', sectionId)
        .withGraphFetched('members')
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllBySpecialIssue({ specialIssueId }) {
    try {
      return await this.query()
        .select('t.*', 'si.name AS specialIssueName')
        .from('team AS t')
        .join('special_issue AS si', 't.special_issue_id', 'si.id')
        .where('si.id', specialIssueId)
        .withGraphFetched('members')
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllBySubmissionAndRole({ submissionId, role }) {
    try {
      return await this.query()
        .from('team AS t')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('m.submission_id', submissionId)
        .andWhere('t.role', role)
    } catch (e) {
      throw new Error(e)
    }
  }

  static get Role() {
    return {
      author: 'author',
      admin: 'admin',
      triageEditor: 'triageEditor',
      reviewer: 'reviewer',
      academicEditor: 'academicEditor',
      editorialAssistant: 'editorialAssistant',
      submittingStaffMember: 'submittingStaffMember',
      researchIntegrityPublishingEditor: 'researchIntegrityPublishingEditor',
    }
  }

  static get JournalRoles() {
    return [
      this.Role.triageEditor,
      this.Role.academicEditor,
      this.Role.editorialAssistant,
      this.Role.researchIntegrityPublishingEditor,
    ]
  }

  static get StaffRoles() {
    return [
      this.Role.admin,
      this.Role.editorialAssistant,
      this.Role.researchIntegrityPublishingEditor,
    ]
  }

  addMember({ user, options }) {
    this.members = this.members || []
    const existingMember = this.members.find(
      member => member.userId === user.id,
    )
    const { defaultIdentity } = user

    if (existingMember) {
      if (existingMember.status === TeamMember.Statuses.removed) {
        throw new Error(
          `${this.role} invitation for ${defaultIdentity.email} was removed and can't be invited again`,
        )
      }
      if (existingMember.status === TeamMember.Statuses.expired) {
        existingMember.updateProperties({
          status: TeamMember.Statuses.pending,
        })
        return existingMember
      }
      throw new ValidationError(
        `User ${defaultIdentity.email} is already invited as ${this.role}`,
      )
    }

    const newMember = new TeamMember({
      ...options,
      userId: user.id,
      teamId: this.id,
      position: this.members.length,
    })
    newMember.setAlias(defaultIdentity)
    newMember.user = user

    this.members.push(newMember)

    return newMember
  }

  async findCorrespondingTeamMember(trx) {
    const teamMember = await this.$relatedQuery('members', trx)
      .where('is_corresponding', true)
      .first()

    return teamMember
  }

  static async updateAndSaveMany(teams, trx) {
    return this.query(trx).upsertGraph(teams, {
      insertMissing: true,
      relate: true,
    })
  }

  async changeCorrespondingTeamMember(newCorrespondingTeamMember) {
    try {
      return await transaction(Team.knex(), async trx => {
        const currentCorrespondingTeamMember = await this.findCorrespondingTeamMember(
          trx,
        )
        if (currentCorrespondingTeamMember) {
          await currentCorrespondingTeamMember.$query(trx).update({
            alias: currentCorrespondingTeamMember.alias,
            isCorresponding: false,
          })
        }

        const correspondingMember = await newCorrespondingTeamMember
          .$query(trx)
          .update({
            alias: newCorrespondingTeamMember.alias,
            isCorresponding: true,
          })

        return correspondingMember
      })
    } catch (err) {
      logger.error(err)
      throw new Error(
        'Something went wrong while trying to change the corresponding team member',
      )
    }
  }

  toDTO() {
    const object = this.manuscript || this.journal

    return {
      ...this,
      object: object ? object.toDTO() : undefined,
      objectType: this.manuscriptId ? 'manuscript' : 'journal',
      members: this.members ? this.members.map(m => m.toDTO()) : undefined,
    }
  }
}

module.exports = Team
