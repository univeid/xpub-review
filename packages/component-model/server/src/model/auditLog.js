const HindawiBaseModel = require('../hindawiBaseModel')
const { get } = require('lodash')

class AuditLog extends HindawiBaseModel {
  static get tableName() {
    return 'audit_log'
  }

  static get schema() {
    return {
      properties: {
        userId: { type: ['string', null], format: 'uuid' },
        manuscriptId: { type: ['string', null], format: 'uuid' },
        action: {
          type: 'string',
        },
        objectType: {
          type: 'string',
          enum: ['manuscript', 'user', 'review', 'file'],
        },
        objectId: { type: 'string', format: 'uuid' },
        journalId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./user'),
        join: {
          from: 'audit_log.userId',
          to: 'user.id',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'audit_log.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }
  static async findAllBySubmissionId(submissionId) {
    try {
      return await this.query()
        .select('au.*', 'm.version')
        .from('audit_log AS au')
        .join('manuscript AS m', 'au.manuscript_id', 'm.id')
        .where('m.submissionId', submissionId)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async getTarget({ TeamMember, log, Identity }) {
    let target
    target = await TeamMember.findOneByManuscriptAndUser({
      userId: log.objectId,
      manuscriptId: log.manuscriptId,
      eagerLoadRelations: 'team',
    })
    if (target) return target

    target = await Identity.findOneBy({
      queryObject: { userId: log.objectId },
    })
    return target
  }

  transformLog({ userMember, target, activityLogEvents }) {
    return {
      id: this.id,
      created: this.created,
      version: this.version,
      action: get(activityLogEvents, this.action),
      user: {
        role: get(userMember, 'role'),
        email: get(userMember, 'alias.email'),
        reviewerNumber: get(userMember, 'reviewerNumber'),
      },
      target:
        target && target.team && target.team.role
          ? {
              role: target.team.role,
              email: target.getEmail(),
              reviewerNumber: target.reviewerNumber,
            }
          : {
              email: get(target, 'email'),
            },
    }
  }

  toDTO() {
    return {
      ...this,
      user: this.user ? this.user.toDTO() : undefined,
    }
  }
}

module.exports = AuditLog
