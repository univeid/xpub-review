import PeerReviewModel from '../model/peerReviewModel'
import JournalArticleType from '../model/journalArticleType'

const { groupResultsByKey } = require('component-dataloader-tools')

const groupResults = ({ keys, results }) =>
  groupResultsByKey({
    keys,
    results,
    getKey: property => property.journalId,
  })

const peerReviewModelLoader = async journalIds => {
  const prms = await PeerReviewModel.findAllByJournals(journalIds)
  const prmsByJournals = groupResults({ keys: journalIds, results: prms })
  return Promise.all(prmsByJournals)
}
const journalArticleTypeLoader = async journalIds => {
  const articleTypes = await JournalArticleType.findAllByJournals(journalIds)
  const articleTypesByJournal = groupResults({
    keys: journalIds,
    results: articleTypes,
  })
  return Promise.all(articleTypesByJournal)
}

module.exports = {
  peerReviewModelLoader,
  journalArticleTypeLoader,
}
