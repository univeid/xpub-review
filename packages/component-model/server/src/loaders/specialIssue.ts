import PeerReviewModel from '../model/peerReviewModel'
import TeamMember from '../model/teamMember'

const { groupResultsByKey } = require('component-dataloader-tools')

const groupResults = ({ keys, results }) =>
  groupResultsByKey({
    keys,
    results,
    getKey: property => property.specialIssueId,
  })

const peerReviewModelLoader = async siIds => {
  const prms = await PeerReviewModel.findAllBySIs(siIds)
  const prmsBySI = groupResults({ keys: siIds, results: prms })
  return Promise.all(prmsBySI)
}

module.exports = {
  peerReviewModelLoader,
}
