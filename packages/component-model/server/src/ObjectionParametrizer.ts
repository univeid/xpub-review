import { FindQueryBuilder } from 'objection-find'
import { Model } from 'objection' // this might be replaced in the future by our own ts model

enum RelationParametersMappings {
  academicEditorId = 'academicEditor.userId',
  triageEditorId = 'triageEditor.userId',
  reviewerId = 'reviewer.userId',
  authorId = 'author.userId',
  editorialAssistantId = 'editorialAssistant.userId',
  editorialAssistantStatus = 'editorialAssistant.status',
  journalId = 'journal.id',
  sectionId = 'section.id',
  specialIssueId = 'specialIssue.id',
  authorEmail = 'authors.email',
  academicEditorEmail = 'academicEditors.email',
  academicEditorStatus = 'academicEditors.status',
  triageEditorEmail = 'triageEditor.email',
  title = 'title:likeLower',
  name = 'name:likeLower',
  status = 'status:in',
}

type ObjectionInput<I> = I & FindQueryBuilder<Model>

export class ObjectionParametrizer<I> {
  private params = {}
  constructor(input) {
    this.mapParams(input)
  }

  private mapParams(input: Partial<ObjectionInput<I>>) {
    const regex = RegExp(':like')

    const params = Object.keys(input).reduce((parameters, currentParameter) => {
      if (Object.keys(RelationParametersMappings).includes(currentParameter)) {
        const val = regex.test(RelationParametersMappings[currentParameter])
          ? `%${input[currentParameter]}%`
          : input[currentParameter]
        parameters[RelationParametersMappings[currentParameter]] = val
      } else {
        parameters[currentParameter] = input[currentParameter]
      }
      return parameters
    }, {})

    this.params = params
  }

  public getParams() {
    return this.params
  }
}
