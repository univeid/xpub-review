const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  Query: {
    async manuscript(_, { manuscriptId }, ctx) {
      return useCases.manuscriptUseCase
        .initialize(models)
        .execute({ manuscriptId, userId: ctx.user })
    },
    async getSubmission(_, { submissionId }, ctx) {
      return useCases.getSubmissionUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
    async getDraftRevision(_, { submissionId }, ctx) {
      return useCases.getDraftRevisionUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
  },
  Manuscript: {
    async reviews(manuscript, query, ctx) {
      return useCases.getManuscriptReviewsUseCase
        .initialize(models)
        .execute({ manuscript, userId: ctx.user })
    },
    async files(manuscript, query, ctx) {
      return useCases.getManuscriptFilesUseCase
        .initialize(models)
        .execute({ manuscript, userId: ctx.user })
    },
    async journal(manuscript, query, ctx) {
      return useCases.getManuscriptJournalUseCase
        .initialize({ loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async articleType(manuscript, query, ctx) {
      return useCases.getManuscriptArticleTypeUseCase
        .initialize({ loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async peerReviewModel(manuscript, query, ctx) {
      return useCases.getManuscriptPeerReviewModelUseCase
        .initialize({ loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async section(manuscript, query, ctx) {
      return useCases.getManuscriptSectionUseCase
        .initialize(models)
        .execute({ manuscript, userId: ctx.user })
    },
    async specialIssue(manuscript, query, ctx) {
      return useCases.getManuscriptSpecialIssueUseCase
        .initialize({ loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async isApprovalEditor(manuscript, query, ctx) {
      return useCases.getManuscriptIsApprovalEditorUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id, userId: ctx.user })
    },
    async authors(manuscript, query, ctx) {
      return useCases.getManuscriptAuthorsUseCase
        .initialize({ models, loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async reviewers(manuscript, query, ctx) {
      return useCases.getManuscriptReviewersUseCase
        .initialize({ models, loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async triageEditor(manuscript, query, ctx) {
      return useCases.getManuscriptTriageEditorUseCase
        .initialize({ models, loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async academicEditor(manuscript, query, ctx) {
      return useCases.getManuscriptAcademicEditorUseCase
        .initialize({ models, loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async pendingAcademicEditor(manuscript, query, ctx) {
      return useCases.getManuscriptPendingAcademicEditorUseCase
        .initialize({ models, loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async researchIntegrityPublishingEditor(manuscript, query, ctx) {
      return useCases.getManuscriptRIPEUseCase
        .initialize({ models, loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async statusColor(manuscript) {
      return useCases.getManuscriptStatusColorUseCase
        .initialize(models)
        .execute({ manuscript })
    },
    async hasSpecialIssueEditorialConflictOfInterest(manuscript, _, ctx) {
      return useCases.getManuscriptHasSpecialIssueEditorialConflictOfInterestUseCase
        .initialize({ models, loaders: ctx.loaders })
        .execute({ manuscript })
    },
    async editorDecisions(manuscript, query, ctx) {
      return useCases.getManuscriptEditorDecisionsUseCase
        .initialize({ models, useCases, userId: ctx.user })
        .execute({ manuscript })
    },
    isEditable(manuscript) {
      // we need to edit some details up until a certain status of the manuscript
      // the logic for this property should is dependent on the status
      // this is why we need to keep it here, so we don't have to keep the statuses list in sync server/client
      return useCases.getManuscriptIsEditableUseCase
        .initialize(models)
        .execute({ manuscript })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
