const uuid = require("uuid");

interface ExecuteProps {
  copyFromManuscriptId: string;
  copyToManuscriptId: string;
  trx?: any; // whatever shape comes from objection
}

const initialize = ({ models: { File }, services: { s3service } }) => ({
  execute: async function copyBetweenManuscripts({
    copyFromManuscriptId,
    copyToManuscriptId,
    trx = null,
  }: ExecuteProps) {
    const files = await File.findBy({ manuscriptId: copyFromManuscriptId });

    const newFiles = [];

    for (const file of files) {
      const newKey = `${copyToManuscriptId}/${uuid.v4()}`;
      await s3service.copyObject({ prevKey: file.providerKey, newKey });

      const newFile = new File({
        ...file,
        providerKey: newKey,
        manuscriptId: copyToManuscriptId,
      });

      delete newFile.id;
      const newSavedFile = await newFile.save(trx);
      newFiles.push(newSavedFile);
    }

    return newFiles;
  },
});

export const copyFilesBetweenManuscripts = {
  initialize,
};
