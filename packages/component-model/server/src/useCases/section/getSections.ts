import { ObjectionParametrizer } from '../../ObjectionParametrizer'

interface GetSectionsInput {
  name: string
  journalId: string
}

const initialize = ({ Section }) => ({
  async execute({ input }: { input: GetSectionsInput }) {
    const parameters = new ObjectionParametrizer<GetSectionsInput>(
      input,
    ).getParams()
    const sections = await Section.findAllV2(parameters)
    return sections
  },
})

export default { initialize }
