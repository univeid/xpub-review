export const initialize = ({ models, loaders }) => ({
  execute: async ({ manuscript }) => {
    if (manuscript.academicEditor) return manuscript.academicEditor
    const { Team } = models

    const hideEmailIfNotAdminOrEa = aeDTO => {
      const { role } = manuscript
      const eligibleRoles = [
        Team.Role.admin,
        Team.Role.editorialAssistant,
        Team.Role.researchIntegrityPublishingEditor,
      ]
      const canSeeEmail = !!role && eligibleRoles.includes(role)
      if (aeDTO.alias && aeDTO.alias.email && !canSeeEmail) {
        delete aeDTO.alias.email
      }
      return aeDTO
    }

    const academicEditors = await loaders.Manuscript.academicEditorsLoader.load(
      manuscript.id,
    )

    return academicEditors?.map(ae => hideEmailIfNotAdminOrEa(ae)).shift()
  },
})
