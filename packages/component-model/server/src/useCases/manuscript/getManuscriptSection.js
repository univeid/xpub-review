const initialize = models => ({
  execute: async ({ manuscript }) => {
    const { Section } = models
    if (manuscript.section) return manuscript.section

    if (manuscript.sectionId) {
      const section = await Section.find(manuscript.sectionId)
      return section
    }
  },
})

module.exports = {
  initialize,
}
