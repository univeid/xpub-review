import { ObjectionParametrizer } from '../../ObjectionParametrizer'

interface GetManuscriptsInput {
  id: string
  academicEditorId: string
  triageEditorId: string
  reviewerId: string
  authorId: string
  editorialAssistantId: string
  editorialAssistantStatus: string
  journalId: string
  sectionId: string
  specialIssueId: string
  authorEmail: string
  academicEditorEmail: string
  academicEditorStatus: string
  triageEditorEmail: string
  isLatestVersion: Boolean
  customId: String
  title: String
  status: String
}

export const initialize = ({ Manuscript }) => ({
  async execute({ input }: { input: GetManuscriptsInput }) {
    const parameters = new ObjectionParametrizer<GetManuscriptsInput>(
      input,
    ).getParams()
    const manuscripts = await Manuscript.findAllV2(parameters)
    return manuscripts
  },
})

// the use-cases in the model component should not be accessed directly
// move the authsomepolicy to the useCase that is using this usecase
// const authsomePolicies = ['refererIsSameAsOwner']
