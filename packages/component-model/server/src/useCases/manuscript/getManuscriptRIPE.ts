export const initialize = ({ loaders }) => ({
  execute: async ({ manuscript }) => {
    if (manuscript.researchIntegrityPublishingEditor)
      return manuscript.researchIntegrityPublishingEditor
    const researchIntegrityPublishingEditors = await loaders.Manuscript.researchIntegrityPublishingEditorsLoader.load(
      manuscript.id,
    )

    return researchIntegrityPublishingEditors?.shift()
  },
})
