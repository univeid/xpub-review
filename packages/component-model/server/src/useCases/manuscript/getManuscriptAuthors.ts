export const initialize = ({ loaders }) => ({
  execute: async ({ manuscript }) => {
    if (manuscript.authors && manuscript.authors.length > 0)
      return manuscript.authors
    const authors = await loaders.Manuscript.authorsLoader.load(manuscript.id)
    return authors
  },
})
