const manuscriptUseCase = require('./manuscript')
const getDraftRevisionUseCase = require('./getDraftRevision')
const getManuscriptRIPEUseCase = require('./getManuscriptRIPE')
const getManuscriptFilesUseCase = require('./getManuscriptFiles')
const getManuscriptSectionUseCase = require('./getManuscriptSection')
const getManuscriptAuthorsUseCase = require('./getManuscriptAuthors')
const getManuscriptReviewsUseCase = require('./getManuscriptReviews')
const getManuscriptReviewersUseCase = require('./getManuscriptReviewers')
const getManuscriptStatusColorUseCase = require('./getManuscriptStatusColor')
const getManuscriptSpecialIssueUseCase = require('./getManuscriptSpecialIssue')
const getManuscriptTriageEditorUseCase = require('./getManuscriptTriageEditor')
const getManuscriptAcademicEditorUseCase = require('./getManuscriptAcademicEditor')
const getManuscriptEditorDecisionsUseCase = require('./getManuscriptEditorDecisions')
const getManuscriptIsApprovalEditorUseCase = require('./getManuscriptIsApprovalEditor')
const redistributeEditorialAssistantsUseCases = require('./redistributeEditorialAssistants')
const getManuscriptPendingAcademicEditorUseCase = require('./getManuscriptPendingAcademicEditor')
const getManuscriptHasSpecialIssueEditorialConflictOfInterestUseCase = require('./getManuscriptHasSpecialIssueEditorialConflictOfInterest')
const getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase = require('./getManuscriptEditorDecisions/triageEditorIsApprovalEditor')
const getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase = require('./getManuscriptEditorDecisions/academicEditorIsApprovalEditor')
const getManuscriptIsEditableUseCase = require('./getManuscriptIsEditable')
const createVersionsUseCases = require('./createVersion')
const getManuscriptsUseCase = require('./getManuscripts')
const getManuscriptJournalUseCase = require('./getManuscriptJournal')
const getManuscriptArticleTypeUseCase = require('./getManuscriptArticleType')
const getManuscriptPeerReviewModelUseCase = require('./getManuscriptPeerReviewModel')

module.exports = {
  manuscriptUseCase,
  getDraftRevisionUseCase,
  getManuscriptRIPEUseCase,
  getManuscriptFilesUseCase,
  getManuscriptReviewsUseCase,
  getManuscriptSectionUseCase,
  getManuscriptAuthorsUseCase,
  getManuscriptReviewersUseCase,
  getManuscriptStatusColorUseCase,
  getManuscriptSpecialIssueUseCase,
  getManuscriptJournalUseCase,
  getManuscriptArticleTypeUseCase,
  getManuscriptTriageEditorUseCase,
  getManuscriptAcademicEditorUseCase,
  getManuscriptEditorDecisionsUseCase,
  getManuscriptIsApprovalEditorUseCase,
  getManuscriptPendingAcademicEditorUseCase,
  ...redistributeEditorialAssistantsUseCases,
  getManuscriptHasSpecialIssueEditorialConflictOfInterestUseCase,
  getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase,
  getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase,
  getManuscriptIsEditableUseCase,
  ...createVersionsUseCases,
  getManuscriptsUseCase,
  getManuscriptPeerReviewModelUseCase,
}
