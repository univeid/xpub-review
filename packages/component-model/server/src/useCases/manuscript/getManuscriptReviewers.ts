export const initialize = ({ models, loaders }) => ({
  execute: async ({ manuscript }) => {
    const { Team } = models
    if (manuscript.reviewers && manuscript.reviewers.length > 0)
      return manuscript.reviewers

    const hideEmailIfNotAdminOrEa = reDTO => {
      const { role } = manuscript
      const eligibleRoles = [
        Team.Role.admin,
        Team.Role.editorialAssistant,
        Team.Role.researchIntegrityPublishingEditor,
      ]
      const canSeeEmail = !!role && eligibleRoles.includes(role)
      if (reDTO.alias && reDTO.alias.email && !canSeeEmail) {
        delete reDTO.alias.email
      }
      return reDTO
    }

    const reviewers = await loaders.Manuscript.reviewersLoader.load(
      manuscript.id,
    )

    return reviewers?.map(reviewer => hideEmailIfNotAdminOrEa(reviewer))
  },
})
