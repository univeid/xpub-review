export const initialize = ({ loaders }) => ({
  execute: async ({ manuscript }) => {
    if (manuscript.journal) return manuscript.journal
    const journal = await loaders.Manuscript.journalLoader.load(manuscript.id)
    return journal?.shift()
  },
})
