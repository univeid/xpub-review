export const initialize = ({ models, loaders }) => ({
  execute: async ({ manuscript }) => {
    if (manuscript.triageEditor) return manuscript.triageEditor
    const { Team } = models

    const hideEmailIfNotAdminOrEa = teDTO => {
      const { role } = manuscript
      const eligibleRoles = [
        Team.Role.admin,
        Team.Role.editorialAssistant,
        Team.Role.researchIntegrityPublishingEditor,
      ]
      const canSeeEmail = !!role && eligibleRoles.includes(role)
      if (teDTO.alias && teDTO.alias.email && !canSeeEmail) {
        delete teDTO.alias.email
      }
      return teDTO
    }

    const triageEditors = await loaders.Manuscript.triageEditorsLoader.load(
      manuscript.id,
    )

    return triageEditors?.map(te => hideEmailIfNotAdminOrEa(te)).shift()
  },
})
