const initialize = ({ userId, models: { Team, TeamMember, Review } }) => ({
  execute: async ({ manuscript }) => {
    const { reject, publish, revision } = Review.Recommendations

    const requestingUserIsAdminOrEA = await TeamMember.findOneByUserAndRoles({
      userId,
      roles: [Team.Role.admin, Team.Role.editorialAssistant],
    })

    const submissionReviewerReports = await Review.findAllValidAndSubmitedBySubmissionAndRole(
      { submissionId: manuscript.submissionId, role: Team.Role.reviewer },
    )
    // As an approval editor, i can publish, reject or ask for a revision if I
    // have a reviewer report on any previous version
    if (submissionReviewerReports.length) {
      return [publish, revision, reject]
    }

    if (requestingUserIsAdminOrEA) {
      const pendingOrAcceptedAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatuses(
        {
          role: Team.Role.academicEditor,
          statuses: [TeamMember.Statuses.pending, TeamMember.Statuses.accepted],
          manuscriptId: manuscript.id,
        },
      )
      // As an EA or Admin I should only be able to reject the
      // manuscript if the academic editor is pending/accepted and I have no
      // previous editor recommendations or reviewer reports
      if (pendingOrAcceptedAcademicEditor) return [reject]
    }

    return [revision, reject]
  },
})

module.exports = {
  initialize,
}
