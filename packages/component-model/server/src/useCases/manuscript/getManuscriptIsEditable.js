const initialize = models => ({
  execute: async ({ manuscript }) => {
    const { Manuscript } = models
    return [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.technicalChecks,
    ].includes(manuscript.status)
  },
})

module.exports = {
  initialize,
}
