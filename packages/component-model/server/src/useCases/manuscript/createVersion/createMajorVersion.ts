import { createVersion } from "./createVersion";

const initialize = args => ({
  execute: async function({ manuscript }): Promise<void> {
    const version = (+manuscript.version + 1).toString();

    await createVersion.initialize(args).execute({
      manuscript,
      version,
      isMajorVersion: true,
    });
  },
});

export const createMajorVersion = {
  initialize,
};
