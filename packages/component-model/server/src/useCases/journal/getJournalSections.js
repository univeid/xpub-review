const initialize = () => ({
  execute: ({ journal }) => {
    if (!journal.sections.length) return []

    return journal.sections.map(section => {
      if (section.specialIssues.length === 0) return section.toDTO()

      return section.toDTO()
    })
  },
})

module.exports = {
  initialize,
}
