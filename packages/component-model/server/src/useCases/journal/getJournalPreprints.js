const initialize = () => ({
  execute: ({ journal }) =>
    journal.preprints && journal.preprints.length ? journal.preprints : [],
})

module.exports = {
  initialize,
}
