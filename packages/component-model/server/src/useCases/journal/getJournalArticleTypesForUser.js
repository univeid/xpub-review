const isJournalRole = ({ Team, role }) =>
  role && Team.JournalRoles.includes(role)

const isTypeWithRIPE = ({ ArticleType, articleType }) =>
  ArticleType.TypesWithRIPE.includes(articleType.name)

const isEditorialType = ({ ArticleType, articleType }) =>
  ArticleType.EditorialTypes.includes(articleType.name)

const initialize = ({ ArticleType, Team }) => ({
  execute: async ({ loaders, journalId, userId }) => {
    const articleTypes = await loaders.Journal.journalArticleTypeLoader.load(
      journalId,
    )
    const role = await loaders.TeamMember.journalRolesLoader.load({
      userId,
      journalId,
    })

    return articleTypes.filter(
      articleType =>
        (isJournalRole({ Team, role }) ||
          !isEditorialType({ ArticleType, articleType })) &&
        !isTypeWithRIPE({ ArticleType, articleType }),
    )
  },
})

module.exports = {
  initialize,
}
