import { ObjectionParametrizer } from '../../ObjectionParametrizer'

interface GetSpecialIssuesInput {
  name: string
  journalId: string
}

const initialize = ({ SpecialIssue }) => ({
  async execute({ input }: { input: GetSpecialIssuesInput }) {
    const parameters = new ObjectionParametrizer<GetSpecialIssuesInput>(
      input,
    ).getParams()
    const specialIssues = await SpecialIssue.findAllV2(parameters)
    return specialIssues
  },
})

export default { initialize }
