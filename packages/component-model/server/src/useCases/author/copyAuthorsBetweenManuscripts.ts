interface ExecuteProps {
  copyFromManuscriptId: string;
  copyToManuscriptId: string;
  trx?: any; // whatever shape comes from objection
}

export const initialize = ({ models: { Team, TeamMember } }) => ({
  execute: async function({
    copyFromManuscriptId,
    copyToManuscriptId,
    trx = null,
  }: ExecuteProps) {
    const newAuthorTeam = new Team({
      role: Team.Role.author,
      manuscriptId: copyToManuscriptId,
    });

    await newAuthorTeam.save(trx);

    const existingAuthors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: copyFromManuscriptId,
      role: Team.Role.author,
    });

    const newAuthors = [];

    for (const existingAuthor of existingAuthors) {
      const newAuthor = new TeamMember({
        ...existingAuthor,
        teamId: newAuthorTeam.id,
      });

      delete newAuthor.id;
      const savedNewAuthor = await newAuthor.save(trx);
      newAuthors.push(savedNewAuthor);
    }

    return newAuthors;
  },
});

export const copyAuthorsBetweenManuscripts = {
  initialize,
};
