const getTeamMemberUserUseCase = require('./getTeamMemberUser')
const getTeamMemberWorkloadUseCase = require('./getTeamMemberWorkload')
const getSuggestedAcademicEditorUseCase = require('./getSuggestedAcademicEditor')
const getAcademicEditorWorkloadUseCase = require('./getAcademicEditorWorkloadUseCase')
const getReviewerStatusLabelUseCase = require('./getReviewerStatusLabel')

module.exports = {
  getTeamMemberUserUseCase,
  getTeamMemberWorkloadUseCase,
  getAcademicEditorWorkloadUseCase,
  getSuggestedAcademicEditorUseCase,
  getReviewerStatusLabelUseCase,
}
