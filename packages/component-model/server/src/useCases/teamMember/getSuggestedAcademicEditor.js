const { isEmpty, chain } = require('lodash')

function initialize({
  models: { TeamMember, EditorSuggestion, Manuscript, Team },
}) {
  return {
    execute,
  }

  async function execute({
    manuscriptId,
    journalId,
    sectionId,
    specialIssueId,
  }) {
    const role = Team.Role.academicEditor
    const teamMemberStatuses = [
      TeamMember.Statuses.pending,
      TeamMember.Statuses.accepted,
    ]
    const manuscriptStatuses = Manuscript.InProgressStatuses

    let teamMembers
    let manuscriptTeamMembersAndWorkload

    if (specialIssueId) {
      teamMembers = await TeamMember.findAllBySpecialIssueAndRole({
        role,
        specialIssueId,
      })
      manuscriptTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkloadBySpecialIssue(
        {
          role,
          specialIssueId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )
    }

    if (isEmpty(teamMembers) && sectionId) {
      teamMembers = await TeamMember.findAllBySectionAndRole({
        sectionId,
        role,
      })
      manuscriptTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkloadBySection(
        {
          role,
          sectionId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )
    }

    if (isEmpty(teamMembers) && journalId) {
      teamMembers = await TeamMember.findAllByJournalAndRole({
        journalId,
        role,
      })
      manuscriptTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkloadByJournal(
        {
          role,
          journalId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )
    }

    if (isEmpty(teamMembers)) {
      throw new Error('No manuscript parent ID has been provided')
    }

    const editorSuggestions = await EditorSuggestion.findBy({
      manuscriptId,
    })

    const manuscriptAuthors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
    })

    const manuscriptAcademicEditors = await TeamMember.findAllByManuscriptAndRole(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
      },
    )

    const users = teamMembers
      .filter(teamMember => {
        const memberIsAlreadyInvited = manuscriptAcademicEditors.find(
          academicEditor => academicEditor.userId === teamMember.userId,
        )
        const memberIsAuthor = manuscriptAuthors.find(
          author => author.userId === teamMember.userId,
        )
        return !memberIsAlreadyInvited && !memberIsAuthor
      })
      .map(teamMember => {
        const editorSuggestion = editorSuggestions.find(
          editorSubmissionScore =>
            editorSubmissionScore.teamMemberId === teamMember.id,
        )

        const editorWorkload = manuscriptTeamMembersAndWorkload.find(
          editorWithWorkload => editorWithWorkload.userId === teamMember.userId,
        )

        return {
          ...teamMember,
          score: editorSuggestion ? editorSuggestion.score : 0,
          workload: editorWorkload ? editorWorkload.workload : 0,
        }
      })

    return getUserByScoreAndWorkload(users)
  }

  function getUserByScoreAndWorkload(users) {
    const MAX_WORKLOAD_ALLOWED = 4

    let userByScoreAndWorkload = chain(users)
      .orderBy(['score', 'workload'], ['desc', 'asc'])
      .find(e => e.workload < MAX_WORKLOAD_ALLOWED)
      .value()

    if (!userByScoreAndWorkload) {
      userByScoreAndWorkload = chain(users)
        .orderBy(['workload', 'score'], ['asc', 'desc'])
        .head()
        .value()
    }
    return userByScoreAndWorkload
  }
}
module.exports = {
  initialize,
}
