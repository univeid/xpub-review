const userUseCases = require('./user')
const journalUseCases = require('./journal')
const manuscriptUseCases = require('./manuscript')
const teamMemberUseCases = require('./teamMember')
const submissionUseCases = require('./submission')
const specialIssueUseCases = require('./specialIssue')
const academicEditorUseCases = require('./academicEditor')
const authorUseCases = require('./author')
const editorialAssistantUseCases = require('./editorialAssistant')
const fileUseCases = require('./file')
const triageEditorUseCases = require('./triageEditor')
const sectionUseCases = require('./section')

module.exports = {
  ...userUseCases,
  ...journalUseCases,
  ...manuscriptUseCases,
  ...teamMemberUseCases,
  ...submissionUseCases,
  ...specialIssueUseCases,
  ...academicEditorUseCases,
  ...authorUseCases,
  ...editorialAssistantUseCases,
  ...fileUseCases,
  ...triageEditorUseCases,
  ...sectionUseCases,
}
