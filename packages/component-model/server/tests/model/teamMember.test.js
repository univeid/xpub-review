const TeamMember = require('../../dist/model/teamMember')
const EditorSuggestion = require('../../dist/model/editorSuggestion')

describe('Team Member model', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('orderAcademicEditorsByScore', () => {
    it('should order academic editors by their score', async () => {
      const editorSuggestions = [
        { teamMemberId: 'team-member-id-1', score: 0.43 },
        { teamMemberId: 'team-member-id-2', score: 0.88 },
        { teamMemberId: 'team-member-id-3', score: 0.25 },
      ]

      jest
        .spyOn(EditorSuggestion, 'findBy')
        .mockResolvedValueOnce(editorSuggestions)

      const orderedAcademicEditors = await TeamMember.orderAcademicEditorsByScore(
        {
          academicEditors: [
            { id: 'team-member-id-1' },
            { id: 'team-member-id-3' },
            { id: 'team-member-id-2' },
          ],
        },
      )

      expect(orderedAcademicEditors).toEqual([
        { id: 'team-member-id-2' },
        { id: 'team-member-id-1' },
        { id: 'team-member-id-3' },
      ])
    })
  })
})
