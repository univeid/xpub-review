const {
  getTeamRoles,
  getRecommendations,
  getTeamMemberStatuses,
} = require('component-generators')

const {
  getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase,
} = require('../../dist/useCases/manuscript')

const models = {
  Review: {
    Recommendations: getRecommendations(),
    findAllValidByManuscriptAndRole: jest.fn(),
    findAllValidAndSubmitedBySubmissionAndRole: jest.fn(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
    findOneByUserAndRoles: jest.fn(),
    findOneByManuscriptAndRoleAndStatuses: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
}

const { Review, TeamMember } = models

describe('Get manuscript editor decisions when academic editor is approval editor use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('should return publish, reject and revision when reviewer reports exist', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(Review, 'findAllValidAndSubmitedBySubmissionAndRole')
      .mockResolvedValue([{ id: 'review-id' }])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['publish', 'revision', 'reject'])
  })
  it('should return reject when no reports exist, AE is pending or accepted, and the user is Admin or EA', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(Review, 'findAllValidAndSubmitedBySubmissionAndRole')
      .mockResolvedValue([])
    jest.spyOn(TeamMember, 'findOneByUserAndRoles').mockResolvedValue({})
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({})
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['reject'])
  })
  it('should return revision and reject when no reports exist, AE is neither pending nor accepted, and the user is Admin or EA', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(Review, 'findAllValidAndSubmitedBySubmissionAndRole')
      .mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({})
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue()
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['revision', 'reject'])
  })
  it('should return revision and reject when no reports exist and the user is neither Admin or EA', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(Review, 'findAllValidAndSubmitedBySubmissionAndRole')
      .mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue()
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['revision', 'reject'])
  })
})
