import { copyTEsBetweenManuscripts } from "../../src/useCases/triageEditor/copyTEsBetweenManuscripts";
import { generateTeamMember } from "component-generators";

const copyFromManuscriptId = "manuscript1";
const copyToManuscriptId = "manuscript2";
const newTeamId = "team2";

const mockedTeamMember = generateTeamMember({
  id: "tm1",
  teamId: "team1",
});

class mockTeamModel {
  constructor(args) {
    return {
      id: newTeamId,
      save() {
        return this;
      },
      ...args,
    };
  }
  static Role = {
    editorialAssistant: "editorialAssistant",
  };
}

class mockTeamMemberModel {
  constructor(args) {
    return {
      ...args,
      save() {
        return this;
      },
    };
  }
  static Statuses = {
    active: "active",
  };
  static findOneByManuscriptAndRoleAndStatus() {
    return mockedTeamMember;
  }
}

const mockModels = {
  Team: mockTeamModel,
  TeamMember: mockTeamMemberModel,
};

let newEA;

describe("copy triage editor", () => {
  beforeAll(async () => {
    newEA = await copyTEsBetweenManuscripts
      .initialize({
        models: mockModels,
      })
      .execute({
        copyFromManuscriptId,
        copyToManuscriptId,
      });
  });
  it("should have copied the existing triage editor", () => {
    expect(newEA).not.toBeFalsy;
    expect(newEA.hasOwnProperty("userId")).toBe(true);
  });
  it("should have createad a new te team and add it to the new team members", () => {
    expect(newEA.hasOwnProperty("teamId")).toBe(true);
    expect(newEA.teamId).toEqual(newTeamId);
  });
});
