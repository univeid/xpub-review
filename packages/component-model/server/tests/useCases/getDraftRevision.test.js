const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Journal, Team, Manuscript, Review } = models

const chance = new Chance()

const { getDraftRevisionUseCase } = require('../../dist/useCases')

describe('Get Draft Version Use Case', () => {
  it('returns a draft version if present', async () => {
    const journal = fixtures.generateJournal({ Journal })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })
    const submissionId = chance.guid()
    const firstVersion = fixtures.generateManuscript({
      properties: {
        version: '1',
        submissionId,
        journal: journal.id,
        status: Manuscript.Statuses.revisionRequested,
      },
      Manuscript,
    })
    const secondVersion = fixtures.generateManuscript({
      properties: {
        version: '2',
        submissionId,
        journal: journal.id,
        status: Manuscript.Statuses.draft,
      },
      Manuscript,
    })

    const author = await dataService.createUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.author,
      manuscript: firstVersion,
      input: { isSubmitting: true, isCorresponding: false },
    })

    const review = await dataService.createReviewOnManuscript({
      models,
      fixtures,
      teamMember: author,
      manuscript: secondVersion,
      recommendation: Review.Recommendations.responseToRevision,
    })

    const authorUser = await fixtures.users.find(
      user => user.id === author.userId,
    )

    await dataService.addUserOnManuscript({
      models,
      fixtures,
      user: authorUser,
      role: Team.Role.author,
      manuscript: secondVersion,
      input: { isSubmitting: true, isCorresponding: false },
    })

    const draftRevision = await getDraftRevisionUseCase
      .initialize(models)
      .execute({ submissionId, userId: author.userId })

    expect(draftRevision.status).toEqual(Manuscript.Statuses.draft)
    expect(draftRevision.version).toEqual('2')
    expect(draftRevision.submissionId).toEqual(submissionId)
    expect(draftRevision.comment.reviewId).toEqual(review.id)
  })
})
