const {
  getTeamRoles,
  getRecommendations,
  getTeamMemberStatuses,
  getManuscriptStatuses,
} = require('component-generators')

const {
  getManuscriptEditorDecisionsUseCase,
} = require('../../dist/useCases/manuscript')

const models = {
  Manuscript: {
    Statuses: getManuscriptStatuses(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
  },
  Review: {
    Recommendations: getRecommendations(),
  },
  PeerReviewModel: {
    findOneByManuscriptParent: jest.fn(),
  },
}

describe('Get manuscript editor decisions use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('should return an empty array for drafts', async () => {
    const manuscript = { status: models.Manuscript.Statuses.draft }

    const decisions = await getManuscriptEditorDecisionsUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual([])
  })
  it('should return publish and reject for article types without peer review', async () => {
    const manuscript = { articleType: { hasPeerReview: false } }

    const decisions = await getManuscriptEditorDecisionsUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['publish', 'reject'])
  })
  it('should return publish, revision and reject for manuscripts returned because of a coi between the author and the triage editor', async () => {
    const manuscript = { hasTriageEditorConflictOfInterest: true }

    const decisions = await getManuscriptEditorDecisionsUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['publish', 'revision', 'reject'])
  })
})
