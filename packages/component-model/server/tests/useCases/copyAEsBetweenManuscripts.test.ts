import { copyAEsBetweenManuscripts } from "../../src/useCases/academicEditor/copyAEsBetweenManuscripts";
import { generateTeamMember } from "component-generators";

const copyFromManuscriptId = "manuscript1";
const copyToManuscriptId = "manuscript2";
const newTeamId = "team2";

const mockedTeamMembers = [
  generateTeamMember({
    id: "tm1",
    teamId: "team1",
  }),
  generateTeamMember({
    id: "tm2",
    teamId: "team1",
  }),
];

class mockTeamModel {
  constructor(args) {
    return {
      id: newTeamId,
      save() {
        return this;
      },
      ...args,
    };
  }
  static Role = {
    academicEditor: "academicEditor",
  };
}

class mockTeamMemberModel {
  constructor(args) {
    return {
      ...args,
      save() {
        return this;
      },
    };
  }
  static findAllByManuscriptAndRole() {
    return mockedTeamMembers;
  }
}

const mockModels = {
  Team: mockTeamModel,
  TeamMember: mockTeamMemberModel,
};

let newAEs;

describe("copy academic editors", () => {
  beforeAll(async () => {
    newAEs = await copyAEsBetweenManuscripts
      .initialize({
        models: mockModels,
      })
      .execute({
        copyFromManuscriptId,
        copyToManuscriptId,
      });
  });
  it("should have copied all the existing academicEditors", () => {
    expect(newAEs.length).toEqual(mockedTeamMembers.length);
  });
  it("should have createad a new academicEditor team and add it to the new team members", () => {
    expect(newAEs[0].hasOwnProperty("teamId")).toBe(true);
    expect(newAEs[0].teamId).toEqual(newTeamId);
  });
});
