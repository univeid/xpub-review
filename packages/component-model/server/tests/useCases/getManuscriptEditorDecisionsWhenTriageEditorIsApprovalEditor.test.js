const {
  getTeamRoles,
  getRecommendations,
  getTeamMemberStatuses,
} = require('component-generators')

const {
  getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase,
} = require('../../dist/useCases/manuscript')

const models = {
  Review: {
    Recommendations: getRecommendations(),
    findAllValidByManuscriptAndRole: jest.fn(),
    findAllValidAndSubmitedBySubmissionAndRole: jest.fn(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
    findOneByManuscriptAndRoleAndStatuses: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
}

const { Review, TeamMember } = models

describe('Get manuscript editor decisions when triage editor is approval editor use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('should return no options when a revision recommendation exists', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([
        { id: 'review-id', recommendation: Review.Recommendations.revision },
      ])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual([])
  })
  it('should return no options when a major revision recommendation exists', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([
        { id: 'review-id', recommendation: Review.Recommendations.major },
      ])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual([])
  })
  it('should return no options when a minor revision recommendation exists', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([
        { id: 'review-id', recommendation: Review.Recommendations.minor },
      ])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual([])
  })
  it('should return publish, reject and returnToAcademicEditor when publish exists', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([
        { id: 'review-id', recommendation: Review.Recommendations.publish },
      ])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['publish', 'returnToAcademicEditor', 'reject'])
  })
  it('should return publish, reject and returnToAcademicEditor when reject exists', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([
        { id: 'review-id', recommendation: Review.Recommendations.reject },
      ])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['publish', 'returnToAcademicEditor', 'reject'])
  })
  it('should return publish, reject and revision when no recommendations exist and reviewer reports exist', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(Review, 'findAllValidAndSubmitedBySubmissionAndRole')
      .mockResolvedValue([{ id: 'review-id' }])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['publish', 'revision', 'reject'])
  })
  it('should return reject when no recommendations exist, no reports exist and AE is pending or accepted', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(Review, 'findAllValidAndSubmitedBySubmissionAndRole')
      .mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({})
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['reject'])
  })
  it('should return revision and reject when no recommendations exists, no reports exist and AE is neither pending nor accepted', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(Review, 'findAllValidAndSubmitedBySubmissionAndRole')
      .mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue()
    const manuscript = { articleType: { hasPeerReview: true } }

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['revision', 'reject'])
  })
})
