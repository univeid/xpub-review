const {
  getTeamMemberWorkloadUseCase,
} = require('../../../dist/useCases/teamMember')

const models = {
  Team: { Role: { academicEditor: 'academicEditor' } },
}

const useCases = {
  getAcademicEditorWorkloadUseCase: {
    initialize: jest.fn(() => ({
      execute: jest.fn(),
    })),
  },
}

const ctx = {
  loaders: {
    TeamMember: {
      teamMemberTeamLoader: {
        load: jest.fn(),
      },
    },
  },
}

describe('getTeamMemberWorkload', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('correctly selects the use-case', async () => {
    const teamMember = {}
    ctx.loaders.TeamMember.teamMemberTeamLoader.load = () => ({
      team: { role: 'academicEditor' },
    })

    await getTeamMemberWorkloadUseCase
      .initialize({ models, useCases })
      .execute({
        teamMember,
        ctx,
      })

    expect(
      useCases.getAcademicEditorWorkloadUseCase.initialize,
    ).toHaveBeenCalled()
  })
})
