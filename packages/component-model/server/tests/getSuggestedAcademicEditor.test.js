const {
  TeamMember,
  EditorSuggestion,
  Manuscript,
  Team,
} = require('@pubsweet/models')
const { getSuggestedAcademicEditorUseCase } = require('../dist/useCases')

describe('Get Suggested Academic Editor', () => {
  afterEach(() => {
    jest.clearAllMocks()
    jest.resetAllMocks()
  })

  it('should process team members and their workload from specialIssue', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([
        {
          id: 'team-member-id-1',
        },
        {
          id: 'team-member-id-2',
        },
      ])

    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadBySpecialIssue')
      .mockResolvedValueOnce([
        {
          userId: 'team-member-id-1',
        },
        {
          userId: 'team-member-id-2',
        },
      ])
    jest.spyOn(TeamMember, 'findAllBySectionAndRole')
    jest.spyOn(TeamMember, 'findTeamMembersWorkloadBySection')
    jest.spyOn(TeamMember, 'findAllByJournalAndRole')
    jest.spyOn(TeamMember, 'findTeamMembersWorkloadByJournal')
    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })

    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
    })

    expect(
      TeamMember.findTeamMembersWorkloadBySpecialIssue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findTeamMembersWorkloadBySpecialIssue,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      specialIssueId: 'some-special-issue-id',
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
      manuscriptStatuses: Manuscript.InProgressStatuses,
    })

    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(0)
    expect(TeamMember.findTeamMembersWorkloadBySection).toHaveBeenCalledTimes(0)
    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(0)
    expect(TeamMember.findTeamMembersWorkloadByJournal).toHaveBeenCalledTimes(0)
  })

  it('should process team members and their workload from section', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadBySpecialIssue')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([
      {
        id: 'team-member-id-1',
      },
      {
        id: 'team-member-id-2',
      },
    ])
    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadBySection')
      .mockResolvedValueOnce([
        {
          userId: 'team-member-id-1',
        },
        {
          userId: 'team-member-id-2',
        },
      ])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole')
    jest.spyOn(TeamMember, 'findTeamMembersWorkloadByJournal')
    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
    })

    expect(
      TeamMember.findTeamMembersWorkloadBySpecialIssue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findTeamMembersWorkloadBySpecialIssue,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      specialIssueId: 'some-special-issue-id',
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
      manuscriptStatuses: Manuscript.InProgressStatuses,
    })

    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledWith({
      sectionId: 'some-section-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findTeamMembersWorkloadBySection).toHaveBeenCalledTimes(1)
    expect(TeamMember.findTeamMembersWorkloadBySection).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      sectionId: 'some-section-id',
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
      manuscriptStatuses: Manuscript.InProgressStatuses,
    })

    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(0)
    expect(TeamMember.findTeamMembersWorkloadByJournal).toHaveBeenCalledTimes(0)
  })

  it('should process team members and their workload from journal', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadBySpecialIssue')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadBySection')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValueOnce([
      {
        userId: 'team-member-id-1',
      },
      {
        userId: 'team-member-id-2',
      },
    ])

    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadByJournal')
      .mockResolvedValueOnce([
        {
          userId: 'team-member-id-1',
        },
        {
          userId: 'team-member-id-2',
        },
      ])

    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
    })

    expect(
      TeamMember.findTeamMembersWorkloadBySpecialIssue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findTeamMembersWorkloadBySpecialIssue,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      specialIssueId: 'some-special-issue-id',
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
      manuscriptStatuses: Manuscript.InProgressStatuses,
    })

    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledWith({
      sectionId: 'some-section-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findTeamMembersWorkloadBySection).toHaveBeenCalledTimes(1)
    expect(TeamMember.findTeamMembersWorkloadBySection).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      sectionId: 'some-section-id',
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
      manuscriptStatuses: Manuscript.InProgressStatuses,
    })

    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findTeamMembersWorkloadByJournal).toHaveBeenCalledTimes(1)
    expect(TeamMember.findTeamMembersWorkloadByJournal).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      role: Team.Role.academicEditor,
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
      manuscriptStatuses: Manuscript.InProgressStatuses,
    })
  })

  it('should throw an error if no team members are found', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadBySpecialIssue')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadBySection')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadByJournal')
      .mockResolvedValueOnce([])

    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    const result = getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })

    await expect(result).rejects.toThrow(
      'No manuscript parent ID has been provided',
    )

    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
    })

    expect(
      TeamMember.findTeamMembersWorkloadBySpecialIssue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findTeamMembersWorkloadBySpecialIssue,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      specialIssueId: 'some-special-issue-id',
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
      manuscriptStatuses: Manuscript.InProgressStatuses,
    })

    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledWith({
      sectionId: 'some-section-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findTeamMembersWorkloadBySection).toHaveBeenCalledTimes(1)
    expect(TeamMember.findTeamMembersWorkloadBySection).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      sectionId: 'some-section-id',
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
      manuscriptStatuses: Manuscript.InProgressStatuses,
    })

    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findTeamMembersWorkloadByJournal).toHaveBeenCalledTimes(1)
    expect(TeamMember.findTeamMembersWorkloadByJournal).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      role: Team.Role.academicEditor,
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
      manuscriptStatuses: Manuscript.InProgressStatuses,
    })
  })

  it('should retrieve the user based on score and workload', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadBySpecialIssue')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadBySection')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValueOnce([
      {
        id: 'team-member-id-1',
        userId: 'user-id-1',
      },
      {
        id: 'team-member-id-2',
        userId: 'user-id-2',
      },
      {
        id: 'team-member-id-3',
        userId: 'user-id-3',
      },
    ])

    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadByJournal')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
          workload: 1,
        },
        {
          userId: 'user-id-2',
          workload: 3,
        },
        {
          userId: 'user-id-3',
          workload: 4,
        },
      ])

    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([
      {
        teamMemberId: 'team-member-id-1',
        score: 2,
      },
      {
        teamMemberId: 'team-member-id-2',
        score: 4,
      },
      {
        teamMemberId: 'team-member-id-3',
        score: 3,
      },
    ])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    const user = await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })

    expect(user).toEqual({
      id: 'team-member-id-2',
      userId: 'user-id-2',
      score: 4,
      workload: 3,
    })
  })
})
