type Preprint implements Object {
  id: ID!
  created: DateTime!
  updated: DateTime
  type: String
  format: String
}

extend type Journal {
  issn: String
  email: String
  apc: Int
  code: String
  name: String
  isActive: Boolean
  activationDate: DateTime
  articleTypes: [ArticleType]
  peerReviewModel: PeerReviewModel
  sections: [Section]
  specialIssues: [SpecialIssue]
  preprints: [Preprint]
  preprintDescription: String
}

extend type Manuscript {
  customId: String
  academicEditor: TeamMember
  pendingAcademicEditor: TeamMember
  authors: [TeamMember]
  reviewers: [TeamMember]
  technicalCheckToken: String
  hasPassedEQA: Boolean
  hasPassedEQS: Boolean
  version: String
  role: ValidRoles
  title: String
  journalId: String
  journal: Journal
  section: Section
  specialIssue: SpecialIssue
  submissionId: String
  comment: Comment
  articleType: ArticleType
  visibleStatus: String
  inProgress: Boolean
  researchIntegrityPublishingEditor: TeamMember
  triageEditor: TeamMember
  isApprovalEditor: Boolean
  statusColor: String
  hasSpecialIssueEditorialConflictOfInterest: Boolean
  editorDecisions: [ValidDecisions]
  preprintValue: String
  isEditable: Boolean
  hasTriageEditorConflictOfInterest: Boolean
  peerReviewModel: PeerReviewModel
}

enum ValidDecisions {
  publish
  revision
  returnToAcademicEditor
  reject
}

enum ValidRoles {
  author
  academicEditor
  reviewer
  admin
  triageEditor
  editorialAssistant
  submittingStaffMember
  researchIntegrityPublishingEditor
}

extend type ManuscriptMeta {
  dataAvailability: String
  conflictOfInterest: String
  fundingStatement: String
  agreeTc: Boolean
  articleTypeId: String
}

extend type TeamMember {
  id: ID
  role: String
  invited: DateTime
  responded: DateTime
  reviewerNumber: Int
  isSubmitting: Boolean
  isCorresponding: Boolean
  workload: Int
  reviewerStatusLabel: String
}

extend type Review {
  submitted: DateTime
  member: TeamMember
}

extend type Comment {
  id: ID!
  created: DateTime!
  updated: DateTime
}

extend type File {
  originalName: String
  manuscriptId: ID
  commentId: ID
  position: Int
  providerKey: String
}

extend type Alias {
  country: String
  title: String
}

extend type User {
  isActive: Boolean
  isSubscribedToEmails: Boolean
  confirmationToken: String
  passwordResetToken: String
  unsubscribeToken: String
  invitationToken: String
  role: String
  isAdmin: Boolean
  isRIPE: Boolean
}

extend type Local {
  isConfirmed: Boolean
  country: String
}

type ReviewerSuggestion {
  id: ID
  givenNames: String
  surname: String
  email: String
  numberOfReviews: Int
  aff: String
  profileUrl: String
  isInvited: Boolean
}

type ArticleType {
  id: ID
  name: String
  hasPeerReview: Boolean
}

type PeerReviewModel {
  id: ID
  name: String
  hasSections: Boolean
  hasTriageEditor: Boolean
  triageEditorLabel: String
  academicEditorLabel: String
  hasFigureheadEditor: Boolean
  figureheadEditorLabel: String
  approvalEditors: [String]
}

type Section {
  id: ID
  name: String
  sectionEditors: [TeamMember]
  specialIssues: [SpecialIssue]
  customId: String
}

type SpecialIssue {
  id: ID
  name: String
  isCancelled: Boolean
  cancelReason: String
  isActive: Boolean
  startDate: DateTime
  endDate: DateTime
  expirationDate: DateTime
  leadGuestEditor: TeamMember
  section: Section
  callForPapers: String
  peerReviewModel: PeerReviewModel
  customId: String
}
