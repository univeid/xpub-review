import { get } from 'lodash'
import PropTypes from 'prop-types'
import { space } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const tagCSS = props => {
  if (get(props, `status`)) {
    return css`
      background-color: ${th('tag.statusBackgroundColor')};
      padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
      height: calc(${th('gridUnit')} * 6);
      display: flex;
      align-items: center;
    `
  }

  if (get(props, `pending`)) {
    return css`
      background-color: ${th('colorFurnitureHue')};
      height: calc(${th('gridUnit')} * 6);
      width: calc(${th('gridUnit')} * 20);
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: ${th('button.smallSize')};
      border-radius: ${th('borderRadius')};
    `
  }

  if (get(props, `dateLabel`)) {
    return css`
      background-color: ${th('grey20')};
      padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
      font-weight: 700;
    `
  }

  if (get(props, `authorTag`)) {
    return css`
      background-color: ${th('grey70')};
      font-weight: 700;
    `
  }

  return css`
    background-color: ${th('tag.backgroundColor')};
  `
}

/** @component */
const Tag = styled.div`
  border-radius: ${th('tag.borderRadius')
    ? th('tag.borderRadius')
    : th('borderRadius')};
  color: ${th('tag.color')};
  display: initial;
  font-weight: ${th('tag.fontWeight')};
  font-size: ${th('smallLabelSize')};
  line-height: ${th('smallLabelLineHeight')};
  font-family: ${th('defaultFont')};
  padding: 1px 2px;
  text-align: center;
  white-space: nowrap;
  width: fit-content;

  ${tagCSS};
  ${space};
`

Tag.propTypes = {
  /** Old status of the corresponding user. */
  oldStatus: PropTypes.bool,
  /** New status of the corresponding user. */
  status: PropTypes.bool,
}

Tag.defaultProps = {
  oldStatus: false,
  status: false,
}

/** @component */
export default Tag
