import React from 'react'
import { H4 } from '@pubsweet/ui'
import PropTypes from 'prop-types'
import { space } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { heightHelper } from '../'

const Label = ({ children, required, height, ...rest }) => (
  <Root height={height} {...rest}>
    <H4>{children}</H4>
    {required && <Required height={height}>*</Required>}
  </Root>
)

Label.propTypes = {
  /** If true the label is required. */
  required: PropTypes.bool,
  height: PropTypes.number,
  disabled: PropTypes.bool,
}

Label.defaultProps = {
  required: false,
  height: 4,
  disabled: false,
}
export default Label

// #region styles
const Required = styled.span`
  color: ${th('warningColor')};
  margin-left: calc(${th('gridUnit')} / 2);

  ${heightHelper};
`

const Root = styled.div`
  align-items: center;
  display: flex;
  opacity: ${props => (props.disabled ? 0.3 : 1)};
  ${heightHelper};
  ${space};
`
// #endregion
