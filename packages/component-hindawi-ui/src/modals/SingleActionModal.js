import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, H2 } from '@pubsweet/ui'
import { compose, setDisplayName, withHandlers } from 'recompose'

import { IconButton, Text } from '../../'

const SingleActionModal = ({
  error,
  title,
  content,
  onClick,
  subtitle,
  confirmText,
  onConfirm,
  renderContent,
  hasCloseIcon,
  ...rest
}) => (
  <Root {...rest}>
    {hasCloseIcon && (
      <IconButton icon="x" onClick={onClick} right={8} secondary top={12} />
    )}
    <IconButton
      error={error}
      fontIcon={error ? 'removeIcon' : 'saveIcon'}
      primary={!error}
      size={4}
    />
    {title && <H2>{title}</H2>}
    {subtitle && <Text secondary>{subtitle}</Text>}
    {renderContent()}
    <Button data-test-id="modal-confirm" onClick={onClick} primary>
      {confirmText}
    </Button>
  </Root>
)

export default compose(
  withHandlers({
    onClick: ({ onCancel, onConfirm, hideModal }) => () => {
      typeof onCancel === 'function' && onCancel()
      typeof onConfirm === 'function' && onConfirm()
      hideModal()
    },
    renderContent: ({ content, ...props }) => () => {
      if (!content) return null
      if (typeof content === 'object') {
        return content
      } else if (typeof content === 'function') {
        return content(props)
      }
      return <Text dangerouslySetInnerHTML={{ __html: content }} mb={2} />
    },
  }),
  setDisplayName('SingleActionModal'),
)(SingleActionModal)

// #region styles
const Root = styled.div`
  align-items: center;
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-radius: ${th('borderRadius')};
  background: ${th('colorBackground')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  position: relative;
  padding: calc(${th('gridUnit')} * 10);
  width: calc(${props => (props.width ? props.width : 80)} * ${th('gridUnit')});

  ${H2} {
    margin-bottom: calc(${th('gridUnit')} * 4);
  }

  ${Button} {
    margin-top: calc(${th('gridUnit')} * 2);
  }
`

SingleActionModal.propTypes = {
  /** Title that will be showed on the card. */
  title: PropTypes.string,
  /** Subtitle that will be showed on the card. */
  subtitle: PropTypes.string,
  /** Callback function fired when confirm confirmation card. */
  onConfirm: PropTypes.func,
  /** The text you want to see on the button when someone submit the report. */
  confirmText: PropTypes.string,
  /** If true success icon is replaced with error icon.  */
  error: PropTypes.bool,
  /** If true close icon is displayed.  */
  hasCloseIcon: PropTypes.bool,
}

SingleActionModal.defaultProps = {
  title: undefined,
  subtitle: undefined,
  onConfirm: undefined,
  confirmText: 'OK',
  error: undefined,
  hasCloseIcon: true,
}
// #endregion
