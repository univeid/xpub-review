import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const checkIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
  <path fill="#fff" d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/>
</svg>`

const Checkbox = css`
  display: flex; 
  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};
  opacity: ${props => (props.disabled ? 0.3 : 1)};

  input:checked + span::before {
    border-color: ${th('actionPrimaryColor')};
    background: url('data:image/svg+xml;utf8,${encodeURIComponent(
      checkIcon,
    )}') center no-repeat, ${th('actionPrimaryColor')};
    background-size: contain;
  }

  input:disabled + span::before {
    border-color: ${th('actionSecondaryColor')};
    background: ${props =>
      props.checked
        ? `url('data:image/svg+xml;utf8,${encodeURIComponent(
            checkIcon,
          )}') center no-repeat, grey`
        : null} ;
    background-size: contain;
  }

  input:disabled + span {
    opacity: 0.3;
  }
`

Checkbox.Input = css`
  position: absolute;
  opacity: 0;
  z-index: -1;
`

Checkbox.Label = css`
  display: flex;
  justify-content: center;
  font-weight: 600;
  display: flex;
  flex-direction: row;
  &::before {
    content: ' ';
    background: transparent;
    border: 2px solid ${th('checkbox.borderColor')};
    border-radius: ${th('borderRadius')};
    box-sizing: border-box;
    display: inline-block;
    margin-right: ${th('gridUnit')};
    vertical-align: text-bottom;

    height: calc(${th('gridUnit')} * 4);
    width: calc(${th('gridUnit')} * 4);
  }
`

export default Checkbox
