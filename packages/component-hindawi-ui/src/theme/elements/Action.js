import { get } from 'lodash'
import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { heightHelper } from '../../styledHelpers'

export default css`
  align-items: ${props => get(props, 'alignItems', 'baseline')};
  cursor: pointer;
  color: ${th('action.color')};
  font-size: ${props => (props.fontSize ? props.fontSize : '')};
  font-weight: ${props => (props.fontWeight ? props.fontWeight : '')};
  line-height: 1;

  ${heightHelper};

  &:hover {
    color: ${th('textSecondaryColor')};
    text-decoration: none;
  }

  &:active,
  &:focus {
    color: ${th('action.colorActive')};
    text-decoration: none;
  }
`
