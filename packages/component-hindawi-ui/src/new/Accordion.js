import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Icon, Text, Counter } from '@hindawi/ui'

const Accordion = ({ children }) => {
  const [expandedIndex, setExpandedIndex] = useState(null)
  const renderChildren = () =>
    React.Children.map(
      children,
      (child, i) =>
        child &&
        React.cloneElement(child, {
          index: i,
          expandedIndex,
          setExpandedIndex,
        }),
    )

  return <AccordionWrapper>{renderChildren()}</AccordionWrapper>
}

export const accordionChildrenPropTypes = {
  index: PropTypes.number,
  expandedIndex: PropTypes.number,
  setExpandedIntex: PropTypes.func,
}

const Item = ({
  title,
  number,
  index,
  expandedIndex,
  setExpandedIndex,
  children,
  meta,
  metaState,
}) => {
  const isExpanded = index === expandedIndex
  const toggle = () => setExpandedIndex(isExpanded ? null : index)
  return (
    <ItemWrapper>
      <Header onClick={toggle}>
        <Icon
          fontSize="16px"
          icon={isExpanded ? 'caretDown' : 'caretRight'}
          mr={1}
        />
        <Title mr={2}>{title}</Title>
        {number && <Counter number={number} />}
        {meta && <Subtitle state={metaState}>{meta}</Subtitle>}
      </Header>
      {isExpanded && <Content>{children}</Content>}
    </ItemWrapper>
  )
}

Item.propTypes = {
  ...accordionChildrenPropTypes,
  /** Item title displayed over component */
  title: PropTypes.string,
  /** Optional number displayed in the title */
  number: PropTypes.number,
  /** Optional subtitle details displayed at the right of the title */
  meta: PropTypes.string,
  /** Optional styling for subtitle */
  metaState: PropTypes.string,
}

Item.defaultProps = {
  title: '',
  number: null,
  meta: null,
  metaState: null,
}

const AccordionWrapper = styled.div`
  background-color: ${th('white')};
  border: 1px solid ${th('furnitureColor')};
  border-radius: ${th('borderRadiusContainer')};
`

const ItemWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 2) calc(${th('gridUnit')} * 4);
  border-bottom: 1px solid ${th('furnitureColor')};
  &:last-child {
    border: none;
  }
`

const Header = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  cursor: pointer;
`

const Title = styled(Text)`
  color: ${th('grey70')};
  font-size: ${th('h3Size')};
  font-weight: ${th('fontWeightSemibold')};
  line-height: ${th('h3LineHeight')};
`
const color = state => {
  switch (state) {
    case 'warning':
      return th('warning')
    case 'error':
      return th('warningColor')
    case 'success':
      return th('actionPrimaryColor')
    default:
      return null
  }
}

const Subtitle = styled(Text)`
  margin-left: auto;
  color: ${({ state }) => color(state)};
`

const Content = styled.div`
  margin-top: calc(${th('gridUnit')} * 2);
`

Accordion.Item = Item
export default Accordion
