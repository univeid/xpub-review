A component to render tabs.

```js
const tabItems = [
  { content: () => <div>Tab one content</div> },
  { content: () => <div>Tab two content</div> },
  { content: () => <div>Tab three content</div> },
]
;<Tabs>
  {({ selectedTab, changeTab }) => (
    <div>
      <div>
        <button onClick={() => changeTab(0)}>Tab 1</button>
        <button onClick={() => changeTab(1)}>Tab 2</button>
        <button onClick={() => changeTab(2)}>Tab 3</button>
      </div>

      {tabItems[selectedTab].content()}
    </div>
  )}
</Tabs>
```
