import { get, chain, find } from 'lodash'

export const handleError = fn => e => {
  fn(get(JSON.parse(e.response), 'error', 'Oops! Something went wrong!'))
}

export const getReportComments = ({ report, type = 'public' }) =>
  chain(report)
    .get('comments', [])
    .find(c => c.type === type)
    .get('content')
    .value()

export const getShortRole = ({
  role,
  academicEditorLabel,
  triageEditorLabel,
}) => {
  const roles = {
    admin: 'Admin',
    author: 'Author',
    reviewer: 'Reviewer',
    triageEditor: triageEditorLabel,
    academicEditor: academicEditorLabel,
    editorialAssistant: 'Editorial Assistant',
    researchIntegrityPublishingEditor: 'Research Integrity Publishing Editor',
  }

  const tags = [
    { tag: 'Admin', role: 'Admin' },
    { tag: 'EA', role: 'Editorial Assistant' },
    { tag: 'RIPE', role: 'Research Integrity Publishing Editor' },
    { tag: 'CE', role: 'Chief Editor' },
    { tag: 'SE', role: 'Section Editor' },
    { tag: 'LGE', role: 'Lead Guest Editor' },
    { tag: 'AE', role: 'Academic Editor' },
    { tag: 'AssE', role: 'Associate Editor' },
    { tag: 'GE', role: 'Guest Editor' },
    { tag: 'SA', role: 'Author' },
    { tag: 'Reviewer', role: 'Reviewer' },
    { tag: 'HE', role: 'Handling Editor' },
  ]

  const roleLabel = roles[role]
  const roleTag = tags.find(tag => tag.role === roleLabel)

  return roleTag && roleTag.tag
}

export const indexReviewers = (reports = [], invitations = []) => {
  reports.forEach(report => {
    report.reviewerNumber = get(
      find(invitations, ['userId', report.userId]),
      'reviewerNumber',
      0,
    )
  })
  return reports
}

export const trimFormStringValues = values =>
  Object.entries(values).reduce(
    (acc, [key, value]) => ({
      ...acc,
      [key]: typeof value === 'string' ? value.trim() : value,
    }),
    {},
  )

export const parseSearchParams = url => {
  const params = new URLSearchParams(url)
  const parsedObject = {}
  /* eslint-disable */
  for (let [key, value] of params) {
    parsedObject[key] = value
  }
  /* eslint-enable */
  return parsedObject
}
