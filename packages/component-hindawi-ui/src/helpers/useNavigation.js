import { useCallback } from 'react'
import { useHistory } from 'react-router-dom'

export const useNavigation = () => {
  const history = useHistory()

  const goTo = useCallback(
    to => () => {
      history.push(to)
    },
    [history],
  )

  return {
    goTo,
  }
}
