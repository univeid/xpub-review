import PropTypes from 'prop-types'
import Tippy from '@tippy.js/react'
import React, { Fragment } from 'react'
import styled, { ThemeProvider, withTheme, css } from 'styled-components'

import { Text, Row } from '../'

const TitleTooltip = ({ theme = {}, title = '' }) => (
  <ThemeProvider theme={theme}>
    <Fragment>
      <Row mt={1}>
        <Text selected small>
          {title}
        </Text>
      </Row>
    </Fragment>
  </ThemeProvider>
)

const TextTooltip = ({ theme = {}, overflow, children, ...rest }) => (
  <Tippy
    arrow
    content={<TitleTooltip theme={theme} {...rest} />}
    placement="top"
    theme="dark"
  >
    <StyledText overflow={overflow}>{children}</StyledText>
  </Tippy>
)

TextTooltip.propTypes = {
  /** User can hover over an item, without clicking it, and a new window will appear with a new title */
  title: PropTypes.string,
}
TextTooltip.defaultProps = {
  title:
    'β-Carboline Silver Compound Binding Studies with Human Serum Albumin: A Comprehensive Multispectroscopic Analysis and Molecular Modeling Study',
}

const overflowHelper = props => {
  if (props.overflow) {
    return css`
      overflow: ${props.overflow};
    `
  }
}

const StyledText = styled.span`
  ${overflowHelper}
`

export default withTheme(TextTooltip)
