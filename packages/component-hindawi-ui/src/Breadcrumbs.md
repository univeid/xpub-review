A clickable text button.

```js
<Breadcrumbs onClick={() => console.log('I am clicked.')}>
  Default action
</Breadcrumbs>
```