const hasRequiredEnvVariables = envVars =>
  envVars.every(v => v !== undefined && v !== '' && v !== null)

module.exports = {
  hasRequiredEnvVariables,
}
