import React, { Fragment } from 'react'
import { chain } from 'lodash'
import { Route } from 'react-router-dom'
import { NewSVGLogo, UserDropDown, ActionLink, Banner } from '@hindawi/ui'
import { useJournal } from 'component-journal-info'
import { AutosaveIndicator, SubmitDraft } from 'component-submission/client'

import { Root, LogoContainer, RightContainer } from '../sharedStyledComponents'

// to do: this needs to be refactored. rendering routes here leads to bugs
const autosaveIndicatorPaths = [
  '/submit/:submissionId/:manuscriptId',
  '/details/:submissionId/:manuscriptId',
]

const AuthenticatedAppBarContent = ({
  goTo,
  logo,
  logout,
  publisher,
  currentUser = {},
  goToDashboard,
  showSubmitButton,
}) => {
  const { name, supportEmail } = useJournal()
  const isConfirmed = chain(currentUser)
    .get('identities', [])
    .find(i => i.__typename === 'Local')
    .get('isConfirmed')
    .value()

  return (
    <Fragment>
      <Root>
        <LogoContainer>
          <NewSVGLogo
            ml={5}
            onClick={goToDashboard}
            publisher={publisher}
            src={logo}
          />
        </LogoContainer>
        <RightContainer>
          {autosaveIndicatorPaths.map(path => (
            <Route component={AutosaveIndicator} exact key={path} path={path} />
          ))}
          {showSubmitButton && <SubmitDraft currentUser={currentUser} />}
          <UserDropDown currentUser={currentUser} goTo={goTo} logout={logout} />
        </RightContainer>
      </Root>
      {!isConfirmed && (
        <Banner name="confirmation">
          Your account is not confirmed. Please check your email.
        </Banner>
      )}
      {name === 'Hindawi' && (
        <Banner name="covid19">
          All our journals remain open as normal during the COVID-19 crisis.
          However, if you are unable to carry on with your activities as normal,
          please contact
          <ActionLink
            data-test-id="email-support"
            display="inline"
            ml={1}
            mr={1}
            to={`mailto:${supportEmail}`}
          >
            {supportEmail}
          </ActionLink>
          and we will work with you to find a solution.
        </Banner>
      )}
    </Fragment>
  )
}

export default AuthenticatedAppBarContent
