import React from 'react'
import { get } from 'lodash'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router'
import { useJournal } from 'component-journal-info'
import { compose, withProps, withHandlers } from 'recompose'
import { useKeycloak } from 'component-sso/client'

import { queries } from '../../graphql'
import { AuthenticatedAppBarContent, UnauthenticatedAppBarContent } from './'

const unauthedPaths = [
  '/login',
  '/signup',
  '/invite',
  '/eqs-decision',
  '/confirm-signup',
  '/password-reset',
  '/forgot-password',
  '/emails/decline-review',
  '/emails/accept-review-new-user',
]

const submitButtonPaths = ['/', '/ea-dashboard']

const AppBar = ({ goTo, logout, goToDashboard, isUnauthedRoute, location }) => {
  const { logo, name } = useJournal()

  if (isUnauthedRoute) {
    return (
      <UnauthenticatedAppBarContent goTo={goTo} logo={logo} publisher={name} />
    )
  }

  const showSubmitButton = submitButtonPaths.includes(location.pathname)

  return (
    <Query query={queries.currentUser}>
      {({ data, loading, client }) => {
        const currentUser = get(data, 'currentUser', null)

        return loading || !currentUser ? null : (
          <AuthenticatedAppBarContent
            currentUser={currentUser}
            goTo={goTo}
            goToDashboard={goToDashboard}
            logo={logo}
            logout={logout(client)}
            publisher={name}
            showSubmitButton={showSubmitButton}
          />
        )
      }}
    </Query>
  )
}

export default compose(
  withRouter,
  withProps(({ location }) => ({
    isUnauthedRoute: unauthedPaths.includes(location.pathname),
    keycloak: useKeycloak(),
  })),
  withHandlers({
    goToDashboard: ({ history }) => () => {
      history.push('/')
    },
    goTo: ({ history }) => path => {
      history.push(path)
    },
    logout: ({ history, keycloak }) => gqlClient => () => {
      gqlClient.resetStore()
      window.localStorage.removeItem('token')

      if (keycloak) {
        keycloak.logout()
      } else {
        history.replace('/login')
      }
    },
  }),
)(AppBar)
