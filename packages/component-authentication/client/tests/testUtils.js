import React from 'react'
import { theme } from '@hindawi/ui'
import { MemoryRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { fireEvent, wait, render as rtlRender } from '@testing-library/react'
import { JournalProvider as HindawiContextProvider } from 'component-journal-info'

import { publisher, journal } from './mockData'

const ENTER = 13

export const render = (ui, cfg = {}) => {
  const Component = () => (
    <HindawiContextProvider journal={journal} publisher={publisher}>
      <ThemeProvider theme={theme}>
        <MemoryRouter initialEntries={cfg.initialEntries}>{ui}</MemoryRouter>
      </ThemeProvider>
    </HindawiContextProvider>
  )

  const utils = rtlRender(<Component />)

  return {
    ...utils,
    clickCheckbox: () => {
      fireEvent.click(utils.container.querySelector(`input[type=checkbox]`))
    },
    selectOption: value => {
      fireEvent.click(utils.container.querySelector(`button[type=button]`))
      wait(() => utils.getByText(value))
      fireEvent.click(utils.getAllByText(value)[1])
    },
    selectCountry: value => {
      fireEvent.change(utils.getByTestId('country-menu'), {
        target: {
          value,
        },
      })
      fireEvent.keyUp(utils.getByTestId('country-menu'), {
        target: {
          which: ENTER,
        },
      })
    },
  }
}
