import React, { useContext } from 'react'
import { get } from 'lodash'
import { Spinner } from '@pubsweet/ui'
import { useQuery } from 'react-apollo'
import { Redirect } from 'react-router-dom'
import { useKeycloak } from 'component-sso/client'

import { queries } from './graphql'
import { setToken, removeToken } from './utils'
import CurrentUserManager, {
  CurrentUserContext,
} from './components/CurrentUserManager'

import { TrackedRoute } from './TrackedRoute'

const toLogin = location => ({
  pathname: '/login',
  state: {
    from: location,
  },
})

const AuthenticatedComponent = ({
  component: Component,
  location,
  ...rest
}) => {
  const { loading, data } = useQuery(queries.currentUser)
  const currentUser = get(data, 'currentUser')

  if (!window.localStorage.getItem('token')) {
    return <Redirect to={toLogin(location)} />
  }

  if (loading) return <Spinner />

  if (!currentUser) {
    return <Redirect to={toLogin(location)} />
  }

  return (
    <TrackedRoute
      render={routerProps => (
        <Component currentUser={currentUser} {...routerProps} />
      )}
      {...rest}
    />
  )
}

const AuthenticatedKeycloakComponent = ({
  component,
  keycloak,
  location,
  ...rest
}) => {
  const { loading, data } = useQuery(queries.currentUser)
  const currentUser = get(data, 'currentUser')
  const { setCurrentUser } = useContext(CurrentUserContext)

  if (!window.localStorage.getItem('token')) {
    keycloak.login()
    return null
  }

  if (loading) return <Spinner />

  if (!currentUser) {
    removeToken()
    keycloak.logout()
    return <Redirect to={toLogin(location)} />
  }

  setCurrentUser(currentUser)

  return <TrackedRoute component={component} {...rest} />
}

const AuthenticatedKeycloakRoute = ({
  keycloak,
  component: Component,
  location,
  ...rest
}) => {
  if (keycloak.authenticated) {
    if (!window.localStorage.getItem('token')) {
      setToken(keycloak.token)
    }
  } else {
    removeToken()
    keycloak.login()
  }
  return (
    <CurrentUserManager>
      <AuthenticatedKeycloakComponent
        component={Component}
        keycloak={keycloak}
        location={location}
        {...rest}
      />
    </CurrentUserManager>
  )
}

const hasAccessRights = (allow, keycloak) => {
  const { clientId, hasResourceRole } = keycloak

  // either allow was not provided or is just an empty array
  if (!allow || !allow.length) return true

  return allow.map(role => hasResourceRole(role, clientId)).some(Boolean)
}

const AuthenticatedRoute = props => {
  const keycloak = useKeycloak()

  if (keycloak) {
    if (!hasAccessRights(props.allow, keycloak)) {
      return <Redirect to="/" />
    }

    return <AuthenticatedKeycloakRoute keycloak={keycloak} {...props} />
  }
  return <AuthenticatedComponent {...props} />
}

export default AuthenticatedRoute
