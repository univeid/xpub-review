import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as queries from './queries'
import * as mutations from './mutations'

export default compose(
  graphql(mutations.loginUser, {
    name: 'loginUser',
  }),
  graphql(mutations.signUp, {
    name: 'signUp',
  }),
  graphql(mutations.confirmUser, {
    name: 'confirmUser',
    options: {
      refetchQueries: [
        {
          query: queries.currentUser,
        },
      ],
    },
  }),
  graphql(mutations.requestPasswordReset, {
    name: 'requestPasswordReset',
  }),
  graphql(mutations.resetPassword, {
    name: 'setPassword',
  }),
  graphql(mutations.signUpFromInvitation, {
    name: 'signUpFromInvitation',
  }),
)
