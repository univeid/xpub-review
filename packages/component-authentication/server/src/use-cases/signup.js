const { passwordStrengthRegex } = require('config')
const uuid = require('uuid')

const initialize = (notificationService, tokenService, { User, Identity }) => ({
  execute: async ({
    aff,
    email,
    title,
    country,
    agreeTc,
    surname,
    password,
    givenNames,
  }) => {
    if (!agreeTc) {
      throw new ConflictError('Terms & Conditions must be read and approved.')
    }
    if (!passwordStrengthRegex.test(password))
      throw new ValidationError(
        'Password is too weak. Please check password requirements.',
      )

    let user = new User({
      agreeTc,
      isActive: true,
      isSubscribedToEmails: true,
      defaultIdentityType: 'local',
      confirmationToken: uuid.v4(),
    })

    const identity = new Identity({
      aff,
      email,
      title,
      country,
      surname,
      givenNames,
      type: 'local',
      isConfirmed: false,
      passwordHash: Identity.hashPassword(password),
    })

    user = await user.insertWithIdentity(identity)

    const token = tokenService.create({
      username: identity.email,
      id: user.id,
    })

    notificationService.notifyUserOnSignUp({ user, identity })

    return { token }
  },
})

const authsomePolicies = ['public']

module.exports = {
  initialize,
  authsomePolicies,
}
