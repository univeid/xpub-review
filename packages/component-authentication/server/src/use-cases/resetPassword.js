const config = require('config')

const initialize = (logger, { User, Identity }) => ({
  execute: async ({ email, password, token }) => {
    if (!config.passwordStrengthRegex.test(password))
      throw new ValidationError(
        'Password is too weak. Please check password requirements.',
      )

    const identity = await Identity.findOneByEmail(email)
    if (!identity) return

    const user = await User.find(identity.userId)
    if (!user) return

    if (user.passwordResetToken !== token) {
      logger.error(
        `User password reset tokens do not match: REQ ${token} vs. DB ${user.passwordResetToken}`,
      )
      throw new Error('Invalid request.')
    }

    user.passwordResetToken = null
    user.passwordResetTimestamp = null
    identity.isConfirmed = true
    identity.passwordHash = Identity.hashPassword(password)

    await user.updateWithIdentity(identity)
  },
})

const authsomePolicies = ['public']

module.exports = {
  initialize,
  authsomePolicies,
}
