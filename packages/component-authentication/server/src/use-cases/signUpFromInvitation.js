const { passwordStrengthRegex } = require('config')

const initialize = ({ logger, tokenService, models: { Identity, User } }) => ({
  execute: async ({
    aff,
    email,
    title,
    country,
    agreeTc,
    surname,
    password,
    givenNames,
    confirmationToken,
  }) => {
    if (!agreeTc)
      throw new ConflictError('Terms & Conditions must be read and approved.')

    if (!passwordStrengthRegex.test(password))
      throw new ValidationError(
        'Password is too weak. Please check password requirements.',
      )

    const identity = await Identity.findOneByEmail(email)
    if (!identity) throw new Error('Invalid request.')

    let user = await User.find(identity.userId)
    if (!user) throw new Error('Invalid request.')

    if (user.confirmationToken !== confirmationToken) {
      logger.error(
        `User confirmation tokens do not match: REQ ${confirmationToken} vs. DB ${user.confirmationToken}`,
      )
      throw new Error('Invalid request.')
    }

    if (!user.isActive) throw new Error('User has been deactivated.')
    if (identity.isConfirmed)
      throw new Error('User has already been confirmed.')

    identity.updateProperties({
      aff,
      title,
      surname,
      country,
      givenNames,
      isConfirmed: true,
      passwordHash: Identity.hashPassword(password),
    })
    user.confirmationToken = null

    user = await user.updateWithIdentity(identity)

    const token = tokenService.create({
      username: identity.email,
      id: user.id,
    })

    return { token }
  },
})

const authsomePolicies = ['public']

module.exports = {
  initialize,
  authsomePolicies,
}
