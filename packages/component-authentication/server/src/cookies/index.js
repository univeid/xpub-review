const { cookieService } = require('component-cookie-service')

const cookiesKeys = {
  userId: 'puid',
}

const initCookieService = context => {
  const { req, res } = context
  return cookieService({ req, res })
}

const setUserId = context => {
  const { user } = context
  const cookies = initCookieService(context)
  const maxAge = 1000 * 60 * 60 * 24 // 1 day
  return cookies.set({
    cookie: cookiesKeys.userId,
    value: user,
    options: {
      maxAge,
      httpOnly: false,
    },
  })
}

const removeUserId = context => {
  const cookies = initCookieService(context)
  return cookies.remove(cookiesKeys.userId)
}

module.exports = {
  setUserId,
  removeUserId,
}
