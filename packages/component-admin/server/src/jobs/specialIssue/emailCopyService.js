const getEmailCopy = ({ emailType, specialIssueName }) => {
  let paragraph
  const hasIntro = true
  const hasLink = false
  const hasSignature = true
  switch (emailType) {
    case 'special-issue-deactivated':
      paragraph = `Special Issue ${specialIssueName} has been deactivated.<br/><br/>
          If you have any questions regarding this action, please let us know.<br/><br/>`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return { hasSignature, paragraph, hasIntro, hasLink }
}

module.exports = {
  getEmailCopy,
}
