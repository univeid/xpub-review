const initialize = ({ User }) => ({
  async execute({ input }) {
    const users = await User.findByEmailOrName({
      input,
      eagerLoadRelations: ['identities'],
    })

    return users.map(user => {
      user.identities = [user.defaultIdentity]
      return user.toDTO()
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
