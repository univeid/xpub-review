const initialize = ({
  models: { Team, TeamMember, Section },
  eventsService,
}) => ({
  execute: async ({ user, role, sectionId }) => {
    const exitingUserSectionMember = await TeamMember.findOneBySectionAndUser({
      userId: user.id,
      sectionId,
    })
    if (exitingUserSectionMember) {
      throw new ValidationError(
        `User already has an editorial role on this section.`,
      )
    }

    const queryObject = {
      role,
      sectionId,
    }
    const team = await Team.findOrCreate({
      queryObject,
      options: queryObject,
      eagerLoadRelations: 'members',
    })

    const teamMember = team.addMember({
      user,
    })
    await teamMember.save()

    const section = await Section.find(sectionId)

    eventsService.publishJournalEvent({
      journalId: section.journalId,
      eventName: 'JournalSectionEditorAssigned',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
