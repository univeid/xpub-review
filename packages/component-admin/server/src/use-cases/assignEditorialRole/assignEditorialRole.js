const initialize = ({
  models,
  logger,
  useCases,
  transaction,
  eventsService,
  updateUserRoleToEA,
  redistributeEditorialAssistants,
}) => ({
  execute: async ({ input }) => {
    const { userId, role, journalId, sectionId, specialIssueId } = input
    const { User } = models

    const user = await User.find(userId, 'identities')
    let useCase = 'assignEditorialRoleOnJournalUseCase'
    if (specialIssueId) {
      useCase = 'assignEditorialRoleOnSpecialIssueUseCase'
    }
    if (sectionId) {
      useCase = 'assignEditorialRoleOnSectionUseCase'
    }

    const localIdentity = user.identities.find(
      identity => identity.type === 'local',
    )

    const userEmail = localIdentity.email

    try {
      updateUserRoleToEA(userEmail)
    } catch (err) {
      logger.error('ERROR: There was a problem with SSO role update:', err)
    }

    await useCases[useCase]
      .initialize({
        models,
        logger,
        useCases,
        transaction,
        eventsService,
        redistributeEditorialAssistants,
      })
      .execute({ user, role, journalId, sectionId, specialIssueId })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
