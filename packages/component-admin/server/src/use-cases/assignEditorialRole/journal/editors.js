const initialize = ({ models: { Team, TeamMember }, eventsService }) => ({
  execute: async ({ user, journalId, role }) => {
    let team = await Team.findOneByJournalAndRole({ journalId, role })
    if (!team) team = new Team({ journalId, role })
    team.addMember({
      user,
      options: { status: TeamMember.Statuses.pending },
    })

    await team.saveGraph({
      noDelete: true,
      noUpdate: true,
      relate: true,
    })

    eventsService.publishJournalEvent({
      journalId,
      eventName: 'JournalEditorAssigned',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
