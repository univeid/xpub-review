const initialize = ({
  models: { Journal, User, Team, TeamMember },
  isDateToday,
  jobsService,
  eventsService,
}) => ({
  execute: async ({ input, userId }) => {
    let journal = {}
    const {
      apc,
      name,
      code,
      issn,
      email,
      articleTypes,
      activationDate,
      peerReviewModelId,
    } = input

    if (apc < 0) {
      throw new ConflictError(`Journal APC ${apc} cannot be a negative number.`)
    }

    journal = new Journal({
      apc,
      name,
      code,
      issn,
      email,
      activationDate,
      peerReviewModelId,
      isActive: isDateToday(activationDate),
    })

    await journal.saveJournalAndJournalArticleTypes(articleTypes)

    if (activationDate && !journal.isActive) {
      const admin = await TeamMember.findOneByUserAndRole({
        userId,
        role: Team.Role.admin,
      })
      jobsService.scheduleJournalActivation({ journal, teamMemberId: admin.id })
    }

    eventsService.publishJournalEvent({
      journalId: journal.id,
      eventName: 'JournalAdded',
    })

    return journal
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
