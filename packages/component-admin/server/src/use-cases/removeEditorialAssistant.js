const { minBy, unionBy, groupBy } = require('lodash')
const Promise = require('bluebird')

const initialize = ({
  models: { Manuscript, Team, TeamMember, User },
  eventsService,
  transaction,
}) => ({
  execute: async teamMemberId => {
    const teamMember = await TeamMember.find(teamMemberId, 'team')
    if (!teamMember) throw new Error('Cannot remove editor.')

    const allJournalEditorialAssistants = await TeamMember.findAllByJournalAndRole(
      {
        journalId: teamMember.team.journalId,
        role: Team.Role.editorialAssistant,
      },
    )

    if (allJournalEditorialAssistants.length === 1)
      throw new Error(
        "You can't remove the only Editorial Assistant on the journal.",
      )

    const otherJournalEditorialAssistants = allJournalEditorialAssistants.filter(
      tm => tm.id !== teamMember.id,
    )

    const editorRemovedSubmissions = []
    const editorAssignedSubmissions = []
    const oldManuscriptEditorialAssistants = []
    const newManuscriptEditorialAssistants = []

    // Find all removed editor manuscripts
    const manuscripts = await Manuscript.findAllByJournalAndUserAndRole({
      journalId: teamMember.team.journalId,
      userId: teamMember.userId,
      role: Team.Role.editorialAssistant,
    })
    const submissions = Object.values(groupBy(manuscripts, 'submissionId'))

    // Change Manuscript.InProgressStatuses in a future tech-debt
    const inProgressStatuses = [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.qualityChecksRequested,
      Manuscript.Statuses.qualityChecksSubmitted,
      Manuscript.Statuses.technicalChecks,
      Manuscript.Statuses.olderVersion,
      Manuscript.Statuses.draft,
    ]

    // Workload
    const assignedWorkload = await TeamMember.findTeamMembersWorkloadByJournal({
      role: Team.Role.editorialAssistant,
      journalId: teamMember.team.journalId,
      teamMemberStatuses: [TeamMember.Statuses.active],
      manuscriptStatuses: inProgressStatuses,
    })
    const unassignedWorkload = otherJournalEditorialAssistants.map(member => ({
      userId: member.userId,
      workload: 0,
    }))
    const allWorkload = unionBy(assignedWorkload, unassignedWorkload, 'userId')

    // Filter the EA we want to remove
    const workload = allWorkload.filter(wl => wl.userId !== teamMember.userId)

    await Promise.each(submissions, async submission => {
      const { submissionId } = submission[0]

      // Early return for finished submissions
      submission.sort(Manuscript.compareVersion)
      const lastManuscript = submission[submission.length - 1]

      if (!inProgressStatuses.includes(lastManuscript.status)) return

      let oldEA
      const fitEAWorkload = minBy(workload, tm => +tm.workload)
      const fitEA = otherJournalEditorialAssistants.find(
        ea => ea.userId === fitEAWorkload.userId,
      )

      await Promise.each(submission, async manuscript => {
        oldEA = await TeamMember.findOneByUserAndRoleAndStatusOnManuscript({
          manuscriptId: manuscript.id,
          role: Team.Role.editorialAssistant,
          status: TeamMember.Statuses.active,
          userId: teamMember.userId,
        })

        if (!oldEA) return

        oldEA.status = TeamMember.Statuses.removed
        oldManuscriptEditorialAssistants.push(oldEA)

        if (!fitEA) return

        // Assign a new EA
        const newEA = new TeamMember({
          userId: fitEA.userId,
          teamId: oldEA.teamId,
          status: TeamMember.Statuses.active,
          alias: fitEA.alias,
        })
        newManuscriptEditorialAssistants.push(newEA)
      })

      if (!oldEA) return
      editorRemovedSubmissions.push(submissionId)

      if (!fitEA) return
      editorAssignedSubmissions.push(submissionId)
      fitEAWorkload.workload = +fitEAWorkload.workload + 1
    })

    await TeamMember.removeEditorialAssistantTransaction({
      journalEA: teamMember,
      manuscriptEAs: [
        ...oldManuscriptEditorialAssistants,
        ...newManuscriptEditorialAssistants,
      ],
    })

    eventsService.publishJournalEvent({
      journalId: teamMember.team.journalId,
      eventName: 'JournalEditorRemoved',
    })
    editorRemovedSubmissions.forEach(submissionId => {
      eventsService.publishSubmissionEvent({
        submissionId,
        eventName: 'SubmissionEditorialAssistantRemoved',
      })
    })
    editorAssignedSubmissions.forEach(submissionId => {
      eventsService.publishSubmissionEvent({
        submissionId,
        eventName: 'SubmissionEditorialAssistantAssigned',
      })
    })
  },
})

module.exports = { initialize }
