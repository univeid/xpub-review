const initialize = ({
  userId,
  isDateToday,
  jobsService,
  eventsService,
  models: { SpecialIssue, PeerReviewModel, Team, TeamMember },
}) => ({
  execute: async ({
    name,
    startDate,
    endDate,
    journalId,
    sectionId,
    callForPapers,
  }) => {
    if (endDate <= startDate) {
      throw new Error(`End date must be after start date.`)
    }

    const existingSpecialIssue = await SpecialIssue.findUnique(name)
    if (existingSpecialIssue) throw new Error(`Special issue already exists.`)

    const specialIssuePeerReviewModel = await PeerReviewModel.findOneBy({
      queryObject: {
        name: 'Special Issue Associate Editor',
      },
    })

    const specialIssue = new SpecialIssue({
      name,
      endDate,
      startDate,
      callForPapers,
      peerReviewModelId: specialIssuePeerReviewModel.id,
      isActive: isDateToday(startDate),
      ...(journalId && !sectionId ? { journalId } : {}),
      ...(sectionId ? { sectionId } : {}),
    })

    await specialIssue.save()

    const admin = await TeamMember.findOneByUserAndRole({
      userId,
      role: Team.Role.admin,
    })

    if (!specialIssue.isActive) {
      jobsService.scheduleSpecialIssueActivation({
        specialIssue,
        teamMemberId: admin.id,
      })
    }

    jobsService.scheduleSpecialIssueDeactivation({
      specialIssue,
      teamMemberId: admin.id,
    })

    eventsService.publishJournalEvent({
      journalId,
      eventName: sectionId
        ? 'JournalSectionSpecialIssueAdded'
        : 'JournalSpecialIssueAdded',
    })

    return specialIssue
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
