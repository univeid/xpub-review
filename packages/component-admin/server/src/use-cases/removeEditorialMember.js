const initialize = ({
  models: { Team, TeamMember },
  models,
  eventsService,
  useCases,
}) => ({
  execute: async teamMemberId => {
    const teamMember = await TeamMember.find(teamMemberId, 'team')
    if (!teamMember) throw new Error('Cannot remove editor.')

    let useCase
    switch (teamMember.team.role) {
      case Team.Role.editorialAssistant:
        useCase = 'removeEditorialAssistantUseCase'
        break
      default:
        useCase = 'removeOtherMemberUseCase'
    }

    await useCases[useCase]
      .initialize({ models, eventsService })
      .execute(teamMemberId)
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
