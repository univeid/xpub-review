const { getArticleTypesUseCase } = require('../../src/use-cases')
const { models, fixtures } = require('fixture-service')

const { ArticleType } = models
describe('getArticleTypes use-case', () => {
  it('returns an array', async () => {
    await fixtures.generateArticleTypes(ArticleType)

    const res = await getArticleTypesUseCase.initialize(models).execute()
    expect(Array.isArray(res)).toBeTruthy()
  })

  it('contains objects with article type names', async () => {
    const res = await getArticleTypesUseCase.initialize(models).execute()
    expect(res).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: expect.any(String),
        }),
      ]),
    )
  })

  it('contains correct number of article types', async () => {
    const res = await getArticleTypesUseCase.initialize(models).execute()
    expect(res.length).toEqual(10)
  })
})
