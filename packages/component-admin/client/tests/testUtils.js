import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { theme } from '@hindawi/ui'
import { createMemoryHistory } from 'history'
import { ModalProvider } from 'component-modal'
import { ThemeProvider } from 'styled-components'
import { render as rtlRender } from '@testing-library/react'
import { MockedProvider } from '@apollo/react-testing'
import { BrowserRouter as Router } from 'react-router-dom'
import {
  getArticleTypes,
  getUsersForEditorialAssignment,
} from '../graphql/queries'

import { articleTypes, users } from './mockData'

const mocks = [
  {
    request: {
      query: getArticleTypes,
      variables: {},
    },
    result: {
      data: {
        getArticleTypes: articleTypes,
      },
    },
  },
  {
    request: {
      query: getUsersForEditorialAssignment,
      variables: {},
    },
    result: {
      data: {
        getUsersForEditorialAssignment: users,
      },
    },
  },
]

export const render = (
  ui,
  {
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
  } = {},
) => {
  const Component = () => (
    <Router>
      <MockedProvider mocks={mocks}>
        <ModalProvider>
          <div id="ps-modal-root" />
          <ThemeProvider theme={theme}>{ui}</ThemeProvider>
        </ModalProvider>
      </MockedProvider>
    </Router>
  )
  const utils = rtlRender(<Component />)
  return {
    ...utils,
    history,
  }
}
