import React from 'react'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'
import Chance from 'chance'

import { render } from './testUtils'
import AdminAddUserForm from '../components/AdminAddUserForm'

const chance = new Chance()

describe('Add user', () => {
  let onConfirmMock
  let container

  beforeEach(() => {
    cleanup()
    onConfirmMock = jest.fn()
    container = render(<AdminAddUserForm onConfirm={onConfirmMock} />)
  })

  it('Should show "Required" for every field that is required', async done => {
    const { getByText, getByTestId, getAllByText } = container

    expect(getByText('First Name')).toBeInTheDocument()
    expect(getByText('Last Name')).toBeInTheDocument()
    expect(getByText('Title')).toBeInTheDocument()
    expect(getByText('Email')).toBeInTheDocument()
    expect(getByText('Country')).toBeInTheDocument()
    expect(getByText('Affiliation')).toBeInTheDocument()
    expect(getByTestId('modal-confirm')).toBeInTheDocument()

    fireEvent.click(getByTestId('modal-confirm'))

    setTimeout(() => {
      expect(getAllByText('Required').length).toEqual(6)
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('Should throw error if "Email" input is invalid', async done => {
    const { getByText, getByTestId } = container

    expect(getByText('Email')).toBeInTheDocument()
    expect(getByTestId('modal-confirm')).toBeInTheDocument()
    fireEvent.change(getByTestId('email-input'), {
      target: { value: chance.word() },
    })
    fireEvent.click(getByTestId('modal-confirm'))

    setTimeout(() => {
      expect(getByText('Invalid email')).toBeInTheDocument()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })
})
