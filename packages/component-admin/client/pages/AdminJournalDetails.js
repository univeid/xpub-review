import React, { Fragment } from 'react'
import { useQuery } from 'react-apollo'
import { get } from 'lodash'
import moment from 'moment'
import { space } from 'styled-system'
import styled from 'styled-components'
import { Spinner, H3 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { Row, Item, Text, Label, Breadcrumbs, ShadowedBox } from '@hindawi/ui'
import { getJournal } from '../graphql/queries'
import { formatAPC } from '../components/utils'

import JournalDetailsTabs from '../components/JournalDetailsTabs'
import Sections from '../components/Sections'
import SpecialIssues from '../components/SpecialIssues'
import AssignEditorialRole from '../components/AssignEditorialRole'

const AdminJournalDetails = ({ match: { params } }) => {
  const { journalId } = params

  const tabOptions = ['Sections', 'Special Issues', 'Assign Editorial Role']
  const { data, loading } = useQuery(getJournal, {
    variables: {
      journalId,
    },
    fetchPolicy: 'network-only',
  })

  const {
    name,
    code,
    isActive,
    activationDate,
    email,
    issn,
    apc,
    articleTypes,
    sections = [],
    specialIssues = [],
    peerReviewModel = {},
    peerReviewModel: { name: peerReviewModelName } = {},
  } = get(data, 'getJournal', {})

  return (
    <Root mb={10} mx={14}>
      <Row>
        <Item my={6}>
          <Breadcrumbs path="/admin/journals">JOURNALS</Breadcrumbs>
        </Item>
      </Row>
      <CustomShadowedBox p={6} pb={10}>
        {loading ? (
          <Spinner />
        ) : (
          <Fragment>
            <Row justify="start" mb={4}>
              <JournalName>{name}</JournalName>
              <Item>
                <DotSeparator mx={3} />
                <JournalCode>{code}</JournalCode>
              </Item>
            </Row>
            <Row justify="start">
              <ActiveStatus isActive={isActive} mr={2}>
                {isActive ? 'Active' : 'Inactive'}
              </ActiveStatus>
              {activationDate && (
                <Text>
                  {`${isActive ? 'since ' : 'until '} ${moment(
                    activationDate,
                  ).format('YYYY-MM-DD')}`}
                </Text>
              )}
            </Row>
            <Row justify="start">
              <Item mt={4} vertical>
                <Label mb={1}>Peer Review Model</Label>
                <Text>{peerReviewModelName}</Text>
              </Item>
            </Row>
            <Row flexWrap="wrap" justify="start">
              {!!articleTypes.length && (
                <Item fitContent flex="unset" mr={22} mt={4} vertical>
                  <Label mb={1}>Article Types</Label>
                  <Item>
                    {articleTypes.map(({ id, name }) => (
                      <ArticleType key={id}>{name}</ArticleType>
                    ))}
                  </Item>
                </Item>
              )}
              <Item mt={4}>
                {email && (
                  <Item fitContent flex="unset" mr={16} vertical>
                    <Label mb={1}>Email</Label>
                    <Text whiteSpace="nowrap">{email}</Text>
                  </Item>
                )}
                {issn && (
                  <Item fitContent flex="unset" mr={16} vertical>
                    <Label mb={1}>ISSN</Label>
                    <Text whiteSpace="nowrap">{issn}</Text>
                  </Item>
                )}
                {apc && (
                  <Item fitContent flex="unset" vertical>
                    <Label mb={1}>APC</Label>
                    <Text whiteSpace="nowrap">{formatAPC(apc)}</Text>
                  </Item>
                )}
              </Item>
            </Row>
          </Fragment>
        )}

        <JournalDetailsTabs
          tabButtons={
            peerReviewModel.hasSections ? tabOptions : tabOptions.splice(1, 2)
          }
        >
          {peerReviewModel.hasSections && (
            <Sections
              journalId={journalId}
              loading={loading}
              sections={sections}
            />
          )}
          {isActive ? (
            <SpecialIssues
              journalId={journalId}
              journalSpecialIssues={specialIssues}
              sections={sections}
            />
          ) : (
            <Row alignItems="center" height={24} justify="center">
              <EmptyState>
                Special Issues cannot be added if the journal is inactive
              </EmptyState>
            </Row>
          )}
          <AssignEditorialRole
            journalId={journalId}
            journalSpecialIssues={specialIssues}
            peerReviewModel={peerReviewModel}
            sections={sections}
          />
        </JournalDetailsTabs>
      </CustomShadowedBox>
    </Root>
  )
}

const Root = styled.div`
  ${space}
`

const EmptyState = styled.span`
  background-color: ${th('white')};
  font-size: 20px;
  font-family: 'Nunito';
  color: ${th('actionSecondaryColor')};
`

const ArticleType = styled(Text)`
  border-right: 1px solid ${th('furnitureColor')};
  padding: 0 calc(${th('gridUnit')} * 2);

  :first-of-type {
    padding-left: 0;
  }

  :last-of-type {
    border: none;
  }
`

const ActiveStatus = styled(Text)`
  color: ${({ isActive }) =>
    isActive ? th('actionPrimaryColor') : th('warningColor')};
  font-weight: 700;
  text-transform: uppercase;
  ${space}
`

const CustomShadowedBox = styled(ShadowedBox)`
  min-width: 100%;
`

const JournalName = styled(H3)`
  font-size: 20px;
`
const JournalCode = styled(JournalName)`
  font-weight: normal;
`

const DotSeparator = styled.div`
  align-self: center;
  background-color: ${th('action.color')};
  border-radius: 50%;
  height: ${th('gridUnit')};
  width: ${th('gridUnit')};
  ${space}
`

export default AdminJournalDetails
