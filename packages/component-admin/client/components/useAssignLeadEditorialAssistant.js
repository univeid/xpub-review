import { useMutation } from 'react-apollo'
import { queries, mutations } from '../graphql'

const useAssignLeadEditorialAssistant = journalId => {
  const [assignLeadEditorialAssistant] = useMutation(
    mutations.assignLeadEditorialAssistant,
    {
      refetchQueries: [
        {
          query: queries.getEditorialBoard,
          variables: {
            journalId,
          },
        },
      ],
    },
  )

  const handleAssignLeadEditorialAssistant = id => ({
    setFetching,
    setError,
    hideModal,
  }) => {
    setFetching(true)
    assignLeadEditorialAssistant({
      variables: { id },
    })
      .then(() => {
        setFetching(false)
        hideModal()
      })
      .catch(error => {
        setFetching(false)
        setError(error.message)
      })
  }

  return { handleAssignLeadEditorialAssistant }
}

export default useAssignLeadEditorialAssistant
