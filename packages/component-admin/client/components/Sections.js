import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { ActionLink, Icon } from '@hindawi/ui'
import { Modal } from 'component-modal'
import AdminJournalSectionForm from './AdminJournalSectionForm'
import SectionsTable from './SectionsTable'
import useAddSection from './useAddSection'

const Sections = ({ journalId, sections, loading }) => {
  const { handleAddSection } = useAddSection(journalId)
  return (
    <Root>
      <Modal
        component={AdminJournalSectionForm}
        confirmText="SAVE"
        handleAddSection={handleAddSection}
        modalKey="addJournalSection"
        title="Add Section"
      >
        {showModal => (
          <ActionLink fontWeight="bold" mt={2} onClick={showModal}>
            <Icon fontSize="11px" icon="expand" mr={1} />
            Add section
          </ActionLink>
        )}
      </Modal>
      <SectionsTable loading={loading} sections={sections} />
    </Root>
  )
}

const Root = styled.div`
  min-height: calc(${th('gridUnit')} * 35);
  padding: calc(${th('gridUnit')} * 3);
`

export default Sections
