import { useMutation } from 'react-apollo'
import { useParams } from 'react-router-dom'
import { mutations, queries } from '../graphql'
import { parseError } from './utils'

const useEditSection = () => {
  const [editSectionMutation] = useMutation(mutations.editSection)
  const { journalId } = useParams()

  const handleEditSection = (values, modalProps) => {
    const { name, id } = values
    modalProps.setFetching(true)
    editSectionMutation({
      variables: {
        id,
        input: { name },
      },
      refetchQueries: [
        {
          query: queries.getJournal,
          variables: {
            journalId,
          },
        },
      ],
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(error => {
        modalProps.setFetching(false)
        modalProps.setError(parseError(error))
      })
  }

  return { handleEditSection }
}

export default useEditSection
