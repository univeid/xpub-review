import React, { Fragment } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Spinner, TextField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Pagination,
  ScrollContainer,
  usePaginatedItems,
} from '@hindawi/ui'

import SpecialIssueCard from './SpecialIssueCard'

const filterSpecialIssue = (searchQuery = '') => specialIssue =>
  specialIssue.name.toLowerCase().includes(searchQuery.toLowerCase())

const SpecialIssuesTable = ({
  loading,
  sections,
  journalId,
  specialIssues = [],
}) => {
  const {
    searchQuery,
    paginatedItems,
    handleSearch,
    clearSearch,
    totalCount,
    ...pagination
  } = usePaginatedItems({
    filterFn: filterSpecialIssue,
    items: specialIssues,
  })

  if (loading) return <Spinner />
  return (
    <Fragment>
      <Row alignItems="start" justify="start" mt={1}>
        <Item maxWidth={90} mr={4}>
          <TextField
            onChange={handleSearch}
            placeholder="Search by special issue"
            value={searchQuery}
          />
        </Item>
        <Button invert onClick={clearSearch} secondary small>
          Clear
        </Button>
      </Row>

      {specialIssues.length > 0 && (
        <ScrollContainer>
          {paginatedItems.map((specialIssue, index) => (
            <SpecialIssueCard
              journalId={journalId}
              key={specialIssue.id}
              sections={sections}
              specialIssue={specialIssue}
            />
          ))}
        </ScrollContainer>
      )}
      <Row justify="flex-end" mt={2}>
        <Pagination totalCount={totalCount} {...pagination} />
      </Row>
      {specialIssues.length === 0 && (
        <Row alignItems="center" height={24} justify="center">
          <EmptyState>No special issues added yet</EmptyState>
        </Row>
      )}
    </Fragment>
  )
}

export default SpecialIssuesTable

const EmptyState = styled.span`
  background-color: ${th('white')};
  font-size: 20px;
  font-family: 'Nunito';
  color: ${th('actionSecondaryColor')};
`
