import React, { useState } from 'react'
import { Button } from '@pubsweet/ui'
import { Item, Icon } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const Search = ({ defaultValue, onSearch }) => {
  const [inputValue, setInputValue] = useState(defaultValue)
  const handleOnChange = event => setInputValue(event.target.value)
  const handleOnSearch = value => onSearch(value)

  return (
    <Item>
      <InputWrapper>
        <Input
          minLength={3}
          name="searchFilter"
          onChange={handleOnChange}
          onKeyPress={e => e.key === 'Enter' && handleOnSearch(inputValue)}
          placeholder="Search by Name, Email, Affiliation..."
          value={inputValue}
        />
        {defaultValue && (
          <StyledIcon
            fontSize="16px"
            icon="remove"
            onClick={() => {
              setInputValue('')
              handleOnSearch('')
            }}
          />
        )}
      </InputWrapper>
      <SearchButton
        invert
        ml={1}
        mr={4}
        onClick={() => handleOnSearch(inputValue)}
        secondary
        xs
      >
        Search
      </SearchButton>
    </Item>
  )
}

export default Search

const SearchButton = styled(Button)`
  height: calc(${th('gridUnit')} * 8);
  min-width: calc(${th('gridUnit')} * 17);
`

const Input = styled.input`
  box-sizing: border-box;
  height: calc(${th('gridUnit')} * 8);
  width: calc(${th('gridUnit')} * 121);
  padding: 0;
  padding-left: calc(${th('gridUnit')} * 2);
  padding-right: calc(${th('gridUnit')} * 2);

  border-width: ${th('borderWidth')};
  border-style: ${th('borderStyle')};
  border-color: ${th('colorBorder')};
  border-radius: ${th('borderRadius')};

  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  color: th('textPrimaryColor')};
  ::placeholder {
    color: th('textPrimaryColor')};
    opacity: 1;
    font-family: ${th('defaultFont')};
  }
  :focus {
    outline: none;
    border-color: ${th('action.colorActive')};
  }
`

const InputWrapper = styled.div`
  position: relative;
`

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 7px;
  right: 8px;

  color: ${({ disabled }) =>
    disabled ? th('colorBorder') : th('textPrimaryColor')};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
`
