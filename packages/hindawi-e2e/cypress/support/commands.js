import '@testing-library/cypress/add-commands'

const admin = require('../fixtures/users/admin.json')
const author = require('../fixtures/users/author.json')
const manuscriptData = require('../fixtures/models/fragment.json')

const fileTypes = {
  pdf: 'application/pdf',
  doc: 'application/msword',
  docx:
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  txt: 'text/plain',
  excel: 'application/vnd.ms-excel',
  xlm: 'application/vnd.ms-excel',
}

Cypress.Commands.add(
  'loginApi',
  (email, password = Cypress.env('password')) => {
    const localLogin = `mutation{localLogin ( input: { 
    email: "${Cypress.env('email').concat(email)}", 
    password: "${password}"
    }) {
      token
    }
  }`
    window.localStorage.removeItem('token')
    cy.request({
      method: 'POST',
      url: '/graphql',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: { query: localLogin },
    }).then(response => {
      const { token } = response.body.data.localLogin
      window.localStorage.setItem('token', token)
      return token
    })
  },
)

Cypress.Commands.add(
  'upload',
  { prevSubject: 'element' },
  (subject, { fileName, fileType = '' }) =>
    cy.fixture(fileName, 'base64').then(fileContent => {
      cy.window().then(window =>
        Cypress.Blob.base64StringToBlob(fileContent, fileTypes[fileType])
          .then(
            blob =>
              new window.File([blob], fileName, { type: fileTypes[fileType] }),
          )
          .then(testFile => {
            const dataTransfer = new window.DataTransfer()
            dataTransfer.items.add(testFile)
            const input = subject[0]
            input.files = dataTransfer.files
            // cy.wrap(subject).trigger('change', { force: true })
            input.dispatchEvent(new Event('change'))
          }),
      )
    }),
)

Cypress.Commands.add('upload_file', (fileName, fileType = ' ', selector) => {
  cy.get(selector).then(subject => {
    cy.fixture(fileName, 'base64')
      .then(Cypress.Blob.base64StringToBlob)
      .then(blob => {
        const el = subject[0]
        const testFile = new File([blob], fileName, { type: fileType })
        const dataTransfer = new DataTransfer()
        dataTransfer.items.add(testFile)
        el.files = dataTransfer.files
      })
  })
})

Cypress.Commands.add(
  'uploadFileViaAPI',
  ({
    entityId,
    fileType = 'manuscript',
    token = window.localStorage.getItem('token'),
    onResponse,
  }) => {
    const file = new File(['some text'], 'filename.pdf', { type: 'text/plain' })
    const fileInput = {
      type: fileType,
      size: file.size,
    }
    const query = `
      mutation uploadFile(
        $entityId: String!
        $fileInput: FileInput
        $file: Upload!
      ) {
        uploadFile(entityId: $entityId, fileInput: $fileInput, file: $file) {
          id
          type
          originalName
          size
          mimeType
        }
      }
    `

    const uploadQuery = {
      operationName: 'uploadFile',
      query,
      variables: {
        entityId,
        fileInput,
        file: null,
      },
    }

    const data = new FormData()
    data.append('operations', JSON.stringify(uploadQuery))
    data.append('map', `{ "0": ["variables.file"] }`)
    data.append('0', file)

    const xhr = new XMLHttpRequest()
    xhr.open('POST', '/graphql')
    xhr.setRequestHeader('Accept', `text/plain`)
    xhr.setRequestHeader('Authorization', `Bearer ${token}`)
    xhr.onreadystatechange = function onReadyStateChange() {
      if (xhr.readyState === 4) {
        const res = JSON.parse(xhr.response)
        onResponse(res.data.uploadFile)
      }
    }
    xhr.send(data)
  },
)

Cypress.Commands.add('useFilters', () => {
  cy.get('[data-test-id="row"]').contains('Filters')
  cy.get('[data-test-id="dashboard-filter-priority"]').click()
  cy.get('[role="option"]')
    .contains('Needs Attention')
    .click()
  cy.get('[data-test-id="fragment-status"]').then($fragment => {
    Cypress._.each($fragment => {
      // eslint-disable-next-line jest/no-standalone-expect
      expect($fragment).contain('Submit Revision')
    })
  })
})

Cypress.Commands.add('searchManuscriptByFragmentId', () => {
  cy.get('[data-test-id="/^fragment-w+/"]')
})

Cypress.Commands.add(
  'approveManuscriptEQS',
  (manuscriptId = Cypress.env('manuscriptId')) => {
    cy.loginApi(admin.email, admin.password, true).then(adminToken => {
      const getManuscript = `query{getSubmission(submissionId:"${Cypress.env(
        'submissionId',
      )}")
  {id,
      technicalCheckToken
    }}`
      cy.request({
        method: 'POST',
        url: `/graphql`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${adminToken}`,
        },
        body: { query: getManuscript },
      }).then(response => {
        const { technicalCheckToken } = response.body.data.getSubmission[0]

        const approveEQS = `mutation{approveEQS(
          manuscriptId:"${manuscriptId}",
          input:{
            token: "${technicalCheckToken}",
          })
        }`
        cy.request({
          method: 'POST',
          url: `/graphql`,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${adminToken}`,
          },
          body: { query: approveEQS },
        })
      })
    })
  },
)

Cypress.Commands.add('rejectManuscriptByEQS', () => {})
Cypress.Commands.add('rejectManuscriptByEQA', () => {})

Cypress.Commands.add('createManuscriptViaAPI', noEQS => {
  let manuscriptId = null
  let token = null

  cy.loginApi(admin.email, admin.password, true).then(adminToken => {
    const getJournals = `query{getActiveJournals {
      id
      name
      articleTypes { 
        id
        name}
    }}`
    cy.request({
      method: 'POST',
      url: '/graphql',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${adminToken}`,
      },
      body: { query: getJournals },
    }).then(response => {
      const journal = response.body.data.getActiveJournals.find(
        j => j.name === 'Bioinorganic Chemistry and Applications',
      )
      Cypress.env('journalId', journal.id)

      const article = journal.articleTypes.find(
        article => article.name === 'Research Article',
      )
      Cypress.env('articleTypeId', article.id)
    })
  })

  cy.loginApi(author.email, author.password).then(userToken => {
    token = userToken
    const createManuscript = `mutation{createDraftManuscript (input: {journalId: "${Cypress.env(
      'journalId',
    )}"})
  {id, 
   submissionId
  }}`
    cy.request({
      method: 'POST',
      url: '/graphql',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: { query: createManuscript },
    })
      .then(response => {
        // eslint-disable-next-line
        let submissionId = response.body.data.createDraftManuscript.submissionId
        // eslint-disable-next-line
        manuscriptId = response.body.data.createDraftManuscript.id

        Cypress.env('manuscriptId', manuscriptId)
        Cypress.env('submissionId', submissionId)

        cy.uploadFileViaAPI({
          filename: 'scimakelatex.5292.pdf',
          entityId: manuscriptId,
          token,
          onResponse: () => {},
        })
      })
      .then(() => {
        const updateManuscript = `mutation{updateDraftManuscript(manuscriptId: "${manuscriptId}", autosaveInput:
                { journalId: "${Cypress.env('journalId')}"
                  authors: [
                { aff: "${author.affiliation}"
                  country: "${author.country}"
                  email: "${Cypress.env('email')}${author.email}"
                  givenNames: "${author.firstName}"
                  isCorresponding: true
                  isSubmitting: true
                  surname: "${author.lastName}"
                }]
                  meta: {
                    abstract: "${manuscriptData.abstract}"
                    articleTypeId: "${Cypress.env('articleTypeId')}"
                    title: "${manuscriptData.title}"
                    agreeTc: true
                      conflictOfInterest:"${
                        manuscriptData.conflicts.hasConflicts
                      }"
                      dataAvailability: "${
                        manuscriptData.conflicts.dataAvailabilityMessage
                      }"
                      fundingStatement: "${
                        manuscriptData.conflicts.fundingMessage
                      }"
                  }}){id}}`
        cy.request({
          method: 'POST',
          url: `/graphql`,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: { query: updateManuscript },
        })
      })
      .then(() => {
        const submitManuscript = `mutation{submitManuscript(manuscriptId:"${manuscriptId}")}`
        cy.request({
          method: 'POST',
          url: `/graphql`,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: { query: submitManuscript },
        })
      })
  })

  if (noEQS) return

  cy.loginApi(admin.email, admin.password, true).then(adminToken => {
    const getManuscript = `query{getSubmission(submissionId:"${Cypress.env(
      'submissionId',
    )}")
    {id,
        technicalCheckToken
      }}`
    cy.request({
      method: 'POST',
      url: `/graphql`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${adminToken}`,
      },
      body: { query: getManuscript },
    }).then(response => {
      const { technicalCheckToken } = response.body.data.getSubmission[0]

      const approveEQS = `mutation{approveEQS(
            manuscriptId:"${manuscriptId}",
            input:{
              token: "${technicalCheckToken}",
            })
          }`
      cy.request({
        method: 'POST',
        url: `/graphql`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${adminToken}`,
        },
        body: { query: approveEQS },
      })
    })
  })
})

Cypress.Commands.add('createDraftViaAPI', () => {
  let manuscriptId = null
  let token = null

  cy.loginApi(admin.email, admin.password, true).then(adminToken => {
    const getJournals = `query{getActiveJournals {
      id
      name
      articleTypes { 
        id
        name}
    }}`
    cy.request({
      method: 'POST',
      url: '/graphql',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${adminToken}`,
      },
      body: { query: getJournals },
    }).then(response => {
      const journal = response.body.data.getActiveJournals.find(
        j => j.name === 'Bioinorganic Chemistry and Applications',
      )
      Cypress.env('journalId', journal.id)

      const article = journal.articleTypes.find(
        article => article.name === 'Research Article',
      )
      Cypress.env('articleTypeId', article.id)
    })
  })

  cy.loginApi(author.email, author.password).then(userToken => {
    token = userToken
    const createManuscript = `mutation{createDraftManuscript (input: {journalId: "${Cypress.env(
      'journalId',
    )}"})
  {id, 
   submissionId
  }}`
    cy.request({
      method: 'POST',
      url: '/graphql',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: { query: createManuscript },
    })
      .then(response => {
        // eslint-disable-next-line
        let submissionId = response.body.data.createDraftManuscript.submissionId
        // eslint-disable-next-line
        manuscriptId = response.body.data.createDraftManuscript.id

        Cypress.env('manuscriptId', manuscriptId)
        Cypress.env('submissionId', submissionId)

        cy.uploadFileViaAPI({
          filename: 'scimakelatex.5292.pdf',
          entityId: manuscriptId,
          token,
          onResponse: () => {},
        })
      })
      .then(() => {
        const updateManuscript = `mutation{updateDraftManuscript(manuscriptId: "${manuscriptId}", autosaveInput:
                { journalId: "${Cypress.env('journalId')}"
                  authors: [
                { aff: "${author.affiliation}"
                  country: "${author.country}"
                  email: "${Cypress.env('email')}${author.email}"
                  givenNames: "${author.firstName}"
                  isCorresponding: true
                  isSubmitting: true
                  surname: "${author.lastName}"
                }]
                  meta: {
                    abstract: "${manuscriptData.abstract}"
                    articleTypeId: "${Cypress.env('articleTypeId')}"
                    title: "${manuscriptData.title}"
                    agreeTc: true
                      conflictOfInterest:"${
                        manuscriptData.conflicts.hasConflicts
                      }"
                      dataAvailability: "${
                        manuscriptData.conflicts.dataAvailabilityMessage
                      }"
                      fundingStatement: "${
                        manuscriptData.conflicts.fundingMessage
                      }"
                  }}){id}}`
        cy.request({
          method: 'POST',
          url: `/graphql`,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: { query: updateManuscript },
        })
      })
  })
})
