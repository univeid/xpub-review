describe('AcademicEditor rejects manuscript after reviewer submit report', () => {
  beforeEach(() => {
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Succesfully submit a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Should Invite AcademicEditor to manuscript', function inviteAcademicEditor() {
    const { triageEditor, academicEditor } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
  })

  it('Succesfully accept invitation for manuscript as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.respondToInvitationAsAcademicEditor('yes')
  })

  it('Succesfully invite reviewer to manuscript as AcademicEditor', function inviteReviewer() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.inviteReviewer({ reviewer, academicEditor })
    cy.checkStatus(statuses.reviewersInvited.academicEditor)
  })

  it('Accept invitation as reviewer and submit a report', function acceptInvitationAsreviewer() {
    const { reviewer, statuses } = this

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)
    cy.respondToInvitationAsReviewer('yes')
    cy.checkStatus(statuses.underReview.reviewer)
    cy.submitReview('Minor Revision')

    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)
    cy.respondToInvitationAsReviewer('yes')
    cy.checkStatus(statuses.underReview.reviewer)
    cy.submitReview('Major Revision')
  })

  it('AcademicEditor rejects manuscript after reviewers make an report', function academicEditorRejectsManuscript() {
    const { academicEditor, statuses, triageEditor, admin } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    return cy.academicEditorMakesRecommendation('Reject').then(() => {
      cy.checkStatus(statuses.pendingApproval.academicEditor)
      cy.findByText('Editorial Comments').should('be.visible')

      cy.loginApi(triageEditor.email, triageEditor.password)
      cy.visit('/')

      cy.get(
        `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`,
      ).click()
      cy.checkStatus(statuses.pendingApproval.triageEditor)
      cy.contains('Editorial Comments')
      cy.contains(Cypress.env('academicEditorRecommendationText-author'))
      cy.contains(Cypress.env('academicEditorRecommendationText-triageEditor'))

      cy.loginApi(admin.email, admin.password, true)
      cy.visit('/')

      cy.get(
        `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`,
      ).click()
      cy.checkStatus(statuses.pendingApproval.admin)
      cy.contains('Editorial Comments')
      cy.contains(Cypress.env('academicEditorRecommendationText-author'))
      cy.contains(Cypress.env('academicEditorRecommendationText-triageEditor'))
    })
  })
})
