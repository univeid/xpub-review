describe('Reject Manuscript as AcademicEditor before inviting reviewers', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Should invite AcademicEditor to manuscript', function inviteAcademicEditor() {
    const { triageEditor, academicEditor } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
  })

  it('Successfully accept invitation for manuscript as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsAcademicEditor('yes')
  })

  it('Successfully rejects a manuscript as AcademicEditor', function academicEditorMakesRecommendation() {
    const { academicEditor, triageEditor, statuses, admin } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)

    return cy.academicEditorMakesRecommendation('Reject').then(() => {
      cy.checkStatus(statuses.pendingApproval.academicEditor)
      cy.get('[data-test-id="contextual-box-editorial-comments"]')
        .contains('Your Editorial Recommendation')
        .should('not.be.visible')

      cy.loginApi(triageEditor.email, triageEditor.password)
      cy.visit('/')

      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.triageEditor)
      cy.get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should(
          'contain',
          Cypress.env('academicEditorRecommendationText-author'),
        )
        .should(
          'contain',
          Cypress.env('academicEditorRecommendationText-triageEditor'),
        )

      cy.loginApi(admin.email, admin.password, true)
      cy.visit('/')

      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.admin)
      cy.get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should(
          'contain',
          Cypress.env('academicEditorRecommendationText-author'),
        )
        .should(
          'contain',
          Cypress.env('academicEditorRecommendationText-triageEditor'),
        )
    })
  })
})
