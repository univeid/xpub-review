describe('Triage Editor revokes AcademicEditor before AcademicEditor responds to invitation', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Triage Editor revokes AcademicEditor before AcademicEditor responds to invite', function TriageEditorRevokesAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)

    cy.get(`h3`).contains('Respond to Editorial Invitation')
    cy.get(`button`).contains('Respond to Invitation')

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)
    cy.triageEditorRevokesAcademicEditor()
    cy.checkStatus(statuses.submitted.triageEditor)

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )
  })
})
