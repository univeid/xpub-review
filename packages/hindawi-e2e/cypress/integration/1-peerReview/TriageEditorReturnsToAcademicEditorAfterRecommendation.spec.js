describe('Triage Editor returns to AcademicEditor after recommendation', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
    cy.respondToInvitationAsAcademicEditor('yes')

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
  })

  it('AcademicEditor makes recommendation to reject', function majorRevisionRecommendation() {
    const { academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.academicEditorMakesRecommendation('Reject')
  })

  it('Triage Editor return manuscript to AcademicEditor', function returnManuscript() {
    const { triageEditor, statuses } = this

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.pendingApproval.triageEditor)

    return cy
      .triageEditorMakesDecision('Return to Academic Editor')
      .then(() => {
        cy.get(`h3`).contains('Editorial Comments')
        cy.get(`span`)
          .should('be.visible')
          .contains(Cypress.env('triageEditorDecisionText'))
      })
  })

  it('AcademicEditor sees correct information', function checkAcademicEditor() {
    const { academicEditor, statuses } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.academicEditor)
    cy.get(`h3`).contains('Editorial Comments')
    cy.get(`span`)
      .should('be.visible')
      .contains(Cypress.env('triageEditorDecisionText'))
  })

  it('Admin sees correct information', function checkAdmin() {
    const { admin, statuses } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.admin)
    cy.get(`h3`).contains('Editorial Comments')
    cy.get(`span`)
      .should('be.visible')
      .contains(Cypress.env('triageEditorDecisionText'))
  })
})
