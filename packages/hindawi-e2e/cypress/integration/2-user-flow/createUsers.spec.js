describe('Create Users', () => {
  beforeEach(() => {
    cy.clearLocalStorage()
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
  })

  it('create new Admin account', function createAdmin() {
    const { admin } = this
    cy.createUser({ admin, user: admin })
  })

  it('create new user account', function createUser() {
    const { author, admin } = this
    cy.createUser({ admin, user: author })
  })
})
