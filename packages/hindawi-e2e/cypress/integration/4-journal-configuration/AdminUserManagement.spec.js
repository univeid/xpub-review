describe('Admin uses user management table', () => {
  beforeEach(() => {
    cy.fixture('users/admin')
      .as('admin')
      .then(admin => {
        cy.loginApi(admin.email, admin.password, true).visit('/')
        cy.get('[data-test-id="admin-menu-button"]').click()
        cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
        cy.contains('Users').click()
      })
  })

  it(' should status label be visible on emails', () => {
    cy.get('[data-test-id="email"]').contains('INVITED')
  })

  it('should be able to navigate through pagination', () => {
    cy.get('[data-test-id="page-numbers')
      .findByText('2')
      .click()
  })

  it('should click on the "more" button', () => {
    cy.wait(2000)
    cy.get('.icn_icn_moreDefault')
      .eq(6)
      .click()
    cy.contains('Edit User').click()
    cy.contains('CANCEL').click()
    cy.contains('Deactivate').click()
    cy.contains('CLOSE').click()
  })
})
