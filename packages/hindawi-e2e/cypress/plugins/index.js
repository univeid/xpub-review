// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const uuid = require('uuid')
const Promise = require('bluebird')

const getDBConfig = config => ({
  user: config.env.dbConfig.user,
  host: config.env.dbConfig.host,
  database: config.env.dbConfig.database,
  password: config.env.dbConfig.password,
  port: 5432,
})

const passwordHash = config => config.env.passwordHash

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config

  on('task', {
    async clearDatabase() {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()
      await qaClient.query('DROP SCHEMA public CASCADE;')
      await qaClient.query('CREATE SCHEMA public;')
      qaClient.end()
      return true
    },

    async createJournal() {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      const prmId = await qaClient.query(`SELECT id FROM peer_review_model`)

      await qaClient.query(
        `INSERT INTO journal(name, code, email, apc, is_active, peer_review_model_id)
        VALUES ('Bioinorganic Chemistry and Applications', 'BCA', 'sabina.deliu+bca@hindawi.com', '3000', true, '${prmId.rows[0].id}')`,
      )
      qaClient.end()
      return true
    },

    async assignArticleType() {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      const article_ids = await qaClient.query(`SELECT id FROM article_type;`)
      const journal = await qaClient.query(
        `SELECT id FROM journal WHERE name = 'Bioinorganic Chemistry and Applications'`,
      )

      await Promise.each(article_ids.rows, async article => {
        await qaClient.query(`INSERT INTO journal_article_type(journal_id, article_type_id)
        VALUES('${journal.rows[0].id}', '${article.id}')`)
      })
      qaClient.end()
      return true
    },

    async createGlobalUser({ email, givenName, surname, role }) {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      const userId = uuid.v4()
      const teamId = uuid.v4()

      await qaClient.query(
        `INSERT INTO "user"(id, default_identity_type, is_subscribed_to_emails, unsubscribe_token, agree_tc, is_active)
        VALUES ('${userId}',  'local', true, '${uuid.v4()}', true, true)`,
      )
      await qaClient.query(
        `INSERT INTO identity(user_id, type, is_confirmed, email, given_names, surname, password_hash, title, aff, country)
        VALUES ('${userId}', 'local', true, '${email}', '${givenName}', '${surname}', '${passwordHash(
          config,
        )}', 'mrs', 'Hindawi Research', 'RO')`,
      )
      const journal_id = await qaClient.query(
        `SELECT id FROM journal WHERE name = 'Bioinorganic Chemistry and Applications'`,
      )
      await qaClient.query(
        `INSERT INTO team(id, role, journal_id)
        VALUES('${teamId}', '${role}', '${journal_id.rows[0].id}')`,
      )
      await qaClient.query(
        `INSERT INTO team_member(user_id, team_id, status, alias)
        VALUES('${userId}', '${teamId}', 'accepted' , '{
          "aff": "Hindawi Research",
          "email": "${email}",
          "title": "mrs",
          "country": "RO",
          "surname": "${surname}",
          "givenNames": "${givenName}"
        }')`,
      )
      qaClient.end()
      return true
    },

    async createUser({ email, givenName, surname }) {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      const userId = uuid.v4()

      await qaClient.query(
        `INSERT INTO "user"(id, default_identity_type, is_subscribed_to_emails, unsubscribe_token, agree_tc, is_active)
        VALUES ('${userId}',  'local', true, '${uuid.v4()}', true, true)`,
      )
      await qaClient.query(
        `INSERT INTO identity(user_id, type, is_confirmed, email, given_names, surname, password_hash, title, aff, country)
        VALUES ('${userId}', 'local', true, '${email}', '${givenName}', '${surname}', '${passwordHash(
          config,
        )}', 'mrs', 'Hindawi Research', 'RO')`,
      )
      qaClient.end()
      return true
    },
  })
}
