import {
  getTeamRoles,
  generateJournal,
  generateManuscript,
  generateSubmission,
  getManuscriptStatuses,
} from "component-generators";

const useCases = require("../src/useCases");
const { handleQualityCheckedSubmissionUseCase } = useCases;

interface ManuscriptType {
  find(): Promise<object>;
  Statuses: object;
  findLastManuscriptBySubmissionId(): Promise<object>;
}

const Manuscript: ManuscriptType = {
  find: jest.fn(),
  findLastManuscriptBySubmissionId: jest.fn(),
  Statuses: getManuscriptStatuses(),
};

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  Manuscript,
};

const applicationEventBus = {
  publishMessage: jest.fn(),
};
const mockedUseCases = {
  getDataForSubmittedManuscriptUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
  getDataForPeerReviewedManuscriptUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
  getFilesForFtpUploadUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
};

describe("handle Submission Quality Check Passed event use case", () => {
  beforeEach(() => {});

  it("sends the package to Production if the manuscript is the latest version", async () => {
    const journal = generateJournal();
    const submission = generateSubmission({
      noOfVersions: 2,
      generateManuscript,
      statuses: getManuscriptStatuses(),
      props: { journal },
    });

    const manuscript = submission[submission.length - 1];

    jest
      .spyOn(Manuscript, "findLastManuscriptBySubmissionId")
      .mockResolvedValue(manuscript);

    jest.spyOn(Manuscript, "find").mockResolvedValueOnce(manuscript);

    const sendPackageToProduction = jest.fn();
    await handleQualityCheckedSubmissionUseCase
      .initialize({
        models,
        useCases: mockedUseCases,
        applicationEventBus,
        generateXML: jest.fn(),
        sendPackageToProduction,
        sendAcceptedPackageToMTS: jest.fn(),
      })
      .execute({
        submissionId: manuscript.submissionId,
        manuscripts: [manuscript],
        qc: {},
        qcLeaders: [],
        es: {},
        esLeaders: [],
      });

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1);
    expect(sendPackageToProduction).toHaveBeenCalledTimes(1);
  });
  it("throws an error if manuscript is not found", async () => {
    jest.spyOn(Manuscript, "find").mockResolvedValueOnce(undefined);

    const result = handleQualityCheckedSubmissionUseCase
      .initialize({
        models,
        useCases: mockedUseCases,
        generateXML: jest.fn(),
        applicationEventBus: jest.fn(),
        sendPackageToProduction: jest.fn(),
        sendAcceptedPackageToMTS: jest.fn(),
      })
      .execute({
        manuscripts: [{ id: "some-id" }],
        qc: {},
        qcLeaders: [],
        es: {},
        esLeaders: [],
        submissionId: "some-id",
      });

    expect(result).rejects.toThrowError("Manuscript with id some-id not found");
  });
});
