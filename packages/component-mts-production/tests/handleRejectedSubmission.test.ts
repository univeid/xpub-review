const {
  getTeamRoles,
  generateJournal,
  generateManuscript,
  getManuscriptStatuses,
} = require("component-generators");

const useCases = require("../src/useCases");
const { handleRejectedSubmissionUseCase } = useCases;

interface ManuscriptType {
  find(): Promise<object>;
  Statuses: object;
  findLastManuscriptBySubmissionId(): Promise<object>;
}

const Manuscript: ManuscriptType = {
  find: jest.fn(),
  findLastManuscriptBySubmissionId: jest.fn(),
  Statuses: getManuscriptStatuses(),
};

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  Manuscript,
};

const mockedUseCases = {
  getDataForSubmittedManuscriptUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
  getDataForPeerReviewedManuscriptUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
  getFilesForFtpUploadUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
};

describe("handle Submission Rejected event use case", () => {
  it("sends the package to MTS if the data is correct", async () => {
    const journal = generateJournal();
    const manuscript = generateManuscript({
      journal,
    });

    jest.spyOn(Manuscript, "find").mockResolvedValueOnce(manuscript);

    const sendRejectedPackageToMTS = jest.fn();
    await handleRejectedSubmissionUseCase
      .initialize({
        models,
        useCases: mockedUseCases,
        generateXML: jest.fn(),
        sendRejectedPackageToMTS,
      })
      .execute({
        manuscripts: [manuscript],
      });
    expect(sendRejectedPackageToMTS).toHaveBeenCalledTimes(1);
  });
  it("throws an error if manuscript is not found", async () => {
    jest.spyOn(Manuscript, "find").mockResolvedValueOnce(undefined);
    const result = handleRejectedSubmissionUseCase
      .initialize({
        models,
        useCases: mockedUseCases,
        generateXML: jest.fn(),
      })
      .execute({
        manuscripts: [{ id: "some-id" }],
      });

    expect(result).rejects.toThrowError("Manuscript with id some-id not found");
  });
});
