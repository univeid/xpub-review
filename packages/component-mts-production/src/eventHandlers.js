const models = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

const { generateXML } = require('component-xml-generator')
const {
  sendRejectedPackageToMTS,
  sendSubmittedPackageToMTS,
} = require('component-ftp-upload')
const useCases = require('./useCases')

module.exports = {
  async SubmissionSubmitted(data) {
    if (!data.manuscripts || !data.manuscripts.length)
      throw new Error(`Manuscripts not found on SubmissionSubmitted event`)

    const manuscriptId = data.manuscripts[0].id
    if (!manuscriptId) {
      throw new Error(
        'SubmissionSubmitted event could not be processed due to missing manuscript id',
      )
    }

    const { Manuscript } = models
    // TO DO: check if special issue sections are needed
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[journal.journalPreprints.preprint, articleType, section, specialIssue, files]',
    )
    if (!manuscript)
      throw new Error(`Manuscript with id ${manuscriptId} not found`)

    const {
      authors,
      editorialAssistant,
      correspondingEditorialAssistant,
    } = await useCases.getDataForSubmittedManuscriptUseCase
      .initialize(models)
      .execute({
        manuscriptId,
        journalId: manuscript.journalId,
      })

    const submissionXML = await generateXML({
      authors,
      manuscript,
      editorialAssistant,
      correspondingEditorialAssistant,
    })

    const {
      manuscriptFiles,
      coverLetterFiles,
      supplementalFiles,
    } = await useCases.getFilesForFtpUploadUseCase
      .initialize({ models })
      .execute(manuscriptId)

    await sendSubmittedPackageToMTS({
      manuscriptFiles,
      coverLetterFiles,
      supplementalFiles,
      xmlFile: submissionXML,
      journalCode: manuscript.journal.code,
      manuscriptCustomId: manuscript.customId,
    })

    logger.info(
      `[component-mts-production]: Submission ${manuscript.submissionId} has been succesfully uploaded to MTS FTP`,
    )
  },
  async SubmissionRejected(data) {
    await useCases.handleRejectedSubmissionUseCase
      .initialize({ models, generateXML, useCases, sendRejectedPackageToMTS })
      .execute(data)
  },
}
