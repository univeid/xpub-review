const getDataForSubmittedManuscriptUseCase = require('./getDataForSubmittedManuscript')
const getDataForPeerReviewedManuscriptUseCase = require('./getDataForPeerReviewedManuscript')
const handleQualityCheckedSubmissionUseCase = require('./handleQualityCheckedSubmission')
const handleRejectedSubmissionUseCase = require('./handleRejectedSubmission')
const getFilesForFtpUploadUseCase = require('./getFilesForFtpUpload')

module.exports = {
  getFilesForFtpUploadUseCase,
  handleRejectedSubmissionUseCase,
  getDataForSubmittedManuscriptUseCase,
  handleQualityCheckedSubmissionUseCase,
  getDataForPeerReviewedManuscriptUseCase,
}
