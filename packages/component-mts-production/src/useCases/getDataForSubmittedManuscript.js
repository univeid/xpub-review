const initialize = ({ TeamMember, Team }) => ({
  async execute({ manuscriptId, journalId }) {
    const correspondingEditorialAssistant = await TeamMember.findCorrespondingEditorialAssistant(
      journalId,
    )

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.editorialAssistant,
    })

    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
      eagerLoadRelations: 'user.identities',
    })

    return { correspondingEditorialAssistant, editorialAssistant, authors }
  },
})

module.exports = {
  initialize,
}
