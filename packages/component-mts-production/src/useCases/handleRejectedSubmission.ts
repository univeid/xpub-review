const BluebirdPromise = require('bluebird')

interface EventManuscript {
  id: string
}

interface EventData {
  manuscripts: Array<EventManuscript>
}

const initialize = ({
  useCases,
  models,
  generateXML,
  sendRejectedPackageToMTS,
}) => ({
  async execute(data: EventData) {
    const { Manuscript, Team } = models

    await BluebirdPromise.each(
      data.manuscripts,
      async (eventManuscript: EventManuscript) => {
        const manuscriptId = eventManuscript.id

        const manuscript = await Manuscript.find(
          manuscriptId,
          '[journal.journalPreprints.preprint, articleType, section, specialIssue, files]',
        )
        if (!manuscript)
          throw new Error(`Manuscript with id ${manuscriptId} not found`)

        const {
          authors,
          editorialAssistant,
          correspondingEditorialAssistant,
        } = await useCases.getDataForSubmittedManuscriptUseCase
          .initialize(models)
          .execute({
            manuscriptId,
            journalId: manuscript.journalId,
          })

        const {
          academicEditor,
          reviews,
        } = await useCases.getDataForPeerReviewedManuscriptUseCase
          .initialize(models)
          .execute(manuscriptId)

        const {
          reviewFiles,
          figureFiles,
          manuscriptFiles,
          coverLetterFiles,
          supplementalFiles,
        } = await useCases.getFilesForFtpUploadUseCase
          .initialize({ models })
          .execute(manuscriptId)

        const rejectedXML = await generateXML({
          authors,
          reviews,
          manuscript,
          figureFiles,
          academicEditor,
          editorialAssistant,
          TeamRoles: Team.Role,
          correspondingEditorialAssistant,
        })

        await sendRejectedPackageToMTS({
          xmlFile: rejectedXML,
          journalCode: manuscript.journal.code,
          reviewFiles,
          figureFiles,
          manuscriptFiles,
          coverLetterFiles,
          supplementalFiles,
          manuscriptCustomId: manuscript.customId,
          manuscriptVersion: manuscript.version,
        })
      },
    )
  },
})

export { initialize }
