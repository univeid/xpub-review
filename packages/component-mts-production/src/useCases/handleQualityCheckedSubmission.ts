const BluebirdPromise = require("bluebird");

interface EventData {
  manuscripts: Array<EventManuscript>;
  submissionId: string;
  qc: object;
  qcLeaders: Array<object>;
  es: object;
  esLeaders: Array<object>;
}
interface EventManuscript {
  id: string;
}

const initialize = ({
  useCases,
  models,
  generateXML,
  applicationEventBus,
  sendPackageToProduction,
  sendAcceptedPackageToMTS,
}) => ({
  async execute(data: EventData) {
    const { Manuscript, Team } = models;

    const { submissionId, qc, qcLeaders = [], es, esLeaders = [] } = data;
    const screeners = [
      ...qcLeaders.map(e => ({ ...e, role: "qcLeader" })),
      ...esLeaders.map(e => ({ ...e, role: "esLeader" })),
      ...(qc ? [{ ...qc, role: "qc" }] : []),
      ...(es ? [{ ...es, role: "es" }] : []),
    ];

    await BluebirdPromise.each(
      data.manuscripts,
      async (eventManuscript: EventManuscript) => {
        const manuscriptId = eventManuscript.id;

        const manuscript = await Manuscript.find(
          manuscriptId,
          "[journal.journalPreprints.preprint, articleType, section, specialIssue, files]",
        );
        if (!manuscript)
          throw new Error(`Manuscript with id ${manuscriptId} not found`);

        const {
          authors,
          editorialAssistant,
          correspondingEditorialAssistant,
        } = await useCases.getDataForSubmittedManuscriptUseCase
          .initialize(models)
          .execute({
            manuscriptId,
            journalId: manuscript.journalId,
          });

        const {
          academicEditor,
          reviews,
        } = await useCases.getDataForPeerReviewedManuscriptUseCase
          .initialize(models)
          .execute(manuscriptId);

        const {
          reviewFiles,
          figureFiles,
          manuscriptFiles,
          coverLetterFiles,
          supplementalFiles,
        } = await useCases.getFilesForFtpUploadUseCase
          .initialize({ models })
          .execute(manuscriptId);

        const qualityChecksPassedXML = await generateXML({
          authors,
          reviews,
          screeners,
          manuscript,
          figureFiles,
          academicEditor,
          editorialAssistant,
          TeamRoles: Team.Role,
          correspondingEditorialAssistant,
        });

        const { customId } = manuscript;
        await sendAcceptedPackageToMTS({
          xmlFile: qualityChecksPassedXML,
          journalCode: manuscript.journal.code,
          reviewFiles,
          figureFiles,
          manuscriptFiles,
          coverLetterFiles,
          supplementalFiles,
          manuscriptCustomId: customId,
          manuscriptVersion: manuscript.version,
        });

        if (manuscript.isLatestVersion) {
          await sendPackageToProduction({
            xmlFile: qualityChecksPassedXML,
            journalCode: manuscript.journal.code,
            reviewFiles,
            figureFiles,
            supplementalFiles,
            manuscriptFiles,
            coverLetterFiles,
            manuscriptCustomId: customId,
          });
        }
      },
    );

    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    });

    // This is a non-standard event as the current Production system does not know how to process Submission events :)
    await applicationEventBus.publishMessage({
      event: "SubmissionPackageCreated",
      data: {
        journalId: lastManuscript.journalId,
        manuscriptId: lastManuscript.id,
        customId: lastManuscript.customId,
      },
    });
  },
});

export { initialize };
