const initialize = ({ TeamMember, Team, Review }) => ({
  async execute(manuscriptId) {
    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
        eagerLoadRelations: 'user.identities',
      },
    )

    const eagerLoadRelations = ['member, comments.files']
    const reviewerReviews = await Review.findAllSubmittedByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.reviewer,
      eagerLoadRelations,
    })
    const authorReviews = await Review.findAllSubmittedByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
      eagerLoadRelations,
    })
    const academicEditorReviews = await Review.findAllSubmittedByManuscriptAndRole(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
        eagerLoadRelations,
      },
    )
    const triageEditorReviews = await Review.findAllSubmittedByManuscriptAndRole(
      {
        manuscriptId,
        role: Team.Role.triageEditor,
        eagerLoadRelations,
      },
    )
    const adminReviews = await Review.findAllSubmittedByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.admin,
      eagerLoadRelations,
    })
    const editorialAssistantReviews = await Review.findAllSubmittedByManuscriptAndRole(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        eagerLoadRelations,
      },
    )

    return {
      academicEditor,
      reviews: {
        adminReviews,
        authorReviews,
        reviewerReviews,
        triageEditorReviews,
        academicEditorReviews,
        editorialAssistantReviews,
      },
    }
  },
})

module.exports = {
  initialize,
}
