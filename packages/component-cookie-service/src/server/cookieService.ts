export const cookieService = ({ req, res }) => {
  const get = cookie => req.cookies[cookie]
  const set = ({ cookie, value, options }) => {
    const opts = {
      maxAge: 1000 * 60 * 60, // would expire after 1 hour
      httpOnly: true, // The cookie only accessible by the web server
      // signed: true, // Indicates if the cookie should be signed // maybe we will need this
      ...options,
    }

    res.cookie(cookie, value, opts)
    return { [cookie]: value }
  }
  const remove = cookie => {
    // res.clearCookie(cookie) //check this first
    res.cookie(cookie, { maxAge: 0 })
    return true
  }
  return {
    get,
    set,
    remove,
  }
}
