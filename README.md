# Hindawi Peer Review Platform

[Review](review.hindawi.com) is a system that allows authors to submit manuscripts and editors to manage the peer review for a particular journal on Hindawi.
For more details about Hindawi, please visit: https://www.hindawi.com/

## Requirements

- node 12
- docker

## Structure

This repository is a monorepo containing Pubsweet components used and created by Hindawi.
The app entry point is `packages/app`, the place where the `xpub-review` setup and configurations are stored.

Docs and more information about Pubsweet can be found [here](https://pubsweet.coko.foundation/).

## Installing Dependencies

In the root directory, run `yarn` to install all the dependencies.

## Configuration

xPub-review is using external services as AWS, MTS-FTP, Publons, ORCID. In order to run the app locally a `.env` file is mandatory with keys and settings for each service. For that, copy `packages/app/.env.example` to `packages/app/.env`.

In order to create an admin account, set the `ADMIN_EMAIL` property in the `.env` file to a valid email address.

In order to add the needed data in the database, set the `PUBLISHER_NAME` property in the `.env` file to the `hindawi` or `gsw` or the default peer review model: `Chief Minus` will be added and run the seeds as described bellow, in the Scripts section.

Contact us at technology@hindawi.com for help getting setup.

## Running the app

1. Open Docker engine.
2. Start services with `yarn startdb`. This will create a local Postgres database inside a docker container and a few other services (sns, sqs).
3. Start app with `yarn start` in root. The migration is started by an npm script pre hook and run before the app actually starts.
4. Run `yarn seeddb` in order to populate local DB and create your admin user.
5. Login with your `ADMIN_EMAIL` (from `.env`) (eg. john.smith@hindawi.com). The password will be `Password1!` and can be changed later.

Alternative using docker-compose
1. Overwrite the following variables in the `.env` file:
```
NODE_ENV=production
DB_HOST=postgres
DB_SSL=false
AWS_SQS_ENDPOINT=http://sqs:9324
AWS_SNS_ENDPOINT=http://sns:9911
```
2. Execute `docker-compose up` in root project directory

## Community

Join [the Mattermost channel](https://mattermost.coko.foundation/coko/channels/xpub) for discussion of xpub.

## Migrations

1. use folder `./packages/app/migrations`
2. add file named `${timestamp}-${name}(.sql/.js)` e.g. `1560160800-user-add-name-field.sql`

## Seeds

1. use folder `./packages/app/seeds`
2. add file named `${00}-${name}(.js)` e.g. `01-create-peer-review-models.js`

## Scripts

Run a specific script with `yarn script_name`. Current scripts in the application:

- `yarn migrate` - Run all migrations manually (note: successful migrations will not run twice, unless deleted from migrations table)
- `yarn cleandb` - Cleans the database of all the tables, including pgboss jobs and runs migrations
- `yarn addusers [usertype]` - Will add users with `EMAIL_SENDER` (from `.env`) + `usertype` (eg. john.smith+ae@hindawi.com). Multiple users can be added by separating names trough space (eg. `yarn addusers ce ae a`) and if no user is specified, the default users will be added (ea, ce, ae, a1, r1, r2). The passwords will be `Password1!` and can be changed.
- `yarn obfuscatedb` - Obfuscates sensitive data (usually used for a production replica, but without any real user data) .
- `yarn seeddb` - Will run all the files from the `./packages/app/seeds` folder and will add the needed data in the database .
- `yarn seeddb --specific=01-create-peer-review-models.js` - Will run a specific seed to add data in the database .
